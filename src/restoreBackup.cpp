/**
 *   Copyright (C) 2016 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "restoreBackup.h"
#include "BackupPlan.h"
#include "BackupSnapshot.h"
#include "FileSnapshot.h"
#include "LinkSnapshot.h"
#include "DirectorySnapshot.h"

#include <QDir>
#include <QFile>

void restoreFile( const BackupPlan& plan, const QString& fileHash );
void restoreLink( const BackupPlan& plan, const QString& linkHash );
void restoreDirectory( const BackupPlan& plan, const QString& dirHash );

void restoreFile( const BackupPlan& plan
                , const QString& fileHash
                , const QString& restorePath );

void restoreLink( const BackupPlan& plan
                , const QString& linkHash
                , const QString& restorePath );

void restoreDirectory( const BackupPlan& plan
                     , const QString& dirHash
                     , QString restorePath );


void restoreBackup( const QString& backupName
                  , const QDateTime& backupDate
                  , const QString& configPath )
{
	BackupPlan plan( BackupPlan::fromXml( backupName, configPath ) );
	BackupSnapshot snapshot( BackupSnapshot::fromXml( plan, backupDate ) );
	
	foreach ( const QString& hash, snapshot.files() )
	{
		restoreFile( plan, hash );
	}
	
	foreach ( const QString& hash, snapshot.links() )
	{
		restoreLink( plan, hash );
	}
	
	foreach ( const QString& hash, snapshot.directories() )
	{
		restoreDirectory( plan, hash );
	}
}

void restoreBackup( const QString& backupName
                  , const QDateTime& backupDate
                  , const QString& configPath
                  , const QString& restorePath )
{
	BackupPlan plan( BackupPlan::fromXml( backupName, configPath ) );
	BackupSnapshot snapshot( BackupSnapshot::fromXml( plan, backupDate ) );
	
	foreach ( const QString& hash, snapshot.files() )
	{
		restoreFile( plan, hash, restorePath );
	}
	
	foreach ( const QString& hash, snapshot.links() )
	{
		restoreLink( plan, hash, restorePath );
	}
	
	foreach ( const QString& hash, snapshot.directories() )
	{
		restoreDirectory( plan, hash, restorePath );
	}
}

void restoreFile( const BackupPlan& plan, const QString& fileHash )
{
	FileSnapshot snapshot( FileSnapshot::fromXml( plan, fileHash ) );
	
	QDir origDir( plan.storageDevice().path() );
	QDir destDir( snapshot.path() );
	
	QString orig( origDir.filePath( snapshot.data() + ".bin" ) );
	QString dest( destDir.filePath( snapshot.name() ) );
	
	if ( QFile::exists( dest ) )
	{
		QFile::remove( dest );
	}
	
	QFile::copy( orig, dest );
	
	QFile file( dest );
	file.setPermissions( snapshot.permissions().toFlags() );
}

void restoreFile( const BackupPlan& plan
                , const QString& fileHash
                , const QString& restorePath )
{
	FileSnapshot snapshot( FileSnapshot::fromXml( plan, fileHash ) );
	
	QDir origDir( plan.storageDevice().path() );
	QDir destDir( restorePath );
	
	QString orig( origDir.filePath( snapshot.data() + ".bin" ) );
	QString dest( destDir.filePath( snapshot.name() ) );
	
	if ( QFile::exists( dest ) )
	{
		QFile::remove( dest );
	}
	
	QFile::copy( orig, dest );
	
	QFile file( dest );
	file.setPermissions( snapshot.permissions().toFlags() );
}

void restoreLink( const BackupPlan& plan, const QString& linkHash )
{
	LinkSnapshot snapshot( LinkSnapshot::fromXml( plan, linkHash ) );
	
	QDir dir( snapshot.path() );
	QString link( dir.filePath( snapshot.name() ) );
	
	if ( QFileInfo( link ).isSymLink() )
	{
		QFile::remove( link );
	}
	
	QFile::link( snapshot.target(), link );
}

void restoreLink( const BackupPlan& plan
                , const QString& linkHash
                , const QString& restorePath )
{
	LinkSnapshot snapshot( LinkSnapshot::fromXml( plan, linkHash ) );
	
	QDir dir( restorePath );
	QString link( dir.filePath( snapshot.name() ) );
	
	if ( QFileInfo( link ).isSymLink() )
	{
		QFile::remove( link );
	}
	
	QFile::link( snapshot.target(), link );
}

void restoreDirectory( const BackupPlan& plan, const QString& dirHash )
{
	DirectorySnapshot snapshot( DirectorySnapshot::fromXml( plan, dirHash ) );
	
	QDir baseDir( snapshot.path() );
	baseDir.mkpath( snapshot.name() );
	
	QFile file( baseDir.filePath( snapshot.name() ) );
	file.setPermissions( snapshot.permissions().toFlags() );
	
	foreach ( const QString& hash, snapshot.files() )
	{
		restoreFile( plan, hash );
	}
	
	foreach ( const QString& hash, snapshot.links() )
	{
		restoreLink( plan, hash );
	}
	
	foreach ( const QString& hash, snapshot.directories() )
	{
		restoreDirectory( plan, hash );
	}
}

void restoreDirectory( const BackupPlan& plan
                     , const QString& dirHash
                     , QString restorePath )
{
	DirectorySnapshot snapshot( DirectorySnapshot::fromXml( plan, dirHash ) );
	
	QDir baseDir( restorePath );
	baseDir.mkpath( snapshot.name() );
	
	restorePath = baseDir.filePath( snapshot.name() );
	
	QFile file( restorePath );
	file.setPermissions( snapshot.permissions().toFlags() );
	
	foreach ( const QString& hash, snapshot.files() )
	{
		restoreFile( plan, hash, restorePath );
	}
	
	foreach ( const QString& hash, snapshot.links() )
	{
		restoreLink( plan, hash, restorePath );
	}
	
	foreach ( const QString& hash, snapshot.directories() )
	{
		restoreDirectory( plan, hash, restorePath );
	}
}

