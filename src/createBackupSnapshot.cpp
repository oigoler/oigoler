/**
 *   Copyright (C) 2013, 2015, 2016 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "createBackupSnapshot.h"
#include "BackupPlan.h"

#include <QDir>
#include <QDateTime>
#include <QCryptographicHash>
#include <QXmlStreamWriter>

#include <unistd.h>


QByteArray permissionsToByteArray( QFile::Permissions permissions );
QString readTarget( const QFileInfo& info );

void makeSnapshot( const BackupPlan& plan, const QString& name );
QString makeFileSnapshot( const FilePlan& file, const QDir& destinationDir );
QString makeLinkSnapshot( const LinkPlan& link, const QDir& destinationDir );
QString makeDirectorySnapshot( const DirectoryPlan& dir, const QDir& destDir );
QString makeDataSnapshot( const FilePlan& file, const QDir& destinationDir );

QList<FilePlan> extractFiles( QDir dir );
QList<DirectoryPlan> extractDirectories( QDir dir );
QList<LinkPlan> extractLinks( QDir dir );


void createBackupSnapshot( const QString& backupName
                         , const QString& configPath )
{
	BackupPlan plan( BackupPlan::fromXml( backupName, configPath ) );
	
	makeSnapshot( plan, backupName );
}

void makeSnapshot( const BackupPlan& plan, const QString& name )
{
	QDir dir( plan.storageDevice().path() );
	
	QString filename( name
	                + "-"
	                + QString::number( QDateTime::currentMSecsSinceEpoch() )
	                + ".xml.part" );
	
	QFile file( dir.filePath( filename ) );
	file.open( QFile::WriteOnly );
	
	QXmlStreamWriter writer( &file );
	
	writer.writeStartDocument();
	writer.writeStartElement( "snapshot" );
	
	foreach ( const FilePlan& file, plan.files() )
	{
		QString hash( makeFileSnapshot( file, dir ) );
		writer.writeTextElement( "file", hash );
	}
	
	foreach ( const LinkPlan& link, plan.links() )
	{
		QString hash( makeLinkSnapshot( link, dir ) );
		writer.writeTextElement( "link", hash );
	}
	
	foreach ( const DirectoryPlan& directory, plan.directories() )
	{
		QString hash( makeDirectorySnapshot( directory, dir ) );
		writer.writeTextElement( "directory", hash );
	}
	
	writer.writeEndElement();
	writer.writeEndDocument();
	
	filename.chop( 5 );
	file.rename( dir.filePath( filename ) );
}

QString makeFileSnapshot( const FilePlan& file, const QDir& destinationDir )
{
	QDir dir( file.path() );
	QFileInfo info( dir.filePath( file.name() ) );
	qint64 timestamp( info.lastModified().toMSecsSinceEpoch() );
	
	QByteArray permissionsBA( permissionsToByteArray( info.permissions() ) );
	QString permissionsStr( permissionsBA );
	
	QCryptographicHash hasher( QCryptographicHash::Sha1 );
	hasher.addData( info.absoluteFilePath().toUtf8() );
	hasher.addData( permissionsBA );
	hasher.addData( QByteArray::number( timestamp ) );
	
	QString filehash( hasher.result().toHex() );
	
	if ( destinationDir.exists( filehash + ".xml" ) )
	{
		return filehash;
	}
	
	QString datahash( makeDataSnapshot( file, destinationDir ) );
	
	QFile xmlFile( destinationDir.filePath( filehash + ".xml.part" ) );
	xmlFile.open( QFile::WriteOnly );
	
	QXmlStreamWriter writer( &xmlFile );
	
	writer.writeStartDocument();
	writer.writeStartElement( "filesnapshot" );
	
	writer.writeTextElement( "name", info.fileName() );
	writer.writeTextElement( "path", info.absolutePath() );
	writer.writeTextElement( "data", datahash );
	writer.writeTextElement( "permissions", permissionsStr );
	
	writer.writeEndElement();
	writer.writeEndDocument();
	
	xmlFile.rename( destinationDir.filePath( filehash + ".xml" ) );
	
	return filehash;
}

QString makeLinkSnapshot( const LinkPlan& link, const QDir& destinationDir )
{
	QDir dir( link.path() );
	QFileInfo info( dir.filePath( link.name() ) );
	QString target( readTarget( info ) );
	
	QCryptographicHash hasher( QCryptographicHash::Sha1 );
	hasher.addData( info.absoluteFilePath().toUtf8() );
	hasher.addData( target.toUtf8() );
	
	QString linkhash( hasher.result().toHex() );
	
	if ( destinationDir.exists( linkhash + ".xml" ) )
	{
		return linkhash;
	}
	
	QFile xmlFile( destinationDir.filePath( linkhash + ".xml.part" ) );
	xmlFile.open( QFile::WriteOnly );
	
	QXmlStreamWriter writer( &xmlFile );
	
	writer.writeStartDocument();
	writer.writeStartElement( "linksnapshot" );
	
	writer.writeTextElement( "name", info.fileName() );
	writer.writeTextElement( "path", info.absolutePath() );
	writer.writeTextElement( "target", target );
	
	writer.writeEndElement();
	writer.writeEndDocument();
	
	xmlFile.rename( destinationDir.filePath( linkhash + ".xml" ) );
	
	return linkhash;
}

QString makeDirectorySnapshot( const DirectoryPlan& directory
                             , const QDir& destDir )
{
	QDir dir( directory.path() );
	dir.cd( directory.name() );
	
	QFileInfo info( dir.path() );
	qint64 timestamp( info.lastModified().toMSecsSinceEpoch() );
	
	QByteArray permissionsBA( permissionsToByteArray( info.permissions() ) );
	QString permissionsStr( permissionsBA );
	
	QList<QString> filehashes;
	QList<QString> linkhashes;
	QList<QString> dirhashes;
	
	foreach ( const FilePlan& file, extractFiles( dir ) )
	{
		filehashes.append( makeFileSnapshot( file, destDir ) );
	}
	
	foreach ( const LinkPlan& link, extractLinks( dir ) )
	{
		linkhashes.append( makeLinkSnapshot( link, destDir ) );
	}
	
	foreach ( const DirectoryPlan& directory, extractDirectories( dir ) )
	{
		dirhashes.append( makeDirectorySnapshot( directory, destDir ) );
	}
	
	qSort( filehashes );
	qSort( linkhashes );
	qSort( dirhashes );
	
	QCryptographicHash hasher( QCryptographicHash::Sha1 );
	hasher.addData( info.absoluteFilePath().toUtf8() );
	hasher.addData( permissionsBA );
	
	foreach ( const QString& hash, filehashes + linkhashes + dirhashes )
	{
		hasher.addData( hash.toUtf8() );
	}
	
	QString dirhash( hasher.result().toHex() );
	
	if ( destDir.exists( dirhash + ".xml" ) )
	{
		return dirhash;
	}
	
	QFile xmlFile( destDir.filePath( dirhash + ".xml.part" ) );
	xmlFile.open( QFile::WriteOnly );
	
	QXmlStreamWriter writer( &xmlFile );
	
	writer.writeStartDocument();
	writer.writeStartElement( "directorysnapshot" );
	
	writer.writeTextElement( "name", info.fileName() );
	writer.writeTextElement( "path", info.absolutePath() );
	writer.writeTextElement( "permissions", permissionsStr );
	
	foreach ( const QString& hash, filehashes )
	{
		writer.writeTextElement( "file", hash );
	}
	
	foreach ( const QString& hash, linkhashes )
	{
		writer.writeTextElement( "link", hash );
	}
	
	foreach ( const QString& hash, dirhashes )
	{
		writer.writeTextElement( "directory", hash );
	}
	
	writer.writeEndElement();
	writer.writeEndDocument();
	
	xmlFile.rename( destDir.filePath( dirhash + ".xml" ) );
	
	return dirhash;
}

QString makeDataSnapshot( const FilePlan& file, const QDir& destinationDir )
{
	QDir dir( file.path() );
	
	QFile orig( dir.filePath( file.name() ) );
	orig.open( QFile::ReadOnly );
	
	QCryptographicHash hasher( QCryptographicHash::Sha1 );
	hasher.addData( orig.readAll() );
	
	QString datahash( hasher.result().toHex() );
	
	if ( destinationDir.exists( datahash + ".bin" ) )
	{
		return datahash;
	}
	
	QString destFilePath( destinationDir.filePath( datahash + ".bin.part" ) );
	
	orig.copy( destFilePath );
	
	QFile dest( destFilePath );
	
	destFilePath.chop( 5 );
	dest.rename( destFilePath );
	
	return datahash;
}

QByteArray permissionsToByteArray( QFile::Permissions permissions )
{
	QByteArray ret;
	
	int num( 0 );
	
	if ( permissions & QFile::ReadOwner )
		num += 4;
	
	if ( permissions & QFile::WriteOwner )
		num += 2;
	
	if ( permissions & QFile::ExeOwner )
		num += 1;
	
	ret += QByteArray::number( num );
	
	num = 0;
	
	if ( permissions & QFile::ReadGroup )
		num += 4;
	
	if ( permissions & QFile::WriteGroup )
		num += 2;
	
	if ( permissions & QFile::ExeGroup )
		num += 1;
	
	ret += QByteArray::number( num );
	
	num = 0;
	
	if ( permissions & QFile::ReadOther )
		num += 4;
	
	if ( permissions & QFile::WriteOther )
		num += 2;
	
	if ( permissions & QFile::ExeOther )
		num += 1;
	
	ret += QByteArray::number( num );
	
	return ret;
}

QString readTarget( const QFileInfo& info )
{
	QByteArray link( info.absoluteFilePath().toUtf8() );
	size_t bufsize( 1024 );
	ssize_t retsize( 0 );
	char* buf( 0 );
	
	while ( true )
	{
		buf = (char*) realloc( buf, bufsize );
		retsize = readlink( link.data(), buf, bufsize );
		
		if ( retsize < 0 )
		{
			free( buf );
			return QString();
		}
		
		if ( retsize < bufsize )
		{
			buf[retsize] = '\0';
			break;
		}
		
		bufsize *= 2;
	}
	
	QString target( buf );
	free( buf );
	
	return target;
}

QList<FilePlan> extractFiles( QDir dir )
{
	QList<FilePlan> ret;
	
	dir.setSorting( QDir::Name | QDir::IgnoreCase );
	dir.setFilter( QDir::Files | QDir::NoSymLinks );
	
	foreach ( const QFileInfo& info, dir.entryInfoList() )
	{
		FilePlan file;
		
		file.setName( info.fileName() );
		file.setPath( info.absolutePath() );
		
		ret.append( file );
	}
	
	return ret;
}

QList<DirectoryPlan> extractDirectories( QDir dir )
{
	QList<DirectoryPlan> ret;
	
	dir.setSorting( QDir::Name | QDir::IgnoreCase );
	dir.setFilter( QDir::Dirs | QDir::NoSymLinks | QDir::NoDotAndDotDot );
	
	foreach ( const QFileInfo& info, dir.entryInfoList() )
	{
		DirectoryPlan directory;
		
		directory.setName( info.fileName() );
		directory.setPath( info.absolutePath() );
		
		ret.append( directory );
	}
	
	return ret;
}

QList<LinkPlan> extractLinks( QDir dir )
{
	QList<LinkPlan> ret;
	
	dir.setSorting( QDir::Name | QDir::IgnoreCase );
	dir.setFilter( QDir::Dirs 
	             | QDir::Files
	             | QDir::System 
	             | QDir::NoDotAndDotDot );
	
	foreach ( const QFileInfo& info, dir.entryInfoList() )
	{
		if ( info.isSymLink() )
		{
			LinkPlan link;
			
			link.setName( info.fileName() );
			link.setPath( info.absolutePath() );
			
			ret.append( link );
		}
	}
	
	return ret;
}

