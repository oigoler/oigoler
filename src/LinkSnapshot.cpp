/**
 *   Copyright (C) 2016 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "LinkSnapshot.h"
#include "BackupPlan.h"

#include <QXmlStreamReader>
#include <QDir>
#include <QFile>

LinkSnapshot::LinkSnapshot()
{
}

LinkSnapshot::LinkSnapshot( const LinkSnapshot& other )
	: m_name( other.m_name )
	, m_path( other.m_path )
	, m_target( other.m_target )
{
}

LinkSnapshot::~LinkSnapshot()
{
}

LinkSnapshot& LinkSnapshot::operator=( const LinkSnapshot& other )
{
	if ( this == &other )
		return *this;
	
	m_name  = other.m_name;
	m_path  = other.m_path;
	m_target  = other.m_target;
	
	return *this;
}

const QString& LinkSnapshot::name() const
{
	return m_name;
}

void LinkSnapshot::setName( const QString& name )
{
	m_name = name;
}

const QString& LinkSnapshot::path() const
{
	return m_path;
}

void LinkSnapshot::setPath( const QString& path )
{
	m_path = path;
}

const QString& LinkSnapshot::target() const
{
	return m_target;
}

void LinkSnapshot::setTarget( const QString& target )
{
	m_target = target;
}

LinkSnapshot LinkSnapshot::fromXml( const BackupPlan& plan
                                  , const QString& hash )
{
	QDir dir( plan.storageDevice().path() );
	
	QFile file( dir.filePath( hash + ".xml" ) );
	file.open( QFile::ReadOnly );
	
	QXmlStreamReader reader( &file );
	
	LinkSnapshot snapshot;
	
	while ( !reader.atEnd() )
	{
		switch ( reader.readNext() )
		{
			case QXmlStreamReader::EndElement:
				if ( reader.name() == "linksnapshot" )
				{
					return snapshot;
				}
				
				break;
				
			case QXmlStreamReader::StartElement:
				if ( reader.name() == "name" )
				{
					snapshot.setName( reader.readElementText() );
				}
				else if ( reader.name() == "path" )
				{
					snapshot.setPath( reader.readElementText() );
				}
				else if ( reader.name() == "target" )
				{
					snapshot.setTarget( reader.readElementText() );
				}
				
				break;
		}
	}
	
	return snapshot;
}
