/**
 *   Copyright (C) 2015, 2016 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "DirectoryPlan.h"

#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QFileInfo>

DirectoryPlan::DirectoryPlan()
{
}

DirectoryPlan::DirectoryPlan( const QString& dirPath )
{
	QFileInfo fileInfo( dirPath );
	
	m_name = fileInfo.fileName();
	m_path = fileInfo.path();
}

DirectoryPlan::DirectoryPlan( const DirectoryPlan& other )
	: m_name( other.m_name )
	, m_path( other.m_path )
{
}

DirectoryPlan::~DirectoryPlan()
{
}

DirectoryPlan& DirectoryPlan::operator=( const DirectoryPlan& other )
{
	if ( this == &other )
		return *this;
	
	m_name  = other.m_name;
	m_path  = other.m_path;
	
	return *this;
}

const QString& DirectoryPlan::name() const
{
	return m_name;
}

void DirectoryPlan::setName( const QString& name )
{
	m_name = name;
}

const QString& DirectoryPlan::path() const
{
	return m_path;
}

void DirectoryPlan::setPath( const QString& path )
{
	m_path = path;
}

DirectoryPlan DirectoryPlan::fromXml( QXmlStreamReader* reader )
{
	DirectoryPlan directory;
	
	while ( !reader->atEnd() )
	{
		switch ( reader->readNext() )
		{
			case QXmlStreamReader::EndElement:
				if ( reader->name() == "directory" )
				{
					return directory;
				}
				
				break;
				
			case QXmlStreamReader::StartElement:
				if ( reader->name() == "name" )
				{
					directory.setName( reader->readElementText() );
				}
				else if ( reader->name() == "path" )
				{
					directory.setPath( reader->readElementText() );
				}
				
				break;
		}
	}
	
	return directory;
}

void DirectoryPlan::toXml( QXmlStreamWriter* writer ) const
{
	writer->writeStartElement( "directory" );
	
	if ( m_name.isEmpty() )
		writer->writeEmptyElement( "name" );
	else
		writer->writeTextElement( "name", m_name );
	
	writer->writeTextElement( "path", m_path );
	writer->writeEndElement();
}
