###############################################################################
# oigolerlib
###############################################################################

set( oigolerlib_SRCS createBackupPlan.cpp
                     createBackupSnapshot.cpp
                     listBackupPlans.cpp
                     listBackupSnapshots.cpp
                     restoreBackup.cpp
                     StorageDevice.cpp
                     BackupPlan.cpp
                     FilePlan.cpp
                     LinkPlan.cpp
                     DirectoryPlan.cpp
                     BackupSnapshot.cpp
                     FileSnapshot.cpp
                     LinkSnapshot.cpp
                     DirectorySnapshot.cpp
                     Permissions.cpp )

add_library( oigolerlib STATIC ${oigolerlib_SRCS} )

target_link_libraries( oigolerlib ${QT_LIBRARIES} )


###############################################################################
# unittest
###############################################################################

add_subdirectory( unittest )


