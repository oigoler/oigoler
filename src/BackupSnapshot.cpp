/**
 *   Copyright (C) 2016 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "BackupSnapshot.h"
#include "BackupPlan.h"

#include <QXmlStreamReader>
#include <QDir>
#include <QFile>
#include <QDateTime>

BackupSnapshot::BackupSnapshot()
{
}

BackupSnapshot::BackupSnapshot( const BackupSnapshot& other )
	: m_files( other.m_files )
	, m_links( other.m_links )
	, m_directories( other.m_directories )
{
}

BackupSnapshot::~BackupSnapshot()
{
}

BackupSnapshot& BackupSnapshot::operator=( const BackupSnapshot& other )
{
	if ( this == &other )
		return *this;
	
	m_files = other.m_files;
	m_links = other.m_links;
	m_directories  = other.m_directories;
	
	return *this;
}

const QStringList& BackupSnapshot::files() const
{
	return m_files;
}

void BackupSnapshot::addFile( const QString& hash )
{
	m_files.append( hash );
}

const QStringList& BackupSnapshot::links() const
{
	return m_links;
}

void BackupSnapshot::addLink( const QString& hash )
{
	m_links.append( hash );
}

const QStringList& BackupSnapshot::directories() const
{
	return m_directories;
}

void BackupSnapshot::addDirectory( const QString& hash )
{
	m_directories.append( hash );
}

BackupSnapshot BackupSnapshot::fromXml( const BackupPlan& plan
                                      , const QDateTime& timestamp )
{
	QDir dir( plan.storageDevice().path() );
	QString filename( plan.name()
	                + "-"
	                + QString::number( timestamp.toMSecsSinceEpoch() )
	                + ".xml" );
	
	QFile file( dir.filePath( filename ) );
	file.open( QFile::ReadOnly );
	
	QXmlStreamReader reader( &file );
	
	BackupSnapshot snapshot;
	
	while ( !reader.atEnd() )
	{
		switch ( reader.readNext() )
		{
			case QXmlStreamReader::EndElement:
				if ( reader.name() == "snapshot" )
				{
					return snapshot;
				}
				
				break;
				
			case QXmlStreamReader::StartElement:
				if ( reader.name() == "file" )
				{
					snapshot.addFile( reader.readElementText() );
				}
				else if ( reader.name() == "link" )
				{
					snapshot.addLink( reader.readElementText() );
				}
				else if ( reader.name() == "directory" )
				{
					snapshot.addDirectory( reader.readElementText() );
				}
				
				break;
		}
	}
	
	return snapshot;
}
