/**
 *   Copyright (C) 2015, 2016 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef BACKUPPLAN_H
#define BACKUPPLAN_H

#include "StorageDevice.h"
#include "FilePlan.h"
#include "LinkPlan.h"
#include "DirectoryPlan.h"

#include <QList>

class QXmlStreamWriter;

class BackupPlan
{
	
public:
	BackupPlan();
	BackupPlan( const BackupPlan& other );
	virtual ~BackupPlan();
	
	BackupPlan& operator=( const BackupPlan& other );
	
	const QString& name() const;
	void setName( const QString& name );
	
	const QString& path() const;
	void setPath( const QString& path );
	
	const StorageDevice& storageDevice() const;
	void setStorageDevice( const StorageDevice& device );
	
	const QList<FilePlan>& files() const;
	void addFile( const FilePlan& file );
	
	const QList<LinkPlan>& links() const;
	void addLink( const LinkPlan& link );
	
	const QList<DirectoryPlan>& directories() const;
	void addDirectory( const DirectoryPlan& directory );
	
	static BackupPlan fromXml( const QString& name, const QString& path );
	void toXml( QXmlStreamWriter* writer ) const;
	
private:
	QString m_name;
	QString m_path;
	StorageDevice m_storageDevice;
	QList<FilePlan> m_files;
	QList<LinkPlan> m_links;
	QList<DirectoryPlan> m_directories;
	
};

#endif // BACKUPPLAN_H
