/**
 *   Copyright (C) 2016 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "FileSnapshot.h"
#include "BackupPlan.h"

#include <QXmlStreamReader>
#include <QDir>
#include <QFile>

FileSnapshot::FileSnapshot()
{
}

FileSnapshot::FileSnapshot( const FileSnapshot& other )
	: m_name( other.m_name )
	, m_path( other.m_path )
	, m_data( other.m_data )
	, m_permissions( other.m_permissions )
{
}

FileSnapshot::~FileSnapshot()
{
}

FileSnapshot& FileSnapshot::operator=( const FileSnapshot& other )
{
	if ( this == &other )
		return *this;
	
	m_name  = other.m_name;
	m_path  = other.m_path;
	m_data  = other.m_data;
	m_permissions  = other.m_permissions;
	
	return *this;
}

const QString& FileSnapshot::name() const
{
	return m_name;
}

void FileSnapshot::setName( const QString& name )
{
	m_name = name;
}

const QString& FileSnapshot::path() const
{
	return m_path;
}

void FileSnapshot::setPath( const QString& path )
{
	m_path = path;
}

const QString& FileSnapshot::data() const
{
	return m_data;
}

void FileSnapshot::setData( const QString& data )
{
	m_data = data;
}

const Permissions& FileSnapshot::permissions() const
{
	return m_permissions;
}

void FileSnapshot::setPermissions( const Permissions& permissions )
{
	m_permissions = permissions;
}

FileSnapshot FileSnapshot::fromXml( const BackupPlan& plan
                                  , const QString& hash )
{
	QDir dir( plan.storageDevice().path() );
	
	QFile file( dir.filePath( hash + ".xml" ) );
	file.open( QFile::ReadOnly );
	
	QXmlStreamReader reader( &file );
	
	FileSnapshot snapshot;
	
	while ( !reader.atEnd() )
	{
		switch ( reader.readNext() )
		{
			case QXmlStreamReader::EndElement:
				if ( reader.name() == "filesnapshot" )
				{
					return snapshot;
				}
				
				break;
				
			case QXmlStreamReader::StartElement:
				if ( reader.name() == "name" )
				{
					snapshot.setName( reader.readElementText() );
				}
				else if ( reader.name() == "path" )
				{
					snapshot.setPath( reader.readElementText() );
				}
				else if ( reader.name() == "data" )
				{
					snapshot.setData( reader.readElementText() );
				}
				else if ( reader.name() == "permissions" )
				{
					snapshot.setPermissions( reader.readElementText() );
				}
				
				break;
		}
	}
	
	return snapshot;
}
