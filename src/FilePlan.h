/**
 *   Copyright (C) 2015, 2016 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef FILEPLAN_H
#define FILEPLAN_H

#include <QString>

class QXmlStreamReader;
class QXmlStreamWriter;

class FilePlan
{
	
public:
	FilePlan();
	FilePlan( const QString& filePath );
	FilePlan( const FilePlan& other );
	virtual ~FilePlan();
	
	FilePlan& operator=( const FilePlan& other );
	
	const QString& name() const;
	void setName( const QString& name );
	
	const QString& path() const;
	void setPath( const QString& path );
	
	static FilePlan fromXml( QXmlStreamReader* reader );
	void toXml( QXmlStreamWriter* writer ) const;
	
private:
	QString m_name;
	QString m_path;
	
};

#endif // FILEPLAN_H
