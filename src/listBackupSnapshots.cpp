/**
 *   Copyright (C) 2016 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "listBackupSnapshots.h"
#include "BackupPlan.h"

#include <QList>
#include <QDateTime>
#include <QDir>

QList<QDateTime> listBackupSnapshots( const QString& backupName
                                    , const QString& configPath )
{
	BackupPlan backupPlan( BackupPlan::fromXml( backupName, configPath ) );
	
	QDir backupDir( backupPlan.storageDevice().path() );
	
	QStringList filters;
	filters << backupName + "-*.xml";
	
	backupDir.setSorting( QDir::Name | QDir::IgnoreCase );
	backupDir.setNameFilters( filters );
	
	QList<QDateTime> ret;
	
	foreach ( QString entry, backupDir.entryList() )
	{
		entry = entry.mid( backupName.length() + 1 );
		entry.chop( 4 );
		
		qint64 timestamp( entry.toLongLong() );
		
		ret.append( QDateTime::fromMSecsSinceEpoch( timestamp ) );
	}
	
	return ret;
}


