/**
 *   Copyright (C) 2015, 2016 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "BackupPlan.h"

#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QDir>
#include <QFile>

BackupPlan::BackupPlan()
{
}

BackupPlan::BackupPlan( const BackupPlan& other )
	: m_name( other.m_name )
	, m_path( other.m_path )
	, m_storageDevice( other.m_storageDevice )
	, m_files( other.m_files )
	, m_links( other.m_links )
	, m_directories( other.m_directories )
{
}

BackupPlan::~BackupPlan()
{
}

BackupPlan& BackupPlan::operator=( const BackupPlan& other )
{
	if ( this == &other )
		return *this;
	
	m_name = other.m_name;
	m_path = other.m_path;
	m_storageDevice  = other.m_storageDevice;
	m_files  = other.m_files;
	m_links  = other.m_links;
	m_directories  = other.m_directories;
	
	return *this;
}

const QString& BackupPlan::name() const
{
	return m_name;
}

void BackupPlan::setName( const QString& name )
{
	m_name = name;
}

const QString& BackupPlan::path() const
{
	return m_path;
}

void BackupPlan::setPath( const QString& path )
{
	m_path = path;
}
	
const StorageDevice& BackupPlan::storageDevice() const
{
	return m_storageDevice;
}

void BackupPlan::setStorageDevice( const StorageDevice& device )
{
	m_storageDevice = device;
}

const QList<FilePlan>& BackupPlan::files() const
{
	return m_files;
}

void BackupPlan::addFile( const FilePlan& file )
{
	m_files.append( file );
}

const QList<LinkPlan>& BackupPlan::links() const
{
	return m_links;
}

void BackupPlan::addLink( const LinkPlan& link )
{
	m_links.append( link );
}

const QList<DirectoryPlan>& BackupPlan::directories() const
{
	return m_directories;
}

void BackupPlan::addDirectory( const DirectoryPlan& directory )
{
	m_directories.append( directory );
}

BackupPlan BackupPlan::fromXml( const QString& name, const QString& path )
{
	QDir dir( path );
	
	QFile file( dir.filePath( name + ".xml" ) );
	file.open( QFile::ReadOnly );
	
	QXmlStreamReader reader( &file );
	
	BackupPlan plan;
	
	plan.setName( name );
	plan.setPath( path );
	
	while ( !reader.atEnd() )
	{
		switch ( reader.readNext() )
		{
			case QXmlStreamReader::EndElement:
				if ( reader.name() == "backupplan" )
				{
					return plan;
				}
				
				break;
				
			case QXmlStreamReader::StartElement:
				if ( reader.name() == "storagedevice" )
				{
					plan.setStorageDevice( StorageDevice::fromXml( &reader ) );
				}
				else if ( reader.name() == "file" )
				{
					plan.addFile( FilePlan::fromXml( &reader ) );
				}
				else if ( reader.name() == "link" )
				{
					plan.addLink( LinkPlan::fromXml( &reader ) );
				}
				else if ( reader.name() == "directory" )
				{
					plan.addDirectory( DirectoryPlan::fromXml( &reader ) );
				}
				
				break;
		}
	}
	
	return plan;
}

void BackupPlan::toXml( QXmlStreamWriter* writer ) const
{
	writer->writeStartDocument();
	writer->writeStartElement( "backupplan" );
	
	m_storageDevice.toXml( writer );
	
	foreach ( const FilePlan& plan, m_files )
	{
		plan.toXml( writer );
	}
	
	foreach ( const LinkPlan& plan, m_links )
	{
		plan.toXml( writer );
	}
	
	foreach ( const DirectoryPlan& plan, m_directories )
	{
		plan.toXml( writer );
	}
	
	writer->writeEndElement();
	writer->writeEndDocument();
}
