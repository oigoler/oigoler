/**
 *   Copyright (C) 2015, 2016 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "LinkPlan.h"

#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QFileInfo>

LinkPlan::LinkPlan()
{
}

LinkPlan::LinkPlan( const QString& linkPath )
{
	QFileInfo fileInfo( linkPath );
	
	m_name = fileInfo.fileName();
	m_path = fileInfo.path();
}

LinkPlan::LinkPlan( const LinkPlan& other )
	: m_name( other.m_name )
	, m_path( other.m_path )
{
}

LinkPlan::~LinkPlan()
{
}

LinkPlan& LinkPlan::operator=( const LinkPlan& other )
{
	if ( this == &other )
		return *this;
	
	m_name  = other.m_name;
	m_path  = other.m_path;
	
	return *this;
}

const QString& LinkPlan::name() const
{
	return m_name;
}

void LinkPlan::setName( const QString& name )
{
	m_name = name;
}

const QString& LinkPlan::path() const
{
	return m_path;
}

void LinkPlan::setPath( const QString& path )
{
	m_path = path;
}

LinkPlan LinkPlan::fromXml( QXmlStreamReader* reader )
{
	LinkPlan link;
	
	while ( !reader->atEnd() )
	{
		switch ( reader->readNext() )
		{
			case QXmlStreamReader::EndElement:
				if ( reader->name() == "link" )
				{
					return link;
				}
				
				break;
				
			case QXmlStreamReader::StartElement:
				if ( reader->name() == "name" )
				{
					link.setName( reader->readElementText() );
				}
				else if ( reader->name() == "path" )
				{
					link.setPath( reader->readElementText() );
				}
				
				break;
		}
	}
	
	return link;
}

void LinkPlan::toXml( QXmlStreamWriter* writer ) const
{
	writer->writeStartElement( "link" );
	writer->writeTextElement( "name", m_name );
	writer->writeTextElement( "path", m_path );
	writer->writeEndElement();
}
