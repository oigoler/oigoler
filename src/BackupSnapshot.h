/**
 *   Copyright (C) 2016 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef BACKUPSNAPSHOT_H
#define BACKUPSNAPSHOT_H

#include <QStringList>

class BackupPlan;
class QDateTime;

class BackupSnapshot
{
	
public:
	BackupSnapshot();
	BackupSnapshot( const BackupSnapshot& other );
	virtual ~BackupSnapshot();
	
	BackupSnapshot& operator=( const BackupSnapshot& other );
	
	const QStringList& files() const;
	void addFile( const QString& hash );
	
	const QStringList& links() const;
	void addLink( const QString& hash );
	
	const QStringList& directories() const;
	void addDirectory( const QString& hash );
	
	static BackupSnapshot fromXml( const BackupPlan& plan
	                             , const QDateTime& timestamp );
	
private:
	QStringList m_files;
	QStringList m_links;
	QStringList m_directories;
	
};

#endif // BACKUPSNAPSHOT_H
