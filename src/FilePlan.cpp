/**
 *   Copyright (C) 2015, 2016 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "FilePlan.h"

#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QFileInfo>

FilePlan::FilePlan()
{
}

FilePlan::FilePlan( const QString& filePath )
{
	QFileInfo fileInfo( filePath );
	
	m_name = fileInfo.fileName();
	m_path = fileInfo.path();
}

FilePlan::FilePlan( const FilePlan& other )
	: m_name( other.m_name )
	, m_path( other.m_path )
{
}

FilePlan::~FilePlan()
{
}

FilePlan& FilePlan::operator=( const FilePlan& other )
{
	if ( this == &other )
		return *this;
	
	m_name  = other.m_name;
	m_path  = other.m_path;
	
	return *this;
}

const QString& FilePlan::name() const
{
	return m_name;
}

void FilePlan::setName( const QString& name )
{
	m_name = name;
}

const QString& FilePlan::path() const
{
	return m_path;
}

void FilePlan::setPath( const QString& path )
{
	m_path = path;
}

FilePlan FilePlan::fromXml( QXmlStreamReader* reader )
{
	FilePlan filePlan;
	
	while ( !reader->atEnd() )
	{
		switch ( reader->readNext() )
		{
			case QXmlStreamReader::EndElement:
				if ( reader->name() == "file" )
				{
					return filePlan;
				}
				
				break;
				
			case QXmlStreamReader::StartElement:
				if ( reader->name() == "name" )
				{
					filePlan.setName( reader->readElementText() );
				}
				else if ( reader->name() == "path" )
				{
					filePlan.setPath( reader->readElementText() );
				}
				
				break;
		}
	}
	
	return filePlan;
}

void FilePlan::toXml( QXmlStreamWriter* writer ) const
{
	writer->writeStartElement( "file" );
	writer->writeTextElement( "name", m_name );
	writer->writeTextElement( "path", m_path );
	writer->writeEndElement();
}
