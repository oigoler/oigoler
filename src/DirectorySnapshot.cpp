/**
 *   Copyright (C) 2016 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "DirectorySnapshot.h"
#include "BackupPlan.h"

#include <QXmlStreamReader>
#include <QDir>
#include <QFile>

DirectorySnapshot::DirectorySnapshot()
{
}

DirectorySnapshot::DirectorySnapshot( const DirectorySnapshot& other )
	: m_name( other.m_name )
	, m_path( other.m_path )
	, m_permissions( other.m_permissions )
	, m_files( other.m_files )
	, m_links( other.m_links )
	, m_directories( other.m_directories )
{
}

DirectorySnapshot::~DirectorySnapshot()
{
}

DirectorySnapshot& DirectorySnapshot::operator=(
	const DirectorySnapshot& other )
{
	if ( this == &other )
		return *this;
	
	m_name  = other.m_name;
	m_path  = other.m_path;
	m_permissions  = other.m_permissions;
	m_files  = other.m_files;
	m_links  = other.m_links;
	m_directories  = other.m_directories;
	
	return *this;
}

const QString& DirectorySnapshot::name() const
{
	return m_name;
}

void DirectorySnapshot::setName( const QString& name )
{
	m_name = name;
}

const QString& DirectorySnapshot::path() const
{
	return m_path;
}

void DirectorySnapshot::setPath( const QString& path )
{
	m_path = path;
}

const Permissions& DirectorySnapshot::permissions() const
{
	return m_permissions;
}

void DirectorySnapshot::setPermissions( const Permissions& permissions )
{
	m_permissions = permissions;
}

const QStringList& DirectorySnapshot::files() const
{
	return m_files;
}

void DirectorySnapshot::addFile( const QString& hash )
{
	m_files.append( hash );
}

const QStringList& DirectorySnapshot::links() const
{
	return m_links;
}

void DirectorySnapshot::addLink( const QString& hash )
{
	m_links.append( hash );
}

const QStringList& DirectorySnapshot::directories() const
{
	return m_directories;
}

void DirectorySnapshot::addDirectory( const QString& hash )
{
	m_directories.append( hash );
}

DirectorySnapshot DirectorySnapshot::fromXml( const BackupPlan& plan
                                            , const QString& hash )
{
	QDir dir( plan.storageDevice().path() );
	
	QFile file( dir.filePath( hash + ".xml" ) );
	file.open( QFile::ReadOnly );
	
	QXmlStreamReader reader( &file );
	
	DirectorySnapshot snapshot;
	
	while ( !reader.atEnd() )
	{
		switch ( reader.readNext() )
		{
			case QXmlStreamReader::EndElement:
				if ( reader.name() == "directorysnapshot" )
				{
					return snapshot;
				}
				
				break;
				
			case QXmlStreamReader::StartElement:
				if ( reader.name() == "name" )
				{
					snapshot.setName( reader.readElementText() );
				}
				else if ( reader.name() == "path" )
				{
					snapshot.setPath( reader.readElementText() );
				}
				else if ( reader.name() == "permissions" )
				{
					snapshot.setPermissions( reader.readElementText() );
				}
				else if ( reader.name() == "file" )
				{
					snapshot.addFile( reader.readElementText() );
				}
				else if ( reader.name() == "link" )
				{
					snapshot.addLink( reader.readElementText() );
				}
				else if ( reader.name() == "directory" )
				{
					snapshot.addDirectory( reader.readElementText() );
				}
				
				break;
		}
	}
	
	return snapshot;
}
