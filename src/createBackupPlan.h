/**
 *   Copyright (C) 2015 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef CREATEBACKUPPLAN_H
#define CREATEBACKUPPLAN_H

#include <QStringList>

class QString;

void createBackupPlan( const QString& backupName
                     , const QString& configPath
                     , const QString& storagePath
                     , const QStringList& fileList = QStringList()
                     , const QStringList& linkList = QStringList()
                     , const QStringList& directoryList = QStringList() );

#endif // CREATEBACKUPPLAN_H
