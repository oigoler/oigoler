/**
 *   Copyright (C) 2016 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "Permissions.h"

#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QFileInfo>

Permissions::Permissions()
	: m_permissions( 9 )
{
}

Permissions::Permissions( const QString& permissions )
	: m_permissions( 9 )
{
	int owner( permissions.mid( 0, 1 ).toInt() );
	int group( permissions.mid( 1, 1 ).toInt() );
	int others( permissions.mid( 2, 1 ).toInt() );
	
	int all( ( owner << 6 ) + ( group << 3 ) + others );
	
	int test( 0x100 );
	
	for ( int i = 0; i < 9; ++i )
	{
		if ( all & test )
		{
			m_permissions.setBit( i );
		}
		
		test = test >> 1;
	}
}

Permissions::Permissions( const Permissions& other )
	: m_permissions( other.m_permissions )
{
}

Permissions::~Permissions()
{
}

Permissions& Permissions::operator=( const Permissions& other )
{
	if ( this == &other )
		return *this;
	
	m_permissions  = other.m_permissions;
	
	return *this;
}

QFile::Permissions Permissions::toFlags() const
{
	QFile::Permissions ret;
	
	for ( int i = 0; i < 9; ++i )
	{
		if ( m_permissions.testBit( i ) )
		{
			switch ( i )
			{
				case 0:
					ret |= QFile::ReadOwner;
					break;
					
				case 1:
					ret |= QFile::WriteOwner;
					break;
					
				case 2:
					ret |= QFile::ExeOwner;
					break;
					
				case 3:
					ret |= QFile::ReadGroup;
					break;
					
				case 4:
					ret |= QFile::WriteGroup;
					break;
					
				case 5:
					ret |= QFile::ExeGroup;
					break;
					
				case 6:
					ret |= QFile::ReadOther;
					break;
					
				case 7:
					ret |= QFile::WriteOther;
					break;
					
				case 8:
					ret |= QFile::ExeOther;
					break;
			}
		}
	}
	
	return ret;
}
