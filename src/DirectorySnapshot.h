/**
 *   Copyright (C) 2016 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef DIRECTORYSNAPSHOT_H
#define DIRECTORYSNAPSHOT_H

#include "Permissions.h"

#include <QString>
#include <QStringList>

class BackupPlan;

class DirectorySnapshot
{
	
public:
	DirectorySnapshot();
	DirectorySnapshot( const DirectorySnapshot& other );
	virtual ~DirectorySnapshot();
	
	DirectorySnapshot& operator=( const DirectorySnapshot& other );
	
	const QString& name() const;
	void setName( const QString& name );
	
	const QString& path() const;
	void setPath( const QString& path );
	
	const Permissions& permissions() const;
	void setPermissions( const Permissions& permissions );
	
	const QStringList& files() const;
	void addFile( const QString& hash );
	
	const QStringList& links() const;
	void addLink( const QString& hash );
	
	const QStringList& directories() const;
	void addDirectory( const QString& hash );
	
	static DirectorySnapshot fromXml( const BackupPlan& plan, const QString& hash );
	
private:
	QString m_name;
	QString m_path;
	Permissions m_permissions;
	QStringList m_files;
	QStringList m_links;
	QStringList m_directories;
	
};

#endif // DIRECTORYSNAPSHOT_H
