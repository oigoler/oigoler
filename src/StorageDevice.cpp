/**
 *   Copyright (C) 2015, 2016 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "StorageDevice.h"

#include <QXmlStreamReader>
#include <QXmlStreamWriter>

StorageDevice::StorageDevice()
{
}

StorageDevice::StorageDevice( const QString& path )
	: m_path( path )
{
}

StorageDevice::StorageDevice( const StorageDevice& other )
	: m_path( other.m_path )
{
}

StorageDevice::~StorageDevice()
{
}

StorageDevice& StorageDevice::operator=( const StorageDevice& other )
{
	if ( this == &other )
		return *this;
	
	m_path  = other.m_path;
	
	return *this;
}

QString StorageDevice::path() const
{
	return m_path;
}

void StorageDevice::setPath( const QString& path )
{
	m_path = path;
}

StorageDevice StorageDevice::fromXml( QXmlStreamReader* reader )
{
	StorageDevice device;
	
	while ( !reader->atEnd() )
	{
		switch ( reader->readNext() )
		{
			case QXmlStreamReader::EndElement:
				if ( reader->name() == "storagedevice" )
				{
					return device;
				}
				
				break;
				
			case QXmlStreamReader::StartElement:
				if ( reader->name() == "path" )
				{
					device.setPath( reader->readElementText() );
				}
				
				break;
		}
	}
	
	return device;
}

void StorageDevice::toXml( QXmlStreamWriter* writer ) const
{
	writer->writeStartElement( "storagedevice" );
	writer->writeTextElement( "path", m_path );
	writer->writeEndElement();
}
