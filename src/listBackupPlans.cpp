/**
 *   Copyright (C) 2015 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "listBackupPlans.h"

#include <QStringList>
#include <QDir>

QStringList listBackupPlans( const QString& configPath )
{
	QDir dir( configPath );
	QStringList filters;
	
	filters << "*.xml";
	
	dir.setSorting( QDir::Name | QDir::IgnoreCase );
	dir.setNameFilters( filters );
	
	QStringList entries( dir.entryList() );
	
	for ( int i = 0; i < entries.size(); ++i )
	{
		entries[i].chop( 4 );
	}
	
	return entries;
}
