/**
 *   Copyright (C) 2016 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef FILESNAPSHOT_H
#define FILESNAPSHOT_H

#include "Permissions.h"

#include <QString>

class BackupPlan;

class FileSnapshot
{
	
public:
	FileSnapshot();
	FileSnapshot( const FileSnapshot& other );
	virtual ~FileSnapshot();
	
	FileSnapshot& operator=( const FileSnapshot& other );
	
	const QString& name() const;
	void setName( const QString& name );
	
	const QString& path() const;
	void setPath( const QString& path );
	
	const QString& data() const;
	void setData( const QString& data );
	
	const Permissions& permissions() const;
	void setPermissions( const Permissions& permissions );
	
	static FileSnapshot fromXml( const BackupPlan& plan, const QString& hash );
	
private:
	QString m_name;
	QString m_path;
	QString m_data;
	Permissions m_permissions;
	
};

#endif // FILESNAPSHOT_H
