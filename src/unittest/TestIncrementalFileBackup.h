/**
 *   Copyright (C) 2015 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef TESTINCREMENTALFILEBACKUP_H
#define TESTINCREMENTALFILEBACKUP_H

#include <QtTest/QtTest>

class TestIncrementalFileBackup : public QObject
{
	Q_OBJECT

private slots:
	void initTestCase();
	
	void testNumberOfCreatedFiles();
	void testSnapshotFiles();
	void testFile1();
	void testFile2();
	void testFile3();
	void testData1();
	void testData2();
	void testTimestamps1();
	void testTimestamps2();
	void testTimestamps3();
	
	//void cleanupTestCase();
	
private:
	void extractSnapshotInfo( QHash< QString, QDateTime >* info );
	void setFileHash( QByteArray* dest, QByteArray permissions );
	
	QByteArray m_filehash1;
	QByteArray m_filehash2;
	QByteArray m_filehash3;
	
	QByteArray m_datahash1;
	QByteArray m_datahash2;
	
	QHash< QString, QDateTime > m_snapshot1Info;
	QHash< QString, QDateTime > m_snapshot2Info;
	QHash< QString, QDateTime > m_snapshot3Info;
	QHash< QString, QDateTime > m_snapshot4Info;
	
};

#endif // TESTINCREMENTALFILEBACKUP_H
