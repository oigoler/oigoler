/**
 *   Copyright (C) 2015 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestCreateBackupPlan.h"
#include "DirectoryGenerator.h"
#include "XmlTester.h"
#include "../createBackupPlan.h"


void TestCreateBackupPlan::initTestCase()
{
	new DirectoryGenerator("config", this);
	
	QString configPath( QDir::current().filePath( "config" ) );
	
	QStringList files;
	files << "/home/file1.txt" << "/home/file2.avi";
	
	QStringList links;
	links << "/home/work" << "/home/video.avi";
	
	QStringList dirs;
	dirs << "/pron" << "/";
	
	createBackupPlan( "home", configPath, "/backup", files, links, dirs );
}

void TestCreateBackupPlan::testFileName()
{
	QVERIFY( QDir::current().exists( "config/home.xml" ) );
}

void TestCreateBackupPlan::testFileContents()
{
	QFile file( "config/home.xml" );
	QVERIFY( file.open( QFile::ReadOnly ) );
	
	QXmlStreamReader reader( &file );
	XmlTester tester( &reader );
	
	QVERIFY( tester.testStartDocument() );
	QVERIFY( tester.testStartElement( "backupplan" ) );
	
	QVERIFY( tester.testStartElement( "storagedevice" ) );
	QVERIFY( tester.testStartElement( "path" ) );
	QVERIFY( tester.testCharacters( "/backup" ) );
	QVERIFY( tester.testEndElement( "path" ) );
	QVERIFY( tester.testEndElement( "storagedevice" ) );
	
	QVERIFY( tester.testStartElement( "file" ) );
	QVERIFY( tester.testStartElement( "name" ) );
	QVERIFY( tester.testCharacters( "file1.txt" ) );
	QVERIFY( tester.testEndElement( "name" ) );
	QVERIFY( tester.testStartElement( "path" ) );
	QVERIFY( tester.testCharacters( "/home" ) );
	QVERIFY( tester.testEndElement( "path" ) );
	QVERIFY( tester.testEndElement( "file" ) );
	
	QVERIFY( tester.testStartElement( "file" ) );
	QVERIFY( tester.testStartElement( "name" ) );
	QVERIFY( tester.testCharacters( "file2.avi" ) );
	QVERIFY( tester.testEndElement( "name" ) );
	QVERIFY( tester.testStartElement( "path" ) );
	QVERIFY( tester.testCharacters( "/home" ) );
	QVERIFY( tester.testEndElement( "path" ) );
	QVERIFY( tester.testEndElement( "file" ) );
	
	QVERIFY( tester.testStartElement( "link" ) );
	QVERIFY( tester.testStartElement( "name" ) );
	QVERIFY( tester.testCharacters( "work" ) );
	QVERIFY( tester.testEndElement( "name" ) );
	QVERIFY( tester.testStartElement( "path" ) );
	QVERIFY( tester.testCharacters( "/home" ) );
	QVERIFY( tester.testEndElement( "path" ) );
	QVERIFY( tester.testEndElement( "link" ) );
	
	QVERIFY( tester.testStartElement( "link" ) );
	QVERIFY( tester.testStartElement( "name" ) );
	QVERIFY( tester.testCharacters( "video.avi" ) );
	QVERIFY( tester.testEndElement( "name" ) );
	QVERIFY( tester.testStartElement( "path" ) );
	QVERIFY( tester.testCharacters( "/home" ) );
	QVERIFY( tester.testEndElement( "path" ) );
	QVERIFY( tester.testEndElement( "link" ) );
	
	QVERIFY( tester.testStartElement( "directory" ) );
	QVERIFY( tester.testStartElement( "name" ) );
	QVERIFY( tester.testCharacters( "pron" ) );
	QVERIFY( tester.testEndElement( "name" ) );
	QVERIFY( tester.testStartElement( "path" ) );
	QVERIFY( tester.testCharacters( "/" ) );
	QVERIFY( tester.testEndElement( "path" ) );
	QVERIFY( tester.testEndElement( "directory" ) );
	
	QVERIFY( tester.testStartElement( "directory" ) );
	QVERIFY( tester.testStartElement( "name" ) );
	QVERIFY( tester.testEndElement( "name" ) );
	QVERIFY( tester.testStartElement( "path" ) );
	QVERIFY( tester.testCharacters( "/" ) );
	QVERIFY( tester.testEndElement( "path" ) );
	QVERIFY( tester.testEndElement( "directory" ) );
	
	QVERIFY( tester.testEndElement( "backupplan" ) );
	QVERIFY( tester.testEndDocument() );
}

QTEST_MAIN(TestCreateBackupPlan)
