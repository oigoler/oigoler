/**
 *   Copyright (C) 2015 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef TESTINCREMENTALDIRECTORYBACKUP_H
#define TESTINCREMENTALDIRECTORYBACKUP_H

#include <QtTest/QtTest>

class TestIncrementalDirectoryBackup : public QObject
{
	Q_OBJECT

private slots:
	void initTestCase();
	
	void testNumberOfCreatedFiles();
	void testSnapshotFiles();
	void testDir1();
	void testDir2();
	void testDir3();
	void testFile1();
	void testData1();
	void testTimestamps1();
	void testTimestamps2();
	void testTimestamps3();
	
	//void cleanupTestCase();
	
private:
	void extractSnapshotInfo( QHash< QString, QDateTime >* info );
	void setFileHash( QByteArray* dest, QByteArray permissions );
	
	void setDirHash( QByteArray* dest
	               , QByteArray permissions
	               , const QByteArray& filehash = QByteArray() );
	
	QByteArray m_dirhash1;
	QByteArray m_dirhash2;
	QByteArray m_dirhash3;
	
	QByteArray m_filehash1;
	QByteArray m_datahash1;
	
	QHash< QString, QDateTime > m_snapshot1Info;
	QHash< QString, QDateTime > m_snapshot2Info;
	QHash< QString, QDateTime > m_snapshot3Info;
	QHash< QString, QDateTime > m_snapshot4Info;
	
};

#endif // TESTINCREMENTALDIRECTORYBACKUP_H
