/**
 *   Copyright (C) 2016 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestRestoreLink.h"
#include "LinkGenerator.h"
#include "LinkCleaner.h"
#include "LinkTester.h"
#include "DirectoryGenerator.h"
#include "../createBackupPlan.h"
#include "../createBackupSnapshot.h"
#include "../listBackupSnapshots.h"
#include "../restoreBackup.h"


void TestRestoreLink::initTestCase()
{
	// link -> a
	
	LinkGenerator* testLink = new LinkGenerator( "link", "a" );
	
	// Backup plan
	
	new DirectoryGenerator( "config", this );
	new DirectoryGenerator( "backup", this );
	
	QString configPath( QDir::current().filePath( "config" ) );
	QString backupPath( QDir::current().filePath( "backup" ) );
	
	QStringList empty;
	QStringList links;
	links << QDir::current().filePath( "link" );
	
	createBackupPlan( "plan", configPath, backupPath, empty, links );
	
	// Snapshot
	
	createBackupSnapshot( "plan", configPath );
	
	// Delete link
	
	delete testLink;
	
	// Restore link
	
	QList<QDateTime> timestamps( listBackupSnapshots( "plan", configPath ) );
	QCOMPARE( timestamps.count(), 1 );
	
	restoreBackup( "plan", timestamps.at( 0 ), configPath );
}

void TestRestoreLink::testLinkExists()
{
	QVERIFY( QFileInfo( "link" ).isSymLink() );
}

void TestRestoreLink::testLinkTarget()
{
	LinkTester tester( "link" );
	
	QVERIFY( tester.isLink() );
	QCOMPARE( tester.target(), QString( "a" ) );
}

void TestRestoreLink::cleanupTestCase()
{
	LinkCleaner cleaner( "link" );
}

QTEST_MAIN(TestRestoreLink)
