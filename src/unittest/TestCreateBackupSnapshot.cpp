/**
 *   Copyright (C) 2015 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestCreateBackupSnapshot.h"
#include "FileGenerator.h"
#include "LinkGenerator.h"
#include "DirectoryGenerator.h"
#include "XmlTester.h"
#include "../createBackupSnapshot.h"


void TestCreateBackupSnapshot::initTestCase()
{
	/* file (666) (100 * 'a')
	 * links -> dir/dir
	 * dir (777)
	 * | file (644) (100 * 'b')
	 * | dir (755)
	 * | | file (600) (100 * 'c')
	 * | | link -> ../file
	 */
	
	new FileGenerator( "file"
	                 , QByteArray( 100, 'a' )
	                 , QFile::ReadUser 
	                 | QFile::WriteUser 
	                 | QFile::ReadGroup 
	                 | QFile::WriteGroup
	                 | QFile::ReadOther 
	                 | QFile::WriteOther
	                 , this );
	
	new LinkGenerator( "link", "dir/dir", this );
	
	new DirectoryGenerator( "dir"
	                      , QFile::ReadUser
	                      | QFile::WriteUser
	                      | QFile::ExeUser
	                      | QFile::ReadGroup
	                      | QFile::WriteGroup
	                      | QFile::ExeGroup
	                      | QFile::ReadOther
	                      | QFile::WriteOther
	                      | QFile::ExeOther
						  , this);
	
	new FileGenerator( "dir/file"
	                 , QByteArray( 100, 'b' )
	                 , QFile::ReadUser
	                 | QFile::WriteUser
	                 | QFile::ReadGroup
	                 | QFile::ReadOther 
	                 , this );
	
	new DirectoryGenerator( "dir/dir"
	                      , QFile::ReadUser
	                      | QFile::WriteUser
	                      | QFile::ExeUser
	                      | QFile::ReadGroup
	                      | QFile::ExeGroup
	                      | QFile::ReadOther
	                      | QFile::ExeOther
						  , this);
	
	new FileGenerator( "dir/dir/file"
	                 , QByteArray( 100, 'c' )
	                 , QFile::ReadUser
	                 | QFile::WriteUser
	                 , this );
	
	new LinkGenerator( "dir/dir/link", "../file", this );
	
	/* config/test.xml
	 * 
	 * <backupplan>
	 *     <storagedevice>
	 *         <path>/.../backup</path>
	 *     </storagedevice>
	 *     <file>
	 *         <name>file</name>
	 *         <path>/.../</path>
	 *     </file>
	 *     <link>
	 *         <name>link</name>
	 *         <path>/.../</path>
	 *     </link>
	 *     <directory>
	 *         <name>dir</name>
	 *         <path>/.../</path>
	 *     </directory>
	 * </backupplan>
	 */
	
	new DirectoryGenerator( "config", this );
	
	QFile file( "config/test.xml" );
	file.open( QFile::WriteOnly );
	
	QXmlStreamWriter writer( &file );
	
	writer.writeStartDocument();
	writer.writeStartElement( "backupplan" );
	
	writer.writeStartElement( "storagedevice" );
	writer.writeTextElement( "path", QDir::current().filePath( "backup" ) );
	writer.writeEndElement();
	
	writer.writeStartElement( "file" );
	writer.writeTextElement( "name", "file" );
	writer.writeTextElement( "path", QDir::currentPath() );
	writer.writeEndElement();
	
	writer.writeStartElement( "link" );
	writer.writeTextElement( "name", "link" );
	writer.writeTextElement( "path", QDir::currentPath() );
	writer.writeEndElement();
	
	writer.writeStartElement( "directory" );
	writer.writeTextElement( "name", "dir" );
	writer.writeTextElement( "path", QDir::currentPath() );
	writer.writeEndElement();
	
	writer.writeEndElement(); // backupplan
	writer.writeEndDocument();
	
	file.close();
	
	// Storage Device
	
	new DirectoryGenerator( "backup", this );
	
	// Generate all hashes
	
	calculateHashes();
	
	// Create Backup Snapshot
	
	m_startTime = QDateTime::currentMSecsSinceEpoch();
	createBackupSnapshot( "test", "config" );
	m_endTime = QDateTime::currentMSecsSinceEpoch();
}

void TestCreateBackupSnapshot::testNumberOfCreatedFiles()
{
	QDir storageDir( "backup" );
	storageDir.setFilter( QDir::AllEntries | QDir::NoDotAndDotDot );
	
	QCOMPARE( storageDir.entryList().size(), 11 );
}

void TestCreateBackupSnapshot::testSnapshotFilename()
{
	QDir dir( "backup" );
	QStringList filters;
	
	filters << "test-*.xml";
	dir.setNameFilters( filters );
	
	QStringList entries( dir.entryList() );
	QCOMPARE( entries.size(), 1 );
	
	bool ok;
	QString filename( entries[0] );
	QString strTimestamp( filename.mid( 5, filename.length() - 9 ) );
	qlonglong timestamp( strTimestamp.toLongLong( &ok ) );
	
	QVERIFY( ok );
	QVERIFY( m_startTime <= timestamp );
	QVERIFY( timestamp <= m_endTime );
}

void TestCreateBackupSnapshot::testSnapshotContents()
{
	QDir dir( "backup" );
	QStringList filters;
	filters << "test-*.xml";
	dir.setNameFilters( filters );
	QCOMPARE( dir.entryList().size(), 1 );
	
	QFile file( dir.entryInfoList()[0].filePath() );
	QVERIFY( file.open( QFile::ReadOnly ) );
	
	QXmlStreamReader reader( &file );
	XmlTester tester( &reader );
	
	QVERIFY( tester.testStartDocument() );
	QVERIFY( tester.testStartElement( "snapshot" ) );
	
	QVERIFY( tester.testStartElement( "file" ) );
	QVERIFY( tester.testCharacters( m_filehash1 ) );
	QVERIFY( tester.testEndElement( "file" ) );
	
	QVERIFY( tester.testStartElement( "link" ) );
	QVERIFY( tester.testCharacters( m_linkhash1 ) );
	QVERIFY( tester.testEndElement( "link" ) );
	
	QVERIFY( tester.testStartElement( "directory" ) );
	QVERIFY( tester.testCharacters( m_dirhash1 ) );
	QVERIFY( tester.testEndElement( "directory" ) );
	
	QVERIFY( tester.testEndElement( "snapshot" ) );
	QVERIFY( tester.testEndDocument() );
}

void TestCreateBackupSnapshot::testFile1Filename()
{
	QDir dir( "backup" );
	QStringList filters;
	QString filename( m_filehash1 + ".xml" );
	
	filters << filename;
	dir.setNameFilters( filters );
	
	QCOMPARE( dir.entryList().size(), 1 );
}

void TestCreateBackupSnapshot::testFile1Contents()
{
	QFile file( "backup/" + m_filehash1 + ".xml" );
	QVERIFY( file.open( QFile::ReadOnly ) );
	
	QXmlStreamReader reader( &file );
	XmlTester tester( &reader );
	
	QVERIFY( tester.testStartDocument() );
	QVERIFY( tester.testStartElement( "filesnapshot" ) );
	
	QVERIFY( tester.testStartElement( "name" ) );
	QVERIFY( tester.testCharacters( "file" ) );
	QVERIFY( tester.testEndElement( "name" ) );
	
	QVERIFY( tester.testStartElement( "path" ) );
	QVERIFY( tester.testCharacters( QDir::currentPath() ) );
	QVERIFY( tester.testEndElement( "path" ) );
	
	QVERIFY( tester.testStartElement( "data" ) );
	QVERIFY( tester.testCharacters( m_datahash1 ) );
	QVERIFY( tester.testEndElement( "data" ) );
	
	QVERIFY( tester.testStartElement( "permissions" ) );
	QVERIFY( tester.testCharacters( "666" ) );
	QVERIFY( tester.testEndElement( "permissions" ) );
	
	QVERIFY( tester.testEndElement( "filesnapshot" ) );
	QVERIFY( tester.testEndDocument() );
}

void TestCreateBackupSnapshot::testData1Filename()
{
	QDir dir( "backup" );
	QStringList filters;
	QString filename( m_datahash1 + ".bin" );
	
	filters << filename;
	dir.setNameFilters( filters );
	
	QCOMPARE( dir.entryList().size(), 1 );
}

void TestCreateBackupSnapshot::testData1Contents()
{
	QByteArray data( 100, 'a' );
	
	QFile file( "backup/" + m_datahash1 + ".bin" );
	QVERIFY( file.open( QFile::ReadOnly ) );
	
	QCOMPARE( file.readAll(), data );
}

void TestCreateBackupSnapshot::testLink1Filename()
{
	QDir dir( "backup" );
	QStringList filters;
	QString filename( m_linkhash1 + ".xml" );
	
	filters << filename;
	dir.setNameFilters( filters );
	
	QCOMPARE( dir.entryList().size(), 1 );
}

void TestCreateBackupSnapshot::testLink1Contents()
{
	QFile file( "backup/" + m_linkhash1 + ".xml" );
	QVERIFY( file.open( QFile::ReadOnly ) );
	
	QXmlStreamReader reader( &file );
	XmlTester tester( &reader );
	
	QVERIFY( tester.testStartDocument() );
	QVERIFY( tester.testStartElement( "linksnapshot" ) );
	
	QVERIFY( tester.testStartElement( "name" ) );
	QVERIFY( tester.testCharacters( "link" ) );
	QVERIFY( tester.testEndElement( "name" ) );
	
	QVERIFY( tester.testStartElement( "path" ) );
	QVERIFY( tester.testCharacters( QDir::currentPath() ) );
	QVERIFY( tester.testEndElement( "path" ) );
	
	QVERIFY( tester.testStartElement( "target" ) );
	QVERIFY( tester.testCharacters( "dir/dir" ) );
	QVERIFY( tester.testEndElement( "target" ) );
	
	QVERIFY( tester.testEndElement( "linksnapshot" ) );
	QVERIFY( tester.testEndDocument() );
}

void TestCreateBackupSnapshot::testDir1Filename()
{
	QDir dir( "backup" );
	QStringList filters;
	QString filename( m_dirhash1 + ".xml" );
	
	filters << filename;
	dir.setNameFilters( filters );
	
	QCOMPARE( dir.entryList().size(), 1 );
}

void TestCreateBackupSnapshot::testDir1Contents()
{
	QFile file( "backup/" + m_dirhash1 + ".xml" );
	QVERIFY( file.open( QFile::ReadOnly ) );
	
	QXmlStreamReader reader( &file );
	XmlTester tester( &reader );
	
	QVERIFY( tester.testStartDocument() );
	QVERIFY( tester.testStartElement( "directorysnapshot" ) );
	
	QVERIFY( tester.testStartElement( "name" ) );
	QVERIFY( tester.testCharacters( "dir" ) );
	QVERIFY( tester.testEndElement( "name" ) );
	
	QVERIFY( tester.testStartElement( "path" ) );
	QVERIFY( tester.testCharacters( QDir::currentPath() ) );
	QVERIFY( tester.testEndElement( "path" ) );
	
	QVERIFY( tester.testStartElement( "permissions" ) );
	QVERIFY( tester.testCharacters( "777" ) );
	QVERIFY( tester.testEndElement( "permissions" ) );
	
	QVERIFY( tester.testStartElement( "file" ) );
	QVERIFY( tester.testCharacters( m_filehash2 ) );
	QVERIFY( tester.testEndElement( "file" ) );
	
	QVERIFY( tester.testStartElement( "directory" ) );
	QVERIFY( tester.testCharacters( m_dirhash2 ) );
	QVERIFY( tester.testEndElement( "directory" ) );
	
	QVERIFY( tester.testEndElement( "directorysnapshot" ) );
	QVERIFY( tester.testEndDocument() );
}

void TestCreateBackupSnapshot::testFile2Filename()
{
	QDir dir( "backup" );
	QStringList filters;
	QString filename( m_filehash2 + ".xml" );
	
	filters << filename;
	dir.setNameFilters( filters );
	
	QCOMPARE( dir.entryList().size(), 1 );
}

void TestCreateBackupSnapshot::testFile2Contents()
{
	QFile file( "backup/" + m_filehash2 + ".xml" );
	QVERIFY( file.open( QFile::ReadOnly ) );
	
	QXmlStreamReader reader( &file );
	XmlTester tester( &reader );
	
	QVERIFY( tester.testStartDocument() );
	QVERIFY( tester.testStartElement( "filesnapshot" ) );
	
	QVERIFY( tester.testStartElement( "name" ) );
	QVERIFY( tester.testCharacters( "file" ) );
	QVERIFY( tester.testEndElement( "name" ) );
	
	QVERIFY( tester.testStartElement( "path" ) );
	QVERIFY( tester.testCharacters( QDir::current().filePath( "dir" ) ) );
	QVERIFY( tester.testEndElement( "path" ) );
	
	QVERIFY( tester.testStartElement( "data" ) );
	QVERIFY( tester.testCharacters( m_datahash2 ) );
	QVERIFY( tester.testEndElement( "data" ) );
	
	QVERIFY( tester.testStartElement( "permissions" ) );
	QVERIFY( tester.testCharacters( "644" ) );
	QVERIFY( tester.testEndElement( "permissions" ) );
	
	QVERIFY( tester.testEndElement( "filesnapshot" ) );
	QVERIFY( tester.testEndDocument() );
}

void TestCreateBackupSnapshot::testData2Filename()
{
	QDir dir( "backup" );
	QStringList filters;
	QString filename( m_datahash2 + ".bin" );
	
	filters << filename;
	dir.setNameFilters( filters );
	
	QCOMPARE( dir.entryList().size(), 1 );
}

void TestCreateBackupSnapshot::testData2Contents()
{
	QByteArray data( 100, 'b' );
	
	QFile file( "backup/" + m_datahash2 + ".bin" );
	QVERIFY( file.open( QFile::ReadOnly ) );
	
	QCOMPARE( file.readAll(), data );
}

void TestCreateBackupSnapshot::testDir2Filename()
{
	QDir dir( "backup" );
	QStringList filters;
	QString filename( m_dirhash2 + ".xml" );
	
	filters << filename;
	dir.setNameFilters( filters );
	
	QCOMPARE( dir.entryList().size(), 1 );
}

void TestCreateBackupSnapshot::testDir2Contents()
{
	QFile file( "backup/" + m_dirhash2 + ".xml" );
	QVERIFY( file.open( QFile::ReadOnly ) );
	
	QXmlStreamReader reader( &file );
	XmlTester tester( &reader );
	
	QVERIFY( tester.testStartDocument() );
	QVERIFY( tester.testStartElement( "directorysnapshot" ) );
	
	QVERIFY( tester.testStartElement( "name" ) );
	QVERIFY( tester.testCharacters( "dir" ) );
	QVERIFY( tester.testEndElement( "name" ) );
	
	QVERIFY( tester.testStartElement( "path" ) );
	QVERIFY( tester.testCharacters( QDir::current().filePath( "dir" ) ) );
	QVERIFY( tester.testEndElement( "path" ) );
	
	QVERIFY( tester.testStartElement( "permissions" ) );
	QVERIFY( tester.testCharacters( "755" ) );
	QVERIFY( tester.testEndElement( "permissions" ) );
	
	QVERIFY( tester.testStartElement( "file" ) );
	QVERIFY( tester.testCharacters( m_filehash3 ) );
	QVERIFY( tester.testEndElement( "file" ) );
	
	QVERIFY( tester.testStartElement( "link" ) );
	QVERIFY( tester.testCharacters( m_linkhash2 ) );
	QVERIFY( tester.testEndElement( "link" ) );
	
	QVERIFY( tester.testEndElement( "directorysnapshot" ) );
	QVERIFY( tester.testEndDocument() );
}

void TestCreateBackupSnapshot::testFile3Filename()
{
	QDir dir( "backup" );
	QStringList filters;
	QString filename( m_filehash3 + ".xml" );
	
	filters << filename;
	dir.setNameFilters( filters );
	
	QCOMPARE( dir.entryList().size(), 1 );
}

void TestCreateBackupSnapshot::testFile3Contents()
{
	QFile file( "backup/" + m_filehash3 + ".xml" );
	QVERIFY( file.open( QFile::ReadOnly ) );
	
	QXmlStreamReader reader( &file );
	XmlTester tester( &reader );
	
	QVERIFY( tester.testStartDocument() );
	QVERIFY( tester.testStartElement( "filesnapshot" ) );
	
	QVERIFY( tester.testStartElement( "name" ) );
	QVERIFY( tester.testCharacters( "file" ) );
	QVERIFY( tester.testEndElement( "name" ) );
	
	QVERIFY( tester.testStartElement( "path" ) );
	QVERIFY( tester.testCharacters( QDir::current().filePath( "dir/dir" ) ) );
	QVERIFY( tester.testEndElement( "path" ) );
	
	QVERIFY( tester.testStartElement( "data" ) );
	QVERIFY( tester.testCharacters( m_datahash3 ) );
	QVERIFY( tester.testEndElement( "data" ) );
	
	QVERIFY( tester.testStartElement( "permissions" ) );
	QVERIFY( tester.testCharacters( "600" ) );
	QVERIFY( tester.testEndElement( "permissions" ) );
	
	QVERIFY( tester.testEndElement( "filesnapshot" ) );
	QVERIFY( tester.testEndDocument() );
}

void TestCreateBackupSnapshot::testData3Filename()
{
	QDir dir( "backup" );
	QStringList filters;
	QString filename( m_datahash3 + ".bin" );
	
	filters << filename;
	dir.setNameFilters( filters );
	
	QCOMPARE( dir.entryList().size(), 1 );
}

void TestCreateBackupSnapshot::testData3Contents()
{
	QByteArray data( 100, 'c' );
	
	QFile file( "backup/" + m_datahash3 + ".bin" );
	QVERIFY( file.open( QFile::ReadOnly ) );
	
	QCOMPARE( file.readAll(), data );
}

void TestCreateBackupSnapshot::testLink2Filename()
{
	QDir dir( "backup" );
	QStringList filters;
	QString filename( m_linkhash2 + ".xml" );
	
	filters << filename;
	dir.setNameFilters( filters );
	
	QCOMPARE( dir.entryList().size(), 1 );
}

void TestCreateBackupSnapshot::testLink2Contents()
{
	QFile file( "backup/" + m_linkhash2 + ".xml" );
	QVERIFY( file.open( QFile::ReadOnly ) );
	
	QXmlStreamReader reader( &file );
	XmlTester tester( &reader );
	
	QVERIFY( tester.testStartDocument() );
	QVERIFY( tester.testStartElement( "linksnapshot" ) );
	
	QVERIFY( tester.testStartElement( "name" ) );
	QVERIFY( tester.testCharacters( "link" ) );
	QVERIFY( tester.testEndElement( "name" ) );
	
	QVERIFY( tester.testStartElement( "path" ) );
	QVERIFY( tester.testCharacters( QDir::current().filePath( "dir/dir" ) ) );
	QVERIFY( tester.testEndElement( "path" ) );
	
	QVERIFY( tester.testStartElement( "target" ) );
	QVERIFY( tester.testCharacters( "../file" ) );
	QVERIFY( tester.testEndElement( "target" ) );
	
	QVERIFY( tester.testEndElement( "linksnapshot" ) );
	QVERIFY( tester.testEndDocument() );
}

void TestCreateBackupSnapshot::calculateHashes()
{
	QCryptographicHash hasher( QCryptographicHash::Sha1 );

	// m_filehash1
	
	QFileInfo fileInfo1( "file" );
	qint64 fileTimestamp1( fileInfo1.lastModified().toMSecsSinceEpoch() );
	
	hasher.addData( fileInfo1.absoluteFilePath().toUtf8() );
	hasher.addData( "666" );
	hasher.addData( QByteArray::number( fileTimestamp1 ) );
	
	m_filehash1 = hasher.result().toHex();
	
	// m_filehash2
	
	QFileInfo fileInfo2( "dir/file" );
	qint64 fileTimestamp2( fileInfo2.lastModified().toMSecsSinceEpoch() );
	
	hasher.reset();
	hasher.addData( fileInfo2.absoluteFilePath().toUtf8() );
	hasher.addData( "644" );
	hasher.addData( QByteArray::number( fileTimestamp2 ) );
	
	m_filehash2 = hasher.result().toHex();
	
	// m_filehash3
	
	QFileInfo fileInfo3( "dir/dir/file" );
	qint64 fileTimestamp3( fileInfo3.lastModified().toMSecsSinceEpoch() );
	
	hasher.reset();
	hasher.addData( fileInfo3.absoluteFilePath().toUtf8() );
	hasher.addData( "600" );
	hasher.addData( QByteArray::number( fileTimestamp3 ) );
	
	m_filehash3 = hasher.result().toHex();
	
	// m_datahash1
	
	hasher.reset();
	hasher.addData( QByteArray( 100, 'a' ) );
	
	m_datahash1 = hasher.result().toHex();
	
	// m_datahash2
	
	hasher.reset();
	hasher.addData( QByteArray( 100, 'b' ) );
	
	m_datahash2 = hasher.result().toHex();
	
	// m_datahash3
	
	hasher.reset();
	hasher.addData( QByteArray( 100, 'c' ) );
	
	m_datahash3 = hasher.result().toHex();
	
	// m_linkhash1
	
	QFileInfo linkInfo1( "link" );
	
	hasher.reset();
	hasher.addData( linkInfo1.absoluteFilePath().toUtf8() );
	hasher.addData( "dir/dir" );
	
	m_linkhash1 = hasher.result().toHex();
	
	// m_linkhash2
	
	QFileInfo linkInfo2( "dir/dir/link" );
	
	hasher.reset();
	hasher.addData( linkInfo2.absoluteFilePath().toUtf8() );
	hasher.addData( "../file" );
	
	m_linkhash2 = hasher.result().toHex();
	
	// m_dirhash2
	
	QFileInfo dirInfo2( "dir/dir" );
	
	hasher.reset();
	hasher.addData( dirInfo2.absoluteFilePath().toUtf8() );
	hasher.addData( "755" );
	hasher.addData( m_filehash3 );
	hasher.addData( m_linkhash2 );
	
	m_dirhash2 = hasher.result().toHex();

	// m_dirhash1
	
	QFileInfo dirInfo1( "dir" );
	
	hasher.reset();
	hasher.addData( dirInfo1.absoluteFilePath().toUtf8() );
	hasher.addData( "777" );
	hasher.addData( m_filehash2 );
	hasher.addData( m_dirhash2 );
	
	m_dirhash1 = hasher.result().toHex();
}

QTEST_MAIN(TestCreateBackupSnapshot)
