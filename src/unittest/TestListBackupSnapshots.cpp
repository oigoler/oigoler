/**
 *   Copyright (C) 2016 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestListBackupSnapshots.h"
#include "DirectoryGenerator.h"
#include "../listBackupSnapshots.h"
#include "../createBackupPlan.h"
#include "../createBackupSnapshot.h"


void TestListBackupSnapshots::initTestCase()
{
	new DirectoryGenerator("config", this);
	new DirectoryGenerator("backup", this);
	
	QString configPath( QDir::current().filePath( "config" ) );
	QString storagePath( QDir::current().filePath( "backup" ) );
	
	createBackupPlan( "plan", configPath, storagePath );
	
	createBackupSnapshot( "plan", configPath );
	QTest::qSleep( 100 );
	createBackupSnapshot( "plan", configPath );
	QTest::qSleep( 100 );
	createBackupSnapshot( "plan", configPath );
	
	m_snapDates = listBackupSnapshots( "plan", configPath );
}

void TestListBackupSnapshots::testCount()
{
	QCOMPARE( m_snapDates.count(), 3 );
}

void TestListBackupSnapshots::testDates()
{
	QCOMPARE( m_snapDates.count(), 3 );
	qSort( m_snapDates );
	
	QDir dir( "backup" );
	
	QStringList filters;
	filters << "plan-*.xml";
	
	dir.setNameFilters( filters );
	dir.setSorting( QDir::Name );
	
	QStringList entries( dir.entryList() );
	
	QCOMPARE( entries.size(), 3 );
	
	for ( int i = 0; i < 3; ++i )
	{
		QString name( entries.at( i ) );
		name = name.mid( 5 );
		name.chop( 4 );
		
		bool ok( false );
		qint64 timestamp( name.toLongLong( &ok ) );
		QVERIFY( ok );
		
		QDateTime ref( QDateTime::fromMSecsSinceEpoch( timestamp ) );
		
		QCOMPARE( m_snapDates.at( i ), ref );
	}
}

QTEST_MAIN(TestListBackupSnapshots)
