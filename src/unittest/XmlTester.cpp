/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "XmlTester.h"


XmlTester::XmlTester( QXmlStreamReader* reader )
	: m_reader( reader )
{
}

XmlTester::~XmlTester()
{
}

bool XmlTester::testStartDocument()
{
	QXmlStreamReader::TokenType token( readNextToken() );
	return token == QXmlStreamReader::StartDocument;
}

bool XmlTester::testStartElement( const QString& name )
{
	QXmlStreamReader::TokenType token( readNextToken() );
	
	if ( token == QXmlStreamReader::StartElement )
		return m_reader->name() == name;
	else
		return false;
}

bool XmlTester::testCharacters( const QString& text )
{
	QXmlStreamReader::TokenType token( readNextToken() );
	
	if ( token == QXmlStreamReader::Characters )
		return m_reader->text() == text;
	else
		return false;
}

bool XmlTester::testEndElement( const QString& name )
{
	QXmlStreamReader::TokenType token( readNextToken() );
	
	if ( token == QXmlStreamReader::EndElement )
		return m_reader->name() == name;
	else
		return false;
}

bool XmlTester::testEndDocument()
{
	QXmlStreamReader::TokenType token( readNextToken() );
	return token == QXmlStreamReader::EndDocument;
}

QXmlStreamReader::TokenType XmlTester::readNextToken()
{
	QXmlStreamReader::TokenType token;
	
	while ( !m_reader->atEnd() )
	{
		token = m_reader->readNext();
		
		if ( !m_reader->isWhitespace() )
			return token;
	}
	
	return QXmlStreamReader::Invalid;
}
