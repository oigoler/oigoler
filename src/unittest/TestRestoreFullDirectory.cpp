/**
 *   Copyright (C) 2016 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestRestoreFullDirectory.h"
#include "DirectoryCleaner.h"
#include "DirectoryGenerator.h"
#include "FileGenerator.h"
#include "LinkGenerator.h"
#include "LinkTester.h"
#include "../createBackupPlan.h"
#include "../createBackupSnapshot.h"
#include "../listBackupSnapshots.h"
#include "../restoreBackup.h"


void TestRestoreFullDirectory::initTestCase()
{
	// dir (777)
	
	DirectoryGenerator* testDir = new DirectoryGenerator( "dir"
	                                                    , QFile::ReadOwner 
	                                                    | QFile::WriteOwner 
	                                                    | QFile::ExeOwner 
	                                                    | QFile::ReadGroup 
	                                                    | QFile::WriteGroup
	                                                    | QFile::ExeGroup 
	                                                    | QFile::ReadOther 
	                                                    | QFile::WriteOther
	                                                    | QFile::ExeOther );
	
	// dir/file (666) (10 * 'a')
	
	new FileGenerator( "dir/file"
	                 , QByteArray( 10, 'a' )
	                 , QFile::ReadOwner 
	                 | QFile::WriteOwner 
	                 | QFile::ReadGroup 
	                 | QFile::WriteGroup
	                 | QFile::ReadOther 
	                 | QFile::WriteOther
	                 , this );
	
	// dir/link -> a
	
	new LinkGenerator( "dir/link", "a", this );
	
	// dir/dir (700)
	
	new DirectoryGenerator( "dir/dir"
	                      , QFile::ReadOwner 
	                      | QFile::WriteOwner 
	                      | QFile::ExeOwner 
	                      , this );
	
	// Backup plan
	
	new DirectoryGenerator( "config", this );
	new DirectoryGenerator( "backup", this );
	
	QString configPath( QDir::current().filePath( "config" ) );
	QString backupPath( QDir::current().filePath( "backup" ) );
	
	QStringList empty;
	QStringList dirs;
	dirs << QDir::current().filePath( "dir" );
	
	createBackupPlan( "plan", configPath, backupPath, empty, empty, dirs );
	
	// Snapshot
	
	createBackupSnapshot( "plan", configPath );
	
	// Delete directory
	
	delete testDir;
	
	// Restore directory
	
	QList<QDateTime> timestamps( listBackupSnapshots( "plan", configPath ) );
	QCOMPARE( timestamps.count(), 1 );
	
	restoreBackup( "plan", timestamps.at( 0 ), configPath );
}

void TestRestoreFullDirectory::testDirectoryExists()
{
	QVERIFY( QDir( "dir" ).exists() );
}

void TestRestoreFullDirectory::testDirectoryPermissions()
{
	QFileInfo info( "dir" );
	QFile::Permissions permissions( info.permissions() );
	
	QVERIFY( permissions & QFile::ReadOwner );
	QVERIFY( permissions & QFile::WriteOwner );
	QVERIFY( permissions & QFile::ExeOwner );
	
	QVERIFY( permissions & QFile::ReadGroup );
	QVERIFY( permissions & QFile::WriteGroup );
	QVERIFY( permissions & QFile::ExeGroup );
	
	QVERIFY( permissions & QFile::ReadOther );
	QVERIFY( permissions & QFile::WriteOther );
	QVERIFY( permissions & QFile::ExeOther );
}

void TestRestoreFullDirectory::testDirectoryContents()
{
	QDir dir( "dir" );
	QVERIFY( dir.exists() );
	
	dir.setFilter( QDir::AllEntries | QDir::System | QDir::NoDotAndDotDot );
	QCOMPARE( dir.entryList().count(), 3 );
}

void TestRestoreFullDirectory::testFileExists()
{
	QVERIFY( QDir::current().exists( "dir/file" ) );
}

void TestRestoreFullDirectory::testFilePermissions()
{
	QFileInfo info( "dir/file" );
	QFile::Permissions permissions( info.permissions() );
	
	QVERIFY( permissions & QFile::ReadOwner );
	QVERIFY( permissions & QFile::WriteOwner );
	QVERIFY( !( permissions & QFile::ExeOwner ) );
	
	QVERIFY( permissions & QFile::ReadGroup );
	QVERIFY( permissions & QFile::WriteGroup );
	QVERIFY( !( permissions & QFile::ExeGroup ) );
	
	QVERIFY( permissions & QFile::ReadOther );
	QVERIFY( permissions & QFile::WriteOther );
	QVERIFY( !( permissions & QFile::ExeOther ) );
}

void TestRestoreFullDirectory::testFileContents()
{
	QByteArray expected( 10, 'a' );

	QFile file( "dir/file" );
	QVERIFY( file.open( QFile::ReadOnly ) );
	
	QCOMPARE( file.readAll(), expected );
}

void TestRestoreFullDirectory::testLinkExists()
{
	QVERIFY( QFileInfo( "dir/link" ).isSymLink() );
}

void TestRestoreFullDirectory::testLinkTarget()
{
	LinkTester tester( "dir/link" );
	
	QVERIFY( tester.isLink() );
	QCOMPARE( tester.target(), QString( "a" ) );
}

void TestRestoreFullDirectory::testSubDirExists()
{
	QVERIFY( QDir( "dir/dir" ).exists() );
}

void TestRestoreFullDirectory::testSubDirPermissions()
{
	QFileInfo info( "dir/dir" );
	QFile::Permissions permissions( info.permissions() );
	
	QVERIFY( permissions & QFile::ReadOwner );
	QVERIFY( permissions & QFile::WriteOwner );
	QVERIFY( permissions & QFile::ExeOwner );
	
	QVERIFY( !( permissions & QFile::ReadGroup ) );
	QVERIFY( !( permissions & QFile::WriteGroup ) );
	QVERIFY( !( permissions & QFile::ExeGroup ) );
	
	QVERIFY( !( permissions & QFile::ReadOther ) );
	QVERIFY( !( permissions & QFile::WriteOther ) );
	QVERIFY( !( permissions & QFile::ExeOther ) );
}

void TestRestoreFullDirectory::testSubDirIsEmpty()
{
	QDir dir( "dir/dir" );
	QVERIFY( dir.exists() );
	
	dir.setFilter( QDir::AllEntries | QDir::System | QDir::NoDotAndDotDot );
	QVERIFY( dir.entryList().isEmpty() );
}

void TestRestoreFullDirectory::cleanupTestCase()
{
	DirectoryCleaner cleaner( "dir" );
}

QTEST_MAIN(TestRestoreFullDirectory)
