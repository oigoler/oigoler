/**
 *   Copyright (C) 2015 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestListBackupPlans.h"
#include "DirectoryGenerator.h"
#include "../listBackupPlans.h"
#include "../createBackupPlan.h"


void TestListBackupPlans::initTestCase()
{
	new DirectoryGenerator("config", this);
	
	QString configPath( QDir::current().filePath( "config" ) );
	QString storagePath( QDir::current().filePath( "backup" ) );
	
	createBackupPlan( "plan1", configPath, storagePath );
	createBackupPlan( "plan2", configPath, storagePath );
	createBackupPlan( "plan3", configPath, storagePath );
	
	m_planNames = listBackupPlans( configPath );
}

void TestListBackupPlans::testCount()
{
	QCOMPARE( m_planNames.count(), 3 );
}

void TestListBackupPlans::testNames()
{
	QSet<QString> names = m_planNames.toSet();
	
	QVERIFY( names.contains( "plan1" ) );
	QVERIFY( names.contains( "plan2" ) );
	QVERIFY( names.contains( "plan3" ) );
}

QTEST_MAIN(TestListBackupPlans)
