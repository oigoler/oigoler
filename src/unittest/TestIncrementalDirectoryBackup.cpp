/**
 *   Copyright (C) 2015 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestIncrementalDirectoryBackup.h"
#include "FileGenerator.h"
#include "DirectoryGenerator.h"
#include "XmlTester.h"
#include "../createBackupPlan.h"
#include "../createBackupSnapshot.h"


void TestIncrementalDirectoryBackup::initTestCase()
{
	// dir (777)
	
	new DirectoryGenerator( "dir"
	                      , QFile::ReadUser
	                      | QFile::WriteUser
	                      | QFile::ExeUser
	                      | QFile::ReadGroup
	                      | QFile::WriteGroup
	                      | QFile::ExeGroup
	                      | QFile::ReadOther
	                      | QFile::WriteOther
	                      | QFile::ExeOther
	                      , this);
	
	setDirHash( &m_dirhash1, "777" );
	
	// Backup plan
	
	new DirectoryGenerator( "config", this );
	new DirectoryGenerator( "backup", this );
	
	QString configPath( QDir::current().filePath( "config" ) );
	QString backupPath( QDir::current().filePath( "backup" ) );
	
	QStringList files;
	QStringList links;
	QStringList dirs;
	dirs << QDir::current().filePath( "dir" );
	
	createBackupPlan( "test", configPath, backupPath, files, links, dirs );
	
	// First Snapshot
	
	QTest::qSleep( 100 );
	createBackupSnapshot( "test", configPath );
	extractSnapshotInfo( &m_snapshot1Info );
	
	// Change directory permissions (755)
	
	QFile::setPermissions( "dir"
	                     , QFile::ReadUser
	                     | QFile::WriteUser
	                     | QFile::ExeUser
	                     | QFile::ReadGroup
	                     | QFile::ExeGroup
	                     | QFile::ReadOther
	                     | QFile::ExeOther );
	
	setDirHash( &m_dirhash2, "755" );
	
	// Second Snapshot
	
	QTest::qSleep( 100 );
	createBackupSnapshot( "test", configPath );
	extractSnapshotInfo( &m_snapshot2Info );
	
	// Create file inside directory (666) (10 * 'a')
	
	new FileGenerator( "dir/file"
	                 , QByteArray( 10, 'a' )
	                 , QFile::ReadUser 
	                 | QFile::WriteUser 
	                 | QFile::ReadGroup 
	                 | QFile::WriteGroup
	                 | QFile::ReadOther 
	                 | QFile::WriteOther
	                 , this );
	
	setFileHash( &m_filehash1, "666" );
	setDirHash( &m_dirhash3, "755", m_filehash1 );
	
	// Third Snapshot
	
	QTest::qSleep( 100 );
	createBackupSnapshot( "test", configPath );
	extractSnapshotInfo( &m_snapshot3Info );
	
	// Fourth Snapshot
	
	QTest::qSleep( 100 );
	createBackupSnapshot( "test", configPath );
	extractSnapshotInfo( &m_snapshot4Info );
	
	// Data hash
	
	QCryptographicHash hasher( QCryptographicHash::Sha1 );
	hasher.addData( QByteArray( 10, 'a' ) );
	m_datahash1 = hasher.result().toHex();
}

void TestIncrementalDirectoryBackup::testNumberOfCreatedFiles()
{
	QDir storageDir( "backup" );
	storageDir.setFilter( QDir::AllEntries | QDir::NoDotAndDotDot );
	
	QCOMPARE( storageDir.entryList().size(), 9 );
}

void TestIncrementalDirectoryBackup::testSnapshotFiles()
{
	QDir dir( "backup" );
	
	QStringList filters;
	filters << "test-*.xml";
	
	dir.setNameFilters( filters );
	dir.setSorting( QDir::Name );
	
	QFileInfoList entries( dir.entryInfoList() );
	
	QCOMPARE( entries.size(), 4 );
	
	QByteArray hashes[4];
	hashes[0] = m_dirhash1;
	hashes[1] = m_dirhash2;
	hashes[2] = m_dirhash3;
	hashes[3] = m_dirhash3;
	
	for ( int i = 0; i < 4; ++i )
	{
		QFile file( entries.at( i ).filePath() );
		QVERIFY( file.open( QFile::ReadOnly ) );

		QXmlStreamReader reader( &file );
		XmlTester tester( &reader );

		QVERIFY( tester.testStartDocument() );
		QVERIFY( tester.testStartElement( "snapshot" ) );

		QVERIFY( tester.testStartElement( "directory" ) );
		QVERIFY( tester.testCharacters( hashes[i] ) );
		QVERIFY( tester.testEndElement( "directory" ) );

		QVERIFY( tester.testEndElement( "snapshot" ) );
		QVERIFY( tester.testEndDocument() );
	}
}

void TestIncrementalDirectoryBackup::testDir1()
{
	QFile file( "backup/" + m_dirhash1 + ".xml" );
	QVERIFY( file.open( QFile::ReadOnly ) );
	
	QXmlStreamReader reader( &file );
	XmlTester tester( &reader );
	
	QVERIFY( tester.testStartDocument() );
	QVERIFY( tester.testStartElement( "directorysnapshot" ) );
	
	QVERIFY( tester.testStartElement( "name" ) );
	QVERIFY( tester.testCharacters( "dir" ) );
	QVERIFY( tester.testEndElement( "name" ) );
	
	QVERIFY( tester.testStartElement( "path" ) );
	QVERIFY( tester.testCharacters( QDir::currentPath() ) );
	QVERIFY( tester.testEndElement( "path" ) );
	
	QVERIFY( tester.testStartElement( "permissions" ) );
	QVERIFY( tester.testCharacters( "777" ) );
	QVERIFY( tester.testEndElement( "permissions" ) );
	
	QVERIFY( tester.testEndElement( "directorysnapshot" ) );
	QVERIFY( tester.testEndDocument() );
}

void TestIncrementalDirectoryBackup::testDir2()
{
	QFile file( "backup/" + m_dirhash2 + ".xml" );
	QVERIFY( file.open( QFile::ReadOnly ) );
	
	QXmlStreamReader reader( &file );
	XmlTester tester( &reader );
	
	QVERIFY( tester.testStartDocument() );
	QVERIFY( tester.testStartElement( "directorysnapshot" ) );
	
	QVERIFY( tester.testStartElement( "name" ) );
	QVERIFY( tester.testCharacters( "dir" ) );
	QVERIFY( tester.testEndElement( "name" ) );
	
	QVERIFY( tester.testStartElement( "path" ) );
	QVERIFY( tester.testCharacters( QDir::currentPath() ) );
	QVERIFY( tester.testEndElement( "path" ) );
	
	QVERIFY( tester.testStartElement( "permissions" ) );
	QVERIFY( tester.testCharacters( "755" ) );
	QVERIFY( tester.testEndElement( "permissions" ) );
	
	QVERIFY( tester.testEndElement( "directorysnapshot" ) );
	QVERIFY( tester.testEndDocument() );
}

void TestIncrementalDirectoryBackup::testDir3()
{
	QFile file( "backup/" + m_dirhash3 + ".xml" );
	QVERIFY( file.open( QFile::ReadOnly ) );
	
	QXmlStreamReader reader( &file );
	XmlTester tester( &reader );
	
	QVERIFY( tester.testStartDocument() );
	QVERIFY( tester.testStartElement( "directorysnapshot" ) );
	
	QVERIFY( tester.testStartElement( "name" ) );
	QVERIFY( tester.testCharacters( "dir" ) );
	QVERIFY( tester.testEndElement( "name" ) );
	
	QVERIFY( tester.testStartElement( "path" ) );
	QVERIFY( tester.testCharacters( QDir::currentPath() ) );
	QVERIFY( tester.testEndElement( "path" ) );
	
	QVERIFY( tester.testStartElement( "permissions" ) );
	QVERIFY( tester.testCharacters( "755" ) );
	QVERIFY( tester.testEndElement( "permissions" ) );
	
	QVERIFY( tester.testStartElement( "file" ) );
	QVERIFY( tester.testCharacters( m_filehash1 ) );
	QVERIFY( tester.testEndElement( "file" ) );
	
	QVERIFY( tester.testEndElement( "directorysnapshot" ) );
	QVERIFY( tester.testEndDocument() );
}

void TestIncrementalDirectoryBackup::testFile1()
{
	QFile file( "backup/" + m_filehash1 + ".xml" );
	QVERIFY( file.open( QFile::ReadOnly ) );
	
	QXmlStreamReader reader( &file );
	XmlTester tester( &reader );
	
	QVERIFY( tester.testStartDocument() );
	QVERIFY( tester.testStartElement( "filesnapshot" ) );
	
	QVERIFY( tester.testStartElement( "name" ) );
	QVERIFY( tester.testCharacters( "file" ) );
	QVERIFY( tester.testEndElement( "name" ) );
	
	QVERIFY( tester.testStartElement( "path" ) );
	QVERIFY( tester.testCharacters( QDir::current().filePath( "dir" ) ) );
	QVERIFY( tester.testEndElement( "path" ) );
	
	QVERIFY( tester.testStartElement( "data" ) );
	QVERIFY( tester.testCharacters( m_datahash1 ) );
	QVERIFY( tester.testEndElement( "data" ) );
	
	QVERIFY( tester.testStartElement( "permissions" ) );
	QVERIFY( tester.testCharacters( "666" ) );
	QVERIFY( tester.testEndElement( "permissions" ) );
	
	QVERIFY( tester.testEndElement( "filesnapshot" ) );
	QVERIFY( tester.testEndDocument() );
}

void TestIncrementalDirectoryBackup::testData1()
{
	QFile file( "backup/" + m_datahash1 + ".bin" );
	QVERIFY( file.open( QFile::ReadOnly ) );
	
	QByteArray data( 10, 'a' );
	
	QCOMPARE( file.readAll(), data );
}

void TestIncrementalDirectoryBackup::testTimestamps1()
{
	foreach ( const QString& filename, m_snapshot1Info.keys() )
	{
		QDateTime timestamp( m_snapshot1Info.value( filename ) );
		
		QCOMPARE( m_snapshot2Info.value( filename ), timestamp );
		QCOMPARE( m_snapshot3Info.value( filename ), timestamp );
		QCOMPARE( m_snapshot4Info.value( filename ), timestamp );
	}
}

void TestIncrementalDirectoryBackup::testTimestamps2()
{
	foreach ( const QString& filename, m_snapshot2Info.keys() )
	{
		QDateTime timestamp( m_snapshot2Info.value( filename ) );
		
		QCOMPARE( m_snapshot3Info.value( filename ), timestamp );
		QCOMPARE( m_snapshot4Info.value( filename ), timestamp );
	}
}

void TestIncrementalDirectoryBackup::testTimestamps3()
{
	foreach ( const QString& filename, m_snapshot3Info.keys() )
	{
		QDateTime timestamp( m_snapshot3Info.value( filename ) );
		
		QCOMPARE( m_snapshot4Info.value( filename ), timestamp );
	}
}

void TestIncrementalDirectoryBackup::extractSnapshotInfo(
	QHash< QString, QDateTime >* info )
{
	QDir storageDir( "backup" );
	storageDir.setFilter( QDir::Files );
	
	foreach ( QFileInfo fileInfo, storageDir.entryInfoList() )
	{
		info->insert( fileInfo.fileName(), fileInfo.lastModified() );
	}
}

void TestIncrementalDirectoryBackup::setFileHash( QByteArray* dest
                                                , QByteArray permissions )
{
	QTest::qSleep( 1000 );
	QFileInfo fileInfo( "dir/file" );
	qint64 timestamp( fileInfo.lastModified().toMSecsSinceEpoch() );
	
	QCryptographicHash hasher( QCryptographicHash::Sha1 );
	hasher.addData( fileInfo.absoluteFilePath().toUtf8() );
	hasher.addData( permissions );
	hasher.addData( QByteArray::number( timestamp ) );
	
	*dest = hasher.result().toHex();
}

void TestIncrementalDirectoryBackup::setDirHash( QByteArray* dest
                                               , QByteArray permissions
                                               , const QByteArray& filehash )
{
	QFileInfo fileInfo( "dir" );
	
	QCryptographicHash hasher( QCryptographicHash::Sha1 );
	hasher.addData( fileInfo.absoluteFilePath().toUtf8() );
	hasher.addData( permissions );
	hasher.addData( filehash );
	
	*dest = hasher.result().toHex();
}

QTEST_MAIN(TestIncrementalDirectoryBackup)
