/**
 *   Copyright (C) 2016 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestRestoreEmptyDirectory.h"
#include "DirectoryCleaner.h"
#include "DirectoryGenerator.h"
#include "../createBackupPlan.h"
#include "../createBackupSnapshot.h"
#include "../listBackupSnapshots.h"
#include "../restoreBackup.h"


void TestRestoreEmptyDirectory::initTestCase()
{
	// dir (777)
	
	DirectoryGenerator* testDir = new DirectoryGenerator( "dir"
	                                                    , QFile::ReadOwner 
	                                                    | QFile::WriteOwner 
	                                                    | QFile::ExeOwner 
	                                                    | QFile::ReadGroup 
	                                                    | QFile::WriteGroup
	                                                    | QFile::ExeGroup 
	                                                    | QFile::ReadOther 
	                                                    | QFile::WriteOther
	                                                    | QFile::ExeOther );
	
	// Backup plan
	
	new DirectoryGenerator( "config", this );
	new DirectoryGenerator( "backup", this );
	
	QString configPath( QDir::current().filePath( "config" ) );
	QString backupPath( QDir::current().filePath( "backup" ) );
	
	QStringList empty;
	QStringList dirs;
	dirs << QDir::current().filePath( "dir" );
	
	createBackupPlan( "plan", configPath, backupPath, empty, empty, dirs );
	
	// Snapshot
	
	createBackupSnapshot( "plan", configPath );
	
	// Delete directory
	
	delete testDir;
	
	// Restore directory
	
	QList<QDateTime> timestamps( listBackupSnapshots( "plan", configPath ) );
	QCOMPARE( timestamps.count(), 1 );
	
	restoreBackup( "plan", timestamps.at( 0 ), configPath );
}

void TestRestoreEmptyDirectory::testDirectoryExists()
{
	QVERIFY( QDir( "dir" ).exists() );
}

void TestRestoreEmptyDirectory::testDirectoryPermissions()
{
	QFileInfo info( "dir" );
	QFile::Permissions permissions( info.permissions() );
	
	QVERIFY( permissions & QFile::ReadOwner );
	QVERIFY( permissions & QFile::WriteOwner );
	QVERIFY( permissions & QFile::ExeOwner );
	
	QVERIFY( permissions & QFile::ReadGroup );
	QVERIFY( permissions & QFile::WriteGroup );
	QVERIFY( permissions & QFile::ExeGroup );
	
	QVERIFY( permissions & QFile::ReadOther );
	QVERIFY( permissions & QFile::WriteOther );
	QVERIFY( permissions & QFile::ExeOther );
}

void TestRestoreEmptyDirectory::testDirectoryIsEmpty()
{
	QDir dir( "dir" );
	QVERIFY( dir.exists() );
	
	dir.setFilter( QDir::AllEntries | QDir::System | QDir::NoDotAndDotDot );
	QVERIFY( dir.entryList().isEmpty() );
}

void TestRestoreEmptyDirectory::cleanupTestCase()
{
	DirectoryCleaner cleaner( "dir" );
}

QTEST_MAIN(TestRestoreEmptyDirectory)
