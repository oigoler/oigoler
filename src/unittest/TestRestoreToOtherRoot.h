/**
 *   Copyright (C) 2016 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef TESTRESTORETOOTHERROOT_H
#define TESTRESTORETOOTHERROOT_H

#include <QtTest/QtTest>

class TestRestoreToOtherRoot : public QObject
{
	Q_OBJECT

private slots:
	void initTestCase();
	
	void testFileExists();
	void testFilePermissions();
	void testFileContents();
	
	void testLinkExists();
	void testLinkTarget();
	
	void testDirectoryExists();
	void testDirectoryPermissions();
	void testDirectoryContents();

	void testSubFileExists();
	void testSubFilePermissions();
	void testSubFileContents();
	
	void testSubLinkExists();
	void testSubLinkTarget();
	
	void testSubDirExists();
	void testSubDirPermissions();
	void testSubDirIsEmpty();
	
	//void cleanupTestCase();
	
};

#endif // TESTRESTORETOOTHERROOT_H
