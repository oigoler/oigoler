/**
 *   Copyright (C) 2015 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestIncrementalLinkBackup.h"
#include "DirectoryGenerator.h"
#include "LinkGenerator.h"
#include "XmlTester.h"
#include "../createBackupPlan.h"
#include "../createBackupSnapshot.h"


void TestIncrementalLinkBackup::initTestCase()
{
	// link -> a
	
	LinkGenerator* linkGen = new LinkGenerator( "link", "a", this );
	
	setLinkHash( &m_linkhash1, "a" );
	
	// Backup plan
	
	new DirectoryGenerator( "config", this );
	new DirectoryGenerator( "backup", this );
	
	QString configPath( QDir::current().filePath( "config" ) );
	QString backupPath( QDir::current().filePath( "backup" ) );
	
	QStringList files;
	QStringList links;
	QStringList dirs;
	links << QDir::current().filePath( "link" );
	
	createBackupPlan( "test", configPath, backupPath, files, links, dirs );
	
	// First Snapshot
	
	QTest::qSleep( 100 );
	createBackupSnapshot( "test", configPath );
	extractSnapshotInfo( &m_snapshot1Info );
	
	// link -> b
	
	delete linkGen;
	linkGen = new LinkGenerator( "link", "b", this );
	
	setLinkHash( &m_linkhash2, "b" );
	
	// Second Snapshot
	
	QTest::qSleep( 100 );
	createBackupSnapshot( "test", configPath );
	extractSnapshotInfo( &m_snapshot2Info );
	
	// Third Snapshot
	
	QTest::qSleep( 100 );
	createBackupSnapshot( "test", configPath );
	extractSnapshotInfo( &m_snapshot3Info );
}

void TestIncrementalLinkBackup::testNumberOfCreatedFiles()
{
	QDir storageDir( "backup" );
	storageDir.setFilter( QDir::AllEntries | QDir::NoDotAndDotDot );
	
	QCOMPARE( storageDir.entryList().size(), 5 );
}

void TestIncrementalLinkBackup::testSnapshotFiles()
{
	QDir dir( "backup" );
	
	QStringList filters;
	filters << "test-*.xml";
	
	dir.setNameFilters( filters );
	dir.setSorting( QDir::Name );
	
	QFileInfoList entries( dir.entryInfoList() );
	
	QCOMPARE( entries.size(), 3 );
	
	QByteArray hashes[3];
	hashes[0] = m_linkhash1;
	hashes[1] = m_linkhash2;
	hashes[2] = m_linkhash2;
	
	for ( int i = 0; i < 3; ++i )
	{
		QFile file( entries.at( i ).filePath() );
		QVERIFY( file.open( QFile::ReadOnly ) );

		QXmlStreamReader reader( &file );
		XmlTester tester( &reader );

		QVERIFY( tester.testStartDocument() );
		QVERIFY( tester.testStartElement( "snapshot" ) );

		QVERIFY( tester.testStartElement( "link" ) );
		QVERIFY( tester.testCharacters( hashes[i] ) );
		QVERIFY( tester.testEndElement( "link" ) );

		QVERIFY( tester.testEndElement( "snapshot" ) );
		QVERIFY( tester.testEndDocument() );
	}
}

void TestIncrementalLinkBackup::testLink1()
{
	QFile file( "backup/" + m_linkhash1 + ".xml" );
	QVERIFY( file.open( QFile::ReadOnly ) );
	
	QXmlStreamReader reader( &file );
	XmlTester tester( &reader );
	
	QVERIFY( tester.testStartDocument() );
	QVERIFY( tester.testStartElement( "linksnapshot" ) );
	
	QVERIFY( tester.testStartElement( "name" ) );
	QVERIFY( tester.testCharacters( "link" ) );
	QVERIFY( tester.testEndElement( "name" ) );
	
	QVERIFY( tester.testStartElement( "path" ) );
	QVERIFY( tester.testCharacters( QDir::currentPath() ) );
	QVERIFY( tester.testEndElement( "path" ) );
	
	QVERIFY( tester.testStartElement( "target" ) );
	QVERIFY( tester.testCharacters( "a" ) );
	QVERIFY( tester.testEndElement( "target" ) );
	
	QVERIFY( tester.testEndElement( "linksnapshot" ) );
	QVERIFY( tester.testEndDocument() );
}

void TestIncrementalLinkBackup::testLink2()
{
	QFile file( "backup/" + m_linkhash2 + ".xml" );
	QVERIFY( file.open( QFile::ReadOnly ) );
	
	QXmlStreamReader reader( &file );
	XmlTester tester( &reader );
	
	QVERIFY( tester.testStartDocument() );
	QVERIFY( tester.testStartElement( "linksnapshot" ) );
	
	QVERIFY( tester.testStartElement( "name" ) );
	QVERIFY( tester.testCharacters( "link" ) );
	QVERIFY( tester.testEndElement( "name" ) );
	
	QVERIFY( tester.testStartElement( "path" ) );
	QVERIFY( tester.testCharacters( QDir::currentPath() ) );
	QVERIFY( tester.testEndElement( "path" ) );
	
	QVERIFY( tester.testStartElement( "target" ) );
	QVERIFY( tester.testCharacters( "b" ) );
	QVERIFY( tester.testEndElement( "target" ) );
	
	QVERIFY( tester.testEndElement( "linksnapshot" ) );
	QVERIFY( tester.testEndDocument() );
}

void TestIncrementalLinkBackup::testTimestamps1()
{
	foreach ( const QString& filename, m_snapshot1Info.keys() )
	{
		QDateTime timestamp( m_snapshot1Info.value( filename ) );
		
		QCOMPARE( m_snapshot2Info.value( filename ), timestamp );
		QCOMPARE( m_snapshot3Info.value( filename ), timestamp );
	}
}

void TestIncrementalLinkBackup::testTimestamps2()
{
	foreach ( const QString& filename, m_snapshot2Info.keys() )
	{
		QDateTime timestamp( m_snapshot2Info.value( filename ) );
		
		QCOMPARE( m_snapshot3Info.value( filename ), timestamp );
	}
}

void TestIncrementalLinkBackup::extractSnapshotInfo(
	QHash< QString, QDateTime >* info )
{
	QDir storageDir( "backup" );
	storageDir.setFilter( QDir::Files );
	
	foreach ( QFileInfo fileInfo, storageDir.entryInfoList() )
	{
		info->insert( fileInfo.fileName(), fileInfo.lastModified() );
	}
}

void TestIncrementalLinkBackup::setLinkHash( QByteArray* dest
                                           , QByteArray target )
{
	QFileInfo fileInfo( "link" );
	
	QCryptographicHash hasher( QCryptographicHash::Sha1 );
	hasher.addData( fileInfo.absoluteFilePath().toUtf8() );
	hasher.addData( target );
	
	*dest = hasher.result().toHex();
}

QTEST_MAIN(TestIncrementalLinkBackup)
