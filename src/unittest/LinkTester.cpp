/**
 *   Copyright (C) 2013, 2016 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "LinkTester.h"

#include <unistd.h>


LinkTester::LinkTester( const QString& link )
	: m_info( link )
{
}

LinkTester::~LinkTester()
{
}

bool LinkTester::isLink() const
{
	return m_info.isSymLink();
}

QString LinkTester::target() const
{
	QByteArray link( m_info.absoluteFilePath().toUtf8() );
	size_t bufsize( 1024 );
	ssize_t retsize( 0 );
	char* buf( 0 );
	
	while ( true )
	{
		buf = (char*) realloc( buf, bufsize );
		retsize = readlink( link.data(), buf, bufsize );
		
		if ( retsize < 0 )
		{
			free( buf );
			return QString();
		}
		
		if ( retsize < bufsize )
		{
			buf[retsize] = '\0';
			break;
		}
		
		bufsize *= 2;
	}
	
	QString target( buf );
	free( buf );
	
	return target;
}
