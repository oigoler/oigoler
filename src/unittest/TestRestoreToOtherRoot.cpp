/**
 *   Copyright (C) 2016 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestRestoreToOtherRoot.h"
#include "DirectoryGenerator.h"
#include "FileGenerator.h"
#include "LinkGenerator.h"
#include "LinkTester.h"
#include "../createBackupPlan.h"
#include "../createBackupSnapshot.h"
#include "../listBackupSnapshots.h"
#include "../restoreBackup.h"


void TestRestoreToOtherRoot::initTestCase()
{
	// file (666) (10 * 'a')
	
	new FileGenerator( "file"
	                 , QByteArray( 10, 'a' )
	                 , QFile::ReadOwner 
	                 | QFile::WriteOwner 
	                 | QFile::ReadGroup 
	                 | QFile::WriteGroup
	                 | QFile::ReadOther 
	                 | QFile::WriteOther
	                 , this );
	
	// link -> a
	
	new LinkGenerator( "link", "a", this );
	
	// dir (777)
	
	new DirectoryGenerator( "dir"
	                      , QFile::ReadOwner 
	                      | QFile::WriteOwner 
	                      | QFile::ExeOwner 
	                      | QFile::ReadGroup 
	                      | QFile::WriteGroup
	                      | QFile::ExeGroup 
	                      | QFile::ReadOther 
	                      | QFile::WriteOther
	                      | QFile::ExeOther
	                      , this );
	
	// dir/file (644) (10 * 'b')
	
	new FileGenerator( "dir/file"
	                 , QByteArray( 10, 'b' )
	                 , QFile::ReadOwner 
	                 | QFile::WriteOwner 
	                 | QFile::ReadGroup 
	                 | QFile::ReadOther 
	                 , this );
	
	// dir/link -> b
	
	new LinkGenerator( "dir/link", "b", this );
	
	// dir/dir (755)
	
	new DirectoryGenerator( "dir/dir"
	                      , QFile::ReadOwner 
	                      | QFile::WriteOwner 
	                      | QFile::ExeOwner 
	                      | QFile::ReadGroup 
	                      | QFile::ExeGroup 
	                      | QFile::ReadOther 
	                      | QFile::ExeOther
	                      , this );
	
	// Backup plan
	
	new DirectoryGenerator( "config", this );
	new DirectoryGenerator( "backup", this );
	new DirectoryGenerator( "restore", this );
	
	QString configPath( QDir::current().filePath( "config" ) );
	QString backupPath( QDir::current().filePath( "backup" ) );
	QString restorePath( QDir::current().filePath( "restore" ) );
	
	QStringList files;
	QStringList links;
	QStringList dirs;
	
	files << QDir::current().filePath( "file" );
	links << QDir::current().filePath( "link" );
	dirs << QDir::current().filePath( "dir" );
	
	createBackupPlan( "plan", configPath, backupPath, files, links, dirs );
	
	// Snapshot
	
	createBackupSnapshot( "plan", configPath );
	
	// Restore on other path
	
	QList<QDateTime> timestamps( listBackupSnapshots( "plan", configPath ) );
	QCOMPARE( timestamps.count(), 1 );
	
	restoreBackup( "plan", timestamps.at( 0 ), configPath, restorePath );
}

void TestRestoreToOtherRoot::testFileExists()
{
	QVERIFY( QDir::current().exists( "restore/file" ) );
}

void TestRestoreToOtherRoot::testFilePermissions()
{
	QFileInfo info( "restore/file" );
	QFile::Permissions permissions( info.permissions() );
	
	QVERIFY( permissions & QFile::ReadOwner );
	QVERIFY( permissions & QFile::WriteOwner );
	QVERIFY( !( permissions & QFile::ExeOwner ) );
	
	QVERIFY( permissions & QFile::ReadGroup );
	QVERIFY( permissions & QFile::WriteGroup );
	QVERIFY( !( permissions & QFile::ExeGroup ) );
	
	QVERIFY( permissions & QFile::ReadOther );
	QVERIFY( permissions & QFile::WriteOther );
	QVERIFY( !( permissions & QFile::ExeOther ) );
}

void TestRestoreToOtherRoot::testFileContents()
{
	QByteArray expected( 10, 'a' );

	QFile file( "restore/file" );
	QVERIFY( file.open( QFile::ReadOnly ) );
	
	QCOMPARE( file.readAll(), expected );
}

void TestRestoreToOtherRoot::testLinkExists()
{
	QVERIFY( QFileInfo( "restore/link" ).isSymLink() );
}

void TestRestoreToOtherRoot::testLinkTarget()
{
	LinkTester tester( "restore/link" );
	
	QVERIFY( tester.isLink() );
	QCOMPARE( tester.target(), QString( "a" ) );
}

void TestRestoreToOtherRoot::testDirectoryExists()
{
	QVERIFY( QDir( "restore/dir" ).exists() );
}

void TestRestoreToOtherRoot::testDirectoryPermissions()
{
	QFileInfo info( "restore/dir" );
	QFile::Permissions permissions( info.permissions() );
	
	QVERIFY( permissions & QFile::ReadOwner );
	QVERIFY( permissions & QFile::WriteOwner );
	QVERIFY( permissions & QFile::ExeOwner );
	
	QVERIFY( permissions & QFile::ReadGroup );
	QVERIFY( permissions & QFile::WriteGroup );
	QVERIFY( permissions & QFile::ExeGroup );
	
	QVERIFY( permissions & QFile::ReadOther );
	QVERIFY( permissions & QFile::WriteOther );
	QVERIFY( permissions & QFile::ExeOther );
}

void TestRestoreToOtherRoot::testDirectoryContents()
{
	QDir dir( "restore/dir" );
	QVERIFY( dir.exists() );
	
	dir.setFilter( QDir::AllEntries | QDir::System | QDir::NoDotAndDotDot );
	QCOMPARE( dir.entryList().count(), 3 );
}

void TestRestoreToOtherRoot::testSubFileExists()
{
	QVERIFY( QDir::current().exists( "restore/dir/file" ) );
}

void TestRestoreToOtherRoot::testSubFilePermissions()
{
	QFileInfo info( "restore/dir/file" );
	QFile::Permissions permissions( info.permissions() );
	
	QVERIFY( permissions & QFile::ReadOwner );
	QVERIFY( permissions & QFile::WriteOwner );
	QVERIFY( !( permissions & QFile::ExeOwner ) );
	
	QVERIFY( permissions & QFile::ReadGroup );
	QVERIFY( !( permissions & QFile::WriteGroup ) );
	QVERIFY( !( permissions & QFile::ExeGroup ) );
	
	QVERIFY( permissions & QFile::ReadOther );
	QVERIFY( !( permissions & QFile::WriteOther ) );
	QVERIFY( !( permissions & QFile::ExeOther ) );
}

void TestRestoreToOtherRoot::testSubFileContents()
{
	QByteArray expected( 10, 'b' );

	QFile file( "restore/dir/file" );
	QVERIFY( file.open( QFile::ReadOnly ) );
	
	QCOMPARE( file.readAll(), expected );
}

void TestRestoreToOtherRoot::testSubLinkExists()
{
	QVERIFY( QFileInfo( "restore/dir/link" ).isSymLink() );
}

void TestRestoreToOtherRoot::testSubLinkTarget()
{
	LinkTester tester( "restore/dir/link" );
	
	QVERIFY( tester.isLink() );
	QCOMPARE( tester.target(), QString( "b" ) );
}

void TestRestoreToOtherRoot::testSubDirExists()
{
	QVERIFY( QDir( "restore/dir/dir" ).exists() );
}

void TestRestoreToOtherRoot::testSubDirPermissions()
{
	QFileInfo info( "restore/dir/dir" );
	QFile::Permissions permissions( info.permissions() );
	
	QVERIFY( permissions & QFile::ReadOwner );
	QVERIFY( permissions & QFile::WriteOwner );
	QVERIFY( permissions & QFile::ExeOwner );
	
	QVERIFY( permissions & QFile::ReadGroup );
	QVERIFY( !( permissions & QFile::WriteGroup ) );
	QVERIFY( permissions & QFile::ExeGroup );
	
	QVERIFY( permissions & QFile::ReadOther );
	QVERIFY( !( permissions & QFile::WriteOther ) );
	QVERIFY( permissions & QFile::ExeOther );
}

void TestRestoreToOtherRoot::testSubDirIsEmpty()
{
	QDir dir( "restore/dir/dir" );
	QVERIFY( dir.exists() );
	
	dir.setFilter( QDir::AllEntries | QDir::System | QDir::NoDotAndDotDot );
	QVERIFY( dir.entryList().isEmpty() );
}

QTEST_MAIN(TestRestoreToOtherRoot)
