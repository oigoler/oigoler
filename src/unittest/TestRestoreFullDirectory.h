/**
 *   Copyright (C) 2016 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef TESTRESTOREFULLDIRECTORY_H
#define TESTRESTOREFULLDIRECTORY_H

#include <QtTest/QtTest>

class TestRestoreFullDirectory : public QObject
{
	Q_OBJECT

private slots:
	void initTestCase();
	
	void testDirectoryExists();
	void testDirectoryPermissions();
	void testDirectoryContents();

	void testFileExists();
	void testFilePermissions();
	void testFileContents();
	
	void testLinkExists();
	void testLinkTarget();
	
	void testSubDirExists();
	void testSubDirPermissions();
	void testSubDirIsEmpty();
	
	void cleanupTestCase();
	
};

#endif // TESTRESTOREFULLDIRECTORY_H
