/**
 *   Copyright (C) 2015 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef TESTINCREMENTALLINKBACKUP_H
#define TESTINCREMENTALLINKBACKUP_H

#include <QtTest/QtTest>

class TestIncrementalLinkBackup : public QObject
{
	Q_OBJECT

private slots:
	void initTestCase();
	
	void testNumberOfCreatedFiles();
	void testSnapshotFiles();
	void testLink1();
	void testLink2();
	void testTimestamps1();
	void testTimestamps2();
	
	//void cleanupTestCase();
	
private:
	void extractSnapshotInfo( QHash< QString, QDateTime >* info );
	void setLinkHash( QByteArray* dest, QByteArray target );
	
	QByteArray m_linkhash1;
	QByteArray m_linkhash2;
	
	QHash< QString, QDateTime > m_snapshot1Info;
	QHash< QString, QDateTime > m_snapshot2Info;
	QHash< QString, QDateTime > m_snapshot3Info;
	
};

#endif // TESTINCREMENTALLINKBACKUP_H
