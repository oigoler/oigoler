/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef XMLTESTER_H
#define XMLTESTER_H

#include <QXmlStreamReader>


class XmlTester
{

public:
	XmlTester( QXmlStreamReader* reader );
	virtual ~XmlTester();
	
	bool testStartDocument();
	bool testStartElement( const QString& name );
	bool testCharacters( const QString& text );
	bool testEndElement( const QString& name );
	bool testEndDocument();
	
private:
	QXmlStreamReader::TokenType readNextToken();
	
private:
	QXmlStreamReader* m_reader;
	
};

#endif // XMLTESTER_H
