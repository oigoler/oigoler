/**
 *   Copyright (C) 2015 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef TESTCREATEBACKUPSNAPSHOT_H
#define TESTCREATEBACKUPSNAPSHOT_H

#include <QtTest/QtTest>

class TestCreateBackupSnapshot : public QObject
{
	Q_OBJECT

private slots:
	void initTestCase();
	
	void testNumberOfCreatedFiles();
	void testSnapshotFilename();
	void testSnapshotContents();
	void testFile1Filename();
	void testFile1Contents();
	void testData1Filename();
	void testData1Contents();
	void testLink1Filename();
	void testLink1Contents();
	void testDir1Filename();
	void testDir1Contents();
	void testFile2Filename();
	void testFile2Contents();
	void testData2Filename();
	void testData2Contents();
	void testDir2Filename();
	void testDir2Contents();
	void testFile3Filename();
	void testFile3Contents();
	void testData3Filename();
	void testData3Contents();
	void testLink2Filename();
	void testLink2Contents();
	
	//void cleanupTestCase();
	
private:
	void calculateHashes();
	
	qint64 m_startTime;
	qint64 m_endTime;
	
	QByteArray m_filehash1;
	QByteArray m_filehash2;
	QByteArray m_filehash3;
	
	QByteArray m_datahash1;
	QByteArray m_datahash2;
	QByteArray m_datahash3;
	
	QByteArray m_linkhash1;
	QByteArray m_linkhash2;
	
	QByteArray m_dirhash1;
	QByteArray m_dirhash2;
	
};

#endif // TESTCREATEBACKUPSNAPSHOT_H
