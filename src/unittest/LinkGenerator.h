/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef LINKGENERATOR_H
#define LINKGENERATOR_H

#include <QObject>
#include <QString>


class LinkGenerator : public QObject
{
	Q_OBJECT

public:
	LinkGenerator( const QString& link, const QString& target, QObject* parent = 0 );
	virtual ~LinkGenerator();
	
private:
	QString m_link;
	
};

#endif // LINKGENERATOR_H
