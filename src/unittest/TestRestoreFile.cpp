/**
 *   Copyright (C) 2016 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestRestoreFile.h"
#include "FileGenerator.h"
#include "FileCleaner.h"
#include "DirectoryGenerator.h"
#include "../createBackupPlan.h"
#include "../createBackupSnapshot.h"
#include "../listBackupSnapshots.h"
#include "../restoreBackup.h"


void TestRestoreFile::initTestCase()
{
	// file (666) (10 * 'a')
	
	FileGenerator* testFile = new FileGenerator( "file"
	                                           , QByteArray( 10, 'a' )
	                                           , QFile::ReadOwner 
	                                           | QFile::WriteOwner 
	                                           | QFile::ReadGroup 
	                                           | QFile::WriteGroup
	                                           | QFile::ReadOther 
	                                           | QFile::WriteOther );
	
	// Backup plan
	
	new DirectoryGenerator( "config", this );
	new DirectoryGenerator( "backup", this );
	
	QString configPath( QDir::current().filePath( "config" ) );
	QString backupPath( QDir::current().filePath( "backup" ) );
	
	QStringList files;
	files << QDir::current().filePath( "file" );
	
	createBackupPlan( "plan", configPath, backupPath, files );
	
	// Snapshot
	
	createBackupSnapshot( "plan", configPath );
	
	// Delete file
	
	delete testFile;
	
	// Restore file
	
	QList<QDateTime> timestamps( listBackupSnapshots( "plan", configPath ) );
	QCOMPARE( timestamps.count(), 1 );
	
	restoreBackup( "plan", timestamps.at( 0 ), configPath );
}

void TestRestoreFile::testFileExists()
{
	QVERIFY( QDir::current().exists( "file" ) );
}

void TestRestoreFile::testFilePermissions()
{
	QFileInfo info( "file" );
	QFile::Permissions permissions( info.permissions() );
	
	QVERIFY( permissions & QFile::ReadOwner );
	QVERIFY( permissions & QFile::WriteOwner );
	QVERIFY( !( permissions & QFile::ExeOwner ) );
	
	QVERIFY( permissions & QFile::ReadGroup );
	QVERIFY( permissions & QFile::WriteGroup );
	QVERIFY( !( permissions & QFile::ExeGroup ) );
	
	QVERIFY( permissions & QFile::ReadOther );
	QVERIFY( permissions & QFile::WriteOther );
	QVERIFY( !( permissions & QFile::ExeOther ) );
}

void TestRestoreFile::testFileContents()
{
	QByteArray expected( 10, 'a' );

	QFile file( "file" );
	QVERIFY( file.open( QFile::ReadOnly ) );
	
	QCOMPARE( file.readAll(), expected );
}

void TestRestoreFile::cleanupTestCase()
{
	FileCleaner cleaner( "file" );
}

QTEST_MAIN(TestRestoreFile)
