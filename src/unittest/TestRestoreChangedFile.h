/**
 *   Copyright (C) 2016 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef TESTRESTORECHANGEDFILE_H
#define TESTRESTORECHANGEDFILE_H

#include <QtTest/QtTest>

class TestRestoreChangedFile : public QObject
{
	Q_OBJECT

private slots:
	void initTestCase();
	
	void testFileExists();
	void testFilePermissions();
	void testFileContents();
	
	//void cleanupTestCase();
	
};

#endif // TESTRESTORECHANGEDFILE_H
