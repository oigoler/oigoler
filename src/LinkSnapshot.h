/**
 *   Copyright (C) 2016 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef LINKSNAPSHOT_H
#define LINKSNAPSHOT_H

#include <QString>

class BackupPlan;

class LinkSnapshot
{
	
public:
	LinkSnapshot();
	LinkSnapshot( const LinkSnapshot& other );
	virtual ~LinkSnapshot();
	
	LinkSnapshot& operator=( const LinkSnapshot& other );
	
	const QString& name() const;
	void setName( const QString& name );
	
	const QString& path() const;
	void setPath( const QString& path );
	
	const QString& target() const;
	void setTarget( const QString& target );
	
	static LinkSnapshot fromXml( const BackupPlan& plan, const QString& hash );
	
private:
	QString m_name;
	QString m_path;
	QString m_target;
	
};

#endif // LINKSNAPSHOT_H
