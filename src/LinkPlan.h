/**
 *   Copyright (C) 2015, 2016 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef LINKPLAN_H
#define LINKPLAN_H

#include <QString>

class QXmlStreamReader;
class QXmlStreamWriter;

class LinkPlan
{
	
public:
	LinkPlan();
	LinkPlan( const QString& linkPath );
	LinkPlan( const LinkPlan& other );
	virtual ~LinkPlan();
	
	LinkPlan& operator=( const LinkPlan& other );
	
	const QString& name() const;
	void setName( const QString& name );
	
	const QString& path() const;
	void setPath( const QString& path );
	
	static LinkPlan fromXml( QXmlStreamReader* reader );
	void toXml( QXmlStreamWriter* writer ) const;
	
private:
	QString m_name;
	QString m_path;
	
};

#endif // LINKPLAN_H
