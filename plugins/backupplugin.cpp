/**
 *   Copyright (C) 2012 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "backupplugin.h"
#include <QFile>
#include <KPluginFactory>


K_PLUGIN_FACTORY(BackupPluginFactory, registerPlugin<BackupPlugin>();)
K_EXPORT_PLUGIN(BackupPluginFactory("oigoler_backup_plugin"))


BackupPlugin::BackupPlugin(QObject *parent, const QVariantList &args)
	: OigolerPlugin(parent)
{
	Q_UNUSED(args);
}

BackupPlugin::~BackupPlugin()
{
}

void BackupPlugin::backup()
{
	QFile::copy( this->origin(), this->destination() );
}

void BackupPlugin::restore()
{
	QFile::remove( this->destination() );
	QFile::copy( this->origin(), this->destination() );
}

#include "backupplugin.moc"
