/**
 *   Copyright (C) 2012 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef BACKUPPLUGIN_H
#define BACKUPPLUGIN_H

#include <oigolerplugin.h>
#include <QVariant>


class BackupPlugin : public OigolerPlugin
{
    Q_OBJECT
    
	public:
		explicit BackupPlugin(QObject *parent = 0, const QVariantList &args = QVariantList());
		virtual ~BackupPlugin();

	public:
		void backup();
		void restore();
};

#endif // BACKUPPLUGIN_H