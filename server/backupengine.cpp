/**
 *   Copyright (C) 2012 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "backupengine.h"
#include "backupengineadaptor.h"
#include "oigolerplugin.h"


BackupEngine::BackupEngine(QObject* parent)
	: QObject(parent)
	, m_plugin(0)
{
	new BackupEngineAdaptor(this);
    QDBusConnection dbus = QDBusConnection::sessionBus();
    dbus.registerObject("/BackupEngine", this);
    dbus.registerService("org.oigoler.BackupEngine");
}

BackupEngine::~BackupEngine()
{
}

void BackupEngine::makeBackup(const QString& origin, const QString& fileName, const QString& destination, const QString& backupName)
{
	QDir destinationPath(destination);
	destinationPath.mkdir(backupName);
	destinationPath.cd(backupName);
	
	qint64 current = QDateTime::currentMSecsSinceEpoch();
	destinationPath.mkdir( QString::number(current) );
	destinationPath.cd( QString::number(current) );
	
	m_plugin->setOrigin(origin);
	m_plugin->setDestination( destinationPath.path() + "/" + fileName );
	m_plugin->backup();
}

void BackupEngine::restoreBackup( const QString& backupPath, const QString& backupName, const QString& timestamp
                                , const QString& fileName, const QString& restorePath)
{
	QDir path( backupPath );
	path.cd( backupName );
	
	QDateTime date( QDateTime::fromString( timestamp, "dd:MM:yyyy hh:mm:ss.zzz" ) );
	QString text( QString::number( date.toMSecsSinceEpoch() ) );
	path.cd( text );
	
	QString origin( path.path() + "/" + fileName );
	QString destination( restorePath + "/" + fileName );
	
	m_plugin->setOrigin(origin);
	m_plugin->setDestination(destination);
	m_plugin->restore();
}

QStringList BackupEngine::updateBackupList(const QString& path)
{
	QDir directory(path);
	directory.setFilter(QDir::AllDirs | QDir::NoDotAndDotDot);
	QFileInfoList list(directory.entryInfoList());
	
	QStringList ret;
	for (int i = 0; i < list.size(); ++i)
	{
		QFileInfo fileInfo(list.at(i));
		ret.append(fileInfo.fileName());
	}
	
	return ret;
}

QStringList BackupEngine::updateDates(const QString& backupPath, const QString& backupName)
{
	QDir path( backupPath );
	path.cd( backupName );
	path.setFilter(QDir::AllDirs | QDir::NoDotAndDotDot);
	QFileInfoList list(path.entryInfoList());
	
	QStringList ret;
	for (int i = 0; i < list.size(); ++i)
	{
		QFileInfo fileInfo(list.at(i));
		QString text(fileInfo.fileName());
		QDateTime date( QDateTime::fromMSecsSinceEpoch(text.toLongLong()) );
		ret.append(date.toString("dd:MM:yyyy hh:mm:ss.zzz"));
	}
	
	return ret;
}

QStringList BackupEngine::showFiles(const QString& backupPath, const QString& backupName, const QString& timestamp)
{
	QDir path( backupPath );
	path.cd( backupName );
	
	QDateTime date( QDateTime::fromString( timestamp, "dd:MM:yyyy hh:mm:ss.zzz" ) );
	QString text( QString::number( date.toMSecsSinceEpoch() ) );
	path.cd( text );
	
	path.setFilter(QDir::AllDirs | QDir::Files | QDir::NoDotAndDotDot);
	QFileInfoList list(path.entryInfoList());
	
	QStringList ret;
	for (int i = 0; i < list.size(); ++i)
	{
		QFileInfo fileInfo(list.at(i));
		ret.append(fileInfo.fileName());
	}
	
	return ret;
}

void BackupEngine::setPlugin(OigolerPlugin* plugin)
{
	m_plugin = plugin;
}

OigolerPlugin* BackupEngine::plugin() const
{
	return m_plugin;
}

#include "backupengine.moc"
