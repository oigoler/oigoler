/**
 *   Copyright (C) 2012 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef OIGOLERPLUGIN_H
#define OIGOLERPLUGIN_H

#include "oigolerplugin_export.h"

#include <QObject>


class OIGOLERPLUGIN_EXPORT OigolerPlugin : public QObject
{
    Q_OBJECT
    
	public:
		OigolerPlugin(QObject* parent = 0);
		virtual ~OigolerPlugin();

	public:
		QString origin() const;
		void setOrigin(const QString& origin);
		
		QString destination() const;
		void setDestination(const QString& origin);
		
		virtual void backup();
		virtual void restore();
		
	private:
		QString m_origin;
		QString m_destination;
};

#endif // OIGOLERPLUGIN_H