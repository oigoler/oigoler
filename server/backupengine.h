/**
 *   Copyright (C) 2012 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef BACKUPENGINE_H
#define BACKUPENGINE_H

#include <QObject>

class OigolerPlugin;

class BackupEngine : public QObject
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "org.oigoler.BackupEngine")
    
	public:
		BackupEngine(QObject* parent = 0);
		virtual ~BackupEngine();

	public Q_SLOTS:
		void makeBackup(const QString& origin, const QString& fileName, const QString& destination, const QString& backupName);
		void restoreBackup( const QString& backupPath, const QString& backupName, const QString& timestamp
                          , const QString& fileName, const QString& restorePath);
		QStringList updateBackupList(const QString& path);
		QStringList updateDates(const QString& backupPath, const QString& backupName);
		QStringList showFiles(const QString& backupPath, const QString& backupName, const QString& timestamp);
		
	public:
		void setPlugin(OigolerPlugin* plugin);
		OigolerPlugin* plugin() const;
		
	private:
		OigolerPlugin* m_plugin;
};

#endif // BACKUPENGINE_H
