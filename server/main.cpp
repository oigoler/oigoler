/**
 *   Copyright (C) 2012 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "backupengine.h"
#include "oigolerplugin.h"

#include <KUniqueApplication>
#include <KAboutData>
#include <KCmdLineArgs>
#include <KService>
#include <KDebug>
#include <KServiceTypeTrader>


static const char description[] =
    I18N_NOOP("A KDE 4 Application");

static const char version[] = "0.1";

int main(int argc, char **argv)
{
    KAboutData about("oigolerd", 0, ki18n("oigolerd"), version, ki18n(description),
                     KAboutData::License_GPL, ki18n("(C) 2012 Rui Dias"), KLocalizedString(), 0, "ruijgdias@gmail.com");
    about.addAuthor( ki18n("Rui Dias"), KLocalizedString(), "ruijgdias@gmail.com" );
    KCmdLineArgs::init(argc, argv, &about);

    KCmdLineOptions options;
    options.add("+[URL]", ki18n( "Document to open" ));
    KCmdLineArgs::addCmdLineOptions(options);
    KUniqueApplication app;
	
	BackupEngine* engine = new BackupEngine;

    kDebug() << "Load all plugins";
    KService::List offers = KServiceTypeTrader::self()->query("Oigoler/Plugin");
	kDebug() << "offers.size()" << offers.size();
 
    KService::List::const_iterator iter;
    for(iter = offers.begin(); iter < offers.end(); ++iter)
    {
       QString error;
       KService::Ptr service = *iter;
	   kDebug() << "service->desktopEntryName()" << service->desktopEntryName();
	   kDebug() << "service->genericName()" << service->genericName();
	   kDebug() << "service->keywords()" << service->keywords();
	   kDebug() << "service->library()" << service->library();
	   kDebug() << "service->parentApp()" << service->parentApp();
	   kDebug() << "service->pluginKeyword()" << service->pluginKeyword();
	   kDebug() << "service->serviceTypes()" << service->serviceTypes();
	   kDebug() << "service->type()" << service->type();
 
        KPluginFactory *factory = KPluginLoader(service->library()).factory();
 
        if (!factory)
        {
            kError(5001) << "KPluginFactory could not load the plugin:" << service->library();
            continue;
        }
 
       OigolerPlugin *plugin = factory->create<OigolerPlugin>(engine);
 
       if (plugin) {
           kDebug() << "Load plugin:" << service->name();
		   engine->setPlugin(plugin);
       } else {
           kDebug() << "No plugin";
       }
    }

    return app.exec();
}
