/**
 *   Copyright (C) 2013, 2014 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef TESTPOINTERSET_H
#define TESTPOINTERSET_H

#include <QtTest/QtTest>


class TestPointerSet : public QObject
{
	Q_OBJECT

private slots:
	void testEmptyConstructor();
	void testCopyConstructor();
	void testDestructor();
	
	void testBeginConst();
	void testBegin();
	void testCapacity();
	void testClear();
	void testConstBegin();
	void testConstEnd();
	void testConstFind();
	void testContainsValue();
	void testContainsSet();
	void testCount();
	void testEmpty();
	void testEndConst();
	void testEnd();
	void testErase();
	void testFindConst();
	void testFind();
	void testInsert();
	void testIntersect();
	void testIsEmpty();
	void testRemove();
	void testReserve();
	void testSize();
	void testSqueeze();
	void testSubtract();
	void testSwap();
	void testUnite();
	
	void testNotEqualToOperator();
	void testBitwiseAndOperator();
	void testBitwiseAndAssignmentOperatorSet();
	void testBitwiseAndAssignmentOperatorValue();
	void testAdditionOperator();
	void testAdditionAssignmentOperatorSet();
	void testAdditionAssignmentOperatorValue();
	void testSubtractionOperator();
	void testSubtractionAssignmentOperatorSet();
	void testSubtractionAssignmentOperatorValue();
	void testBitwiseLeftShiftOperator();
	void testAssignmentOperator();
	void testEqualToOperator();
	void testBitwiseOrOperator();
	void testBitwiseOrAssignmentOperatorSet();
	void testBitwiseOrAssignmentOperatorValue();
	
	void testConstIteratorTypedef();
	void testConstIteratorCopyConstructor();
	void testConstIteratorConstructorIterator();
	void testConstIteratorNotEqualToOperator();
	void testConstIteratorIndirectionOperator();
	void testConstIteratorAdditionOperator();
	void testConstIteratorIncrementPrefixOperator();
	void testConstIteratorIncrementPostfixOperator();
	void testConstIteratorAdditionAssignmentOperator();
	void testConstIteratorSubtractionOperator();
	void testConstIteratorDecrementPrefixOperator();
	void testConstIteratorDecrementPostfixOperator();
	void testConstIteratorSubtractionAssignmentOperator();
	void testConstIteratorArrowOperator();
	void testConstIteratorAssignmentOperator();
	void testConstIteratorEqualToOperator();
	
	void testIteratorTypedef();
	void testIteratorCopyConstructor();
	void testIteratorNotEqualToIteratorOperator();
	void testIteratorNotEqualToConstIteratorOperator();
	void testIteratorIndirectionOperator();
	void testIteratorAdditionOperator();
	void testIteratorIncrementPrefixOperator();
	void testIteratorIncrementPostfixOperator();
	void testIteratorAdditionAssignmentOperator();
	void testIteratorSubtractionOperator();
	void testIteratorDecrementPrefixOperator();
	void testIteratorDecrementPostfixOperator();
	void testIteratorSubtractionAssignmentOperator();
	void testIteratorArrowOperator();
	void testIteratorAssignmentOperator();
	void testIteratorEqualToIteratorOperator();
	void testIteratorEqualToConstIteratorOperator();
	
	void testPointerSetIteratorConstructor();
	void testPointerSetIteratorFindNext();
	void testPointerSetIteratorFindPrevious();
	void testPointerSetIteratorHasNext();
	void testPointerSetIteratorHasPrevious();
	void testPointerSetIteratorNext();
	void testPointerSetIteratorPeekNext();
	void testPointerSetIteratorPeekPrevious();
	void testPointerSetIteratorPrevious();
	void testPointerSetIteratorToBack();
	void testPointerSetIteratorToFront();
	void testPointerSetIteratorAssignmentOperator();
	
	void testMutablePointerSetIteratorConstructor();
	void testMutablePointerSetIteratorFindNext();
	void testMutablePointerSetIteratorFindPrevious();
	void testMutablePointerSetIteratorHasNext();
	void testMutablePointerSetIteratorHasPrevious();
	void testMutablePointerSetIteratorNext();
	void testMutablePointerSetIteratorPeekNext();
	void testMutablePointerSetIteratorPeekPrevious();
	void testMutablePointerSetIteratorPrevious();
	void testMutablePointerSetIteratorRemove();
	void testMutablePointerSetIteratorToBack();
	void testMutablePointerSetIteratorToFront();
	void testMutablePointerSetIteratorValue();
	void testMutablePointerSetIteratorAssignmentOperator();
	
};

#endif // TESTPOINTERSET_H
