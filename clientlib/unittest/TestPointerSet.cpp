/**
 *   Copyright (C) 2013, 2014 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestPointerSet.h"
#include "../PointerSet.h"
#include "../PointerSetIterator.h"
#include "../MutablePointerSetIterator.h"

#include <typeinfo>


void TestPointerSet::testEmptyConstructor()
{
	PointerSet<QObject> set;
	QVERIFY( set.isEmpty() );
}

void TestPointerSet::testCopyConstructor()
{
	QPointer<QObject> ptr( new QObject( this ) );
	
	PointerSet<QObject>* set1( new PointerSet<QObject> );
	set1->insert( ptr );
	
	PointerSet<QObject> set2( *set1 );
	
	delete set1;
	
	QVERIFY( !ptr.isNull() );
	QCOMPARE( set2.size(), 1 );
	QVERIFY( set2.contains( ptr ) );
}

void TestPointerSet::testDestructor()
{
	QPointer<QObject> ptr1( new QObject( this ) );
	QPointer<QObject> ptr2( new QObject( this ) );
	
	PointerSet<QObject>* set( new PointerSet<QObject> );
	*set << ptr1 << ptr2;
	delete set;
	
	QVERIFY( ptr1.isNull() );
	QVERIFY( ptr2.isNull() );
}

void TestPointerSet::testBeginConst()
{
	const PointerSet<QObject> set1;
	PointerSet<QObject>::const_iterator iter( set1.begin() );
	
	QVERIFY( iter == set1.end() );
	
	PointerSet<QObject> set2;
	QObject* obj( new QObject( this ) );
	set2.insert( obj );
	const PointerSet<QObject> set3( set2 );
	iter = set3.begin();
	
	QVERIFY( iter != set3.end() );
	QVERIFY( *iter == obj );
}

void TestPointerSet::testBegin()
{
	PointerSet<QObject> set;
	PointerSet<QObject>::iterator iter( set.begin() );
	
	QVERIFY( iter == set.end() );
	
	QObject* obj( new QObject( this ) );
	set.insert( obj );
	iter = set.begin();
	
	QVERIFY( iter != set.end() );
	QVERIFY( *iter == obj );
}

void TestPointerSet::testCapacity()
{
	PointerSet<QObject> set;
	int original( set.capacity() );
	set.reserve( original + 1000 );
	
	QVERIFY( set.capacity() > original );
}

void TestPointerSet::testClear()
{
	QPointer<QObject> ptr1( new QObject( this ) );
	QPointer<QObject> ptr2( new QObject( this ) );
	
	PointerSet<QObject> set;
	set << ptr1 << ptr2;
	set.clear();
	
	QVERIFY( set.isEmpty() );
	QVERIFY( ptr1.isNull() );
	QVERIFY( ptr2.isNull() );
}

void TestPointerSet::testConstBegin()
{
	PointerSet<QObject> set;
	PointerSet<QObject>::const_iterator iter( set.constBegin() );
	
	QVERIFY( iter == set.constEnd() );
	
	QObject* obj( new QObject( this ) );
	set.insert( obj );
	iter = set.constBegin();
	
	QVERIFY( iter != set.constEnd() );
	QVERIFY( *iter == obj );
}

void TestPointerSet::testConstEnd()
{
	PointerSet<QObject> set;
	PointerSet<QObject>::const_iterator iter( set.constEnd() );
	
	QVERIFY( iter == set.constBegin() );
	
	set.insert( new QObject( this ) );
	iter = set.constEnd();
	
	QVERIFY( iter != set.constBegin() );
	QVERIFY( iter == ( set.constBegin() + 1 ) );
}

void TestPointerSet::testConstFind()
{
	QObject* obj1( new QObject( this ) );
	QObject* obj2( new QObject( this ) );
	
	PointerSet<QObject> set;
	PointerSet<QObject>::const_iterator expectedIter( set.insert( obj1 ) );
	
	QVERIFY( set.constFind( obj1 ) == expectedIter );
	QVERIFY( set.constFind( obj2 ) == set.constEnd() );
}

void TestPointerSet::testContainsValue()
{
	QObject* obj1( new QObject( this ) );
	QObject* obj2( new QObject( this ) );
	
	PointerSet<QObject> set;
	set.insert( obj1 );
	
	QVERIFY( set.contains( obj1 ) );
	QVERIFY( !set.contains( obj2 ) );
}

void TestPointerSet::testContainsSet()
{
	PointerSet<QObject> set1;
	PointerSet<QObject> set2;
	
	set1.insert( new QObject( this ) );
	QVERIFY( set1.contains( set2 ) );
	
	set2 = set1;
	QVERIFY( set1.contains( set2 ) );
	
	set2.insert( new QObject( this ) );
	QVERIFY( !set1.contains( set2 ) );
}

void TestPointerSet::testCount()
{
	PointerSet<QObject> set;
	QCOMPARE( set.count(), 0 );
	
	set.insert( new QObject( this ) );
	QCOMPARE( set.count(), 1 );
	
	set.insert( new QObject( this ) );
	QCOMPARE( set.count(), 2 );
}

void TestPointerSet::testEmpty()
{
	PointerSet<QObject> set;
	QVERIFY( set.empty() );
	
	set.insert( new QObject( this ) );
	QVERIFY( !set.empty() );
}

void TestPointerSet::testEndConst()
{
	const PointerSet<QObject> set1;
	PointerSet<QObject>::const_iterator iter( set1.end() );
	
	QVERIFY( iter == set1.begin() );
	
	PointerSet<QObject> set2;
	set2.insert( new QObject( this ) );
	const PointerSet<QObject> set3( set2 );
	iter = set3.end();
	
	QVERIFY( iter != set3.begin() );
	QVERIFY( iter == ( set3.begin() + 1 ) );
}

void TestPointerSet::testEnd()
{
	PointerSet<QObject> set;
	PointerSet<QObject>::iterator iter( set.end() );
	
	QVERIFY( iter == set.begin() );
	
	set.insert( new QObject( this ) );
	iter = set.end();
	
	QVERIFY( iter != set.begin() );
	QVERIFY( iter == ( set.begin() + 1 ) );
}

void TestPointerSet::testErase()
{
	QPointer<QObject> ptr1( new QObject( this ) );
	QPointer<QObject> ptr2( new QObject( this ) );
	
	PointerSet<QObject> set;
	set << ptr1 << ptr2;
	
	PointerSet<QObject>::iterator iter( set.begin() );
	int i( 0 );
	
	while ( iter != set.end() )
	{
		if ( *iter == ptr1 )
			iter = set.erase( iter );
		else
			++iter;
		
		QVERIFY( ++i < 3 );
	}
	
	QVERIFY( ptr1.isNull() );
	QVERIFY( !ptr2.isNull() );
	QVERIFY( set.contains( ptr2 ) );
	QCOMPARE( set.size(), 1 );
}

void TestPointerSet::testFindConst()
{
	QObject* obj1( new QObject( this ) );
	QObject* obj2( new QObject( this ) );
	
	PointerSet<QObject> set1;
	set1.insert( obj1 );
	const PointerSet<QObject> set2( set1 );
	
	QVERIFY( set2.find( obj1 ) == set2.begin() );
	QVERIFY( set2.find( obj2 ) == set2.end() );
}

void TestPointerSet::testFind()
{
	QObject* obj1( new QObject( this ) );
	QObject* obj2( new QObject( this ) );
	
	PointerSet<QObject> set;
	set.insert( obj1 );
	
	QVERIFY( set.find( obj1 ) == set.begin() );
	QVERIFY( set.find( obj2 ) == set.end() );
}

void TestPointerSet::testInsert()
{
	QObject* obj( new QObject( this ) );
	PointerSet<QObject> set;
	PointerSet<QObject>::const_iterator iter( set.insert( obj ) );
	
	QCOMPARE( set.size(), 1 );
	QVERIFY( set.contains( obj ) );
	QVERIFY( *iter == obj );
	
	iter = set.insert( obj );
	
	QCOMPARE( set.size(), 1 );
	QVERIFY( set.contains( obj ) );
	QVERIFY( *iter == obj );
}

void TestPointerSet::testIntersect()
{
	QPointer<QObject> ptr1( new QObject( this ) );
	QPointer<QObject> ptr2( new QObject( this ) );
	QPointer<QObject> ptr3( new QObject( this ) );
	QPointer<QObject> ptr4( new QObject( this ) );
	
	PointerSet<QObject> set1;
	set1 << ptr1 << ptr2 << ptr3 << ptr4;
	
	PointerSet<QObject> set2( set1 );
	set2.remove( ptr3 );
	set2.remove( ptr4 );
	
	PointerSet<QObject> set3( set1 );
	set3.remove( ptr1 );
	set3.remove( ptr4 );
	
	set1.intersect( set2 ).intersect( set3 );
	
	QCOMPARE( set1.size(), 1 );
	QVERIFY( set1.contains( ptr2 ) );
	
	QVERIFY( !ptr1.isNull() );
	QVERIFY( !ptr2.isNull() );
	QVERIFY( !ptr3.isNull() );
	QVERIFY( ptr4.isNull() );
}

void TestPointerSet::testIsEmpty()
{
	PointerSet<QObject> set;
	QVERIFY( set.isEmpty() );
	
	set.insert( new QObject( this ) );
	QVERIFY( !set.isEmpty() );
}

void TestPointerSet::testRemove()
{
	QPointer<QObject> ptr( new QObject( this ) );
	PointerSet<QObject> set;
	
	QVERIFY( !set.remove( ptr ) );
	
	set.insert( ptr );

	QVERIFY( set.remove( ptr ) );
	QVERIFY( set.isEmpty() );
	QVERIFY( ptr.isNull() );
}

void TestPointerSet::testReserve()
{
	PointerSet<QObject> set;
	set.reserve( 20000 );
	
	QVERIFY( set.capacity() >= 20000 );
}

void TestPointerSet::testSize()
{
	PointerSet<QObject> set;
	QCOMPARE( set.size(), 0 );
	
	set.insert( new QObject( this ) );
	QCOMPARE( set.size(), 1 );
	
	set.insert( new QObject( this ) );
	QCOMPARE( set.size(), 2 );
}

void TestPointerSet::testSqueeze()
{
	PointerSet<QObject> set;
	set.reserve( 20000 );
	QVERIFY( set.capacity() >= 20000 );
	
	set.squeeze();
	QVERIFY( set.capacity() < 20000 );
}

void TestPointerSet::testSubtract()
{
	QPointer<QObject> ptr1( new QObject( this ) );
	QPointer<QObject> ptr2( new QObject( this ) );
	QPointer<QObject> ptr3( new QObject( this ) );
	QPointer<QObject> ptr4( new QObject( this ) );
	
	PointerSet<QObject> set1;
	set1 << ptr1 << ptr2 << ptr3 << ptr4;
	
	PointerSet<QObject> set2( set1 );
	set2.remove( ptr3 );
	set2.remove( ptr4 );
	
	PointerSet<QObject> set3( set1 );
	set3.remove( ptr1 );
	set3.remove( ptr4 );
	
	set1.subtract( set2 ).subtract( set3 );
	
	QCOMPARE( set1.size(), 1 );
	QVERIFY( set1.contains( ptr4 ) );
	
	QVERIFY( !ptr1.isNull() );
	QVERIFY( !ptr2.isNull() );
	QVERIFY( !ptr3.isNull() );
	QVERIFY( !ptr4.isNull() );
}

void TestPointerSet::testSwap()
{
	QPointer<QObject> ptr1( new QObject( this ) );
	QPointer<QObject> ptr2( new QObject( this ) );
	QPointer<QObject> ptr3( new QObject( this ) );
	
	PointerSet<QObject> set1;
	PointerSet<QObject> set2;
	
	set1 << ptr1;
	set2 << ptr2 << ptr3;

	set1.swap( set2 );
	
	QCOMPARE( set1.size(), 2 );
	QVERIFY( set1.contains( ptr2 ) );
	QVERIFY( set1.contains( ptr3 ) );
	
	QCOMPARE( set2.size(), 1 );
	QVERIFY( set2.contains( ptr1 ) );
	
	QVERIFY( !ptr1.isNull() );
	QVERIFY( !ptr2.isNull() );
	QVERIFY( !ptr3.isNull() );
}

void TestPointerSet::testUnite()
{
	QObject* obj1( new QObject( this ) );
	QObject* obj2( new QObject( this ) );
	QObject* obj3( new QObject( this ) );
	
	PointerSet<QObject> set1;
	set1.insert( obj1 );
	set1.insert( obj2 );
	
	PointerSet<QObject> set2( set1 );
	set2.remove( obj1 );
	set2.insert( obj3 );
	
	PointerSet<QObject> set3;
	set3.unite( set1 ).unite( set2 );
	
	QCOMPARE( set3.size(), 3 );
	QVERIFY( set3.contains( obj1 ) );
	QVERIFY( set3.contains( obj2 ) );
	QVERIFY( set3.contains( obj3 ) );
}

void TestPointerSet::testNotEqualToOperator()
{
	PointerSet<QObject> set1;
	PointerSet<QObject> set2;
	QVERIFY( !( set1 != set2 ) );
	
	set1.insert( new QObject( this ) );
	QVERIFY( set1 != set2 );
	
	set2 = set1;
	QVERIFY( !( set1 != set2 ) );
}

void TestPointerSet::testBitwiseAndOperator()
{
	QObject* obj1( new QObject( this ) );
	QObject* obj2( new QObject( this ) );
	QObject* obj3( new QObject( this ) );
	
	PointerSet<QObject> set1;
	set1 << obj1 << obj2;
	
	PointerSet<QObject> set2( set1 );
	set2.remove( obj1 );
	set2.insert( obj3 );
	
	PointerSet<QObject> set3( set1 & set2 );
	
	QCOMPARE( set3.size(), 1 );
	QVERIFY( set3.contains( obj2 ) );
}

void TestPointerSet::testBitwiseAndAssignmentOperatorSet()
{
	QPointer<QObject> ptr1( new QObject( this ) );
	QPointer<QObject> ptr2( new QObject( this ) );
	QPointer<QObject> ptr3( new QObject( this ) );
	QPointer<QObject> ptr4( new QObject( this ) );
	
	PointerSet<QObject> set1;
	set1 << ptr1 << ptr2 << ptr3 << ptr4;
	
	PointerSet<QObject> set2( set1 );
	set2.remove( ptr3 );
	set2.remove( ptr4 );
	
	PointerSet<QObject> set3( set1 );
	set3.remove( ptr1 );
	set3.remove( ptr4 );
	
	( set1 &= set2 ) &= set3;
	
	QCOMPARE( set1.size(), 1 );
	QVERIFY( set1.contains( ptr2 ) );
	
	QVERIFY( !ptr1.isNull() );
	QVERIFY( !ptr2.isNull() );
	QVERIFY( !ptr3.isNull() );
	QVERIFY( ptr4.isNull() );
}

void TestPointerSet::testBitwiseAndAssignmentOperatorValue()
{
	QPointer<QObject> ptr1( new QObject( this ) );
	QPointer<QObject> ptr2( new QObject( this ) );
	
	PointerSet<QObject> set;
	set << ptr1 << ptr2;
	
	set &= ptr1;
	
	QCOMPARE( set.size(), 1 );
	QVERIFY( set.contains( ptr1 ) );
	QVERIFY( !ptr1.isNull() );
	QVERIFY( ptr2.isNull() );
	
	ptr2 = new QObject( this );
	set.insert( ptr2 );
	
	( set &= ptr1 ) &= ptr2;
	
	QVERIFY( set.isEmpty() );
	QVERIFY( ptr1.isNull() );
	QVERIFY( ptr2.isNull() );
}

void TestPointerSet::testAdditionOperator()
{
	QObject* obj1( new QObject( this ) );
	QObject* obj2( new QObject( this ) );
	QObject* obj3( new QObject( this ) );
	
	PointerSet<QObject> set1;
	set1 << obj1 << obj2;
	
	PointerSet<QObject> set2( set1 );
	set2.remove( obj1 );
	set2.insert( obj3 );
	
	PointerSet<QObject> set3( set1 + set2 );
	
	QCOMPARE( set3.size(), 3 );
	QVERIFY( set3.contains( obj1 ) );
	QVERIFY( set3.contains( obj2 ) );
	QVERIFY( set3.contains( obj3 ) );
}

void TestPointerSet::testAdditionAssignmentOperatorSet()
{
	QObject* obj1( new QObject( this ) );
	QObject* obj2( new QObject( this ) );
	QObject* obj3( new QObject( this ) );
	
	PointerSet<QObject> set1;
	set1.insert( obj1 );
	set1.insert( obj2 );
	
	PointerSet<QObject> set2( set1 );
	set2.remove( obj1 );
	set2.insert( obj3 );
	
	PointerSet<QObject> set3;
	( set3 += set1 ) += set2;
	
	QCOMPARE( set3.size(), 3 );
	QVERIFY( set3.contains( obj1 ) );
	QVERIFY( set3.contains( obj2 ) );
	QVERIFY( set3.contains( obj3 ) );
}

void TestPointerSet::testAdditionAssignmentOperatorValue()
{
	QObject* obj( new QObject( this ) );
	PointerSet<QObject> set;
	
	( set += obj ) += obj;
	
	QCOMPARE( set.size(), 1 );
	QVERIFY( set.contains( obj ) );
}

void TestPointerSet::testSubtractionOperator()
{
	QPointer<QObject> ptr1( new QObject( this ) );
	QPointer<QObject> ptr2( new QObject( this ) );
	QPointer<QObject> ptr3( new QObject( this ) );
	
	PointerSet<QObject> set1;
	set1 << ptr1 << ptr2;
	
	PointerSet<QObject> set2( set1 );
	set2.remove( ptr1 );
	set2.insert( ptr3 );
	
	PointerSet<QObject> set3( set1 - set2 );
	
	QCOMPARE( set3.size(), 1 );
	QVERIFY( set3.contains( ptr1 ) );
	
	QVERIFY( !ptr1.isNull() );
	QVERIFY( !ptr2.isNull() );
	QVERIFY( !ptr3.isNull() );
}

void TestPointerSet::testSubtractionAssignmentOperatorSet()
{
	QPointer<QObject> ptr1( new QObject( this ) );
	QPointer<QObject> ptr2( new QObject( this ) );
	QPointer<QObject> ptr3( new QObject( this ) );
	QPointer<QObject> ptr4( new QObject( this ) );
	
	PointerSet<QObject> set1;
	set1 << ptr1 << ptr2 << ptr3 << ptr4;
	
	PointerSet<QObject> set2( set1 );
	set2.remove( ptr3 );
	set2.remove( ptr4 );
	
	PointerSet<QObject> set3( set1 );
	set3.remove( ptr1 );
	set3.remove( ptr4 );
	
	( set1 -= set2 ) -= set3;
	
	QCOMPARE( set1.size(), 1 );
	QVERIFY( set1.contains( ptr4 ) );
	
	QVERIFY( !ptr1.isNull() );
	QVERIFY( !ptr2.isNull() );
	QVERIFY( !ptr3.isNull() );
	QVERIFY( !ptr4.isNull() );
}

void TestPointerSet::testSubtractionAssignmentOperatorValue()
{
	QPointer<QObject> ptr1( new QObject( this ) );
	QPointer<QObject> ptr2( new QObject( this ) );
	
	PointerSet<QObject> set;
	set.insert( ptr1 );
	
	( set -= ptr1 ) -= ptr2;
	
	QVERIFY( ptr1.isNull() );
	QVERIFY( !ptr2.isNull() );
}

void TestPointerSet::testBitwiseLeftShiftOperator()
{
	QObject* obj1( new QObject( this ) );
	QObject* obj2( new QObject( this ) );
	
	PointerSet<QObject> set;
	set << obj1 << obj2 << obj1;
	
	QCOMPARE( set.size(), 2 );
	QVERIFY( set.contains( obj1 ) );
	QVERIFY( set.contains( obj2 ) );
}

void TestPointerSet::testAssignmentOperator()
{
	QPointer<QObject> ptr1( new QObject( this ) );
	QPointer<QObject> ptr2( new QObject( this ) );
	QPointer<QObject> ptr3( new QObject( this ) );
	
	PointerSet<QObject> set1;
	set1 << ptr1 << ptr2;
	
	PointerSet<QObject> set2( set1 );
	set2.remove( ptr1 );
	set2.insert( ptr3 );
	
	PointerSet<QObject> set3;
	set3 = set2 = set1;
	
	QCOMPARE( set3.size(), 2 );
	QVERIFY( set3.contains( ptr1 ) );
	QVERIFY( set3.contains( ptr2 ) );
	
	QVERIFY( !ptr1.isNull() );
	QVERIFY( !ptr2.isNull() );
	QVERIFY( ptr3.isNull() );
}

void TestPointerSet::testEqualToOperator()
{
	PointerSet<QObject> set1;
	PointerSet<QObject> set2;
	QVERIFY( set1 == set2 );
	
	set1.insert( new QObject( this ) );
	QVERIFY( !( set1 == set2 ) );
	
	set2 = set1;
	QVERIFY( set1 == set2 );
}

void TestPointerSet::testBitwiseOrOperator()
{
	QObject* obj1( new QObject( this ) );
	QObject* obj2( new QObject( this ) );
	QObject* obj3( new QObject( this ) );
	
	PointerSet<QObject> set1;
	set1 << obj1 << obj2;
	
	PointerSet<QObject> set2( set1 );
	set2.remove( obj1 );
	set2.insert( obj3 );
	
	PointerSet<QObject> set3( set1 | set2 );
	
	QCOMPARE( set3.size(), 3 );
	QVERIFY( set3.contains( obj1 ) );
	QVERIFY( set3.contains( obj2 ) );
	QVERIFY( set3.contains( obj3 ) );
}

void TestPointerSet::testBitwiseOrAssignmentOperatorSet()
{
	QObject* obj1( new QObject( this ) );
	QObject* obj2( new QObject( this ) );
	QObject* obj3( new QObject( this ) );
	
	PointerSet<QObject> set1;
	set1.insert( obj1 );
	set1.insert( obj2 );
	
	PointerSet<QObject> set2( set1 );
	set2.remove( obj1 );
	set2.insert( obj3 );
	
	PointerSet<QObject> set3;
	( set3 |= set1 ) |= set2;
	
	QCOMPARE( set3.size(), 3 );
	QVERIFY( set3.contains( obj1 ) );
	QVERIFY( set3.contains( obj2 ) );
	QVERIFY( set3.contains( obj3 ) );
}

void TestPointerSet::testBitwiseOrAssignmentOperatorValue()
{
	QObject* obj( new QObject( this ) );
	PointerSet<QObject> set;
	
	( set |= obj ) |= obj;
	
	QCOMPARE( set.size(), 1 );
	QVERIFY( set.contains( obj ) );
}

void TestPointerSet::testConstIteratorTypedef()
{
	QVERIFY( typeid( PointerSet<QObject>::const_iterator::iterator_category )
	      == typeid( std::bidirectional_iterator_tag ) );
}

void TestPointerSet::testConstIteratorCopyConstructor()
{
	PointerSet<QObject> set;
	set.insert( new QObject( this ) );
	
	PointerSet<QObject>::const_iterator iter( set.constBegin() );
	
	QVERIFY( iter == set.constBegin() );
	QVERIFY( iter != set.constEnd() );
}

void TestPointerSet::testConstIteratorConstructorIterator()
{
	PointerSet<QObject> set;
	set.insert( new QObject( this ) );
	
	PointerSet<QObject>::const_iterator iter( set.begin() );
	
	QVERIFY( iter == set.constBegin() );
	QVERIFY( iter != set.constEnd() );
}

void TestPointerSet::testConstIteratorNotEqualToOperator()
{
	PointerSet<QObject> set;
	set.insert( new QObject( this ) );
	
	PointerSet<QObject>::const_iterator iter( set.constBegin() );
	
	QVERIFY( !( iter != set.constBegin() ) );
	QVERIFY( iter != set.constEnd() );
}

void TestPointerSet::testConstIteratorIndirectionOperator()
{
	QObject* obj( new QObject( this ) );
	
	PointerSet<QObject> set;
	set.insert( obj );
	
	PointerSet<QObject>::const_iterator iter( set.constBegin() );
	
	QVERIFY( *iter == obj );
}

void TestPointerSet::testConstIteratorAdditionOperator()
{
	PointerSet<QObject> set;
	set.insert( new QObject( this ) );
	
	PointerSet<QObject>::const_iterator iter( set.constBegin() );
	
	QVERIFY( ( iter + 1 ) == set.constEnd() );
}

void TestPointerSet::testConstIteratorIncrementPrefixOperator()
{
	PointerSet<QObject> set;
	set.insert( new QObject( this ) );
	
	PointerSet<QObject>::const_iterator iter1( set.constBegin() );
	PointerSet<QObject>::const_iterator iter2( ++iter1 );
	
	QVERIFY( iter1 == set.constEnd() );
	QVERIFY( iter2 == set.constEnd() );
}

void TestPointerSet::testConstIteratorIncrementPostfixOperator()
{
	PointerSet<QObject> set;
	set.insert( new QObject( this ) );
	
	PointerSet<QObject>::const_iterator iter1( set.constBegin() );
	PointerSet<QObject>::const_iterator iter2( iter1++ );
	
	QVERIFY( iter1 == set.constEnd() );
	QVERIFY( iter2 == set.constBegin() );
}

void TestPointerSet::testConstIteratorAdditionAssignmentOperator()
{
	PointerSet<QObject> set;
	set.insert( new QObject( this ) );
	set.insert( new QObject( this ) );
	set.insert( new QObject( this ) );
	
	PointerSet<QObject>::const_iterator iter( set.constBegin() );
	
	( iter += 1 ) += 2;
	
	QVERIFY( iter == set.constEnd() );
}

void TestPointerSet::testConstIteratorSubtractionOperator()
{
	PointerSet<QObject> set;
	set.insert( new QObject( this ) );
	
	PointerSet<QObject>::const_iterator iter( set.constEnd() );
	
	QVERIFY( ( iter - 1 ) == set.constBegin() );
}

void TestPointerSet::testConstIteratorDecrementPrefixOperator()
{
	PointerSet<QObject> set;
	set.insert( new QObject( this ) );
	
	PointerSet<QObject>::const_iterator iter1( set.constEnd() );
	PointerSet<QObject>::const_iterator iter2( --iter1 );
	
	QVERIFY( iter1 == set.constBegin() );
	QVERIFY( iter2 == set.constBegin() );
}

void TestPointerSet::testConstIteratorDecrementPostfixOperator()
{
	PointerSet<QObject> set;
	set.insert( new QObject( this ) );
	
	PointerSet<QObject>::const_iterator iter1( set.constEnd() );
	PointerSet<QObject>::const_iterator iter2( iter1-- );
	
	QVERIFY( iter1 == set.constBegin() );
	QVERIFY( iter2 == set.constEnd() );
}

void TestPointerSet::testConstIteratorSubtractionAssignmentOperator()
{
	PointerSet<QObject> set;
	set.insert( new QObject( this ) );
	set.insert( new QObject( this ) );
	set.insert( new QObject( this ) );
	
	PointerSet<QObject>::const_iterator iter( set.constEnd() );
	
	( iter -= 1 ) -= 2;
	
	QVERIFY( iter == set.constBegin() );
}

void TestPointerSet::testConstIteratorArrowOperator()
{
	QObject* obj( new QObject( this ) );
	obj->setObjectName( "testConstIteratorArrowOperator" );
	
	PointerSet<QObject> set;
	set.insert( obj );
	
	PointerSet<QObject>::const_iterator iter( set.constBegin() );
	
	QCOMPARE( iter->objectName(), QString( "testConstIteratorArrowOperator" ) );
}

void TestPointerSet::testConstIteratorAssignmentOperator()
{
	PointerSet<QObject> set;
	set.insert( new QObject( this ) );
	
	PointerSet<QObject>::const_iterator iter1;
	PointerSet<QObject>::const_iterator iter2;
	
	iter1 = iter2 = set.constEnd();
	
	QVERIFY( iter1 == set.constEnd() );
	QVERIFY( iter2 == set.constEnd() );
}

void TestPointerSet::testConstIteratorEqualToOperator()
{
	PointerSet<QObject> set;
	set.insert( new QObject( this ) );
	
	PointerSet<QObject>::const_iterator iter( set.constBegin() );
	
	QVERIFY( iter == set.constBegin() );
	QVERIFY( !( iter == set.constEnd() ) );
}

void TestPointerSet::testIteratorTypedef()
{
	QVERIFY( typeid( PointerSet<QObject>::iterator::iterator_category )
	      == typeid( std::bidirectional_iterator_tag ) );
}

void TestPointerSet::testIteratorCopyConstructor()
{
	PointerSet<QObject> set;
	set.insert( new QObject( this ) );
	
	PointerSet<QObject>::iterator iter( set.begin() );
	
	QVERIFY( iter == set.begin() );
	QVERIFY( iter != set.end() );
}

void TestPointerSet::testIteratorNotEqualToIteratorOperator()
{
	PointerSet<QObject> set;
	set.insert( new QObject( this ) );
	
	PointerSet<QObject>::iterator iter( set.begin() );
	
	QVERIFY( !( iter != set.begin() ) );
	QVERIFY( iter != set.end() );
}

void TestPointerSet::testIteratorNotEqualToConstIteratorOperator()
{
	PointerSet<QObject> set;
	set.insert( new QObject( this ) );
	
	PointerSet<QObject>::iterator iter( set.begin() );
	
	QVERIFY( !( iter != set.constBegin() ) );
	QVERIFY( iter != set.constEnd() );
}

void TestPointerSet::testIteratorIndirectionOperator()
{
	QObject* obj( new QObject( this ) );
	
	PointerSet<QObject> set;
	set.insert( obj );
	
	PointerSet<QObject>::iterator iter( set.begin() );
	
	QVERIFY( *iter == obj );
}

void TestPointerSet::testIteratorAdditionOperator()
{
	PointerSet<QObject> set;
	set.insert( new QObject( this ) );
	
	PointerSet<QObject>::iterator iter( set.begin() );
	
	QVERIFY( ( iter + 1 ) == set.end() );
}

void TestPointerSet::testIteratorIncrementPrefixOperator()
{
	PointerSet<QObject> set;
	set.insert( new QObject( this ) );
	
	PointerSet<QObject>::iterator iter1( set.begin() );
	PointerSet<QObject>::iterator iter2( ++iter1 );
	
	QVERIFY( iter1 == set.end() );
	QVERIFY( iter2 == set.end() );
}

void TestPointerSet::testIteratorIncrementPostfixOperator()
{
	PointerSet<QObject> set;
	set.insert( new QObject( this ) );
	
	PointerSet<QObject>::iterator iter1( set.begin() );
	PointerSet<QObject>::iterator iter2( iter1++ );
	
	QVERIFY( iter1 == set.end() );
	QVERIFY( iter2 == set.begin() );
}

void TestPointerSet::testIteratorAdditionAssignmentOperator()
{
	PointerSet<QObject> set;
	set.insert( new QObject( this ) );
	set.insert( new QObject( this ) );
	set.insert( new QObject( this ) );
	
	PointerSet<QObject>::iterator iter( set.begin() );
	
	( iter += 1 ) += 2;
	
	QVERIFY( iter == set.end() );
}

void TestPointerSet::testIteratorSubtractionOperator()
{
	PointerSet<QObject> set;
	set.insert( new QObject( this ) );
	
	PointerSet<QObject>::iterator iter( set.end() );
	
	QVERIFY( ( iter - 1 ) == set.begin() );
}

void TestPointerSet::testIteratorDecrementPrefixOperator()
{
	PointerSet<QObject> set;
	set.insert( new QObject( this ) );
	
	PointerSet<QObject>::iterator iter1( set.end() );
	PointerSet<QObject>::iterator iter2( --iter1 );
	
	QVERIFY( iter1 == set.begin() );
	QVERIFY( iter2 == set.begin() );
}

void TestPointerSet::testIteratorDecrementPostfixOperator()
{
	PointerSet<QObject> set;
	set.insert( new QObject( this ) );
	
	PointerSet<QObject>::iterator iter1( set.end() );
	PointerSet<QObject>::iterator iter2( iter1-- );
	
	QVERIFY( iter1 == set.begin() );
	QVERIFY( iter2 == set.end() );
}

void TestPointerSet::testIteratorSubtractionAssignmentOperator()
{
	PointerSet<QObject> set;
	set.insert( new QObject( this ) );
	set.insert( new QObject( this ) );
	set.insert( new QObject( this ) );
	
	PointerSet<QObject>::iterator iter( set.end() );
	
	( iter -= 1 ) -= 2;
	
	QVERIFY( iter == set.begin() );
}

void TestPointerSet::testIteratorArrowOperator()
{
	QObject* obj( new QObject( this ) );
	obj->setObjectName( "testIteratorArrowOperator" );
	
	PointerSet<QObject> set;
	set.insert( obj );
	
	PointerSet<QObject>::iterator iter( set.begin() );
	
	QCOMPARE( iter->objectName(), QString( "testIteratorArrowOperator" ) );
}

void TestPointerSet::testIteratorAssignmentOperator()
{
	PointerSet<QObject> set;
	set.insert( new QObject( this ) );
	
	PointerSet<QObject>::iterator iter1;
	PointerSet<QObject>::iterator iter2;
	
	iter1 = iter2 = set.end();
	
	QVERIFY( iter1 == set.end() );
	QVERIFY( iter2 == set.end() );
}

void TestPointerSet::testIteratorEqualToIteratorOperator()
{
	PointerSet<QObject> set;
	set.insert( new QObject( this ) );
	
	PointerSet<QObject>::iterator iter( set.begin() );
	
	QVERIFY( iter == set.begin() );
	QVERIFY( !( iter == set.end() ) );
}

void TestPointerSet::testIteratorEqualToConstIteratorOperator()
{
	PointerSet<QObject> set;
	set.insert( new QObject( this ) );
	
	PointerSet<QObject>::const_iterator iter( set.begin() );
	
	QVERIFY( iter == set.constBegin() );
	QVERIFY( !( iter == set.constEnd() ) );
}

void TestPointerSet::testPointerSetIteratorConstructor()
{
	PointerSet<QObject> set;
	set.insert( new QObject( this ) );
	
	PointerSetIterator<QObject> iter( set );
	
	QVERIFY( !iter.hasPrevious() );
	QVERIFY( iter.hasNext() );
}

void TestPointerSet::testPointerSetIteratorFindNext()
{
	QObject* obj1( new QObject( this ) );
	QObject* obj2( new QObject( this ) );
	QObject* obj3( new QObject( this ) );

	PointerSet<QObject> set;
	set << obj1 << obj2;
	
	QObject* obj4( *( set.begin() ) );
	PointerSetIterator<QObject> iter( set );
	
	QVERIFY( iter.findNext( obj4 ) );
	QVERIFY( iter.hasPrevious() );
	QVERIFY( iter.hasNext() );
	
	QVERIFY( !iter.findNext( obj3 ) );
	QVERIFY( iter.hasPrevious() );
	QVERIFY( !iter.hasNext() );
}

void TestPointerSet::testPointerSetIteratorFindPrevious()
{
	QObject* obj1( new QObject( this ) );
	QObject* obj2( new QObject( this ) );
	QObject* obj3( new QObject( this ) );

	PointerSet<QObject> set;
	set << obj1 << obj2;
	
	QObject* obj4( *( set.begin() + 1 ) );
	
	PointerSetIterator<QObject> iter( set );
	iter.toBack();
	
	QVERIFY( iter.findPrevious( obj4 ) );
	QVERIFY( iter.hasPrevious() );
	QVERIFY( iter.hasNext() );
	
	QVERIFY( !iter.findPrevious( obj3 ) );
	QVERIFY( !iter.hasPrevious() );
	QVERIFY( iter.hasNext() );
}

void TestPointerSet::testPointerSetIteratorHasNext()
{
	PointerSet<QObject> set;
	set << new QObject( this ) << new QObject( this );
	
	PointerSetIterator<QObject> iter( set );
	QVERIFY( iter.hasNext() );

	iter.next();
	QVERIFY( iter.hasNext() );
	
	iter.next();
	QVERIFY( !iter.hasNext() );
}

void TestPointerSet::testPointerSetIteratorHasPrevious()
{
	PointerSet<QObject> set;
	set << new QObject( this ) << new QObject( this );
	
	PointerSetIterator<QObject> iter( set );
	iter.toBack();
	QVERIFY( iter.hasPrevious() );

	iter.previous();
	QVERIFY( iter.hasPrevious() );
	
	iter.previous();
	QVERIFY( !iter.hasPrevious() );
}

void TestPointerSet::testPointerSetIteratorNext()
{
	QObject* obj( new QObject( this ) );
	
	PointerSet<QObject> set;
	set.insert( obj );
	
	PointerSetIterator<QObject> iter( set );
	
	QVERIFY( iter.next() == obj );
	QVERIFY( !iter.hasNext() );
}

void TestPointerSet::testPointerSetIteratorPeekNext()
{
	QObject* obj( new QObject( this ) );
	
	PointerSet<QObject> set;
	set.insert( obj );
	
	PointerSetIterator<QObject> iter( set );
	
	QVERIFY( iter.peekNext() == obj );
	QVERIFY( iter.hasNext() );
}

void TestPointerSet::testPointerSetIteratorPeekPrevious()
{
	QObject* obj( new QObject( this ) );
	
	PointerSet<QObject> set;
	set.insert( obj );
	
	PointerSetIterator<QObject> iter( set );
	iter.toBack();
	
	QVERIFY( iter.peekPrevious() == obj );
	QVERIFY( iter.hasPrevious() );
}

void TestPointerSet::testPointerSetIteratorPrevious()
{
	QObject* obj( new QObject( this ) );
	
	PointerSet<QObject> set;
	set.insert( obj );
	
	PointerSetIterator<QObject> iter( set );
	iter.toBack();
	
	QVERIFY( iter.previous() == obj );
	QVERIFY( !iter.hasPrevious() );
}

void TestPointerSet::testPointerSetIteratorToBack()
{
	PointerSet<QObject> set;
	set.insert( new QObject( this ) );
	
	PointerSetIterator<QObject> iter( set );
	iter.toBack();
	
	QVERIFY( !iter.hasNext() );
	QVERIFY( iter.hasPrevious() );
}

void TestPointerSet::testPointerSetIteratorToFront()
{
	PointerSet<QObject> set;
	set.insert( new QObject( this ) );
	
	PointerSetIterator<QObject> iter( set );
	iter.toBack();
	iter.toFront();
	
	QVERIFY( !iter.hasPrevious() );
	QVERIFY( iter.hasNext() );
}

void TestPointerSet::testPointerSetIteratorAssignmentOperator()
{
	QObject* obj1( new QObject( this ) );
	QObject* obj2( new QObject( this ) );
	
	PointerSet<QObject> set1;
	set1.insert( obj1 );
	
	PointerSet<QObject> set2;
	set2.insert( obj2 );
	
	PointerSetIterator<QObject> iter( set1 );
	
	QVERIFY( ( iter = set2 ).next() == obj2 );
}

void TestPointerSet::testMutablePointerSetIteratorConstructor()
{
	PointerSet<QObject> set;
	set.insert( new QObject( this ) );
	
	MutablePointerSetIterator<QObject> iter( set );
	
	QVERIFY( !iter.hasPrevious() );
	QVERIFY( iter.hasNext() );
}

void TestPointerSet::testMutablePointerSetIteratorFindNext()
{
	QObject* obj1( new QObject( this ) );
	QObject* obj2( new QObject( this ) );
	QObject* obj3( new QObject( this ) );

	PointerSet<QObject> set;
	set << obj1 << obj2;
	
	QObject* obj4( *( set.begin() ) );
	MutablePointerSetIterator<QObject> iter( set );
	
	QVERIFY( iter.findNext( obj4 ) );
	QVERIFY( iter.hasPrevious() );
	QVERIFY( iter.hasNext() );
	
	QVERIFY( !iter.findNext( obj3 ) );
	QVERIFY( iter.hasPrevious() );
	QVERIFY( !iter.hasNext() );
}

void TestPointerSet::testMutablePointerSetIteratorFindPrevious()
{
	QObject* obj1( new QObject( this ) );
	QObject* obj2( new QObject( this ) );
	QObject* obj3( new QObject( this ) );

	PointerSet<QObject> set;
	set << obj1 << obj2;
	
	QObject* obj4( *( set.begin() + 1 ) );
	
	MutablePointerSetIterator<QObject> iter( set );
	iter.toBack();
	
	QVERIFY( iter.findPrevious( obj4 ) );
	QVERIFY( iter.hasPrevious() );
	QVERIFY( iter.hasNext() );
	
	QVERIFY( !iter.findPrevious( obj3 ) );
	QVERIFY( !iter.hasPrevious() );
	QVERIFY( iter.hasNext() );
}

void TestPointerSet::testMutablePointerSetIteratorHasNext()
{
	PointerSet<QObject> set;
	set << new QObject( this ) << new QObject( this );
	
	MutablePointerSetIterator<QObject> iter( set );
	QVERIFY( iter.hasNext() );

	iter.next();
	QVERIFY( iter.hasNext() );
	
	iter.next();
	QVERIFY( !iter.hasNext() );
}

void TestPointerSet::testMutablePointerSetIteratorHasPrevious()
{
	PointerSet<QObject> set;
	set << new QObject( this ) << new QObject( this );
	
	MutablePointerSetIterator<QObject> iter( set );
	iter.toBack();
	QVERIFY( iter.hasPrevious() );

	iter.previous();
	QVERIFY( iter.hasPrevious() );
	
	iter.previous();
	QVERIFY( !iter.hasPrevious() );
}

void TestPointerSet::testMutablePointerSetIteratorNext()
{
	QObject* obj( new QObject( this ) );
	
	PointerSet<QObject> set;
	set.insert( obj );
	
	MutablePointerSetIterator<QObject> iter( set );
	
	QVERIFY( iter.next() == obj );
	QVERIFY( !iter.hasNext() );
}

void TestPointerSet::testMutablePointerSetIteratorPeekNext()
{
	QObject* obj( new QObject( this ) );
	
	PointerSet<QObject> set;
	set.insert( obj );
	
	MutablePointerSetIterator<QObject> iter( set );
	
	QVERIFY( iter.peekNext() == obj );
	QVERIFY( iter.hasNext() );
}

void TestPointerSet::testMutablePointerSetIteratorPeekPrevious()
{
	QObject* obj( new QObject( this ) );
	
	PointerSet<QObject> set;
	set.insert( obj );
	
	MutablePointerSetIterator<QObject> iter( set );
	iter.toBack();
	
	QVERIFY( iter.peekPrevious() == obj );
	QVERIFY( iter.hasPrevious() );
}

void TestPointerSet::testMutablePointerSetIteratorPrevious()
{
	QObject* obj( new QObject( this ) );
	
	PointerSet<QObject> set;
	set.insert( obj );
	
	MutablePointerSetIterator<QObject> iter( set );
	iter.toBack();
	
	QVERIFY( iter.previous() == obj );
	QVERIFY( !iter.hasPrevious() );
}

void TestPointerSet::testMutablePointerSetIteratorRemove()
{
	PointerSet<QObject> set;
	set << new QObject( this ) << new QObject( this )
	    << new QObject( this ) << new QObject( this );
	
	QVector< QPointer<QObject> > vector( 4 );
	int i( 0 );
	
	foreach ( QObject* obj, set )
	{
		QVERIFY( i < 4 );
		vector[i] = obj;
		++i;
	}
	
	MutablePointerSetIterator<QObject> iter( set );
	iter.next();
	iter.remove();
	
	QVERIFY( vector[0].isNull() );
	QVERIFY( !vector[1].isNull() );
	QVERIFY( !vector[2].isNull() );
	QVERIFY( !vector[3].isNull() );
	QCOMPARE( set.size(), 3 );
	
	iter.toBack();
	iter.previous();
	iter.remove();
	
	QVERIFY( !vector[1].isNull() );
	QVERIFY( !vector[2].isNull() );
	QVERIFY( vector[3].isNull() );
	QCOMPARE( set.size(), 2 );
	
	iter.findPrevious( vector[1] );
	iter.remove();
	
	QVERIFY( vector[1].isNull() );
	QVERIFY( !vector[2].isNull() );
	QCOMPARE( set.size(), 1 );
	
	iter.findNext( vector[2] );
	iter.remove();
	
	QVERIFY( vector[2].isNull() );
	QCOMPARE( set.size(), 0 );
}

void TestPointerSet::testMutablePointerSetIteratorToBack()
{
	PointerSet<QObject> set;
	set.insert( new QObject( this ) );
	
	MutablePointerSetIterator<QObject> iter( set );
	iter.toBack();
	
	QVERIFY( !iter.hasNext() );
	QVERIFY( iter.hasPrevious() );
}

void TestPointerSet::testMutablePointerSetIteratorToFront()
{
	PointerSet<QObject> set;
	set.insert( new QObject( this ) );
	
	MutablePointerSetIterator<QObject> iter( set );
	iter.toBack();
	iter.toFront();
	
	QVERIFY( !iter.hasPrevious() );
	QVERIFY( iter.hasNext() );
}

void TestPointerSet::testMutablePointerSetIteratorValue()
{
	PointerSet<QObject> set;
	set << new QObject( this ) << new QObject( this )
	    << new QObject( this ) << new QObject( this );
	
	QVector<QObject*> vector( 4 );
	int i( 0 );
	
	foreach ( QObject* obj, set )
	{
		QVERIFY( i < 4 );
		vector[i] = obj;
		++i;
	}
	
	MutablePointerSetIterator<QObject> iter( set );
	iter.next();
	QCOMPARE( iter.value(), vector[0] );
	
	iter.toBack();
	iter.previous();
	QCOMPARE( iter.value(), vector[3] );
	
	iter.findPrevious( vector[1] );
	QCOMPARE( iter.value(), vector[1] );
	
	iter.findNext( vector[2] );
	QCOMPARE( iter.value(), vector[2] );
}

void TestPointerSet::testMutablePointerSetIteratorAssignmentOperator()
{
	QObject* obj1( new QObject( this ) );
	QObject* obj2( new QObject( this ) );
	
	PointerSet<QObject> set1;
	set1.insert( obj1 );
	
	PointerSet<QObject> set2;
	set2.insert( obj2 );
	
	MutablePointerSetIterator<QObject> iter( set1 );
	
	QVERIFY( ( iter = set2 ).next() == obj2 );
}

QTEST_MAIN(TestPointerSet)
