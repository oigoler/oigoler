/**
 *   Copyright (C) 2014 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestFileSystemPathValuable.h"
#include "../FileSystemPathValuable.h"


void TestFileSystemPathValuable::testValuableType()
{
	QCOMPARE( FileSystemPathValuable::valuableType()
	        , QString( "valuable.filesystem.path" ) );
}

void TestFileSystemPathValuable::testEmptyConstructor()
{
	FileSystemPathValuable valuable;
	QCOMPARE( valuable.type(), QString( "valuable.filesystem.path" ) );
	QVERIFY( valuable.address().isEmpty() );
	QVERIFY( valuable.path().isEmpty() );
}

void TestFileSystemPathValuable::testStringConstructor()
{
	FileSystemPathValuable valuable( "." );
	QCOMPARE( valuable.type(), QString( "valuable.filesystem.path" ) );
	QCOMPARE( valuable.address(), QDir::currentPath() );
	QCOMPARE( valuable.path(), QDir::currentPath() );
}

void TestFileSystemPathValuable::testDirConstructor()
{
	FileSystemPathValuable valuable( QDir( "." ) );
	QCOMPARE( valuable.type(), QString( "valuable.filesystem.path" ) );
	QCOMPARE( valuable.address(), QDir::currentPath() );
	QCOMPARE( valuable.path(), QDir::currentPath() );
}

void TestFileSystemPathValuable::testFileConstructor()
{
	FileSystemPathValuable valuable( QFile( "file" ) );
	QCOMPARE( valuable.type(), QString( "valuable.filesystem.path" ) );
	QCOMPARE( valuable.address(), QDir::current().filePath( "file" ) );
	QCOMPARE( valuable.path(), QDir::current().filePath( "file" ) );
}

void TestFileSystemPathValuable::testFileInfoConstructor()
{
	FileSystemPathValuable valuable( QFileInfo( "." ) );
	QCOMPARE( valuable.type(), QString( "valuable.filesystem.path" ) );
	QCOMPARE( valuable.address(), QDir::currentPath() );
	QCOMPARE( valuable.path(), QDir::currentPath() );
}

void TestFileSystemPathValuable::testPath()
{
	FileSystemPathValuable valuable;
	QVERIFY( valuable.address().isEmpty() );
	QVERIFY( valuable.path().isEmpty() );
	
	valuable.setPath( "." );
	QCOMPARE( valuable.address(), QDir::currentPath() );
	QCOMPARE( valuable.path(), QDir::currentPath() );
	
	valuable.setPath( QFile( "file" ) );
	QCOMPARE( valuable.address(), QDir::current().filePath( "file" ) );
	QCOMPARE( valuable.path(), QDir::current().filePath( "file" ) );
	
	valuable.setPath( QDir( "." ) );
	QCOMPARE( valuable.address(), QDir::currentPath() );
	QCOMPARE( valuable.path(), QDir::currentPath() );
	
	valuable.setPath( QFileInfo( "file" ) );
	QCOMPARE( valuable.address(), QDir::current().filePath( "file" ) );
	QCOMPARE( valuable.path(), QDir::current().filePath( "file" ) );
}

void TestFileSystemPathValuable::testType()
{
	FileSystemPathValuable valuable;
	QCOMPARE( valuable.type(), QString( "valuable.filesystem.path" ) );
}

void TestFileSystemPathValuable::testExists()
{
	Valuable* valuable1( new FileSystemPathValuable( ".", this ) );
	Valuable* valuable2( new FileSystemPathValuable( "nonexistent", this ) );
	
	QVERIFY( valuable1->exists() );
	QVERIFY( !valuable2->exists() );
}

QTEST_MAIN(TestFileSystemPathValuable)
