/**
 *   Copyright (C) 2014 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestFSPathValuable.h"
#include "../FSPathValuable.h"


void TestFSPathValuable::testValuableType()
{
	QCOMPARE( FSPathValuable::valuableType()
	        , QString( "valuable.filesystem.path" ) );
}

void TestFSPathValuable::testEmptyConstructor()
{
	FSPathValuable valuable;
	QCOMPARE( valuable.type(), QString( "valuable.filesystem.path" ) );
	QVERIFY( valuable.address().isEmpty() );
	QVERIFY( valuable.path().isEmpty() );
}

void TestFSPathValuable::testStringConstructor()
{
	FSPathValuable valuable( "." );
	QCOMPARE( valuable.type(), QString( "valuable.filesystem.path" ) );
	QCOMPARE( valuable.address(), QDir::currentPath() );
	QCOMPARE( valuable.path(), QDir::currentPath() );
}

void TestFSPathValuable::testDirConstructor()
{
	FSPathValuable valuable( QDir( "." ) );
	QCOMPARE( valuable.type(), QString( "valuable.filesystem.path" ) );
	QCOMPARE( valuable.address(), QDir::currentPath() );
	QCOMPARE( valuable.path(), QDir::currentPath() );
}

void TestFSPathValuable::testFileConstructor()
{
	FSPathValuable valuable( QFile( "file" ) );
	QCOMPARE( valuable.type(), QString( "valuable.filesystem.path" ) );
	QCOMPARE( valuable.address(), QDir::current().filePath( "file" ) );
	QCOMPARE( valuable.path(), QDir::current().filePath( "file" ) );
}

void TestFSPathValuable::testFileInfoConstructor()
{
	FSPathValuable valuable( QFileInfo( "." ) );
	QCOMPARE( valuable.type(), QString( "valuable.filesystem.path" ) );
	QCOMPARE( valuable.address(), QDir::currentPath() );
	QCOMPARE( valuable.path(), QDir::currentPath() );
}

void TestFSPathValuable::testPath()
{
	FSPathValuable valuable;
	QVERIFY( valuable.address().isEmpty() );
	QVERIFY( valuable.path().isEmpty() );
	
	valuable.setPath( "." );
	QCOMPARE( valuable.address(), QDir::currentPath() );
	QCOMPARE( valuable.path(), QDir::currentPath() );
	
	valuable.setPath( QFile( "file" ) );
	QCOMPARE( valuable.address(), QDir::current().filePath( "file" ) );
	QCOMPARE( valuable.path(), QDir::current().filePath( "file" ) );
	
	valuable.setPath( QDir( "." ) );
	QCOMPARE( valuable.address(), QDir::currentPath() );
	QCOMPARE( valuable.path(), QDir::currentPath() );
	
	valuable.setPath( QFileInfo( "file" ) );
	QCOMPARE( valuable.address(), QDir::current().filePath( "file" ) );
	QCOMPARE( valuable.path(), QDir::current().filePath( "file" ) );
}

void TestFSPathValuable::testType()
{
	FSPathValuable valuable;
	QCOMPARE( valuable.type(), QString( "valuable.filesystem.path" ) );
}

void TestFSPathValuable::testExists()
{
	Valuable* valuable1( new FSPathValuable( ".", this ) );
	Valuable* valuable2( new FSPathValuable( "nonexistent", this ) );
	
	QVERIFY( valuable1->exists() );
	QVERIFY( !valuable2->exists() );
}

QTEST_MAIN(TestFSPathValuable)
