/**
 *   Copyright (C) 2014 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestValuableSet.h"
#include "../ValuableSet.h"


class DummyValuable : public Valuable
{
	QString type() const { return QString(); }
	bool exists() const { return false; }
};


void TestValuableSet::testSelfManagement()
{
	QPointer<Valuable> valuable1( new DummyValuable );
	QPointer<Valuable> valuable2( new DummyValuable );
	QPointer<Valuable> valuable3( new DummyValuable );
	
	ValuableSet set;
	set.insert( valuable1 );
	set.insert( valuable2 );
	set.insert( valuable2 );
	
	QCOMPARE( set.size(), 2 );
	QVERIFY( set.contains( valuable1 ) );
	QVERIFY( set.contains( valuable2 ) );
	QVERIFY( !set.contains( valuable3 ) );
	
	set.clear();
	
	QVERIFY( set.isEmpty() );
	QVERIFY( valuable1.isNull() );
	QVERIFY( valuable2.isNull() );
	QVERIFY( !valuable3.isNull() );
	
	delete valuable3;
}

QTEST_MAIN(TestValuableSet)
