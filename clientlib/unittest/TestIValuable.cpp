/**
 *   Copyright (C) 2014 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestIValuable.h"
#include "../IValuable.h"
#include "../Valuable.h"
#include "../FSValuable.h"

Q_DECLARE_METATYPE( IValuable* )


void TestIValuable::testEqualToOperator()
{
	FSValuable fs( "." );
	Valuable v( fs.toValuable() );
	
	QCOMPARE( fs == v, true );
	QCOMPARE( v == fs, true );
	
	v.setType( "type" );
	
	QCOMPARE( fs == v, false );
	QCOMPARE( v == fs, false );
}

void TestIValuable::testNotEqualToOperator()
{
	FSValuable fs( "." );
	Valuable v( fs.toValuable() );
	
	QCOMPARE( fs != v, false );
	QCOMPARE( v != fs, false );
	
	v.setType( "type" );
	
	QCOMPARE( fs != v, true );
	QCOMPARE( v != fs, true );
}

QTEST_MAIN(TestIValuable)
