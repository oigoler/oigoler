/**
 *   Copyright (C) 2014 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef TESTFSVALUABLE_H
#define TESTFSVALUABLE_H

#include <QtTest/QtTest>


class TestFSValuable : public QObject
{
	Q_OBJECT

private slots:
	void testEmptyConstructor();
	
	void testAttributesConstructor_data();
	void testAttributesConstructor();
	
	void testPathSetter_data();
	void testPathSetter();
	
	void testCopyConstructor();
	void testAssignmentOperator();
	
	void testEqualToOperator_data();
	void testEqualToOperator();
	
	void testNotEqualToOperator_data();
	void testNotEqualToOperator();
	
	void testType();
	void testValuableCicleConstructor();
	void testFromOtherValuableTypeConstructor();
	void testValuableCicleAssignment();
	void testFromOtherValuableTypeAssignment();
	
	void testTypeTesting();
};

#endif // TESTFSVALUABLE_H
