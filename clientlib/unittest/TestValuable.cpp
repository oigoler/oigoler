/**
 *   Copyright (C) 2014 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestValuable.h"
#include "../Valuable.h"

Q_DECLARE_METATYPE( Valuable )


void TestValuable::testEmptyConstructor()
{
	Valuable valuable;
	
	QVERIFY( valuable.type().isEmpty() );
	QVERIFY( valuable.address().isEmpty() );
}

void TestValuable::testAttributesConstructor()
{
	Valuable valuable( "type", "address" );
	
	QCOMPARE( valuable.type(), QString( "type" ) );
	QCOMPARE( valuable.address(), QString( "address" ) );
}

void TestValuable::testTypeSetter()
{
	Valuable valuable;
	valuable.setType( "type" );
	
	QCOMPARE( valuable.type(), QString( "type" ) );
}

void TestValuable::testAddressSetter()
{
	Valuable valuable;
	valuable.setAddress( "address" );
	
	QCOMPARE( valuable.address(), QString( "address" ) );
}

void TestValuable::testCopyConstructor()
{
	Valuable valuable1( "type", "address" );
	Valuable valuable2( valuable1 );
	
	QCOMPARE( valuable2.type(), QString( "type" ) );
	QCOMPARE( valuable2.address(), QString( "address" ) );
}

void TestValuable::testAssignmentOperator()
{
	Valuable valuable1( "type", "address" );
	Valuable valuable2;
	Valuable& ref = ( valuable2 = valuable1 );
	
	QVERIFY( &ref == &valuable2 );
	QCOMPARE( valuable2.type(), QString( "type" ) );
	QCOMPARE( valuable2.address(), QString( "address" ) );
}

void TestValuable::testEqualToOperator_data()
{
	QTest::addColumn<Valuable>( "valuable1" );
	QTest::addColumn<Valuable>( "valuable2" );
	QTest::addColumn<bool>( "expected" );
	
	QTest::newRow( "FF" ) << Valuable( "t", "a" )
	                      << Valuable( "o", "o" )
	                      << false;
	
	QTest::newRow( "FT" ) << Valuable( "t", "a" )
	                      << Valuable( "o", "a" )
	                      << false;
	
	QTest::newRow( "TF" ) << Valuable( "t", "a" )
	                      << Valuable( "t", "o" )
	                      << false;
	
	QTest::newRow( "TT" ) << Valuable( "t", "a" )
	                      << Valuable( "t", "a" )
	                      << true;
}

void TestValuable::testEqualToOperator()
{
	QFETCH( Valuable, valuable1 );
	QFETCH( Valuable, valuable2 );
	QFETCH( bool, expected );
	
	QCOMPARE( valuable1 == valuable2, expected );
}

void TestValuable::testNotEqualToOperator_data()
{
	QTest::addColumn<Valuable>( "valuable1" );
	QTest::addColumn<Valuable>( "valuable2" );
	QTest::addColumn<bool>( "expected" );
	
	QTest::newRow( "FF" ) << Valuable( "t", "a" )
	                      << Valuable( "o", "o" )
	                      << true;
	
	QTest::newRow( "FT" ) << Valuable( "t", "a" )
	                      << Valuable( "o", "a" )
	                      << true;
	
	QTest::newRow( "TF" ) << Valuable( "t", "a" )
	                      << Valuable( "t", "o" )
	                      << true;
	
	QTest::newRow( "TT" ) << Valuable( "t", "a" )
	                      << Valuable( "t", "a" )
	                      << false;
}

void TestValuable::testNotEqualToOperator()
{
	QFETCH( Valuable, valuable1 );
	QFETCH( Valuable, valuable2 );
	QFETCH( bool, expected );
	
	QCOMPARE( valuable1 != valuable2, expected );
}

void TestValuable::testToValuable()
{
	Valuable v1( "type", "address" );
	Valuable v2( v1.toValuable() );
	
	QVERIFY( v1 == v2 );
}

QTEST_MAIN(TestValuable)
