/**
 *   Copyright (C) 2013, 2014 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestBackupStrategy.h"
#include "../BackupStrategy.h"


class DummyValuable : public Valuable
{
	QString type() const { return QString(); }
	bool exists() const { return false; }
};


void TestBackupStrategy::testName()
{
	BackupStrategy strategy;
	QVERIFY( strategy.name().isEmpty() );
	
	QString name( "name" );
	strategy.setName( name );
	QCOMPARE( strategy.name(), name );
}

void TestBackupStrategy::testDescription()
{
	BackupStrategy strategy;
	QVERIFY( strategy.description().isEmpty() );
	
	QString description( "description" );
	strategy.setDescription( description );
	QCOMPARE( strategy.description(), description );
}

void TestBackupStrategy::testDataSet()
{
	BackupStrategy strategy;
	ValuableSet valuableSet;
	QVERIFY( strategy.valuableSet() == valuableSet );
	
	valuableSet.insert( new DummyValuable );
	strategy.setValuableSet( valuableSet );
	QCOMPARE( strategy.valuableSet(), valuableSet );
}

void TestBackupStrategy::testSchedule()
{
	BackupStrategy strategy;
	BackupSchedule schedule;
	QVERIFY( strategy.schedule() == schedule );
	
	schedule.setDifferent();
	strategy.setSchedule( schedule );
	QCOMPARE( strategy.schedule(), schedule );
}

void TestBackupStrategy::testStorageSet()
{
	BackupStrategy strategy;
	BackupStorageSet storageSet;
	QVERIFY( strategy.storageSet() == storageSet );
	
	storageSet.setDifferent();
	strategy.setStorageSet( storageSet );
	QCOMPARE( strategy.storageSet(), storageSet );
}

void TestBackupStrategy::testRotationScheme()
{
	BackupStrategy strategy;
	BackupRotationScheme rotationScheme;
	QVERIFY( strategy.rotationScheme() == rotationScheme );
	
	rotationScheme.setDifferent();
	strategy.setRotationScheme( rotationScheme );
	QCOMPARE( strategy.rotationScheme(), rotationScheme );
}

void TestBackupStrategy::testWindow()
{
	BackupStrategy strategy;
	BackupWindow window;
	QVERIFY( strategy.window() == window );
	
	window.setDifferent();
	strategy.setWindow( window );
	QCOMPARE( strategy.window(), window );
}

void TestBackupStrategy::testEqualsOperator()
{
	BackupStrategy strategy1;
	BackupStrategy strategy2;
	QVERIFY( strategy1 == strategy2 );
	
	QString name( "name" );
	QString description( "description" );
	
	ValuableSet valuableSet;
	BackupSchedule schedule;
	BackupStorageSet storageSet;
	BackupRotationScheme rotationScheme;
	BackupWindow window;
	
	valuableSet.insert( new DummyValuable );
	schedule.setDifferent();
	storageSet.setDifferent();
	rotationScheme.setDifferent();
	window.setDifferent();
	
	strategy1.setName( name );
	QVERIFY( !( strategy1 == strategy2 ) );
	
	strategy2.setName( name );
	strategy1.setDescription( description );
	QVERIFY( !( strategy1 == strategy2 ) );
	
	strategy2.setDescription( description );
	strategy1.setValuableSet( valuableSet );
	QVERIFY( !( strategy1 == strategy2 ) );
	
	strategy2.setValuableSet( valuableSet );
	strategy1.setSchedule( schedule );
	QVERIFY( !( strategy1 == strategy2 ) );
	
	strategy2.setSchedule( schedule );
	strategy1.setStorageSet( storageSet );
	QVERIFY( !( strategy1 == strategy2 ) );
	
	strategy2.setStorageSet( storageSet );
	strategy1.setRotationScheme( rotationScheme );
	QVERIFY( !( strategy1 == strategy2 ) );
	
	strategy2.setRotationScheme( rotationScheme );
	strategy1.setWindow( window );
	QVERIFY( !( strategy1 == strategy2 ) );
	
	strategy2.setWindow( window );
	QVERIFY( strategy1 == strategy2 );
}

void TestBackupStrategy::testDifferenceOperator()
{
	BackupStrategy strategy1;
	BackupStrategy strategy2;
	QVERIFY( !( strategy1 != strategy2 ) );
	
	QString name( "name" );
	QString description( "description" );
	
	ValuableSet valuableSet;
	BackupSchedule schedule;
	BackupStorageSet storageSet;
	BackupRotationScheme rotationScheme;
	BackupWindow window;
	
	valuableSet.insert( new DummyValuable );
	schedule.setDifferent();
	storageSet.setDifferent();
	rotationScheme.setDifferent();
	window.setDifferent();
	
	strategy1.setName( name );
	QVERIFY( strategy1 != strategy2 );
	
	strategy2.setName( name );
	strategy1.setDescription( description );
	QVERIFY( strategy1 != strategy2 );
	
	strategy2.setDescription( description );
	strategy1.setValuableSet( valuableSet );
	QVERIFY( strategy1 != strategy2 );
	
	strategy2.setValuableSet( valuableSet );
	strategy1.setSchedule( schedule );
	QVERIFY( strategy1 != strategy2 );
	
	strategy2.setSchedule( schedule );
	strategy1.setStorageSet( storageSet );
	QVERIFY( strategy1 != strategy2 );
	
	strategy2.setStorageSet( storageSet );
	strategy1.setRotationScheme( rotationScheme );
	QVERIFY( strategy1 != strategy2 );
	
	strategy2.setRotationScheme( rotationScheme );
	strategy1.setWindow( window );
	QVERIFY( strategy1 != strategy2 );
	
	strategy2.setWindow( window );
	QVERIFY( !( strategy1 != strategy2 ) );
}

void TestBackupStrategy::testAssignmentOperator()
{
	QString name( "name" );
	QString description( "description" );
	
	ValuableSet valuableSet;
	BackupSchedule schedule;
	BackupStorageSet storageSet;
	BackupRotationScheme rotationScheme;
	BackupWindow window;
	
	BackupStrategy strategy1;
	BackupStrategy strategy2;
	
	strategy1.setName( name );
	strategy1.setDescription( description );
	strategy1.setValuableSet( valuableSet );
	strategy1.setSchedule( schedule );
	strategy1.setStorageSet( storageSet );
	strategy1.setRotationScheme( rotationScheme );
	strategy1.setWindow( window );
	
	strategy2 = strategy1;
	QVERIFY( strategy1 == strategy2 );
}

void TestBackupStrategy::testCopyConstructor()
{
	QString name( "name" );
	QString description( "description" );
	
	ValuableSet valuableSet;
	BackupSchedule schedule;
	BackupStorageSet storageSet;
	BackupRotationScheme rotationScheme;
	BackupWindow window;
	
	BackupStrategy strategy1;
	
	strategy1.setName( name );
	strategy1.setDescription( description );
	strategy1.setValuableSet( valuableSet );
	strategy1.setSchedule( schedule );
	strategy1.setStorageSet( storageSet );
	strategy1.setRotationScheme( rotationScheme );
	strategy1.setWindow( window );
	
	BackupStrategy strategy2( strategy1 );
	QVERIFY( strategy1 == strategy2 );
}

QTEST_MAIN(TestBackupStrategy)
