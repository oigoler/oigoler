/**
 *   Copyright (C) 2014 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestOnDemandSchedule.h"
#include "../OnDemandSchedule.h"


void TestOnDemandSchedule::testCopyConstructor()
{
	OnDemandSchedule schedule1;
	OnDemandSchedule schedule2( schedule1 );
	
	QVERIFY( schedule1.recurrence() == schedule2.recurrence() );
}

void TestOnDemandSchedule::testAssignmentOperator()
{
	OnDemandSchedule schedule1;
	OnDemandSchedule schedule2;
	OnDemandSchedule schedule3;
	
	schedule3 = schedule2 = schedule1;
	
	QVERIFY( schedule1.recurrence() == schedule2.recurrence() );
	QVERIFY( schedule2.recurrence() == schedule3.recurrence() );
}

void TestOnDemandSchedule::testRecurrence()
{
	OnDemandSchedule schedule;
	QVERIFY( schedule.recurrence() == OnDemandSchedule::OnDemand );
}

QTEST_MAIN(TestOnDemandSchedule)
