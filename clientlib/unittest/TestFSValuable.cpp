/**
 *   Copyright (C) 2014 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestFSValuable.h"
#include "../FSValuable.h"
#include "../Valuable.h"

Q_DECLARE_METATYPE( FSValuable )


void TestFSValuable::testEmptyConstructor()
{
	FSValuable valuable;
	QVERIFY( valuable.path().isEmpty() );
}

void TestFSValuable::testAttributesConstructor_data()
{
	QTest::addColumn<QString>( "inpath" );
	QTest::addColumn<QString>( "outpath" );
	
	QTest::newRow( "absolute" ) << QString( "/home" ) << QString( "/home" );
	QTest::newRow( "relative" ) << QString( "." ) << QDir::currentPath();
}

void TestFSValuable::testAttributesConstructor()
{
	QFETCH( QString, inpath );
	QFETCH( QString, outpath );
	
	FSValuable valuable( inpath );
	QCOMPARE( valuable.path(), outpath );
}

void TestFSValuable::testPathSetter_data()
{
	QTest::addColumn<QString>( "inpath" );
	QTest::addColumn<QString>( "outpath" );
	
	QTest::newRow( "absolute" ) << QString( "/home" ) << QString( "/home" );
	QTest::newRow( "relative" ) << QString( "." ) << QDir::currentPath();
}

void TestFSValuable::testPathSetter()
{
	QFETCH( QString, inpath );
	QFETCH( QString, outpath );
	
	FSValuable valuable;
	valuable.setPath( inpath );
	QCOMPARE( valuable.path(), outpath );
}

void TestFSValuable::testCopyConstructor()
{
	FSValuable valuable1( "." );
	FSValuable valuable2( valuable1 );
	
	QCOMPARE( valuable2.path(), QDir::currentPath() );
}

void TestFSValuable::testAssignmentOperator()
{
	FSValuable valuable1( "." );
	FSValuable valuable2;
	FSValuable& ref = ( valuable2 = valuable1 );
	
	QVERIFY( &ref == &valuable2 );
	QCOMPARE( valuable2.path(), QDir::currentPath() );
}

void TestFSValuable::testEqualToOperator_data()
{
	QTest::addColumn<FSValuable>( "valuable1" );
	QTest::addColumn<FSValuable>( "valuable2" );
	QTest::addColumn<bool>( "expected" );
	
	QTest::newRow( "==" ) << FSValuable( "." ) << FSValuable( "." ) << true;
	QTest::newRow( "!=" ) << FSValuable( ".." ) << FSValuable( "." ) << false;
}

void TestFSValuable::testEqualToOperator()
{
	QFETCH( FSValuable, valuable1 );
	QFETCH( FSValuable, valuable2 );
	QFETCH( bool, expected );
	
	QCOMPARE( valuable1 == valuable2, expected );
}

void TestFSValuable::testNotEqualToOperator_data()
{
	QTest::addColumn<FSValuable>( "valuable1" );
	QTest::addColumn<FSValuable>( "valuable2" );
	QTest::addColumn<bool>( "expected" );
	
	QTest::newRow( "==" ) << FSValuable( "." ) << FSValuable( "." ) << false;
	QTest::newRow( "!=" ) << FSValuable( ".." ) << FSValuable( "." ) << true;
}

void TestFSValuable::testNotEqualToOperator()
{
	QFETCH( FSValuable, valuable1 );
	QFETCH( FSValuable, valuable2 );
	QFETCH( bool, expected );
	
	QCOMPARE( valuable1 != valuable2, expected );
}

void TestFSValuable::testType()
{
	FSValuable v1;
	Valuable v2 = v1.toValuable();
	
	QCOMPARE( v2.type(), QString( "valuable.filesystem" ) );
}

void TestFSValuable::testValuableCicleConstructor()
{
	FSValuable v1( "." );
	Valuable v2 = v1.toValuable();
	FSValuable v3( v2 );
	
	QVERIFY( v1 == v3 );
}

void TestFSValuable::testFromOtherValuableTypeConstructor()
{
	Valuable v1( "valuable", "address" );
	FSValuable v2( v1 );
	
	QVERIFY( v2.path().isEmpty() );
}

void TestFSValuable::testValuableCicleAssignment()
{
	FSValuable v1( "." );
	Valuable v2 = v1.toValuable();
	FSValuable v3;
	FSValuable& v4 = ( v3 = v2 );
	
	QVERIFY( &v3 == &v4 );
	QVERIFY( v1 == v3 );
}

void TestFSValuable::testFromOtherValuableTypeAssignment()
{
	Valuable v1( "valuable", "address" );
	FSValuable v2;
	FSValuable& v3 = ( v2 = v1 );
	
	QVERIFY( &v2 == &v3 );
	QVERIFY( v2.path().isEmpty() );
}

void TestFSValuable::testTypeTesting()
{
}

QTEST_MAIN(TestFSValuable)
