/**
 *   Copyright (C) 2014 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestTimeWindow.h"
#include "../TimeWindow.h"


void TestTimeWindow::testEmptyConstructor()
{
	TimeWindow window;
	QCOMPARE( window.from(), QTime( 0, 0 ) );
	QCOMPARE( window.to(), QTime( 23, 59 ) );
}

void TestTimeWindow::testFromToConstructor()
{
	QTime from( 18, 30 );
	QTime to( 19, 15 );
	
	TimeWindow window( from, to );
	
	QCOMPARE( window.from(), from );
	QCOMPARE( window.to(), to );
}

void TestTimeWindow::testCopyConstructor()
{
	QTime from( 8, 5 );
	QTime to( 17, 37 );
	
	TimeWindow window1( from, to );
	TimeWindow window2( window1 );
	
	QVERIFY( window1 == window2 );
}

void TestTimeWindow::testAssignmentOperator()
{
	QTime from( 3, 8 );
	QTime to( 19, 58 );
	
	TimeWindow window1( from, to );
	TimeWindow window2;
	TimeWindow window3;
	
	window3 = window2 = window1;
	
	QVERIFY( window1 == window2 );
	QVERIFY( window1 == window3 );
}

void TestTimeWindow::testEqualToOperator()
{
	TimeWindow window1;
	TimeWindow window2;
	QVERIFY( window1 == window2 );
	
	window1.setFrom( QTime( 1, 0 ) );
	QVERIFY( !( window1 == window2 ) );
	
	window2.setFrom( QTime( 1, 0 ) );
	QVERIFY( window1 == window2 );
	
	window1.setTo( QTime( 23, 47 ) );
	QVERIFY( !( window1 == window2 ) );
	
	window2.setTo( QTime( 23, 47 ) );
	QVERIFY( window1 == window2 );
}

void TestTimeWindow::testNotEqualToOperator()
{
	TimeWindow window1;
	TimeWindow window2;
	QVERIFY( !( window1 != window2 ) );
	
	window1.setFrom( QTime( 1, 0 ) );
	QVERIFY( window1 != window2 );
	
	window2.setFrom( QTime( 1, 0 ) );
	QVERIFY( !( window1 != window2 ) );
	
	window1.setTo( QTime( 23, 47 ) );
	QVERIFY( window1 != window2 );
	
	window2.setTo( QTime( 23, 47 ) );
	QVERIFY( !( window1 != window2 ) );
}

void TestTimeWindow::testFrom()
{
	TimeWindow window;
	QCOMPARE( window.from(), QTime( 0, 0 ) );
	
	window.setFrom( QTime( 18, 10 ) );
	QCOMPARE( window.from(), QTime( 18, 10 ) );
}

void TestTimeWindow::testTo()
{
	TimeWindow window;
	QCOMPARE( window.to(), QTime( 23, 59 ) );
	
	window.setTo( QTime( 18, 10 ) );
	QCOMPARE( window.to(), QTime( 18, 10 ) );
}

void TestTimeWindow::testAfter()
{
	TimeWindow window( TimeWindow::after( QTime( 18, 10 ) ) );
	QCOMPARE( window.from(), QTime( 18, 10 ) );
	QCOMPARE( window.to(), QTime( 23, 59 ) );
}

void TestTimeWindow::testBefore()
{
	TimeWindow window( TimeWindow::before( QTime( 8, 50 ) ) );
	QCOMPARE( window.from(), QTime( 0, 0 ) );
	QCOMPARE( window.to(), QTime( 8, 50 ) );
}

QTEST_MAIN(TestTimeWindow)
