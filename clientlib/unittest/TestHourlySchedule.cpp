/**
 *   Copyright (C) 2014 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestHourlySchedule.h"
#include "../HourlySchedule.h"


void TestHourlySchedule::testCopyConstructor()
{
	HourlySchedule schedule1;
	schedule1.setMinimumInterval( 5 );
	
	HourlySchedule schedule2( schedule1 );
	QCOMPARE( schedule2.minimumInterval(), 5 );
}

void TestHourlySchedule::testAssignmentOperator()
{
	HourlySchedule schedule1;
	schedule1.setMinimumInterval( 4 );
	
	HourlySchedule schedule2;
	HourlySchedule schedule3;
	
	schedule3 = schedule2 = schedule1;
	
	QCOMPARE( schedule2.minimumInterval(), 4 );
	QCOMPARE( schedule3.minimumInterval(), 4 );
}

void TestHourlySchedule::testRecurrence()
{
	HourlySchedule schedule;
	QVERIFY( schedule.recurrence() == HourlySchedule::Hourly );
}

void TestHourlySchedule::testMinimumInterval()
{
	HourlySchedule schedule;
	QCOMPARE( schedule.minimumInterval(), 1 );
	
	schedule.setMinimumInterval( 3 );
	QCOMPARE( schedule.minimumInterval(), 3 );
}

QTEST_MAIN(TestHourlySchedule)
