/**
 *   Copyright (C) 2014 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef IVALUABLE_H
#define IVALUABLE_H

class Valuable;


class IValuable
{
	
public:
	virtual ~IValuable() {}
	virtual Valuable toValuable() const =0;
	
};


bool operator==( const IValuable& valuable1, const IValuable& valuable2 );
bool operator!=( const IValuable& valuable1, const IValuable& valuable2 );

#endif // IVALUABLE_H
