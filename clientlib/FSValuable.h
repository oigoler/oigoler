/**
 *   Copyright (C) 2014 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef FSVALUABLE_H
#define FSVALUABLE_H

#include "IValuable.h"

class FSValuablePrivate;
class QString;
class Valuable;


class FSValuable : public IValuable
{
	
public:
	FSValuable();
	FSValuable( const QString& path );
	FSValuable( const FSValuable& other );
	explicit FSValuable( const Valuable& valuable );
	virtual ~FSValuable();
	
	FSValuable& operator=( const FSValuable& other );
	FSValuable& operator=( const Valuable& other );
	bool operator==( const FSValuable& other ) const;
	bool operator!=( const FSValuable& other ) const;
	
	QString path() const;
	void setPath( const QString& path );
	
	Valuable toValuable() const;
	
	static bool testType( const Valuable& valuable );
	
private:
	FSValuablePrivate* const d;
	
};

#endif // FSVALUABLE_H
