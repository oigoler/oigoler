/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "BackupWindow.h"


BackupWindow::BackupWindow()
	: m_flag( true )
{
}

BackupWindow::BackupWindow( const BackupWindow& other )
 	: m_flag( other.m_flag )
{
}

BackupWindow::~BackupWindow()
{
}

BackupWindow& BackupWindow::operator=( const BackupWindow& other )
{
	if ( this == &other )
		return *this;
	
	m_flag = other.m_flag;

	return *this;
}

bool BackupWindow::operator==( const BackupWindow& other ) const
{
	return m_flag == other.m_flag;
}

bool BackupWindow::operator!=( const BackupWindow& other ) const
{
	return m_flag != other.m_flag;
}

void BackupWindow::setDifferent()
{
	m_flag = false;
}
