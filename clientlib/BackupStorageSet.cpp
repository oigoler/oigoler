/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "BackupStorageSet.h"


BackupStorageSet::BackupStorageSet()
	: m_flag( true )
{
}

BackupStorageSet::BackupStorageSet( const BackupStorageSet& other )
 	: m_flag( other.m_flag )
{
}

BackupStorageSet::~BackupStorageSet()
{
}

BackupStorageSet& BackupStorageSet::operator=( const BackupStorageSet& other )
{
	if ( this == &other )
		return *this;
	
	m_flag = other.m_flag;

	return *this;
}

bool BackupStorageSet::operator==( const BackupStorageSet& other ) const
{
	return m_flag == other.m_flag;
}

bool BackupStorageSet::operator!=( const BackupStorageSet& other ) const
{
	return m_flag != other.m_flag;
}

void BackupStorageSet::setDifferent()
{
	m_flag = false;
}
