/**
 *   Copyright (C) 2013, 2014 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef MUTABLEPOINTERSETITERATOR_H
#define MUTABLEPOINTERSETITERATOR_H

#include "PointerSet.h"

#include <QMutableSetIterator>


template <class T>
class MutablePointerSetIterator
{
	
public:
	inline MutablePointerSetIterator( PointerSet<T>& set );
	inline virtual ~MutablePointerSetIterator();
	
	inline bool findNext( T* value );
	inline bool findPrevious( T* value );
	inline bool hasNext() const;
	inline bool hasPrevious() const;
	inline T* next();
	inline T* peekNext() const;
	inline T* peekPrevious() const;
	inline T* previous();
	inline void remove();
	inline void toBack();
	inline void toFront();
	inline T* value() const;
	
	inline MutablePointerSetIterator& operator=( PointerSet<T>& set );
	
private:
	QMutableSetIterator<T*> m_iter;
	PointerSet<T>* m_set;
	
};


template <class T>
MutablePointerSetIterator<T>::MutablePointerSetIterator( PointerSet<T>& set )
	: m_iter( set.m_set )
	, m_set( &set )
{
}


template <class T>
MutablePointerSetIterator<T>::~MutablePointerSetIterator()
{
}


template <class T>
bool MutablePointerSetIterator<T>::findNext( T* value )
{
	return m_iter.findNext( value );
}


template <class T>
bool MutablePointerSetIterator<T>::findPrevious( T* value )
{
	return m_iter.findPrevious( value );
}


template <class T>
bool MutablePointerSetIterator<T>::hasNext() const
{
	return m_iter.hasNext();
}


template <class T>
bool MutablePointerSetIterator<T>::hasPrevious() const
{
	return m_iter.hasPrevious();
}


template <class T>
T* MutablePointerSetIterator<T>::next()
{
	return m_iter.next();
}


template <class T>
T* MutablePointerSetIterator<T>::peekNext() const
{
	return m_iter.peekNext();
}


template <class T>
T* MutablePointerSetIterator<T>::peekPrevious() const
{
	return m_iter.peekPrevious();
}


template <class T>
T* MutablePointerSetIterator<T>::previous()
{
	return m_iter.previous();
}


template <class T>
void MutablePointerSetIterator<T>::remove()
{
	T* obj( m_iter.value() );
	m_iter.remove();
	
	typename QList< QSharedPointer<T> >::iterator iter( m_set->m_list.begin() );
	
	while ( iter != m_set->m_list.end() )
	{
		if ( iter->data() == obj )
		{
			m_set->m_list.erase( iter );
			break;
		}
		else
		{
			++iter;
		}
	}
}


template <class T>
void MutablePointerSetIterator<T>::toBack()
{
	m_iter.toBack();
}


template <class T>
void MutablePointerSetIterator<T>::toFront()
{
	m_iter.toFront();
}


template <class T>
inline T* MutablePointerSetIterator<T>::value() const
{
	return m_iter.value();
}


template <class T>
MutablePointerSetIterator<T>&
MutablePointerSetIterator<T>::operator=( PointerSet<T>& set )
{
	m_iter = set.m_set;
	m_set  = &set;
	return *this;
}


#endif // MUTABLEPOINTERSETITERATOR_H
