/**
 *   Copyright (C) 2014 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef ONDEMANDSCHEDULE_H
#define ONDEMANDSCHEDULE_H


class OnDemandSchedulePrivate;


class OnDemandSchedule
{
public:
	enum Recurrence { OnDemand, TestRecurrence };
	
	OnDemandSchedule();
	OnDemandSchedule( const OnDemandSchedule& other );
	virtual ~OnDemandSchedule();
	
	OnDemandSchedule& operator=( const OnDemandSchedule& other );
	
	Recurrence recurrence() const;
	
private:
	OnDemandSchedulePrivate* const d;
	
};

#endif // ONDEMANDSCHEDULE_H
