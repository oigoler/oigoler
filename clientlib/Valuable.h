/**
 *   Copyright (C) 2014 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef VALUABLE_H
#define VALUABLE_H

#include "IValuable.h"

class ValuablePrivate;
class QString;


class Valuable : public IValuable
{
	
public:
	Valuable();
	Valuable( const QString& type, const QString& address );
	Valuable( const Valuable& other );
	virtual ~Valuable();
	
	Valuable& operator=( const Valuable& other );
	bool operator==( const Valuable& other ) const;
	bool operator!=( const Valuable& other ) const;
	
	QString type() const;
	void setType( const QString& type );
	
	QString address() const;
	void setAddress( const QString& address );
	
	Valuable toValuable() const;
	
private:
	ValuablePrivate* const d;
	
};

#endif // VALUABLE_H
