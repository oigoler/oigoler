/**
 *   Copyright (C) 2014 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef TIMEWINDOW_H
#define TIMEWINDOW_H


class TimeWindowPrivate;
class QTime;


class TimeWindow
{
public:
	TimeWindow();
	TimeWindow( const QTime& from, const QTime& to );
	TimeWindow( const TimeWindow& other );
	virtual ~TimeWindow();
	
	TimeWindow& operator=( const TimeWindow& other );
	bool operator==( const TimeWindow& other ) const;
	bool operator!=( const TimeWindow& other ) const;
	
	QTime from() const;
	QTime to() const;
	void setFrom( const QTime& time );
	void setTo( const QTime& time );
	
	static TimeWindow after( const QTime& time );
	static TimeWindow before( const QTime& time );
	
private:
	TimeWindowPrivate* const d;
	
};

#endif // TIMEWINDOW_H
