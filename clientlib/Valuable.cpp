/**
 *   Copyright (C) 2014 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "Valuable.h"

#include <QString>


struct ValuablePrivate
{
	QString type;
	QString address;
	
	ValuablePrivate() {}
	ValuablePrivate( const QString& type, const QString& address )
		: type( type ), address( address ) {}
	ValuablePrivate( const Valuable& v )
		: type( v.type() ), address( v.address() ) {}
		
	ValuablePrivate& operator=( const ValuablePrivate* other )
	{
		type    = other->type;
		address = other->address;
	}
	
	bool operator==( const ValuablePrivate* other ) const
	{
		return type    == other->type
		    && address == other->address;
	}
	
	bool operator!=( const ValuablePrivate* other ) const
	{
		return type    != other->type
		    || address != other->address;
	}
};


Valuable::Valuable()
	: d( new ValuablePrivate )
{
}

Valuable::Valuable( const QString& type, const QString& address )
	: d( new ValuablePrivate( type, address ) )
{
}

Valuable::Valuable( const Valuable& other )
	: d( new ValuablePrivate( other ) )
{
}

Valuable::~Valuable()
{
	delete d;
}

Valuable& Valuable::operator=( const Valuable& other )
{
	if ( this != &other )
		*d = other.d;
	
	return *this;
}

bool Valuable::operator==( const Valuable& other ) const
{
	return *d == other.d;
}

bool Valuable::operator!=( const Valuable& other ) const
{
	return *d != other.d;
}

QString Valuable::type() const
{
	return d->type;
}

void Valuable::setType( const QString& type )
{
	d->type = type;
}

QString Valuable::address() const
{
	return d->address;
}

void Valuable::setAddress( const QString& address )
{
	d->address = address;
}

Valuable Valuable::toValuable() const
{
	return *this;
}
