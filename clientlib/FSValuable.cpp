/**
 *   Copyright (C) 2014 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "FSValuable.h"
#include "Valuable.h"

#include <QString>
#include <QFileInfo>


struct FSValuablePrivate
{
	static const QString type;
	
	QString path;
	
	FSValuablePrivate() {}
	FSValuablePrivate( const QString& path ) : path( path ) {}
	FSValuablePrivate( const FSValuable& v ): path( v.path() ) {}
	
	FSValuablePrivate( const Valuable& v )
	{
		fromValuable( v );
	}
		
	FSValuablePrivate& operator=( const FSValuablePrivate* other )
	{
		path = other->path;
	}
	
	bool operator==( const FSValuablePrivate* other ) const
	{
		return path == other->path;
	}
	
	bool operator!=( const FSValuablePrivate* other ) const
	{
		return path != other->path;
	}
	
	void fromValuable( const Valuable& v )
	{
		if ( v.type() == type )
			path = v.address();
	}
};

const QString FSValuablePrivate::type( "valuable.filesystem" );


FSValuable::FSValuable()
	: d( new FSValuablePrivate )
{
}

FSValuable::FSValuable( const QString& path )
	: d( new FSValuablePrivate( QFileInfo( path ).absoluteFilePath() ) )
{
}

FSValuable::FSValuable( const FSValuable& other )
	: d( new FSValuablePrivate( other ) )
{
}

FSValuable::FSValuable( const Valuable& valuable )
	: d( new FSValuablePrivate( valuable ) )
{
}

FSValuable::~FSValuable()
{
	delete d;
}

FSValuable& FSValuable::operator=( const FSValuable& other )
{
	if ( this != &other )
		*d = other.d;
	
	return *this;
}

FSValuable& FSValuable::operator=( const Valuable& other )
{
	d->fromValuable( other );
	return *this;
}

bool FSValuable::operator==( const FSValuable& other ) const
{
	return *d == other.d;
}

bool FSValuable::operator!=( const FSValuable& other ) const
{
	return *d != other.d;
}

QString FSValuable::path() const
{
	return d->path;
}

void FSValuable::setPath( const QString& path )
{
	QFileInfo info( path );
	d->path = info.absoluteFilePath();
}

Valuable FSValuable::toValuable() const
{
	return Valuable( FSValuablePrivate::type, d->path );
}

bool FSValuable::testType( const Valuable& valuable )
{
	return false;
}
