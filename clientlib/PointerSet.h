/**
 *   Copyright (C) 2013, 2014 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef POINTERSET_H
#define POINTERSET_H

#include <QSet>
#include <QSharedPointer>
#include <QList>


template <class T> class PointerSetIterator;
template <class T> class MutablePointerSetIterator;


template <class T>
class PointerSet
{
	friend class PointerSetIterator<T>;
	friend class MutablePointerSetIterator<T>;
	
public:
	class const_iterator;
	class iterator;
	
	inline PointerSet();
	inline PointerSet( const PointerSet<T>& other );
	inline virtual ~PointerSet();
	
	inline const_iterator begin() const;
	inline iterator begin();
	inline int capacity() const;
	inline void clear();
	inline const_iterator constBegin() const;
	inline const_iterator constEnd() const;
	inline const_iterator constFind( T* value ) const;
	inline bool contains( T* value ) const;
	inline bool contains( const PointerSet<T>& other ) const;
	inline int count() const;
	inline bool empty() const;
	inline const_iterator end() const;
	inline iterator end();
	inline iterator erase( iterator pos );
	inline const_iterator find( T* value ) const;
	inline iterator find( T* value );
	inline const_iterator insert( T* value );
	inline PointerSet<T>& intersect( const PointerSet<T>& other );
	inline bool isEmpty() const;
	inline bool remove( T* value );
	inline void reserve( int size );
	inline int size() const;
	inline void squeeze();
	inline PointerSet<T>& subtract( const PointerSet<T>& other );
	inline void swap( PointerSet<T>& other );
	inline PointerSet<T>& unite( const PointerSet<T>& other );
	
	inline bool operator!=( const PointerSet<T>& other ) const;
	inline PointerSet<T> operator&( const PointerSet<T>& other ) const;
	inline PointerSet<T>& operator&=( const PointerSet<T>& other );
	inline PointerSet<T>& operator&=( T* value );
	inline PointerSet<T> operator+( const PointerSet<T>& other ) const;
	inline PointerSet<T>& operator+=( const PointerSet<T>& other );
	inline PointerSet<T>& operator+=( T* value );
	inline PointerSet<T> operator-( const PointerSet<T>& other ) const;
	inline PointerSet<T>& operator-=( const PointerSet<T>& other );
	inline PointerSet<T>& operator-=( T* value );
	inline PointerSet<T>& operator<<( T* value );
	inline PointerSet<T>& operator=( const PointerSet<T>& other );
	inline bool operator==( const PointerSet<T>& other ) const;
	inline PointerSet<T> operator|( const PointerSet<T>& other ) const;
	inline PointerSet<T>& operator|=( const PointerSet<T>& other );
	inline PointerSet<T>& operator|=( T* value );
	
	
	class const_iterator
	{
		friend class PointerSet<T>;
		
	public:
		typedef std::bidirectional_iterator_tag iterator_category;
		
		inline const_iterator();
		inline const_iterator( const const_iterator& other );
		inline const_iterator( const iterator& other );
	private:
		inline const_iterator( const typename QSet<T*>::const_iterator& iter );
	public:
		inline virtual ~const_iterator();
		
		inline bool operator!=( const const_iterator& other ) const;
		inline T* operator*() const;
		inline const_iterator operator+( int j ) const;
		inline const_iterator& operator++();
		inline const_iterator operator++( int );
		inline const_iterator& operator+=( int j );
		inline const_iterator operator-( int j ) const;
		inline const_iterator& operator--();
		inline const_iterator operator--( int );
		inline const_iterator& operator-=( int j );
		inline T* operator->() const;
		inline const_iterator& operator=( const const_iterator& other );
		inline bool operator==( const const_iterator& other ) const;
		
	private:
		typename QSet<T*>::const_iterator m_iter;
		
	};
	
	
	class iterator
	{
		friend class PointerSet<T>;
		
	public:
		typedef std::bidirectional_iterator_tag iterator_category;
		
		inline iterator();
		inline iterator( const iterator& other );
	private:
		inline iterator( const typename QSet<T*>::iterator& iter );
	public:
		inline virtual ~iterator();
		
		inline bool operator!=( const iterator& other ) const;
		inline bool operator!=( const const_iterator& other ) const;
		inline T* operator*() const;
		inline iterator operator+( int j ) const;
		inline iterator& operator++();
		inline iterator operator++( int );
		inline iterator& operator+=( int j );
		inline iterator operator-( int j ) const;
		inline iterator& operator--();
		inline iterator operator--( int );
		inline iterator& operator-=( int j );
		inline T* operator->() const;
		inline iterator& operator=( const iterator& other );
		inline bool operator==( const iterator& other ) const;
		inline bool operator==( const const_iterator& other ) const;
		
	private:
		typename QSet<T*>::iterator m_iter;
		
	};
	
	
private:
	QSet<T*> m_set;
	QList< QSharedPointer<T> > m_list;
	
};


#include "PointerSet_impl.h"
#include "PointerSet_const_iterator_impl.h"
#include "PointerSet_iterator_impl.h"


#endif // POINTERSET_H
