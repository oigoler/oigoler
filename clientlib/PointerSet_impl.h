/**
 *   Copyright (C) 2013, 2014 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef POINTERSET_IMPL_H
#define POINTERSET_IMPL_H


template <class T>
PointerSet<T>::PointerSet()
{
}


template <class T>
PointerSet<T>::PointerSet( const PointerSet<T>& other )
	: m_set( other.m_set )
	, m_list( other.m_list )
{
}


template <class T>
PointerSet<T>::~PointerSet()
{
}


template <class T>
typename PointerSet<T>::const_iterator
PointerSet<T>::begin() const
{
	return const_iterator( m_set.constBegin() );
}


template <class T>
typename PointerSet<T>::iterator PointerSet<T>::begin()
{
	return iterator( m_set.begin() );
}


template <class T>
int PointerSet<T>::capacity() const
{
	return m_set.capacity();
}


template <class T>
void PointerSet<T>::clear()
{
	m_set.clear();
	m_list.clear();
}


template <class T>
typename PointerSet<T>::const_iterator PointerSet<T>::constBegin() const
{
	return const_iterator( m_set.constBegin() );
}


template <class T>
typename PointerSet<T>::const_iterator PointerSet<T>::constEnd() const
{
	return const_iterator( m_set.constEnd() );
}


template <class T>
typename PointerSet<T>::const_iterator
PointerSet<T>::constFind( T* value ) const
{
	return const_iterator( m_set.constFind( value ) );
}


template <class T>
bool PointerSet<T>::contains( T* value ) const
{
	return m_set.contains( value );
}


template <class T>
bool PointerSet<T>::contains( const PointerSet<T>& other ) const
{
	return m_set.contains( other.m_set );
}


template <class T>
int PointerSet<T>::count() const
{
	return m_set.count();
}


template <class T>
bool PointerSet<T>::empty() const
{
	return m_set.empty();
}


template <class T>
typename PointerSet<T>::const_iterator
PointerSet<T>::end() const
{
	return const_iterator( m_set.constEnd() );
}


template <class T>
typename PointerSet<T>::iterator PointerSet<T>::end()
{
	return iterator( m_set.end() );
}


template <class T>
typename PointerSet<T>::iterator PointerSet<T>::erase( iterator pos )
{
	T* obj( *pos );
	pos = iterator( m_set.erase( pos.m_iter ) );
	
	typename QList< QSharedPointer<T> >::iterator iter;
	
	for ( iter = m_list.begin(); iter != m_list.end(); ++iter )
	{
		if ( iter->data() == obj )
		{
			m_list.erase( iter );
			break;
		}
	}
	
	return pos;
}


template <class T>
typename PointerSet<T>::const_iterator PointerSet<T>::find( T* value ) const
{
	return const_iterator( m_set.constFind( value ) );
}


template <class T>
typename PointerSet<T>::iterator PointerSet<T>::find( T* value )
{
	return iterator( m_set.find( value ) );
}


template <class T>
typename PointerSet<T>::const_iterator PointerSet<T>::insert( T* value )
{
	if ( !m_set.contains( value ) )
	{
		PointerSet<T>::const_iterator ret( m_set.insert( value ) );
		m_list.append( QSharedPointer<T>( value ) );
		return ret;
	}
	
	return const_iterator( m_set.constFind( value ) );
}


template <class T>
PointerSet<T>& PointerSet<T>::intersect( const PointerSet<T>& other )
{
	iterator iter( begin() );
	
	while ( iter != end() )
	{
		if ( other.contains( *iter ) )
			++iter;
		else
			iter = erase( iter );
	}
	
	return *this;
}


template <class T>
bool PointerSet<T>::isEmpty() const
{
	return m_set.isEmpty();
}


template <class T>
bool PointerSet<T>::remove( T* value )
{
	if ( m_set.remove( value ) )
	{
		typename QList< QSharedPointer<T> >::iterator iter;
		
		for ( iter = m_list.begin(); iter != m_list.end(); ++iter )
		{
			if ( iter->data() == value )
			{
				m_list.erase( iter );
				break;
			}
		}
		
		return true;
	}
	else
	{
		return false;
	}
}


template <class T>
void PointerSet<T>::reserve( int size )
{
	m_set.reserve( size );
}


template <class T>
int PointerSet<T>::size() const
{
	return m_set.size();
}


template <class T>
void PointerSet<T>::squeeze()
{
	m_set.squeeze();
}


template <class T>
PointerSet<T>& PointerSet<T>::subtract( const PointerSet<T>& other )
{
	foreach ( T* obj, other.m_set )
		remove( obj );
	
	return *this;
}


template <class T>
void PointerSet<T>::swap( PointerSet<T>& other )
{
	m_set.swap( other.m_set );
	m_list.swap( other.m_list );
}


template <class T>
PointerSet<T>& PointerSet<T>::unite( const PointerSet<T>& other )
{
	foreach ( QSharedPointer<T> ptr, other.m_list )
	{
		if ( !m_set.contains( ptr.data() ) )
		{
			m_list.append( ptr );
			m_set.insert( ptr.data() );
		}
	}
	
	return *this;
}


template <class T>
bool PointerSet<T>::operator!=( const PointerSet<T>& other ) const
{
	return m_set != other.m_set;
}


template <class T>
PointerSet<T> PointerSet<T>::operator&( const PointerSet<T>& other ) const
{
	PointerSet<T> ret;
	
	foreach ( QSharedPointer<T> ptr, m_list )
	{
		if ( other.contains( ptr.data() ) )
		{
			ret.m_set.insert( ptr.data() );
			ret.m_list.append( ptr );
		}
	}
	
	return ret;
}


template <class T>
PointerSet<T>& PointerSet<T>::operator&=( const PointerSet<T>& other )
{
	return intersect( other );
}


template <class T>
PointerSet<T>& PointerSet<T>::operator&=( T* value )
{
	if ( m_set.contains( value ) )
	{
		foreach ( QSharedPointer<T> ptr, m_list )
		{
			if ( ptr.data() == value )
			{
				m_set.clear();
				m_set.insert( ptr.data() );
				m_list.clear();
				m_list.append( ptr );
				break;
			}
		}
	}
	else
	{
		m_set.clear();
		m_list.clear();
	}
	
	return *this;
}


template <class T>
PointerSet<T> PointerSet<T>::operator+( const PointerSet<T>& other ) const
{
	PointerSet<T> ret( other );
	
	foreach ( QSharedPointer<T> ptr, m_list )
	{
		if ( !ret.contains( ptr.data() ) )
		{
			ret.m_set.insert( ptr.data() );
			ret.m_list.append( ptr );
		}
	}
	
	return ret;
}


template <class T>
PointerSet<T>& PointerSet<T>::operator+=( const PointerSet<T>& other )
{
	return unite( other );
}


template <class T>
PointerSet<T>& PointerSet<T>::operator+=( T* value )
{
	insert( value );
	return *this;
}


template <class T>
PointerSet<T> PointerSet<T>::operator-( const PointerSet<T>& other ) const
{
	PointerSet<T> ret( *this );
	ret.subtract( other );
	return ret;
}


template <class T>
PointerSet<T>& PointerSet<T>::operator-=( const PointerSet<T>& other )
{
	return subtract( other );
}


template <class T>
PointerSet<T>& PointerSet<T>::operator-=( T* value )
{
	remove( value );
	return *this;
}


template <class T>
PointerSet<T>& PointerSet<T>::operator<<( T* value )
{
	if ( !m_set.contains( value ) )
	{
		m_set.insert( value );
		m_list.append( QSharedPointer<T>( value ) );
	}
	
	return *this;
}


template <class T>
PointerSet<T>& PointerSet<T>::operator=( const PointerSet<T>& other )
{
	if ( &other == this )
		return *this;
	
	m_set  = other.m_set;
	m_list = other.m_list;
	
	return *this;
}


template <class T>
bool PointerSet<T>::operator==( const PointerSet<T>& other ) const
{
	return m_set == other.m_set;
}


template <class T>
PointerSet<T> PointerSet<T>::operator|( const PointerSet<T>& other ) const
{
	PointerSet<T> ret( *this );
	ret.unite( other );
	return ret;
}


template <class T>
PointerSet<T>& PointerSet<T>::operator|=( const PointerSet<T>& other )
{
	return unite( other );
}


template <class T>
PointerSet<T>& PointerSet<T>::operator|=( T* value )
{
	insert( value );
	return *this;
}


#endif // POINTERSET_IMPL_H
