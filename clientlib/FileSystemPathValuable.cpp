/**
 *   Copyright (C) 2014 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "FileSystemPathValuable.h"

#include <QFileInfo>
#include <QDir>


FileSystemPathValuable::FileSystemPathValuable( QObject* parent )
	: Valuable( parent )
	, d( 0 )
{
}

FileSystemPathValuable::FileSystemPathValuable( const QString& path
                                              , QObject* parent )
	: Valuable( parent )
	, d( 0 )
{
	setPath( path );
}

FileSystemPathValuable::FileSystemPathValuable( const QDir& dir
                                              , QObject* parent )
	: Valuable( parent )
	, d( 0 )
{
	setPath( dir );
}

FileSystemPathValuable::FileSystemPathValuable( const QFile& file
                                              , QObject* parent )
	: Valuable( parent )
	, d( 0 )
{
	setPath( file );
}

FileSystemPathValuable::FileSystemPathValuable( const QFileInfo& info
                                              , QObject* parent )
	: Valuable( parent )
	, d( 0 )
{
	setPath( info );
}

FileSystemPathValuable::~FileSystemPathValuable()
{
}

QString FileSystemPathValuable::type() const
{
	return valuableType();
}

QString FileSystemPathValuable::path() const
{
	return address();
}

void FileSystemPathValuable::setPath( const QString& path )
{
	setPath( QFileInfo( path ) );
}

void FileSystemPathValuable::setPath( const QDir& dir )
{
	setAddress( dir.absolutePath() );
}

void FileSystemPathValuable::setPath( const QFile& file )
{
	setPath( QFileInfo( file.fileName() ) );
}

void FileSystemPathValuable::setPath( const QFileInfo& info )
{
	setAddress( info.absoluteFilePath() );
}

bool FileSystemPathValuable::exists() const
{
	return QFileInfo( address() ).exists();
}

QString FileSystemPathValuable::valuableType() // static
{
	return QString( "valuable.filesystem.path" );
}
