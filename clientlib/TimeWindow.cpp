/**
 *   Copyright (C) 2014 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TimeWindow.h"

#include <QTime>


struct TimeWindowPrivate
{
	QTime from;
	QTime to;
	
	TimeWindowPrivate( const QTime& from, const QTime& to )
		: from( from ), to( to ) {}
};


TimeWindow::TimeWindow()
	: d( new TimeWindowPrivate( QTime( 0, 0 ), QTime( 23, 59 ) ) )
{
}

TimeWindow::TimeWindow( const QTime& from, const QTime& to )
	: d( new TimeWindowPrivate( from, to ) )
{
}

TimeWindow::TimeWindow( const TimeWindow& other )
	: d( new TimeWindowPrivate( other.d->from, other.d->to ) )
{
}

TimeWindow::~TimeWindow()
{
	delete d;
}

TimeWindow& TimeWindow::operator=( const TimeWindow& other )
{
	if ( this == &other )
		return *this;
	
	d->from = other.d->from;
	d->to   = other.d->to;
	
	return *this;
}

bool TimeWindow::operator==( const TimeWindow& other ) const
{
	return d->from == other.d->from
	    && d->to   == other.d->to;
}

bool TimeWindow::operator!=( const TimeWindow& other ) const
{
	return d->from != other.d->from
	    || d->to   != other.d->to;
}

QTime TimeWindow::from() const
{
	return d->from;
}

QTime TimeWindow::to() const
{
	return d->to;
}

void TimeWindow::setFrom( const QTime& time )
{
	d->from = time;
}

void TimeWindow::setTo( const QTime& time )
{
	d->to = time;
}

TimeWindow TimeWindow::after( const QTime& time )
{
	return TimeWindow( time, QTime( 23, 59 ) );
}

TimeWindow TimeWindow::before( const QTime& time )
{
	return TimeWindow( QTime( 0, 0 ), time );
}
