/**
 *   Copyright (C) 2013, 2014 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef POINTERSET_CONST_ITERATOR_IMPL_H
#define POINTERSET_CONST_ITERATOR_IMPL_H


template <class T>
PointerSet<T>::const_iterator::const_iterator()
{
}


template <class T>
PointerSet<T>::const_iterator::const_iterator( const const_iterator& other )
	: m_iter( other.m_iter )
{
}


template <class T>
PointerSet<T>::const_iterator::const_iterator( const iterator& other )
	: m_iter( other.m_iter )
{
}


template <class T>
PointerSet<T>::const_iterator::
const_iterator(	const typename QSet<T*>::const_iterator& iter )
	: m_iter( iter )
{
}


template <class T>
PointerSet<T>::const_iterator::~const_iterator()
{
}


template <class T>
bool
PointerSet<T>::const_iterator::operator!=( const const_iterator& other ) const
{
	return m_iter != other.m_iter;
}


template <class T>
T* PointerSet<T>::const_iterator::operator*() const
{
	return *m_iter;
}


template <class T>
typename PointerSet<T>::const_iterator
PointerSet<T>::const_iterator::operator+( int j ) const
{
	return const_iterator( m_iter + j );
}


template <class T>
typename PointerSet<T>::const_iterator&
PointerSet<T>::const_iterator::operator++()
{
	++m_iter;
	return *this;
}


template <class T>
typename PointerSet<T>::const_iterator
PointerSet<T>::const_iterator::operator++( int )
{
	const_iterator ret( *this );
	++m_iter;
	return ret;
}


template <class T>
typename PointerSet<T>::const_iterator&
PointerSet<T>::const_iterator::operator+=( int j )
{
	m_iter += j;
	return *this;
}


template <class T>
typename PointerSet<T>::const_iterator
PointerSet<T>::const_iterator::operator-( int j ) const
{
	return const_iterator( m_iter - j );
}


template <class T>
typename PointerSet<T>::const_iterator&
PointerSet<T>::const_iterator::operator--()
{
	--m_iter;
	return *this;
}


template <class T>
typename PointerSet<T>::const_iterator
PointerSet<T>::const_iterator::operator--( int )
{
	const_iterator ret( *this );
	--m_iter;
	return ret;
}


template <class T>
typename PointerSet<T>::const_iterator&
PointerSet<T>::const_iterator::operator-=( int j )
{
	m_iter -= j;
	return *this;
}


template <class T>
T* PointerSet<T>::const_iterator::operator->() const
{
	return *m_iter;
}


template <class T>
typename PointerSet<T>::const_iterator&
PointerSet<T>::const_iterator::operator=( const const_iterator& other )
{
	if ( &other == this )
		return *this;
	
	m_iter = other.m_iter;
	
	return *this;
}


template <class T>
bool
PointerSet<T>::const_iterator::operator==( const const_iterator& other ) const
{
	return m_iter == other.m_iter;
}


#endif // POINTERSET_CONST_ITERATOR_IMPL_H
