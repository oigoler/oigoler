/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "BackupStrategy.h"


BackupStrategy::BackupStrategy()
{
}

BackupStrategy::BackupStrategy( const BackupStrategy& other )
	: m_name( other.m_name )
	, m_description( other.m_description )
	, m_valuableSet( other.m_valuableSet )
	, m_schedule( other.m_schedule )
	, m_storageSet( other.m_storageSet )
	, m_rotationScheme( other.m_rotationScheme )
	, m_window( other.m_window )
{
}

BackupStrategy::~BackupStrategy()
{
}

BackupStrategy& BackupStrategy::operator=( const BackupStrategy& other )
{
	if ( this == &other )
		return *this;
	
	m_name           = other.m_name;
	m_description    = other.m_description;
	m_valuableSet    = other.m_valuableSet;
	m_schedule       = other.m_schedule;
	m_storageSet     = other.m_storageSet;
	m_rotationScheme = other.m_rotationScheme;
	m_window         = other.m_window;
	
	return *this;
}

bool BackupStrategy::operator==( const BackupStrategy& other ) const
{
	return ( m_name           == other.m_name           )
	    && ( m_description    == other.m_description    )
	    && ( m_valuableSet    == other.m_valuableSet    )
	    && ( m_schedule       == other.m_schedule       )
	    && ( m_storageSet     == other.m_storageSet     )
	    && ( m_rotationScheme == other.m_rotationScheme )
	    && ( m_window         == other.m_window         );
}

bool BackupStrategy::operator!=( const BackupStrategy& other ) const
{
	return ( m_name           != other.m_name           )
	    || ( m_description    != other.m_description    )
	    || ( m_valuableSet    != other.m_valuableSet    )
	    || ( m_schedule       != other.m_schedule       )
	    || ( m_storageSet     != other.m_storageSet     )
	    || ( m_rotationScheme != other.m_rotationScheme )
	    || ( m_window         != other.m_window         );
}

QString BackupStrategy::name() const
{
	return m_name;
}

void BackupStrategy::setName( const QString& strategyName )
{
	m_name = strategyName;
}

QString BackupStrategy::description() const
{
	return m_description;
}

void BackupStrategy::setDescription( const QString& strategyDescription )
{
	m_description = strategyDescription;
}

ValuableSet BackupStrategy::valuableSet() const
{
	return m_valuableSet;
}

void BackupStrategy::setValuableSet( const ValuableSet& valuableSet )
{
	m_valuableSet = valuableSet;
}

BackupSchedule BackupStrategy::schedule() const
{
	return m_schedule;
}

void BackupStrategy::setSchedule( const BackupSchedule& schedule )
{
	m_schedule = schedule;
}

BackupStorageSet BackupStrategy::storageSet() const
{
	return m_storageSet;
}

void BackupStrategy::setStorageSet( const BackupStorageSet& storageSet )
{
	m_storageSet = storageSet;
}

BackupRotationScheme BackupStrategy::rotationScheme() const
{
	return m_rotationScheme;
}

void BackupStrategy::setRotationScheme( const BackupRotationScheme& scheme )
{
	m_rotationScheme = scheme;
}

BackupWindow BackupStrategy::window() const
{
	return m_window;
}

void BackupStrategy::setWindow( const BackupWindow& window )
{
	m_window = window;
}
