/**
 *   Copyright (C) 2013, 2014 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef POINTERSETITERATOR_H
#define POINTERSETITERATOR_H

#include "PointerSet.h"

#include <QSetIterator>


template <class T>
class PointerSetIterator
{
	
public:
	inline PointerSetIterator( const PointerSet<T>& set );
	inline virtual ~PointerSetIterator();
	
	inline bool findNext( T* value );
	inline bool findPrevious( T* value );
	inline bool hasNext() const;
	inline bool hasPrevious() const;
	inline T* next();
	inline T* peekNext() const;
	inline T* peekPrevious() const;
	inline T* previous();
	inline void toBack();
	inline void toFront();
	
	inline PointerSetIterator& operator=( const PointerSet<T>& set );
	
private:
	QSetIterator<T*> m_iter;
	
};


template <class T>
PointerSetIterator<T>::PointerSetIterator( const PointerSet<T>& set )
	: m_iter( set.m_set )
{
}


template <class T>
PointerSetIterator<T>::~PointerSetIterator()
{
}


template <class T>
bool PointerSetIterator<T>::findNext( T* value )
{
	return m_iter.findNext( value );
}


template <class T>
bool PointerSetIterator<T>::findPrevious( T* value )
{
	return m_iter.findPrevious( value );
}


template <class T>
bool PointerSetIterator<T>::hasNext() const
{
	return m_iter.hasNext();
}


template <class T>
bool PointerSetIterator<T>::hasPrevious() const
{
	return m_iter.hasPrevious();
}


template <class T>
T* PointerSetIterator<T>::next()
{
	return m_iter.next();
}


template <class T>
T* PointerSetIterator<T>::peekNext() const
{
	return m_iter.peekNext();
}


template <class T>
T* PointerSetIterator<T>::peekPrevious() const
{
	return m_iter.peekPrevious();
}


template <class T>
T* PointerSetIterator<T>::previous()
{
	return m_iter.previous();
}


template <class T>
void PointerSetIterator<T>::toBack()
{
	m_iter.toBack();
}


template <class T>
void PointerSetIterator<T>::toFront()
{
	m_iter.toFront();
}


template <class T>
PointerSetIterator<T>&
PointerSetIterator<T>::operator=( const PointerSet<T>& set )
{
	m_iter = set.m_set;
	return *this;
}


#endif // POINTERSETITERATOR_H
