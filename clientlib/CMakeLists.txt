project( liboigolerclient )

cmake_minimum_required( VERSION 2.8 )
find_package( Qt4 REQUIRED )

include( ${QT_USE_FILE} )
add_definitions( ${QT_DEFINITIONS} )

add_definitions( -DVERSION="0.0.1" )

###############################################################################
# oigolerclient
###############################################################################

set( oigolerclient_SRCS IValuable.cpp
                        Valuable.cpp
                        FSValuable.cpp )

# set( oigolerclient_HDRS Valuable.h
#                         FileSystemPathValuable.h )
# 
# qt4_wrap_cpp( oigolerclient_MOCS ${oigolerclient_HDRS} )

#add_library( oigolerclient STATIC ${oigolerclient_SRCS} ${oigolerclient_MOCS} )
add_library( oigolerclient STATIC ${oigolerclient_SRCS} )

target_link_libraries( oigolerclient ${QT_LIBRARIES} )


###############################################################################
# unittest
###############################################################################

enable_testing()
add_subdirectory( unittest )


