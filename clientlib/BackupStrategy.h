/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef BACKUPSTRATEGY_H
#define BACKUPSTRATEGY_H

#include <QString>

#include "ValuableSet.h"
#include "BackupSchedule.h"
#include "BackupStorageSet.h"
#include "BackupRotationScheme.h"
#include "BackupWindow.h"


class BackupStrategy
{
	
public:
	BackupStrategy();
	BackupStrategy( const BackupStrategy& other );
	virtual ~BackupStrategy();
	
	BackupStrategy& operator=( const BackupStrategy& other );
	bool operator==( const BackupStrategy& other ) const;
	bool operator!=( const BackupStrategy& other ) const;
	
	QString name() const;
	void setName( const QString& strategyName );
	
	QString description() const;
	void setDescription( const QString& strategyDescription );
	
	ValuableSet valuableSet() const;
	void setValuableSet( const ValuableSet& valuableSet );
	
	BackupSchedule schedule() const;
	void setSchedule( const BackupSchedule& schedule );
	
	BackupStorageSet storageSet() const;
	void setStorageSet( const BackupStorageSet& storageSet );
	
	BackupRotationScheme rotationScheme() const;
	void setRotationScheme( const BackupRotationScheme& rotationScheme );
	
	BackupWindow window() const;
	void setWindow( const BackupWindow& window );
	
private:
	QString m_name;
	QString m_description;
	ValuableSet m_valuableSet;
	BackupSchedule m_schedule;
	BackupStorageSet m_storageSet;
	BackupRotationScheme m_rotationScheme;
	BackupWindow m_window;
	
};

#endif // BACKUPSTRATEGY_H
