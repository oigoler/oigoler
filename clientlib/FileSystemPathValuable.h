/**
 *   Copyright (C) 2014 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef FILESYSTEMPATHVALUABLE_H
#define FILESYSTEMPATHVALUABLE_H

#include "Valuable.h"


class FileSystemPathValuablePrivate;
class QDir;
class QFile;
class QFileInfo;


class FileSystemPathValuable : public Valuable
{
	Q_OBJECT
	
public:
	FileSystemPathValuable( QObject* parent = 0 );
	FileSystemPathValuable( const QString& path, QObject* parent = 0 );
	FileSystemPathValuable( const QDir& dir, QObject* parent = 0 );
	FileSystemPathValuable( const QFile& file, QObject* parent = 0 );
	FileSystemPathValuable( const QFileInfo& info, QObject* parent = 0 );
	virtual ~FileSystemPathValuable();
	
	virtual QString type() const;
	
	QString path() const;
	void setPath( const QString& path );
	void setPath( const QDir& dir );
	void setPath( const QFile& file );
	void setPath( const QFileInfo& info );
	
	virtual bool exists() const;
	
	static QString valuableType();
	
private:
	Q_DISABLE_COPY( FileSystemPathValuable )
	FileSystemPathValuablePrivate* const d;
	
};

#endif // FILESYSTEMPATHVALUABLE_H
