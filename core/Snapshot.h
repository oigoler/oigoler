/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef SNAPSHOT_H
#define SNAPSHOT_H

#include <QString>
#include <QDateTime>
#include <QList>

#include "RestoreRecipe.h"


class Snapshot
{
	
public:
	Snapshot();
	Snapshot( const Snapshot& other );
	virtual ~Snapshot();
	
	Snapshot& operator=( const Snapshot& other );
	
	QString name() const;
	void setName( const QString& name );
	
	QDateTime timestamp() const;
	void setTimestamp( const QDateTime& timestamp );
	
	QString key() const;
	
	QList< RestoreRecipe > restoreRecipeList() const;
	void insertRestoreRecipe( const RestoreRecipe& recipe );
	bool containsRestoreRecipe( const RestoreRecipe& recipe ) const;
	void removeRestoreRecipe( const RestoreRecipe& recipe );
	void clearRestoreRecipeList();
	
private:
	QString m_name;
	QDateTime m_timestamp;
	QList< RestoreRecipe > m_restoreRecipeList;
	
};


#endif // SNAPSHOT_H
