/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "BackupStorageList.h"

#include <QCryptographicHash>
#include <QFileInfo>
#include <QTextStream>


BackupStorageList::BackupStorageList()
	: m_error( NoError )
	, m_availabilityPrinting( false )
{
}

BackupStorageList::BackupStorageList( BackupStorageListError error )
	: m_error( error )
	, m_availabilityPrinting( false )
{
}

BackupStorageList::BackupStorageList( const BackupStorageList& other )
	: QList< BackupStorage >( other )
	, m_error( other.m_error )
	, m_availabilityPrinting( other.m_availabilityPrinting )
{
}

BackupStorageList::~BackupStorageList()
{
}

BackupStorageList& BackupStorageList::operator=( const BackupStorageList& other )
{
	if ( this == &other )
		return *this;
	
	QList< BackupStorage >::operator=( other );
	
	m_error                = other.m_error;
	m_availabilityPrinting = other.m_availabilityPrinting;
	
	return *this;
}

bool BackupStorageList::isValid() const
{
	return m_error == NoError;
}

BackupStorageList::BackupStorageListError BackupStorageList::error() const
{
	return m_error;
}

void BackupStorageList::setAvailabilityPrinting( bool flag )
{
	m_availabilityPrinting = flag;
}

bool BackupStorageList::availabilityPrinting() const
{
	return m_availabilityPrinting;
}

QTextStream& operator<<( QTextStream& stream, const BackupStorageList& list )
{
	switch ( list.error() )
	{
		case BackupStorageList::NoError:
			
			foreach ( const BackupStorage& storage, list )
			{
				if ( list.availabilityPrinting() )
				{
					if ( storage.isAvailable() )
						stream << "[*] ";
					else
						stream << "[ ] ";
				}
				
				stream << storage << "\n";
			}
			
			break;
			
			
		case BackupStorageList::SessionError:
			stream << "Error: Backup session not setup right!\n";
			break;
	}
	
	return stream;
}
