/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "OigolerSession.h"
#include "BackupRecipe.h"

#include <QDir>
#include <QFileInfo>
#include <QXmlStreamWriter>


OigolerSession::OigolerSession( QObject* parent )
	: QObject( parent )
	, m_configPath( QDir::home().filePath( ".oigoler" ) )
	, m_error( NoError )
{
}

OigolerSession::~OigolerSession()
{
}

QString OigolerSession::configPath() const
{
	return m_configPath;
}

void OigolerSession::setConfigPath( const QString& path )
{
	if ( path.isEmpty() )
		m_configPath.clear();
	else
		m_configPath = QFileInfo( path ).absoluteFilePath();
}

OigolerSession::OigolerSessionError OigolerSession::lastError() const
{
	return m_error;
}

bool OigolerSession::backupRecipeExists( const QString& recipeName ) const
{
	QDir config( m_configPath + "/recipe" );
	return config.exists( recipeName + ".xml" );
}

BackupRecipe* OigolerSession::backupRecipe( const QString& recipeName ) const
{
	if ( !backupRecipeExists( recipeName ) )
		return 0;
	
	QFile file( m_configPath + "/recipe/" + recipeName + ".xml" );
	
	if ( !file.open( QFile::ReadOnly ) )
		return 0;
	
	QXmlStreamReader reader( &file );
	BackupRecipe* recipe( new BackupRecipe );
	recipe->setName( recipeName );
	
	while ( !reader.atEnd() )
	{
		if ( reader.readNextStartElement() )
		{
			if ( reader.name() == "backuprecipe" )
				continue;
			else if ( reader.name() == "path" )
			{
				reader.readNext();
				
				if ( reader.isCharacters() )
					recipe->insertPath( reader.text().toString() );
			}
			else if ( reader.name() == "storage" )
			{
				reader.readNext();
				
				if ( reader.isCharacters() )
					recipe->setStoragePath( reader.text().toString() );
			}
			else
				reader.skipCurrentElement();
		}
	}
	
	return recipe;
}

QList< BackupRecipe > OigolerSession::backupRecipeList() const
{
	QDir config( m_configPath + "/recipe" );
	config.setFilter( QDir::Files );
	
	QStringList names;
	
	foreach ( QString entry, config.entryList() )
	{
		entry.chop( 4 );
		names.append( entry );
	}
	
	names.sort();
	QList< BackupRecipe > recipeList;
	BackupRecipe* recipe;
	
	foreach ( const QString& recipeName, names )
	{
		recipe = this->backupRecipe( recipeName );
		recipeList.append( *recipe );
		delete recipe;
	}
	
	return recipeList;
}

void OigolerSession::registerBackupRecipe( BackupRecipe* recipe )
{
	if ( !this->checkConfigPath()
	  || !this->testBackupRecipeName( recipe )
	  || !this->testPath( recipe )
	  || !this->testStorage( recipe ) )
	{
		return;
	}
	
	QFile file( m_configPath + "/recipe/" + recipe->name() + ".xml" );
	
	if ( !file.open( QFile::WriteOnly ) )
	{
		m_error = IOError;
		emit failed();
		return;
	}
	
	QXmlStreamWriter writer( &file );
	writer.setAutoFormatting( true );
	writer.writeStartDocument();
	writer.writeStartElement( "backuprecipe" );
	
	foreach ( const QString& path, recipe->pathList() )
		writer.writeTextElement( "path", path );
	
	writer.writeTextElement( "storage", recipe->storagePath() );
	writer.writeEndElement();
	writer.writeEndDocument();
	
	emit backupRecipeRegistered( recipe->name() );
}

void OigolerSession::removeBackupRecipe( const QString& recipeName )
{
	if ( !this->checkConfigPath() )
		return;
	
	QDir config( m_configPath + "/recipe" );
	
	if ( config.remove( recipeName + ".xml" ) )
		emit backupRecipeRemoved( recipeName );
}

bool OigolerSession::checkConfigPath()
{
	if ( m_configPath.isEmpty() )
	{
		m_error = ConfigError;
		emit failed();
		return false;
	}
	
	QDir config( m_configPath );
	
	if ( !config.exists() && !config.mkpath( m_configPath ) )
	{
		m_error = ConfigError;
		emit failed();
		return false;
	}
	
	config.setPath( m_configPath );
	
	if ( !config.exists( "recipe" ) )
		config.mkdir( "recipe" );
	
	if ( !config.cd( "recipe" ) )
	{
		m_error = ConfigError;
		emit failed();
		return false;
	}
	
	return true;
}

bool OigolerSession::testBackupRecipeName( BackupRecipe* recipe )
{
	if ( recipe->name().isEmpty() )
	{
		m_error = InvalidRecipeName;
		emit failed();
		return false;
	}
	
	return true;
}

bool OigolerSession::testPath( BackupRecipe* recipe )
{
	foreach ( const QString& path, recipe->pathList() )
	{
		QFileInfo info( path );
		
		if ( !info.exists() )
		{
			m_error = InvalidPath;
			emit failed();
			return false;
		}
	}
	
	return true;
}

bool OigolerSession::testStorage( BackupRecipe* recipe )
{
	QFileInfo info( recipe->storagePath() );
	
	if ( !info.exists() )
	{
		m_error = InvalidStorage;
		emit failed();
		return false;
	}
	
	return true;
}
