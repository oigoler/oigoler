/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef BACKUPRECIPE_H
#define BACKUPRECIPE_H

#include <QString>
#include <QSet>


class BackupRecipe
{
	
public:
	BackupRecipe();
	BackupRecipe( const BackupRecipe& other );
	virtual ~BackupRecipe();
	
	BackupRecipe& operator=( const BackupRecipe& other );
	
	QString name() const;
	void setName( const QString& name );
	
	QList< QString > pathList() const;
	void insertPath( const QString& path );
	bool containsPath( const QString& path ) const;
	void removePath( const QString& path );
	void clearPathList();
	
	QString storagePath() const;
	void setStoragePath( const QString& path );
	
private:
	QString m_name;
	QSet< QString > m_paths;
	QString m_storagePath;
	
};


#endif // BACKUPRECIPE_H
