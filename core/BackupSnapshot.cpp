/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "BackupSnapshot.h"

#include <QMutableLinkedListIterator>
#include <QFileInfo>
#include <QTextStream>
#include <QDir>


BackupSnapshot::BackupSnapshot()
	: m_error( NoError )
{
}

BackupSnapshot::BackupSnapshot( SnapshotError error )
	: m_error( error )
{
}

BackupSnapshot::BackupSnapshot( const BackupSnapshot& other )
	: m_name( other.m_name )
	, m_paths( other.m_paths )
	, m_timestamp( other.m_timestamp )
	, m_error( other.m_error )
{
}

BackupSnapshot::~BackupSnapshot()
{
}

BackupSnapshot& BackupSnapshot::operator=( const BackupSnapshot& other )
{
	if ( this == &other )
		return *this;
	
	m_name      = other.m_name;
	m_paths     = other.m_paths;
	m_timestamp = other.m_timestamp;
	m_error     = other.m_error;
	
	return *this;
}

QString BackupSnapshot::name() const
{
	return m_name;
}

void BackupSnapshot::setName( const QString& name )
{
	m_name = name;
}

QList< QString > BackupSnapshot::pathList() const
{
	return m_paths;
}

void BackupSnapshot::setPathList( const QList< QString >& pathList )
{
	m_paths = pathList;
	qSort( m_paths );
}

QDateTime BackupSnapshot::timestamp() const
{
	return m_timestamp;
}

void BackupSnapshot::setTimestamp( const QDateTime& timestamp )
{
	m_timestamp = timestamp;
}

bool BackupSnapshot::isValid() const
{
	return m_error == NoError;
}

BackupSnapshot::SnapshotError BackupSnapshot::error() const
{
	return m_error;
}

QTextStream& operator<<( QTextStream& stream, const BackupSnapshot& snapshot )
{
	switch ( snapshot.error() )
	{
		case BackupSnapshot::NoError:
			stream << "snapshot "
			       << snapshot.name() << ' '
			       << snapshot.timestamp().toString();
				
			foreach ( const QString& path, snapshot.pathList() )
				stream << '\n' << path;
			
			break;
			
			
		case BackupSnapshot::SessionError:
			stream << "Error: Backup session not setup right!";
			break;
			
		case BackupSnapshot::RecipeError:
			stream << "Error: Invalid backup recipe!";
			break;
			
		case BackupSnapshot::StorageError:
			stream << "Error: Invalid backup storage!";
			break;
			
		case BackupSnapshot::UnavailableStorage:
			stream << "Error: There is no backup storage available!";
			break;
	}
	
	return stream;
}
