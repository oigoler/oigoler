/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "BackupRecipeList.h"

#include <QTextStream>


BackupRecipeList::BackupRecipeList()
	: m_error( NoError )
{
}

BackupRecipeList::BackupRecipeList( BackupRecipeListError error )
	: m_error( error )
{
}

BackupRecipeList::BackupRecipeList( const BackupRecipeList& other )
	: QList< BackupRecipe >( other )
	, m_error( other.m_error )
{
}

BackupRecipeList::~BackupRecipeList()
{
}

BackupRecipeList& BackupRecipeList::operator=( const BackupRecipeList& other )
{
	if ( this == &other )
		return *this;
	
	QList< BackupRecipe >::operator=( other );
	m_error = other.m_error;
	
	return *this;
}

bool BackupRecipeList::isValid() const
{
	return m_error == NoError;
}

BackupRecipeList::BackupRecipeListError BackupRecipeList::error() const
{
	return m_error;
}

QTextStream& operator<<( QTextStream& stream, const BackupRecipeList& list )
{
	switch ( list.error() )
	{
		case BackupRecipeList::NoError:
			
			foreach ( const BackupRecipe& recipe, list )
				stream << recipe.name() << "\n";
			
			break;
			
			
		case BackupRecipeList::SessionError:
			stream << "Error: Backup session not setup right!\n";
			break;
	}

	return stream;
}
