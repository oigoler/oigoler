/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef BACKUPSTORAGELIST_H
#define BACKUPSTORAGELIST_H

#include "BackupStorage.h"

#include <QList>


class BackupStorageList : public QList< BackupStorage >
{
	
public:
	enum BackupStorageListError { NoError, SessionError };

public:
	BackupStorageList();
	BackupStorageList( BackupStorageListError error );
	BackupStorageList( const BackupStorageList& other );
	virtual ~BackupStorageList();
	
	BackupStorageList& operator=( const BackupStorageList& other );
	
	bool isValid() const;
	BackupStorageListError error() const;
	
	void setAvailabilityPrinting( bool flag );
	bool availabilityPrinting() const;
	
private:
	BackupStorageListError m_error;
	bool m_availabilityPrinting;
	
};


QTextStream& operator<<( QTextStream& stream, const BackupStorageList& list );

#endif // BACKUPSTORAGELIST_H
