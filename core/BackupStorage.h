/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef BACKUPSTORAGE_H
#define BACKUPSTORAGE_H

#include <QString>
#include <QByteArray>


class QTextStream;


class BackupStorage
{
	
public:
	enum BackupStorageError { NoError
	                        , SessionError
	                        , InexistentPath
		                    , InvalidPath
		                    , InvalidKey };

public:
	BackupStorage();
	BackupStorage( const QString& path );
	BackupStorage( BackupStorageError error );
	BackupStorage( const QByteArray& key, const QString& path );
	BackupStorage( const BackupStorage& other );
	virtual ~BackupStorage();
	
	BackupStorage& operator=( const BackupStorage& other );
	bool operator<( const BackupStorage& other ) const;
	
	QByteArray key() const;
	QString path() const;
	bool isValid() const;
	BackupStorageError error() const;
	bool isAvailable() const;
	
private:
	QByteArray m_key;
	QString m_path;
	BackupStorageError m_error;
	
};


QTextStream& operator<<( QTextStream& stream, const BackupStorage& storage );

#endif // BACKUPSTORAGE_H
