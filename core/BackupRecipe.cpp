/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "BackupRecipe.h"

#include <QFileInfo>


BackupRecipe::BackupRecipe()
{
}

BackupRecipe::BackupRecipe( const BackupRecipe& other )
	: m_name( other.m_name )
	, m_paths( other.m_paths )
	, m_storagePath( other.m_storagePath )
{
}

BackupRecipe::~BackupRecipe()
{
}

BackupRecipe& BackupRecipe::operator=( const BackupRecipe& other )
{
	if ( this == &other )
		return *this;
	
	m_name        = other.m_name;
	m_paths       = other.m_paths;
	m_storagePath = other.m_storagePath;
	
	return *this;
}

QString BackupRecipe::name() const
{
	return m_name;
}

void BackupRecipe::setName( const QString& name )
{
	m_name = name;
}

QList< QString > BackupRecipe::pathList() const
{
	QList< QString > ret( m_paths.toList() );
	qSort( ret );
	return ret;
}

void BackupRecipe::insertPath( const QString& path )
{
	m_paths.insert( QFileInfo( path ).absoluteFilePath() );
}

bool BackupRecipe::containsPath( const QString& path ) const
{
	return m_paths.contains( QFileInfo( path ).absoluteFilePath() );
}

void BackupRecipe::removePath( const QString& path )
{
	m_paths.remove( QFileInfo( path ).absoluteFilePath() );
}

void BackupRecipe::clearPathList()
{
	m_paths.clear();
}

QString BackupRecipe::storagePath() const
{
	return m_storagePath;
}

void BackupRecipe::setStoragePath( const QString& path )
{
	m_storagePath = QFileInfo( path ).absoluteFilePath();
}
