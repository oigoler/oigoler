/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef TESTOIGOLERSESSION_CLASS_H
#define TESTOIGOLERSESSION_CLASS_H

#include <QtTest/QtTest>


class TestOigolerSession_Class : public QObject
{
	Q_OBJECT

private slots:
	void testConfigPath();
	
};

#endif // TESTOIGOLERSESSION_CLASS_H
