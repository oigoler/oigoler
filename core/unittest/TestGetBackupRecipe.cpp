/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestGetBackupRecipe.h"
#include "DirectoryGenerator.h"
#include "DirectoryCleaner.h"
#include "../BackupRecipe.h"
#include "../OigolerSession.h"


void TestGetBackupRecipe::initTestCase()
{
	new DirectoryGenerator( "storage1", this );
	new DirectoryGenerator( "storage2", this );
	new DirectoryCleaner( "config", this );
	
	m_recipe1 = new BackupRecipe;
	m_recipe1->setName( "recipe1" );
	m_recipe1->insertPath( "." );
	m_recipe1->setStoragePath( "storage1" );
	
	m_recipe2 = new BackupRecipe;
	m_recipe2->setName( "recipe2" );
	m_recipe2->insertPath( ".." );
	m_recipe2->setStoragePath( "storage2" );
	
	m_session = new OigolerSession;
	m_session->setConfigPath( "config" );
	m_session->registerBackupRecipe( m_recipe1 );
	m_session->registerBackupRecipe( m_recipe2 );
}

void TestGetBackupRecipe::testExists()
{
	QVERIFY( m_session->backupRecipeExists( "recipe1" ) );
	QVERIFY( m_session->backupRecipeExists( "recipe2" ) );
	QVERIFY( !m_session->backupRecipeExists( "recipe3" ) );
}

void TestGetBackupRecipe::testAccessor()
{
	BackupRecipe* recipe( m_session->backupRecipe( "recipe1" ) );
	
	QVERIFY( recipe );
	QCOMPARE( recipe->name(), m_recipe1->name() );
	QCOMPARE( recipe->pathList(), m_recipe1->pathList() );
	QCOMPARE( recipe->storagePath(), m_recipe1->storagePath() );
	
	delete recipe;
	recipe = m_session->backupRecipe( "recipe2" );
	
	QVERIFY( recipe );
	QCOMPARE( recipe->name(), m_recipe2->name() );
	QCOMPARE( recipe->pathList(), m_recipe2->pathList() );
	QCOMPARE( recipe->storagePath(), m_recipe2->storagePath() );
	
	delete recipe;
	
	QVERIFY( !m_session->backupRecipe( "recipe3" ) );
}

void TestGetBackupRecipe::testList()
{
	QList< BackupRecipe > list( m_session->backupRecipeList() );
	QCOMPARE( list.size(), 2 );
	
	BackupRecipe recipe1( list.first() );
	QCOMPARE( recipe1.name(), m_recipe1->name() );
	QCOMPARE( recipe1.pathList(), m_recipe1->pathList() );
	QCOMPARE( recipe1.storagePath(), m_recipe1->storagePath() );
	
	BackupRecipe recipe2( list.last() );
	QCOMPARE( recipe2.name(), m_recipe2->name() );
	QCOMPARE( recipe2.pathList(), m_recipe2->pathList() );
	QCOMPARE( recipe2.storagePath(), m_recipe2->storagePath() );
}

void TestGetBackupRecipe::cleanupTestCase()
{
	delete m_session;
	delete m_recipe1;
	delete m_recipe2;
}

QTEST_MAIN(TestGetBackupRecipe)
