/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestListStorage.h"
#include "DirectoryCleaner.h"
#include "../BackupSession.h"


void TestListStorage::initTestCase()
{
	BackupSession session;
	session.setConfigDirectory( "config" );
	new DirectoryCleaner( "config", this );
	
	QDir::current().mkdir( "storage" );
	DirectoryCleaner cleaner( "storage" );
	
	m_storage1 = session.createStorage( "storage" );
	m_storage2 = session.createStorage( "." );
	m_storage3 = session.createStorage( ".." );
	
	QVERIFY( m_storage1.isValid() );
	QVERIFY( m_storage2.isValid() );
	QVERIFY( m_storage3.isValid() );
	
	m_list = session.listStorage();
}

void TestListStorage::testListSize()
{
	QCOMPARE( m_list.size(), 3 );
}

void TestListStorage::testListElements()
{
	QCOMPARE( m_list.size(), 3 );
	QCOMPARE( m_list.at(0).key(), m_storage3.key() );
	QCOMPARE( m_list.at(1).key(), m_storage2.key() );
	QCOMPARE( m_list.at(2).key(), m_storage1.key() );
}

void TestListStorage::testListStringRepresentation()
{
	QString expected;
	QTextStream stream1( &expected );
	
	stream1 << m_storage3 << "\n" << m_storage2 << "\n" << m_storage1 << "\n";
	
	QString actual;
	QTextStream stream( &actual );
	
	stream << m_list;
	
	QCOMPARE( actual, expected );
}

void TestListStorage::testListValidity()
{
	QVERIFY( m_list.isValid() );
}

void TestListStorage::testListError()
{
	QCOMPARE( m_list.error(), BackupStorageList::NoError );
}

void TestListStorage::testStorageAvailability()
{
	QVERIFY( m_list.at(0).isAvailable() );
	QVERIFY( m_list.at(1).isAvailable() );
	QVERIFY( !m_list.at(2).isAvailable() );
}

void TestListStorage::testListStringRepresentationWithAvailability()
{
	QVERIFY( !m_list.availabilityPrinting() );
	m_list.setAvailabilityPrinting( true );
	QVERIFY( m_list.availabilityPrinting() );
	
	QString expected;
	QTextStream stream1( &expected );
	
	stream1 << "[*] " << m_storage3 << "\n"
	        << "[*] " << m_storage2 << "\n"
	        << "[ ] " << m_storage1 << "\n";
	
	QString actual;
	QTextStream stream( &actual );
	
	stream << m_list;
	
	QCOMPARE( actual, expected );
	
	m_list.setAvailabilityPrinting( false );
	QVERIFY( !m_list.availabilityPrinting() );
}

QTEST_MAIN(TestListStorage)
