/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestRegisterBackupRecipe_ConfigError_1.h"
#include "DirectoryGenerator.h"
#include "../BackupRecipe.h"
#include "../OigolerSession.h"


void TestRegisterBackupRecipe_ConfigError_1::initTestCase()
{
	m_successCount = 0;
	m_failureCount = 0;
	
	new DirectoryGenerator( "storage", this );
	
	m_recipe = new BackupRecipe;
	m_recipe->setName( "testrecipe" );
	m_recipe->insertPath( "." );
	m_recipe->insertPath( ".." );
	m_recipe->setStoragePath( "storage" );
	
	m_session = new OigolerSession;
	
	connect( m_session, SIGNAL(backupRecipeRegistered(QString))
	       , this, SLOT(recordSuccess()) );
	
	connect( m_session, SIGNAL(failed()), this, SLOT(recordFailure()) );
	
	m_session->setConfigPath( QString() );
	m_session->registerBackupRecipe( m_recipe );
}

void TestRegisterBackupRecipe_ConfigError_1::testSignals()
{
	QCOMPARE( m_successCount, 0 );
	QCOMPARE( m_failureCount, 1 );
}

void TestRegisterBackupRecipe_ConfigError_1::testError()
{
	QCOMPARE( m_session->lastError(), OigolerSession::ConfigError );
}

void TestRegisterBackupRecipe_ConfigError_1::testFileExistence()
{
	QDir config( "recipe" );
	QVERIFY( !config.exists() );
	QVERIFY( !config.exists( "testrecipe.xml" ) );
}

void TestRegisterBackupRecipe_ConfigError_1::cleanupTestCase()
{
	delete m_session;
	delete m_recipe;
}

void TestRegisterBackupRecipe_ConfigError_1::recordSuccess()
{
	++m_successCount;
}

void TestRegisterBackupRecipe_ConfigError_1::recordFailure()
{
	++m_failureCount;
}

QTEST_MAIN(TestRegisterBackupRecipe_ConfigError_1)
