/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestRemoveBackupRecipe_2.h"
#include "DirectoryCleaner.h"
#include "../OigolerSession.h"


void TestRemoveBackupRecipe_2::initTestCase()
{
	m_successCount = 0;
	m_failureCount = 0;
	
	new DirectoryCleaner( "config", this );
	
	m_session = new OigolerSession;
	
	connect( m_session, SIGNAL(backupRecipeRemoved(QString))
	       , this, SLOT(recordSuccess()) );
	
	connect( m_session, SIGNAL(failed()), this, SLOT(recordFailure()) );
	
	m_session->setConfigPath( "config" );
	m_session->removeBackupRecipe( "testrecipe" );
}

void TestRemoveBackupRecipe_2::testSignals()
{
	QCOMPARE( m_successCount, 0 );
	QCOMPARE( m_failureCount, 0 );
}

void TestRemoveBackupRecipe_2::testError()
{
	QCOMPARE( m_session->lastError(), OigolerSession::NoError );
}

void TestRemoveBackupRecipe_2::cleanupTestCase()
{
	delete m_session;
}

void TestRemoveBackupRecipe_2::recordSuccess()
{
	++m_successCount;
}

void TestRemoveBackupRecipe_2::recordFailure()
{
	++m_failureCount;
}

QTEST_MAIN(TestRemoveBackupRecipe_2)
