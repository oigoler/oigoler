/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestRemoveRecipe.h"
#include "DirectoryCleaner.h"
#include "../BackupSession.h"


void TestRemoveRecipe::initTestCase()
{
	BackupSession session;
	session.setConfigDirectory( "config" );
	new DirectoryCleaner( "config", this );
	
	BackupRecipe recipe( "testrecipe" );
	session.createRecipe( &recipe );
	session.removeRecipe( "testrecipe" );
	
	m_list = session.listRecipe();
	m_recipe = session.recipe( "testrecipe" );
}

void TestRemoveRecipe::testRecipeList()
{
	QVERIFY( m_list.isEmpty() );
}

void TestRemoveRecipe::testRecipe()
{
	QVERIFY( !m_recipe.isValid() );
	QCOMPARE( m_recipe.error(), BackupRecipe::InexistentRecipe );
}

QTEST_MAIN(TestRemoveRecipe)
