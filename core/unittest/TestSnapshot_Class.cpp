/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestSnapshot_Class.h"
#include "../Snapshot.h"


void TestSnapshot_Class::testName()
{
	Snapshot snapshot;
	QVERIFY( snapshot.name().isEmpty() );
	
	snapshot.setName( "snapshotname" );
	QCOMPARE( snapshot.name(), QString( "snapshotname" ) );
}

void TestSnapshot_Class::testTimestamp()
{
	Snapshot snapshot;
	QVERIFY( !snapshot.timestamp().isValid() );
	
	QDateTime current( QDateTime::currentDateTime() );
	snapshot.setTimestamp( current );
	
	QCOMPARE( snapshot.timestamp(), current );
}

void TestSnapshot_Class::testKey()
{
	Snapshot snapshot;
	QVERIFY( snapshot.key().isEmpty() );
	
	QDateTime current( QDateTime::currentDateTime() );
	QString expected( "testsnapshot_" + QString::number( current.toMSecsSinceEpoch() ) );
	
	snapshot.setName( "testsnapshot" );
	snapshot.setTimestamp( current );
	
	QCOMPARE( snapshot.key(), expected );
}

void TestSnapshot_Class::testRestoreRecipeList()
{
	Snapshot snapshot;
	QVERIFY( snapshot.restoreRecipeList().isEmpty() );
	
	RestoreRecipe recipe1;
	RestoreRecipe recipe2;
	RestoreRecipe recipe3;
	RestoreRecipe recipe4;
	
	recipe1.setKey( "1" );
	recipe2.setKey( "2" );
	recipe3.setKey( "3" );
	recipe4.setKey( "4" );
	
	snapshot.insertRestoreRecipe( recipe1 );
	snapshot.insertRestoreRecipe( recipe2 );
	snapshot.insertRestoreRecipe( recipe3 );
	
	QCOMPARE( snapshot.restoreRecipeList().size(), 3 );
	QCOMPARE( snapshot.restoreRecipeList().at(0).key(), QByteArray( "1" ) );
	QCOMPARE( snapshot.restoreRecipeList().at(1).key(), QByteArray( "2" ) );
	QCOMPARE( snapshot.restoreRecipeList().at(2).key(), QByteArray( "3" ) );
	
	QVERIFY( snapshot.containsRestoreRecipe( recipe1 ) );
	QVERIFY( !snapshot.containsRestoreRecipe( recipe4 ) );
	
	snapshot.removeRestoreRecipe( recipe2 );
	
	QCOMPARE( snapshot.restoreRecipeList().size(), 2 );
	QCOMPARE( snapshot.restoreRecipeList().at(0).key(), QByteArray( "1" ) );
	QCOMPARE( snapshot.restoreRecipeList().at(1).key(), QByteArray( "3" ) );
	
	snapshot.clearRestoreRecipeList();
	QVERIFY( snapshot.restoreRecipeList().isEmpty() );
}

void TestSnapshot_Class::testCopyConstructor()
{
	QDateTime current( QDateTime::currentDateTime() );
	
	RestoreRecipe recipe;
	recipe.setKey( "1" );
	
	Snapshot snapshot;
	snapshot.setName( "testsnapshot" );
	snapshot.setTimestamp( current );
	snapshot.insertRestoreRecipe( recipe );
	
	Snapshot copy( snapshot );
	
	QCOMPARE( copy.name(), QString( "testsnapshot" ) );
	QCOMPARE( copy.timestamp(), current );
	QCOMPARE( copy.restoreRecipeList().size(), 1 );
	QCOMPARE( copy.restoreRecipeList().at(0).key(), QByteArray( "1" ) );
}

void TestSnapshot_Class::testAssignmentOperator()
{
	QDateTime current( QDateTime::currentDateTime() );
	
	RestoreRecipe recipe;
	recipe.setKey( "1" );
	
	Snapshot snapshot;
	snapshot.setName( "testsnapshot" );
	snapshot.setTimestamp( current );
	snapshot.insertRestoreRecipe( recipe );
	
	Snapshot copy;
	copy = snapshot;
	
	QCOMPARE( copy.name(), QString( "testsnapshot" ) );
	QCOMPARE( copy.timestamp(), current );
	QCOMPARE( copy.restoreRecipeList().size(), 1 );
	QCOMPARE( copy.restoreRecipeList().at(0).key(), QByteArray( "1" ) );
}

QTEST_MAIN(TestSnapshot_Class)
