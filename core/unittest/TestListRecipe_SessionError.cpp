/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestListRecipe_SessionError.h"
#include "../BackupSession.h"


void TestListRecipe_SessionError::initTestCase()
{
	BackupSession session;
	session.setConfigDirectory( QString() );
	
	m_list = session.listRecipe();
}

void TestListRecipe_SessionError::testValidity()
{
	QVERIFY( !m_list.isValid() );
}

void TestListRecipe_SessionError::testError()
{
	QCOMPARE( m_list.error(), BackupRecipeList::SessionError );
}

void TestListRecipe_SessionError::testSize()
{
	QCOMPARE( m_list.size(), 0 );
}

void TestListRecipe_SessionError::testToString()
{
	QString expected( "Error: Backup session not setup right!\n" );
	
	QString actual;
	QTextStream stream( &actual );
	
	stream << m_list;
	
	QCOMPARE( actual, expected );
}

QTEST_MAIN(TestListRecipe_SessionError)
