/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef TESTREGISTERBACKUPECIPE_2_H
#define TESTREGISTERBACKUPECIPE_2_H

#include <QtTest/QtTest>

class OigolerSession;
class BackupRecipe;


class TestRegisterBackupRecipe_2 : public QObject
{
	Q_OBJECT

private slots:
	void initTestCase();
	
	void testSignals();
	void testError();
	void testFileName();
	void testFileContents();
	
	void cleanupTestCase();
	
public slots:
	void recordSuccess( const QString& recipeName );
	void recordFailure();
	
private:
	OigolerSession* m_session;
	BackupRecipe* m_recipe;
	
	int m_successCount;
	int m_failureCount;
	
	QString m_recipeName;
	
};

#endif // TESTREGISTERBACKUPECIPE_2_H
