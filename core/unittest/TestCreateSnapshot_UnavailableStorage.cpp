/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestCreateSnapshot_UnavailableStorage.h"
#include "DirectoryGenerator.h"
#include "DirectoryCleaner.h"

#include "../BackupSession.h"
#include "../BackupRecipe.h"
#include "../BackupStorage.h"


void TestCreateSnapshot_UnavailableStorage::initTestCase()
{
	new DirectoryGenerator( "testdir", this );
	
	BackupSession session;
	session.setConfigDirectory( "config" );
	new DirectoryCleaner( "config", this );
	
	DirectoryGenerator* cleaner = new DirectoryGenerator( "storage", this );
	
	BackupRecipe recipe( "testbackup" );
	recipe.addPath( "testdir" );
	recipe.addStorage( session.createStorage( "storage" ).key() );
	session.createRecipe( &recipe );
	
	delete cleaner;
	
	m_snapshot = session.backup( "testbackup" );
}

void TestCreateSnapshot_UnavailableStorage::testValidity()
{
	QVERIFY( !m_snapshot.isValid() );
}

void TestCreateSnapshot_UnavailableStorage::testError()
{
	QCOMPARE( m_snapshot.error(), BackupSnapshot::UnavailableStorage );
}

void TestCreateSnapshot_UnavailableStorage::testToString()
{
	QString expected( "Error: There is no backup storage available!" );
	
	QString actual;
	QTextStream stream( &actual );
	
	stream << m_snapshot;
	
	QCOMPARE( actual, expected );
}

QTEST_MAIN(TestCreateSnapshot_UnavailableStorage)
