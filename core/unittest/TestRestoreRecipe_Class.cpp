/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestRestoreRecipe_Class.h"
#include "../RestoreRecipe.h"


void TestRestoreRecipe_Class::testKey()
{
	RestoreRecipe recipe;
	QVERIFY( recipe.key().isEmpty() );
	
	recipe.setKey( "recipekey" );
	QCOMPARE( recipe.key(), QByteArray( "recipekey" ) );
}

void TestRestoreRecipe_Class::testCopyConstructor()
{
	RestoreRecipe recipe;
	recipe.setKey( "recipekey" );
	
	RestoreRecipe copy( recipe );
	
	QCOMPARE( copy.key(), QByteArray( "recipekey" ) );
}

void TestRestoreRecipe_Class::testAssignmentOperator()
{
	RestoreRecipe recipe;
	recipe.setKey( "recipekey" );
	
	RestoreRecipe copy;
	copy = recipe;
	
	QCOMPARE( copy.key(), QByteArray( "recipekey" ) );
}

QTEST_MAIN(TestRestoreRecipe_Class)
