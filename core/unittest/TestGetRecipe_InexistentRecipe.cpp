/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestGetRecipe_InexistentRecipe.h"
#include "DirectoryCleaner.h"
#include "../BackupSession.h"


void TestGetRecipe_InexistentRecipe::initTestCase()
{
	BackupSession session;
	session.setConfigDirectory( "config" );
	new DirectoryCleaner( "config", this );
	
	BackupRecipe recipe( "testrecipe" );
	recipe.addPath( "." );
	recipe.addPath( ".." );
	session.createRecipe( &recipe );
	
	m_recipe = session.recipe( "inexistent" );
}

void TestGetRecipe_InexistentRecipe::testValidity()
{
	QVERIFY( !m_recipe.isValid() );
}

void TestGetRecipe_InexistentRecipe::testError()
{
	QCOMPARE( m_recipe.error(), BackupRecipe::InexistentRecipe );
}

void TestGetRecipe_InexistentRecipe::testName()
{
	QCOMPARE( m_recipe.name(), QString( "inexistent" ) );
}

void TestGetRecipe_InexistentRecipe::testPaths()
{
	QCOMPARE( m_recipe.pathList().size(), 0 );
}

void TestGetRecipe_InexistentRecipe::testToString()
{
	QString expected( "Error: The backup recipe 'inexistent' does not exist!" );
	
	QString actual;
	QTextStream stream( &actual );
	
	stream << m_recipe;
	
	QCOMPARE( actual, expected );
}

QTEST_MAIN(TestGetRecipe_InexistentRecipe)
