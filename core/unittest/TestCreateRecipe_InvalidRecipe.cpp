/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestCreateRecipe_InvalidRecipe.h"
#include "DirectoryCleaner.h"
#include "../BackupSession.h"
#include "../BackupRecipe.h"

#include <QDir>


void TestCreateRecipe_InvalidRecipe::initTestCase()
{
	BackupSession session;
	session.setConfigDirectory( "config" );
	new DirectoryCleaner( "config", this );
	
	m_recipe = new BackupRecipe;
	session.createRecipe( m_recipe );
}

void TestCreateRecipe_InvalidRecipe::testValidity()
{
	QVERIFY( !m_recipe->isValid() );
}

void TestCreateRecipe_InvalidRecipe::testError()
{
	QCOMPARE( m_recipe->error(), BackupRecipe::InvalidRecipe );
}

void TestCreateRecipe_InvalidRecipe::testToString()
{
	QString expected( "Error: Backup recipe is invalid!" );
	
	QString actual;
	QTextStream stream( &actual );
	
	stream << *m_recipe;
	
	QCOMPARE( actual, expected );
}

void TestCreateRecipe_InvalidRecipe::testNoFileCreated()
{
	QDir config( "config/recipe" );
	QVERIFY( config.exists() );
	
	QStringList filters;
	filters.append( "*.xml" );
	config.setNameFilters( filters );
	
	QStringList entries( config.entryList() );
	QCOMPARE( entries.size(), 0 );
}

void TestCreateRecipe_InvalidRecipe::cleanupTestCase()
{
	delete m_recipe;
}

QTEST_MAIN(TestCreateRecipe_InvalidRecipe)
