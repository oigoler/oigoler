/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestCreateSnapshot_StorageError.h"
#include "DirectoryGenerator.h"
#include "DirectoryCleaner.h"

#include "../BackupSession.h"
#include "../BackupRecipe.h"


void TestCreateSnapshot_StorageError::initTestCase()
{
	new DirectoryGenerator( "testdir", this );
	
	BackupSession session;
	session.setConfigDirectory( "config" );
	new DirectoryCleaner( "config", this );
	
	BackupRecipe recipe( "testbackup" );
	recipe.addPath( "testdir" );
	session.createRecipe( &recipe );
	
	m_snapshot1 = session.backup( "testbackup" );
	
	recipe.addStorage( "123" );
	session.registerRecipe( &recipe );
	
	m_snapshot2 = session.backup( "testbackup" );
}

void TestCreateSnapshot_StorageError::testValidity()
{
	QVERIFY( !m_snapshot1.isValid() );
	QVERIFY( !m_snapshot2.isValid() );
}

void TestCreateSnapshot_StorageError::testError()
{
	QCOMPARE( m_snapshot1.error(), BackupSnapshot::StorageError );
	QCOMPARE( m_snapshot2.error(), BackupSnapshot::StorageError );
}

void TestCreateSnapshot_StorageError::testToString()
{
	QString expected( "Error: Invalid backup storage!" );
	
	QString actual1;
	QString actual2;
	QTextStream stream1( &actual1 );
	QTextStream stream2( &actual2 );
	
	stream1 << m_snapshot1;
	stream2 << m_snapshot2;
	
	QCOMPARE( actual1, expected );
	QCOMPARE( actual2, expected );
}

QTEST_MAIN(TestCreateSnapshot_StorageError)
