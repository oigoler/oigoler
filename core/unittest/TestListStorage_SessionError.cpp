/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestListStorage_SessionError.h"
#include "../BackupSession.h"


void TestListStorage_SessionError::initTestCase()
{
	BackupSession session;
	session.setConfigDirectory( QString() );
	
	m_list = session.listStorage();
}

void TestListStorage_SessionError::testListSize()
{
	QCOMPARE( m_list.size(), 0 );
}

void TestListStorage_SessionError::testListStringRepresentation()
{
	QString expected( "Error: Backup session not setup right!\n" );
	
	QString actual;
	QTextStream stream( &actual );
	
	stream << m_list;
	
	QCOMPARE( actual, expected );
}

void TestListStorage_SessionError::testListValidity()
{
	QVERIFY( !m_list.isValid() );
}

void TestListStorage_SessionError::testListError()
{
	QCOMPARE( m_list.error(), BackupStorageList::SessionError );
}

QTEST_MAIN(TestListStorage_SessionError)
