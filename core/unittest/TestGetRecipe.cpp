/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestGetRecipe.h"
#include "DirectoryCleaner.h"
#include "../BackupSession.h"
#include "../BackupStorage.h"


void TestGetRecipe::initTestCase()
{
	BackupSession session;
	session.setConfigDirectory( "config" );
	new DirectoryCleaner( "config", this );
	
	QDir::current().mkdir( "storage1" );
	QDir::current().mkdir( "storage2" );
	QDir::current().mkdir( "invalid" );
	new DirectoryCleaner( "storage1", this );
	new DirectoryCleaner( "storage2", this );
	DirectoryCleaner* cleaner1( new DirectoryCleaner( "invalid" ) );
	m_storage1 = session.createStorage( "storage1" ).key();
	m_storage2 = session.createStorage( "storage2" ).key();
	
	BackupRecipe recipe( "testrecipe" );
	recipe.addPath( "." );
	recipe.addPath( ".." );
	recipe.addPath( "invalid" );
	recipe.addStorage( m_storage1 );
	recipe.addStorage( m_storage2 );
	session.createRecipe( &recipe );
	delete cleaner1;
	
	m_recipe = session.recipe( "testrecipe" );
}

void TestGetRecipe::testValidity()
{
	QVERIFY( m_recipe.isValid() );
}

void TestGetRecipe::testError()
{
	QCOMPARE( m_recipe.error(), BackupRecipe::NoError );
}

void TestGetRecipe::testName()
{
	QCOMPARE( m_recipe.name(), QString( "testrecipe" ) );
}

void TestGetRecipe::testPaths()
{
	QDir dir( QDir::current() );
	QString path2( dir.absolutePath() );
	QString path3( dir.absoluteFilePath( "invalid" ) );
	
	dir.cdUp();
	QString path1( dir.absolutePath() );
	
	QCOMPARE( m_recipe.pathList().size(), 3 );
	QCOMPARE( m_recipe.pathList().at(0), path1 );
	QCOMPARE( m_recipe.pathList().at(1), path2 );
	QCOMPARE( m_recipe.pathList().at(2), path3 );
}

void TestGetRecipe::testStorageList()
{
	QCOMPARE( m_recipe.storageList().size(), 2 );
	QCOMPARE( m_recipe.storageList().first(), m_storage1 );
	QCOMPARE( m_recipe.storageList().last(), m_storage2 );
}

void TestGetRecipe::testToString()
{
	QDir dir( QDir::current() );
	QString path2( dir.absolutePath() );
	QString path3( dir.absoluteFilePath( "invalid" ) );
	
	dir.cdUp();
	QString path1( dir.absolutePath() );
	
	QString expected( "recipe testrecipe\n" );
	expected += "path " + path1 + "\n";
	expected += "path " + path2 + "\n";
	expected += "invalidpath " + path3 + "\n";
	expected += "storage " + m_storage1 + "\n";
	expected += "storage " + m_storage2;
	
	QString actual;
	QTextStream stream( &actual );
	
	stream << m_recipe;
	
	QCOMPARE( actual, expected );
}

QTEST_MAIN(TestGetRecipe)
