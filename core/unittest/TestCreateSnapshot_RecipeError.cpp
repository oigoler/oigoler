/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestCreateSnapshot_RecipeError.h"
#include "DirectoryCleaner.h"

#include "../BackupSession.h"


void TestCreateSnapshot_RecipeError::initTestCase()
{
	BackupSession session;
	session.setConfigDirectory( "config" );
	new DirectoryCleaner( "config", this );
	
	m_snapshot = session.backup( "testbackup" );
}

void TestCreateSnapshot_RecipeError::testValidity()
{
	QVERIFY( !m_snapshot.isValid() );
}

void TestCreateSnapshot_RecipeError::testError()
{
	QCOMPARE( m_snapshot.error(), BackupSnapshot::RecipeError );
}

void TestCreateSnapshot_RecipeError::testToString()
{
	QString expected( "Error: Invalid backup recipe!" );
	
	QString actual;
	QTextStream stream( &actual );
	
	stream << m_snapshot;
	
	QCOMPARE( actual, expected );
}

QTEST_MAIN(TestCreateSnapshot_RecipeError)
