/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestBackupRecipe_Class.h"
#include "../BackupRecipe.h"


void TestBackupRecipe_Class::testName()
{
	BackupRecipe recipe;
	QVERIFY( recipe.name().isEmpty() );
	
	recipe.setName( "recipename" );
	QCOMPARE( recipe.name(), QString( "recipename" ) );
}

void TestBackupRecipe_Class::testPathList()
{
	BackupRecipe recipe;
	QVERIFY( recipe.pathList().isEmpty() );
	
	QDir dir( QDir::current() );
	QString path3( dir.absolutePath() );
	dir.cdUp();
	QString path2( dir.absolutePath() );
	dir.cdUp();
	QString path1( dir.absolutePath() );
	
	recipe.insertPath( "." );
	recipe.insertPath( ".." );
	recipe.insertPath( "../.." );
	recipe.insertPath( path1 );
	
	QCOMPARE( recipe.pathList().size(), 3 );
	QCOMPARE( recipe.pathList().at(0), path1 );
	QCOMPARE( recipe.pathList().at(1), path2 );
	QCOMPARE( recipe.pathList().at(2), path3 );
	
	QVERIFY( recipe.containsPath( ".." ) );
	QVERIFY( !recipe.containsPath( "nopath" ) );
	
	recipe.removePath( "." );
	
	QCOMPARE( recipe.pathList().size(), 2 );
	QCOMPARE( recipe.pathList().at(0), path1 );
	QCOMPARE( recipe.pathList().at(1), path2 );
	
	recipe.clearPathList();
	QVERIFY( recipe.pathList().isEmpty() );
}

void TestBackupRecipe_Class::testStoragePath()
{
	BackupRecipe recipe;
	QVERIFY( recipe.storagePath().isEmpty() );
	
	recipe.setStoragePath( "." );
	QCOMPARE( recipe.storagePath(), QDir::currentPath() );
}

void TestBackupRecipe_Class::testCopyConstructor()
{
	BackupRecipe recipe;
	recipe.setName( "recipename" );
	recipe.insertPath( "." );
	recipe.setStoragePath( "." );
	
	BackupRecipe copy( recipe );
	
	QCOMPARE( copy.name(), QString( "recipename" ) );
	QCOMPARE( copy.pathList().size(), 1 );
	QCOMPARE( copy.pathList().at(0), QDir::currentPath() );
	QCOMPARE( copy.storagePath(), QDir::currentPath() );
}

void TestBackupRecipe_Class::testAssignmentOperator()
{
	BackupRecipe recipe;
	recipe.setName( "recipename" );
	recipe.insertPath( "." );
	recipe.setStoragePath( "." );
	
	BackupRecipe copy;
	copy = recipe;
	
	QCOMPARE( copy.name(), QString( "recipename" ) );
	QCOMPARE( copy.pathList().size(), 1 );
	QCOMPARE( copy.pathList().at(0), QDir::currentPath() );
	QCOMPARE( copy.storagePath(), QDir::currentPath() );
}

QTEST_MAIN(TestBackupRecipe_Class)
