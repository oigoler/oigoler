/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestCreateSnapshot.h"
#include "FileGenerator.h"
#include "DirectoryGenerator.h"
#include "LinkGenerator.h"
#include "DirectoryCleaner.h"

#include "../BackupSession.h"
#include "../BackupRecipe.h"
#include "../BackupStorage.h"


void TestCreateSnapshot::initTestCase()
{
	new FileGenerator( "file0", "This is a shared file!\n", this );
	new DirectoryGenerator( "dir0", this );
	new LinkGenerator( "link0", "dir0", this );
	new FileGenerator( "dir0/file1", "This is a unique file!\n", this );
	new DirectoryGenerator( "dir0/dir1", this );
	new LinkGenerator( "dir0/link1", "file1", this );
	new FileGenerator( "dir0/dir1/file2", "This is a shared file!\n", this );
	new DirectoryGenerator( "dir0/dir1/dir2", this );
	new LinkGenerator( "dir0/dir1/link2", "infinity", this );
	
	BackupSession session;
	session.setConfigDirectory( "config" );
	new DirectoryCleaner( "config", this );
	
	new DirectoryGenerator( "storage", this );
	
	BackupRecipe recipe( "testbackup" );
	recipe.addPath( "file0" );
	recipe.addPath( "dir0" );
	recipe.addPath( "link0" );
	recipe.addStorage( session.createStorage( "storage" ).key() );
	
	session.createRecipe( &recipe );
	
	m_before   = QDateTime::currentMSecsSinceEpoch();
	m_snapshot = session.backup( "testbackup" );
	m_after    = QDateTime::currentMSecsSinceEpoch();
}

void TestCreateSnapshot::testValidity()
{
	QVERIFY( m_snapshot.isValid() );
}

void TestCreateSnapshot::testError()
{
	QCOMPARE( m_snapshot.error(), BackupSnapshot::NoError );
}

void TestCreateSnapshot::testName()
{
	QCOMPARE( m_snapshot.name(), QString( "testbackup" ) );
}

void TestCreateSnapshot::testPathList()
{
	QDir dir( QDir::current() );
	QList< QString > expected;
	
	expected.append( dir.absoluteFilePath( "file0" ) );
	expected.append( dir.absoluteFilePath( "dir0" ) );
	expected.append( dir.absoluteFilePath( "link0" ) );
	expected.append( dir.absoluteFilePath( "dir0/file1" ) );
	expected.append( dir.absoluteFilePath( "dir0/dir1" ) );
	expected.append( dir.absoluteFilePath( "dir0/link1" ) );
	expected.append( dir.absoluteFilePath( "dir0/dir1/file2" ) );
	expected.append( dir.absoluteFilePath( "dir0/dir1/dir2" ) );
	expected.append( dir.absoluteFilePath( "dir0/dir1/link2" ) );
	
	qSort( expected );
	
	QCOMPARE( m_snapshot.pathList(), expected );
}

void TestCreateSnapshot::testTimestamp()
{
	QTest::qWait( 10 );
	QVERIFY( m_before <= m_snapshot.timestamp().toMSecsSinceEpoch() );
	QVERIFY( m_snapshot.timestamp().toMSecsSinceEpoch() <= m_after );
}

void TestCreateSnapshot::testToString()
{
	QDir dir( QDir::current() );
	
	QString expected( "snapshot testbackup " 
	                  + m_snapshot.timestamp().toString()
	                  + '\n' );
	
	expected += dir.absoluteFilePath( "dir0" ) + '\n';
	expected += dir.absoluteFilePath( "dir0/dir1" ) + '\n';
	expected += dir.absoluteFilePath( "dir0/dir1/dir2" ) + '\n';
	expected += dir.absoluteFilePath( "dir0/dir1/file2" ) + '\n';
	expected += dir.absoluteFilePath( "dir0/dir1/link2" ) + '\n';
	expected += dir.absoluteFilePath( "dir0/file1" ) + '\n';
	expected += dir.absoluteFilePath( "dir0/link1" ) + '\n';
	expected += dir.absoluteFilePath( "file0" ) + '\n';
	expected += dir.absoluteFilePath( "link0" );
	
	QString actual;
	QTextStream stream( &actual );
	
	stream << m_snapshot;
	
	QCOMPARE( actual, expected );
}

void TestCreateSnapshot::testNumberOfCreatedFiles()
{
	QDir storage( "storage" );
	storage.setFilter( QDir::Files );
	QCOMPARE( storage.count(), uint( 12 ) );
}

void TestCreateSnapshot::testSnapshotFile()
{
	QFile snapshot( "storage/"
	              + QString::number( m_snapshot.timestamp().toMSecsSinceEpoch() )
				  + "-testbackup" );
	
	QVERIFY( snapshot.exists() );
	
	snapshot.open( QFile::ReadOnly );
	
	QCryptographicHash hasher( QCryptographicHash::Sha1 );
	QFileInfo info( "dir0" );
	hasher.addData( "directory" );
	hasher.addData( info.absoluteFilePath().toUtf8() );
	hasher.addData( permissions( 'd', info.permissions() ) );
	QCOMPARE( snapshot.readLine(), hasher.result().toHex() + '\n' );
	
	hasher.reset();
	info.setFile( "dir0/dir1" );
	hasher.addData( "directory" );
	hasher.addData( info.absoluteFilePath().toUtf8() );
	hasher.addData( permissions( 'd', info.permissions() ) );
	QCOMPARE( snapshot.readLine(), hasher.result().toHex() + '\n' );
	
	hasher.reset();
	info.setFile( "dir0/dir1/dir2" );
	hasher.addData( "directory" );
	hasher.addData( info.absoluteFilePath().toUtf8() );
	hasher.addData( permissions( 'd', info.permissions() ) );
	QCOMPARE( snapshot.readLine(), hasher.result().toHex() + '\n' );
	
	hasher.reset();
	info.setFile( "dir0/dir1/file2" );
	hasher.addData( "file" );
	hasher.addData( info.absoluteFilePath().toUtf8() );
	hasher.addData( permissions( '-', info.permissions() ) );
	hasher.addData( info.lastModified().toString().toUtf8() );
	QCOMPARE( snapshot.readLine(), hasher.result().toHex() + '\n' );
	
	hasher.reset();
	info.setFile( "dir0/dir1/link2" );
	hasher.addData( "symlink" );
	hasher.addData( info.absoluteFilePath().toUtf8() );
	hasher.addData( "infinity" );
	QCOMPARE( snapshot.readLine(), hasher.result().toHex() + '\n' );
	
	hasher.reset();
	info.setFile( "dir0/file1" );
	hasher.addData( "file" );
	hasher.addData( info.absoluteFilePath().toUtf8() );
	hasher.addData( permissions( '-', info.permissions() ) );
	hasher.addData( info.lastModified().toString().toUtf8() );
	QCOMPARE( snapshot.readLine(), hasher.result().toHex() + '\n' );
	
	hasher.reset();
	info.setFile( "dir0/link1" );
	hasher.addData( "symlink" );
	hasher.addData( info.absoluteFilePath().toUtf8() );
	hasher.addData( "file1" );
	QCOMPARE( snapshot.readLine(), hasher.result().toHex() + '\n' );
	
	hasher.reset();
	info.setFile( "file0" );
	hasher.addData( "file" );
	hasher.addData( info.absoluteFilePath().toUtf8() );
	hasher.addData( permissions( '-', info.permissions() ) );
	hasher.addData( info.lastModified().toString().toUtf8() );
	QCOMPARE( snapshot.readLine(), hasher.result().toHex() + '\n' );
	
	hasher.reset();
	info.setFile( "link0" );
	hasher.addData( "symlink" );
	hasher.addData( info.absoluteFilePath().toUtf8() );
	hasher.addData( "dir0" );
	QCOMPARE( snapshot.readLine(), hasher.result().toHex() + '\n' );
}

void TestCreateSnapshot::testFile0()
{
	QCryptographicHash hasher( QCryptographicHash::Sha1 );
	QFileInfo info( "file0" );
	hasher.addData( "file" );
	hasher.addData( info.absoluteFilePath().toUtf8() );
	hasher.addData( permissions( '-', info.permissions() ) );
	hasher.addData( info.lastModified().toString().toUtf8() );
	
	QFile file0( "storage/" + hasher.result().toHex() );
	QVERIFY( file0.exists() );
	
	hasher.reset();
	hasher.addData( "This is a shared file!\n" );
	
	QByteArray contentsHash( hasher.result().toHex() );
	QFile contents( "storage/" + contentsHash );
	QVERIFY( contents.exists() );
	
	contents.open( QFile::ReadOnly );
	QCOMPARE( contents.readAll(), QByteArray( "This is a shared file!\n" ) );
	
	file0.open( QFile::ReadOnly );
	QXmlStreamReader reader( &file0 );
	
	QXmlStreamReader::TokenType token( reader.readNext() );
	QCOMPARE( token, QXmlStreamReader::StartDocument );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "file" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "path" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::Characters );
	QCOMPARE( reader.text().toString(), info.absoluteFilePath() );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "path" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "permissions" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::Characters );
	QCOMPARE( reader.text().toUtf8(), permissions( '-', info.permissions() ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "permissions" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "contents" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::Characters );
	QCOMPARE( reader.text().toUtf8(), contentsHash );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "contents" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "file" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndDocument );
}

void TestCreateSnapshot::testLink0()
{
	QCryptographicHash hasher( QCryptographicHash::Sha1 );
	QFileInfo info( "link0" );
	hasher.addData( "symlink" );
	hasher.addData( info.absoluteFilePath().toUtf8() );
	hasher.addData( "dir0" );
	
	QFile link0( "storage/" + hasher.result().toHex() );
	QVERIFY( link0.exists() );
	
	link0.open( QFile::ReadOnly );
	QXmlStreamReader reader( &link0 );
	
	QXmlStreamReader::TokenType token( reader.readNext() );
	QCOMPARE( token, QXmlStreamReader::StartDocument );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "symlink" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "path" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::Characters );
	QCOMPARE( reader.text().toString(), info.absoluteFilePath() );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "path" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "target" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::Characters );
	QCOMPARE( reader.text().toString(), QString( "dir0" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "target" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "symlink" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndDocument );
}

void TestCreateSnapshot::testDir0()
{
	QCryptographicHash hasher( QCryptographicHash::Sha1 );
	QFileInfo info( "dir0" );
	hasher.addData( "directory" );
	hasher.addData( info.absoluteFilePath().toUtf8() );
	hasher.addData( permissions( 'd', info.permissions() ) );
	
	QFile dir0( "storage/" + hasher.result().toHex() );
	QVERIFY( dir0.exists() );
	
	dir0.open( QFile::ReadOnly );
	QXmlStreamReader reader( &dir0 );
	
	QXmlStreamReader::TokenType token( reader.readNext() );
	QCOMPARE( token, QXmlStreamReader::StartDocument );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "directory" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "path" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::Characters );
	QCOMPARE( reader.text().toString(), info.absoluteFilePath() );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "path" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "permissions" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::Characters );
	QCOMPARE( reader.text().toUtf8(), permissions( 'd', info.permissions() ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "permissions" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "directory" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndDocument );
}

void TestCreateSnapshot::testFile1()
{
	QCryptographicHash hasher( QCryptographicHash::Sha1 );
	QFileInfo info( "dir0/file1" );
	hasher.addData( "file" );
	hasher.addData( info.absoluteFilePath().toUtf8() );
	hasher.addData( permissions( '-', info.permissions() ) );
	hasher.addData( info.lastModified().toString().toUtf8() );
	
	QFile file1( "storage/" + hasher.result().toHex() );
	QVERIFY( file1.exists() );
	
	hasher.reset();
	hasher.addData( "This is a unique file!\n" );
	
	QByteArray contentsHash( hasher.result().toHex() );
	QFile contents( "storage/" + contentsHash );
	QVERIFY( contents.exists() );
	
	contents.open( QFile::ReadOnly );
	QCOMPARE( contents.readAll(), QByteArray( "This is a unique file!\n" ) );
	
	file1.open( QFile::ReadOnly );
	QXmlStreamReader reader( &file1 );
	
	QXmlStreamReader::TokenType token( reader.readNext() );
	QCOMPARE( token, QXmlStreamReader::StartDocument );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "file" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "path" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::Characters );
	QCOMPARE( reader.text().toString(), info.absoluteFilePath() );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "path" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "permissions" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::Characters );
	QCOMPARE( reader.text().toUtf8(), permissions( '-', info.permissions() ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "permissions" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "contents" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::Characters );
	QCOMPARE( reader.text().toUtf8(), contentsHash );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "contents" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "file" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndDocument );
}

void TestCreateSnapshot::testLink1()
{
	QCryptographicHash hasher( QCryptographicHash::Sha1 );
	QFileInfo info( "dir0/link1" );
	hasher.addData( "symlink" );
	hasher.addData( info.absoluteFilePath().toUtf8() );
	hasher.addData( "file1" );
	
	QFile link1( "storage/" + hasher.result().toHex() );
	QVERIFY( link1.exists() );
	
	link1.open( QFile::ReadOnly );
	QXmlStreamReader reader( &link1 );
	
	QXmlStreamReader::TokenType token( reader.readNext() );
	QCOMPARE( token, QXmlStreamReader::StartDocument );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "symlink" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "path" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::Characters );
	QCOMPARE( reader.text().toString(), info.absoluteFilePath() );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "path" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "target" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::Characters );
	QCOMPARE( reader.text().toString(), QString( "file1" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "target" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "symlink" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndDocument );
}

void TestCreateSnapshot::testDir1()
{
	QCryptographicHash hasher( QCryptographicHash::Sha1 );
	QFileInfo info( "dir0/dir1" );
	hasher.addData( "directory" );
	hasher.addData( info.absoluteFilePath().toUtf8() );
	hasher.addData( permissions( 'd', info.permissions() ) );
	
	QFile dir1( "storage/" + hasher.result().toHex() );
	QVERIFY( dir1.exists() );
	
	dir1.open( QFile::ReadOnly );
	QXmlStreamReader reader( &dir1 );
	
	QXmlStreamReader::TokenType token( reader.readNext() );
	QCOMPARE( token, QXmlStreamReader::StartDocument );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "directory" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "path" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::Characters );
	QCOMPARE( reader.text().toString(), info.absoluteFilePath() );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "path" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "permissions" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::Characters );
	QCOMPARE( reader.text().toUtf8(), permissions( 'd', info.permissions() ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "permissions" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "directory" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndDocument );
}

void TestCreateSnapshot::testFile2()
{
	QCryptographicHash hasher( QCryptographicHash::Sha1 );
	QFileInfo info( "dir0/dir1/file2" );
	hasher.addData( "file" );
	hasher.addData( info.absoluteFilePath().toUtf8() );
	hasher.addData( permissions( '-', info.permissions() ) );
	hasher.addData( info.lastModified().toString().toUtf8() );
	
	QFile file2( "storage/" + hasher.result().toHex() );
	QVERIFY( file2.exists() );
	
	hasher.reset();
	hasher.addData( "This is a shared file!\n" );
	
	QByteArray contentsHash( hasher.result().toHex() );
	QFile contents( "storage/" + contentsHash );
	QVERIFY( contents.exists() );
	
	contents.open( QFile::ReadOnly );
	QCOMPARE( contents.readAll(), QByteArray( "This is a shared file!\n" ) );
	
	file2.open( QFile::ReadOnly );
	QXmlStreamReader reader( &file2 );
	
	QXmlStreamReader::TokenType token( reader.readNext() );
	QCOMPARE( token, QXmlStreamReader::StartDocument );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "file" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "path" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::Characters );
	QCOMPARE( reader.text().toString(), info.absoluteFilePath() );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "path" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "permissions" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::Characters );
	QCOMPARE( reader.text().toUtf8(), permissions( '-', info.permissions() ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "permissions" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "contents" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::Characters );
	QCOMPARE( reader.text().toUtf8(), contentsHash );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "contents" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "file" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndDocument );
}

void TestCreateSnapshot::testLink2()
{
	QCryptographicHash hasher( QCryptographicHash::Sha1 );
	QFileInfo info( "dir0/dir1/link2" );
	hasher.addData( "symlink" );
	hasher.addData( info.absoluteFilePath().toUtf8() );
	hasher.addData( "infinity" );
	
	QFile link2( "storage/" + hasher.result().toHex() );
	QVERIFY( link2.exists() );
	
	link2.open( QFile::ReadOnly );
	QXmlStreamReader reader( &link2 );
	
	QXmlStreamReader::TokenType token( reader.readNext() );
	QCOMPARE( token, QXmlStreamReader::StartDocument );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "symlink" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "path" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::Characters );
	QCOMPARE( reader.text().toString(), info.absoluteFilePath() );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "path" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "target" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::Characters );
	QCOMPARE( reader.text().toString(), QString( "infinity" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "target" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "symlink" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndDocument );
}

void TestCreateSnapshot::testDir2()
{
	QCryptographicHash hasher( QCryptographicHash::Sha1 );
	QFileInfo info( "dir0/dir1/dir2" );
	hasher.addData( "directory" );
	hasher.addData( info.absoluteFilePath().toUtf8() );
	hasher.addData( permissions( 'd', info.permissions() ) );
	
	QFile dir2( "storage/" + hasher.result().toHex() );
	QVERIFY( dir2.exists() );
	
	dir2.open( QFile::ReadOnly );
	QXmlStreamReader reader( &dir2 );
	
	QXmlStreamReader::TokenType token( reader.readNext() );
	QCOMPARE( token, QXmlStreamReader::StartDocument );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "directory" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "path" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::Characters );
	QCOMPARE( reader.text().toString(), info.absoluteFilePath() );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "path" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "permissions" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::Characters );
	QCOMPARE( reader.text().toUtf8(), permissions( 'd', info.permissions() ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "permissions" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "directory" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndDocument );
}

QXmlStreamReader::TokenType TestCreateSnapshot::readNextToken( QXmlStreamReader* reader ) const
{
	QXmlStreamReader::TokenType token;
	
	while ( !reader->atEnd() )
	{
		token = reader->readNext();
		
		if ( !reader->isWhitespace() )
			return token;
	}
	
	return QXmlStreamReader::Invalid;
}

QByteArray TestCreateSnapshot::permissions( char type, QFile::Permissions filePermissions ) const
{
	QByteArray ret( 10, '-' );
	ret[0] = type;
	
	if ( filePermissions & QFile::ReadUser   ) ret[1] = 'r';
	if ( filePermissions & QFile::WriteUser  ) ret[2] = 'w';
	if ( filePermissions & QFile::ExeUser    ) ret[3] = 'x';
	if ( filePermissions & QFile::ReadGroup  ) ret[4] = 'r';
	if ( filePermissions & QFile::WriteGroup ) ret[5] = 'w';
	if ( filePermissions & QFile::ExeGroup   ) ret[6] = 'x';
	if ( filePermissions & QFile::ReadOther  ) ret[7] = 'r';
	if ( filePermissions & QFile::WriteOther ) ret[8] = 'w';
	if ( filePermissions & QFile::ExeOther   ) ret[9] = 'x';
	
	return ret;
}

QTEST_MAIN(TestCreateSnapshot)
