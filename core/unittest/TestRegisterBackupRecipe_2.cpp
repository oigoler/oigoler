/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestRegisterBackupRecipe_2.h"
#include "DirectoryGenerator.h"
#include "DirectoryCleaner.h"
#include "XmlTester.h"
#include "../BackupRecipe.h"
#include "../OigolerSession.h"


void TestRegisterBackupRecipe_2::initTestCase()
{
	m_successCount = 0;
	m_failureCount = 0;
	
	new DirectoryGenerator( "storage", this );
	new DirectoryCleaner( "config", this );
	
	m_recipe = new BackupRecipe;
	m_recipe->setName( "testrecipe" );
	m_recipe->insertPath( "." );
	m_recipe->setStoragePath( "storage" );
	
	m_session = new OigolerSession;
	
	connect( m_session, SIGNAL(backupRecipeRegistered(QString))
	       , this, SLOT(recordSuccess(QString)) );
	
	connect( m_session, SIGNAL(failed()), this, SLOT(recordFailure()) );
	
	m_session->setConfigPath( "config" );
	m_session->registerBackupRecipe( m_recipe );
	
	m_recipe->insertPath( ".." );
	m_session->registerBackupRecipe( m_recipe );
}

void TestRegisterBackupRecipe_2::testSignals()
{
	QCOMPARE( m_successCount, 2 );
	QCOMPARE( m_failureCount, 0 );
	QCOMPARE( m_recipeName, m_recipe->name() );
}

void TestRegisterBackupRecipe_2::testError()
{
	QCOMPARE( m_session->lastError(), OigolerSession::NoError );
}

void TestRegisterBackupRecipe_2::testFileName()
{
	QDir config( "config/recipe" );
	QVERIFY( config.exists() );
	QVERIFY( config.exists( "testrecipe.xml" ) );
}

void TestRegisterBackupRecipe_2::testFileContents()
{
	QDir config( "config/recipe" );
	QFile file( config.filePath( "testrecipe.xml" ) );
	QVERIFY( file.exists() );
	file.open( QFile::ReadOnly );
	
	QXmlStreamReader reader( &file );
	XmlTester tester( &reader );
	
	QVERIFY( tester.testStartDocument() );
	QVERIFY( tester.testStartElement( "backuprecipe" ) );
	QVERIFY( tester.testStartElement( "path" ) );
	QVERIFY( tester.testCharacters( m_recipe->pathList().first() ) );
	QVERIFY( tester.testEndElement( "path" ) );
	QVERIFY( tester.testStartElement( "path" ) );
	QVERIFY( tester.testCharacters( m_recipe->pathList().last() ) );
	QVERIFY( tester.testEndElement( "path" ) );
	QVERIFY( tester.testStartElement( "storage" ) );
	QVERIFY( tester.testCharacters( m_recipe->storagePath() ) );
	QVERIFY( tester.testEndElement( "storage" ) );
	QVERIFY( tester.testEndElement( "backuprecipe" ) );
	QVERIFY( tester.testEndDocument() );
}

void TestRegisterBackupRecipe_2::cleanupTestCase()
{
	delete m_session;
	delete m_recipe;
}

void TestRegisterBackupRecipe_2::recordSuccess( const QString& recipeName )
{
	++m_successCount;
	m_recipeName = recipeName;
}

void TestRegisterBackupRecipe_2::recordFailure()
{
	++m_failureCount;
}

QTEST_MAIN(TestRegisterBackupRecipe_2)
