/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestCreateStorage.h"
#include "DirectoryCleaner.h"
#include "../BackupSession.h"


void TestCreateStorage::initTestCase()
{
	BackupSession session;
	session.setConfigDirectory( "config" );
	new DirectoryCleaner( "config", this );
	
	QDir::current().mkdir( "storage" );
	new DirectoryCleaner( "storage", this );
	
	m_storage = session.createStorage( "storage" );
}

void TestCreateStorage::testStorageKey()
{
	QString storagePath( QFileInfo( "storage" ).absoluteFilePath() );

	QCryptographicHash hasher( QCryptographicHash::Sha1 );
	hasher.addData( storagePath.toUtf8() );
	
	QCOMPARE( m_storage.key(), hasher.result().toHex() );
}

void TestCreateStorage::testStorageStringRepresentation()
{
	QString expected( m_storage.key()
	                + ' '
	                + QFileInfo( "storage" ).absoluteFilePath() );
	
	QString actual;
	QTextStream stream( &actual );
	
	stream << m_storage;
	
	QCOMPARE( actual, expected );
}

void TestCreateStorage::testCreatedFile()
{
	QFile file( "config/storage/" + m_storage.key() );
	QVERIFY( file.exists() );
	
	QString storagePath( QFileInfo( "storage" ).absoluteFilePath() );
	
	file.open( QFile::ReadOnly );
	QCOMPARE( QString( file.readAll() ), storagePath + '\n' );
}

void TestCreateStorage::testStorageValidity()
{
	QVERIFY( m_storage.isValid() );
}

void TestCreateStorage::testStorageError()
{
	QCOMPARE( m_storage.error(), BackupStorage::NoError );
}

QTEST_MAIN(TestCreateStorage)
