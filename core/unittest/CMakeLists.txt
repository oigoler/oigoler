###############################################################################
# oigoler_add_test( TestSomething library1 library2 ... )
###############################################################################

macro( oigoler_add_test targetname )
	set( ${targetname}_SRCS ${targetname}.cpp )
	set( ${targetname}_HDRS ${targetname}.h )
	qt4_wrap_cpp( ${targetname}_MOCS ${${targetname}_HDRS} )
	add_executable( ${targetname} ${${targetname}_SRCS} ${${targetname}_MOCS} )
	target_link_libraries( ${targetname} ${ARGN} ${QT_QTTEST_LIBRARY} )
	add_test( NAME ${targetname} COMMAND ${targetname} )
endmacro( oigoler_add_test )


###############################################################################
# oigolertesthelper
###############################################################################

set( oigolertesthelper_SRCS DirectoryCleaner.cpp
                            DirectoryGenerator.cpp
                            FileGenerator.cpp
                            XmlTester.cpp )

set( oigolertesthelper_HDRS DirectoryCleaner.h
                            DirectoryGenerator.h
                            FileGenerator.h )

qt4_wrap_cpp( oigolertesthelper_MOCS ${oigolertesthelper_HDRS} )

add_library( oigolertesthelper STATIC ${oigolertesthelper_SRCS}
                                      ${oigolertesthelper_MOCS} )

target_link_libraries( oigolertesthelper ${QT_LIBRARIES} )


###############################################################################
# Tests
###############################################################################

oigoler_add_test( TestBackupRecipe_Class   oigolercore )
oigoler_add_test( TestOigolerSession_Class oigolercore )
oigoler_add_test( TestRestoreRecipe_Class  oigolercore )
oigoler_add_test( TestSnapshot_Class       oigolercore )

oigoler_add_test( TestRegisterBackupRecipe_1 oigolercore oigolertesthelper )
oigoler_add_test( TestRegisterBackupRecipe_2 oigolercore oigolertesthelper )

oigoler_add_test( TestRegisterBackupRecipe_ConfigError_1 oigolercore
                                                         oigolertesthelper )

oigoler_add_test( TestRegisterBackupRecipe_ConfigError_2 oigolercore
                                                         oigolertesthelper )

oigoler_add_test( TestRegisterBackupRecipe_ConfigError_3 oigolercore
                                                         oigolertesthelper )

oigoler_add_test( TestRegisterBackupRecipe_IOError oigolercore
                                                   oigolertesthelper )

oigoler_add_test( TestRegisterBackupRecipe_InvalidRecipeName
                  oigolercore
                  oigolertesthelper )

oigoler_add_test( TestRegisterBackupRecipe_InvalidPath oigolercore
                                                       oigolertesthelper )

oigoler_add_test( TestRegisterBackupRecipe_InvalidStorage oigolercore
                                                          oigolertesthelper )

oigoler_add_test( TestGetBackupRecipe oigolercore oigolertesthelper )

oigoler_add_test( TestRemoveBackupRecipe_1 oigolercore oigolertesthelper )
oigoler_add_test( TestRemoveBackupRecipe_2 oigolercore oigolertesthelper )
oigoler_add_test( TestRemoveBackupRecipe_ConfigError oigolercore )


