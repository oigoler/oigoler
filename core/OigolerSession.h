/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef OIGOLERSESSION_H
#define OIGOLERSESSION_H

#include <QObject>
#include <QString>

class BackupRecipe;


class OigolerSession : public QObject
{
	Q_OBJECT
	
public:
	enum OigolerSessionError { NoError
	                         , ConfigError
	                         , IOError
	                         , InvalidRecipeName
	                         , InvalidPath
	                         , InvalidStorage };

public:
	OigolerSession( QObject* parent = 0 );
	virtual ~OigolerSession();
	
	QString configPath() const;
	void setConfigPath( const QString& path );
	
	OigolerSessionError lastError() const;
	
	bool backupRecipeExists( const QString& recipeName ) const;
	BackupRecipe* backupRecipe( const QString& recipeName ) const;
	QList< BackupRecipe > backupRecipeList() const;
	
public slots:
	void registerBackupRecipe( BackupRecipe* recipe );
	void removeBackupRecipe( const QString& recipeName );
	
signals:
	void failed();
	void backupRecipeRegistered( const QString& recipeName );
	void backupRecipeRemoved( const QString& recipeName );
	
private:
	bool checkConfigPath();
	bool testBackupRecipeName( BackupRecipe* recipe );
	bool testPath( BackupRecipe* recipe );
	bool testStorage( BackupRecipe* recipe );
	
private:
	OigolerSession( const OigolerSession& other );
	OigolerSession& operator=( const OigolerSession& other );
	
	QString m_configPath;
	OigolerSessionError m_error;
	
};

#endif // OIGOLERSESSION_H
