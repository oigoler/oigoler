/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "Snapshot.h"


Snapshot::Snapshot()
{
}

Snapshot::Snapshot( const Snapshot& other )
	: m_name( other.m_name )
	, m_timestamp( other.m_timestamp )
	, m_restoreRecipeList( other.m_restoreRecipeList )
{
}

Snapshot::~Snapshot()
{
}

Snapshot& Snapshot::operator=( const Snapshot& other )
{
	if ( this == &other )
		return *this;
	
	m_name = other.m_name;
	m_timestamp = other.m_timestamp;
	m_restoreRecipeList = other.m_restoreRecipeList;
	
	return *this;
}

QString Snapshot::name() const
{
	return m_name;
}

void Snapshot::setName( const QString& name )
{
	m_name = name;
}

QDateTime Snapshot::timestamp() const
{
	return m_timestamp;
}

void Snapshot::setTimestamp( const QDateTime& timestamp )
{
	m_timestamp = timestamp;
}

QString Snapshot::key() const
{
	if ( !m_timestamp.isValid() )
		return QString();
	
	return m_name
	     + '_'
	     + QString::number( m_timestamp.toMSecsSinceEpoch() );
}

QList< RestoreRecipe > Snapshot::restoreRecipeList() const
{
	return m_restoreRecipeList;
}

void Snapshot::insertRestoreRecipe( const RestoreRecipe& recipe )
{
	m_restoreRecipeList.append( recipe );
}

bool Snapshot::containsRestoreRecipe( const RestoreRecipe& recipe ) const
{
	foreach ( const RestoreRecipe& elem, m_restoreRecipeList )
	{
		if ( elem.key() == recipe.key() )
			return true;
	}
	
	return false;
}

void Snapshot::removeRestoreRecipe( const RestoreRecipe& recipe )
{
	for ( int i = 0; i < m_restoreRecipeList.size(); ++i )
	{
		if ( m_restoreRecipeList.at(i).key() == recipe.key() )
		{
			m_restoreRecipeList.removeAt( i );
			break;
		}
	}
}

void Snapshot::clearRestoreRecipeList()
{
	m_restoreRecipeList.clear();
}
