/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "BackupFileAction.h"

#include <QFileInfo>
#include <QCryptographicHash>
#include <QDateTime>
#include <QXmlStreamWriter>


BackupFileAction::BackupFileAction()
{
}

BackupFileAction::~BackupFileAction()
{
}

void BackupFileAction::setConfigDirectory( const QString& path )
{
	m_configPath = path + "/storage";
}

QString BackupFileAction::execute( const QString& file, const QString& storageId )
{
	QFileInfo info( file );
	QByteArray permissions( 10, '-' );
	if ( info.permission( QFile::ReadUser   ) ) permissions[1] = 'r';
	if ( info.permission( QFile::WriteUser  ) ) permissions[2] = 'w';
	if ( info.permission( QFile::ExeUser    ) ) permissions[3] = 'x';
	if ( info.permission( QFile::ReadGroup  ) ) permissions[4] = 'r';
	if ( info.permission( QFile::WriteGroup ) ) permissions[5] = 'w';
	if ( info.permission( QFile::ExeGroup   ) ) permissions[6] = 'x';
	if ( info.permission( QFile::ReadOther  ) ) permissions[7] = 'r';
	if ( info.permission( QFile::WriteOther ) ) permissions[8] = 'w';
	if ( info.permission( QFile::ExeOther   ) ) permissions[9] = 'x';
	
	QCryptographicHash hasher( QCryptographicHash::Sha1 );
	hasher.addData( "file" );
	hasher.addData( info.absoluteFilePath().toUtf8() );
	hasher.addData( permissions );
	hasher.addData( info.lastModified().toString( Qt::ISODate ).toUtf8() );
	QByteArray fileHash( hasher.result().toHex() );
	
	QFile original( file );
	original.open( QFile::ReadOnly );
	hasher.reset();
	
	while ( !original.atEnd() )
		hasher.addData( original.read( 2048 ) );
	
	QByteArray dataHash( hasher.result().toHex() );
	original.close();

	QFile storageFile( m_configPath + '/' + storageId );
	storageFile.open( QFile::ReadOnly );
	
	QString storagePath( storageFile.readLine() );
	storagePath.chop( 1 );
	storageFile.close();
	
	QFile xmlFile( storagePath + '/' + fileHash + ".xml" );
	xmlFile.open( QFile::WriteOnly );
	
	QXmlStreamWriter writer( &xmlFile );
	writer.setAutoFormatting( true );
	writer.writeStartDocument();
	writer.writeStartElement( "file" );
	writer.writeTextElement( "path", info.absoluteFilePath() );
	writer.writeTextElement( "permissions", permissions );
	writer.writeTextElement( "contents", dataHash );
	writer.writeEndDocument();
	xmlFile.close();
	
	QFile::copy( file, storagePath + '/' + dataHash );

	return fileHash + ' ' + permissions + ' ' + info.absoluteFilePath() + '\n';
}
