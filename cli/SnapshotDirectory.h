/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef SNAPSHOTDIRECTORY_H
#define SNAPSHOTDIRECTORY_H

#include "SnapshotEntity.h"
#include "Permissions.h"
#include "SnapshotEntityList.h"

#include <QString>

class DefinitionPath;


class SnapshotDirectory : public SnapshotEntity
{
	Q_OBJECT

public:
	SnapshotDirectory();
	virtual ~SnapshotDirectory();
	
	void setPath( const QString& path );
	QString path() const;
	
	void setPermissions( const Permissions& permissions );
	Permissions permissions() const;
	
	const SnapshotEntityList& entries() const;
	void addEntry( SnapshotEntity* entry );
	
	QByteArray id() const;
	
	void toXml( QXmlStreamWriter* writer ) const;
	
	void copyTo( const QString& path ) const;
	
	void fromDirectory( const QString& path );
	void fromDirectory( const DefinitionPath& path );
	
	static SnapshotDirectory* createFromDirectory( const QString& path );
	static SnapshotDirectory* createFromDirectory( const DefinitionPath& path );
	
private:
	QString m_path;
	Permissions m_permissions;
	SnapshotEntityList m_entries;
	
};

#endif // SNAPSHOTDIRECTORY_H
