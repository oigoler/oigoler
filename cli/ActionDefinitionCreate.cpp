/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "ActionDefinitionCreate.h"
#include "Definition.h"

#include <QTextStream>
#include <QStringList>


ActionDefinitionCreate::ActionDefinitionCreate()
{
}

ActionDefinitionCreate::~ActionDefinitionCreate()
{
}

void ActionDefinitionCreate::execute( const QStringList& args )
{
	if ( args.size() < 2 )
		return;
	
	if ( backupEnvironment().definitionExists( args.at( 0 ) ) )
	{
		outputStream() << "Definition '" << args.at( 0 ) << "' already exists!\n";
		return;
	}
	
	Definition definition;
	definition.setName( args.at( 0 ) );
	definition.setBackupStorage( DefinitionStorage( args.at( 1 ) ) );
	
	for ( int i = 2; i < args.size(); ++i )
		definition.addPath( DefinitionPath( args.at( i ) ) );
	
	backupEnvironment().storeDefinition( definition );
	
	outputStream() << "Created definition '" << args.at( 0 ) << "'\n";
}
