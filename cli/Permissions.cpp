/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "Permissions.h"


Permissions::Permissions()
	: m_user( 0 )
	, m_group( 0 )
	, m_other( 0 )
{
}

Permissions::Permissions( QFile::Permissions permissions )
{
	setPermissions( permissions );
}

Permissions::Permissions( int permissions )
	: m_user( permissions / 100 )
	, m_group( ( permissions / 10 ) % 10 )
	, m_other( permissions % 10 )
{
}

Permissions::Permissions( const Permissions& other )
	: m_user( other.m_user )
	, m_group( other.m_group )
	, m_other( other.m_other )
{
}

Permissions::~Permissions()
{
}

Permissions& Permissions::operator=( const Permissions& other )
{
	if ( this == &other )
		return *this;
	
	m_user = other.m_user;
	m_group = other.m_group;
	m_other = other.m_other;
	
	return *this;
}

void Permissions::setPermissions( QFile::Permissions permissions )
{
	m_user = m_group = m_other = 0;
	
	if ( permissions.testFlag( QFile::ReadOwner ) )
		m_user = 4;
	if ( permissions.testFlag( QFile::WriteOwner ) )
		m_user += 2;
	if ( permissions.testFlag( QFile::ExeOwner ) )
		m_user += 1;
	
	if ( permissions.testFlag( QFile::ReadGroup ) )
		m_group = 4;
	if ( permissions.testFlag( QFile::WriteGroup ) )
		m_group += 2;
	if ( permissions.testFlag( QFile::ExeGroup ) )
		m_group += 1;
	
	if ( permissions.testFlag( QFile::ReadOther ) )
		m_other = 4;
	if ( permissions.testFlag( QFile::WriteOther ) )
		m_other += 2;
	if ( permissions.testFlag( QFile::ExeOther ) )
		m_other += 1;
}

void Permissions::setPermissions( int permissions )
{
	m_user = permissions / 100;
	m_group = ( permissions / 10 ) % 10;
	m_other = permissions % 10;
}

QString Permissions::toString() const
{
	return QString::number( m_user )
	     + QString::number( m_group )
		 + QString::number( m_other );
}
