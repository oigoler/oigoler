/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "DefinitionStorage.h"

#include <QFileInfo>
#include <QXmlStreamWriter>


DefinitionStorage::DefinitionStorage()
{
}

DefinitionStorage::DefinitionStorage( const QString& path )
	: m_path( path )
{
}

DefinitionStorage::DefinitionStorage( const DefinitionStorage& other )
	: m_path( other.m_path )
{
}

DefinitionStorage::~DefinitionStorage()
{
}

bool DefinitionStorage::operator==( const DefinitionStorage& other ) const
{
	return ( m_path == other.m_path );
}

bool DefinitionStorage::operator!=( const DefinitionStorage& other ) const
{
	return ( m_path != other.m_path );
}

DefinitionStorage& DefinitionStorage::operator=( const DefinitionStorage& other )
{
	if ( this == &other )
		return *this;
	
	m_path = other.m_path;
	
	return *this;
}

void DefinitionStorage::setPath( const QString& path )
{
	m_path = path;
}

QString DefinitionStorage::path() const
{
	return m_path;
}

void DefinitionStorage::toXml( QXmlStreamWriter* writer ) const
{
	QFileInfo info( m_path );
	writer->writeTextElement( "BackupStorage", info.absoluteFilePath() );
}

void DefinitionStorage::fromXml( QXmlStreamReader* reader )
{
	m_path.clear();
	
	if ( reader->name() != "BackupStorage" )
		return;
	
	if ( reader->readNext() != QXmlStreamReader::Characters )
		return;
	
	m_path = reader->text().toString();
	reader->readNext(); // End Tag
}

QString DefinitionStorage::toString() const
{
	QFileInfo info( m_path );
	return "storage " + info.absoluteFilePath();
}
