/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "SnapshotLink.h"
#include "DefinitionPath.h"

#include <QFileInfo>
#include <QXmlStreamWriter>
#include <QCryptographicHash>


SnapshotLink::SnapshotLink()
{
}

SnapshotLink::~SnapshotLink()
{
}

void SnapshotLink::setPath( const QString& path )
{
	m_path = path;
}

QString SnapshotLink::path() const
{
	return m_path;
}

void SnapshotLink::setTarget( const QString& target )
{
	m_target = target;
}

QString SnapshotLink::target() const
{
	return m_target;
}

QByteArray SnapshotLink::id() const
{
	QCryptographicHash hasher( QCryptographicHash::Sha1 );
	
	hasher.addData( "link" );
	hasher.addData( m_path.toUtf8() );
	hasher.addData( m_target.toUtf8() );
	
	return hasher.result().toHex();
}

void SnapshotLink::toXml( QXmlStreamWriter* writer ) const
{
	QFileInfo info( m_path );
	
	writer->writeStartElement( "link" );
	writer->writeTextElement( "path"  , m_path );
	writer->writeTextElement( "target", m_target );
	writer->writeEndElement();
}

void SnapshotLink::copyTo( const QString& path ) const
{
}

void SnapshotLink::fromLink( const QString& path )
{
	QFileInfo info( path );
	
	m_path = info.absoluteFilePath();
	m_target = p_target();
}

void SnapshotLink::fromLink( const DefinitionPath& path )
{
	fromLink( path.path() );
}

SnapshotLink* SnapshotLink::createFromLink( const QString& path )
{
	SnapshotLink* ret( new SnapshotLink );
	ret->fromLink( path );
	
	return ret;
}

SnapshotLink* SnapshotLink::createFromLink( const DefinitionPath& path )
{
	SnapshotLink* ret( new SnapshotLink );
	ret->fromLink( path );
	
	return ret;
}

QString SnapshotLink::p_target() const
{
	size_t bufsize( 1024 );
	ssize_t retsize( 0 );
	char* buf( 0 );
	
	while ( true )
	{
		buf = (char*) realloc( buf, bufsize );
		retsize = readlink( m_path.toUtf8().data(), buf, bufsize );
		
		if ( retsize < 0 )
		{
			free( buf );
			return QString();
		}
		
		if ( retsize < bufsize )
		{
			buf[retsize] = '\0';
			break;
		}
		
		bufsize *= 2;
	}
	
	QString target( buf );
	free( buf );
	
	return target;
}
