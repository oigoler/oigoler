/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef SNAPSHOTENTITYLIST_H
#define SNAPSHOTENTITYLIST_H

#include <QList>

class SnapshotEntity;
class QXmlStreamWriter;


class SnapshotEntityList : public QList< SnapshotEntity* >
{

public:
	SnapshotEntityList();
	SnapshotEntityList( const SnapshotEntityList& other );
	virtual ~SnapshotEntityList();
	
	SnapshotEntityList& operator=( const SnapshotEntityList& other );
	
	void toXml( QXmlStreamWriter* writer ) const;
	
};

#endif // SNAPSHOTENTITYLIST_H
