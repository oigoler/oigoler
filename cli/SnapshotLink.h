/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef SNAPSHOTLINK_H
#define SNAPSHOTLINK_H

#include "SnapshotEntity.h"

#include <QString>

class DefinitionPath;


class SnapshotLink : public SnapshotEntity
{
	Q_OBJECT

public:
	SnapshotLink();
	virtual ~SnapshotLink();
	
	void setPath( const QString& path );
	QString path() const;
	
	void setTarget( const QString& target );
	QString target() const;
	
	QByteArray id() const;
	
	void toXml( QXmlStreamWriter* writer ) const;
	
	void copyTo( const QString& path ) const;
	
	void fromLink( const QString& path );
	void fromLink( const DefinitionPath& path );
	
	static SnapshotLink* createFromLink( const QString& path );
	static SnapshotLink* createFromLink( const DefinitionPath& path );
	
private:
	QString p_target() const;
	
private:
	QString m_path;
	QString m_target;
	
};

#endif // SNAPSHOTLINK_H
