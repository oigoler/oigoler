/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "DefinitionEntityList.h"

#include <QXmlStreamWriter>
#include <QStringList>


DefinitionEntityList::DefinitionEntityList()
{
}

DefinitionEntityList::~DefinitionEntityList()
{
}

void DefinitionEntityList::toXml( QXmlStreamWriter* writer ) const
{
	writer->writeStartElement( "BackupObjectList" );
	
	foreach ( const DefinitionPath& path, *this )
		path.toXml( writer );
	
	writer->writeEndElement();
}

void DefinitionEntityList::fromXml( QXmlStreamReader* reader )
{
	if ( reader->name() != "BackupObjectList" )
		return;
	
	clear();
	
	while ( reader->readNextStartElement() )
	{
		DefinitionPath path;
		path.fromXml( reader );
		append( path );
	}
}

QStringList DefinitionEntityList::toStringList() const
{
	QStringList ret;
	
	foreach ( const DefinitionPath& path, *this )
		ret.append( path.toString() );
	
	return ret;
}
