/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "Action.h"

#include <QTextStream>


Action::Action()
	: m_writer( p_defaultOutputStream() )
{
}

Action::~Action()
{
	delete m_writer;
}

void Action::setBackupEnvironment( const Environment& environment )
{
	m_environment = environment;
}

Environment Action::backupEnvironment() const
{
	return m_environment;
}

void Action::setOutputString( QString* str )
{
	delete m_writer;
	
	if ( str )
		m_writer = new QTextStream( str );
	else
		m_writer = p_defaultOutputStream();
}

QTextStream& Action::outputStream() const
{
	return *m_writer;
}

QTextStream* Action::p_defaultOutputStream()
{
	return new QTextStream( stdout );
}
