/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "DefinitionPath.h"

#include <QFileInfo>
#include <QXmlStreamWriter>


DefinitionPath::DefinitionPath()
{
}

DefinitionPath::DefinitionPath( const DefinitionPath& other )
	: m_path( other.m_path )
{
}

DefinitionPath::DefinitionPath( const QString& path )
	: m_path( path )
{
}

DefinitionPath::~DefinitionPath()
{
}

bool DefinitionPath::operator==( const DefinitionPath& other ) const
{
	return ( this->toString() == other.toString() );
}

bool DefinitionPath::operator!=( const DefinitionPath& other ) const
{
	return ( this->toString() != other.toString() );
}

DefinitionPath& DefinitionPath::operator=( const DefinitionPath& other )
{
	if ( &other == this )
		return *this;
	
	m_path = other.m_path;
}

void DefinitionPath::setPath( const QString& path )
{
	m_path = path;
}

QString DefinitionPath::path() const
{
	return m_path;
}

void DefinitionPath::toXml( QXmlStreamWriter* writer ) const
{
	QFileInfo info( m_path );
	writer->writeTextElement( "path", info.absoluteFilePath() );
}

void DefinitionPath::fromXml( QXmlStreamReader* reader )
{
	m_path.clear();
	
	if ( reader->name() != "path" )
		return;
	
	if ( reader->readNext() != QXmlStreamReader::Characters )
		return;
	
	m_path = reader->text().toString();
	reader->readNext(); // End Tag
}

QString DefinitionPath::toString() const
{
	QFileInfo info( m_path );
	return "path " + info.absoluteFilePath();
}
