/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "SnapshotDirectory.h"
#include "SnapshotFile.h"
#include "SnapshotLink.h"
#include "DefinitionPath.h"

#include <QFileInfo>
#include <QXmlStreamWriter>
#include <QDir>
#include <QCryptographicHash>


SnapshotDirectory::SnapshotDirectory()
{
}

SnapshotDirectory::~SnapshotDirectory()
{
}

void SnapshotDirectory::setPath( const QString& path )
{
	m_path = path;
}

QString SnapshotDirectory::path() const
{
	return m_path;
}

void SnapshotDirectory::setPermissions( const Permissions& permissions )
{
	m_permissions = permissions;
}

Permissions SnapshotDirectory::permissions() const
{
	return m_permissions;
}

const SnapshotEntityList& SnapshotDirectory::entries() const
{
	return m_entries;
}

void SnapshotDirectory::addEntry( SnapshotEntity* entry )
{
	if ( entry )
	{
		m_entries.append( entry );
		entry->setParent( this );
	}
}

QByteArray SnapshotDirectory::id() const
{
	QCryptographicHash hasher( QCryptographicHash::Sha1 );
	
	hasher.addData( "directory" );
	hasher.addData( m_path.toUtf8() );
	hasher.addData( m_permissions.toString().toUtf8() );
	
	foreach ( SnapshotEntity* entry, m_entries )
		hasher.addData( entry->id() );
	
	return hasher.result().toHex();
}

void SnapshotDirectory::toXml( QXmlStreamWriter* writer ) const
{
	writer->writeStartElement( "directory" );
	writer->writeTextElement( "path", m_path );
	writer->writeTextElement( "permissions", m_permissions.toString() );
	writer->writeStartElement( "entries" );
	m_entries.toXml( writer );
	writer->writeEndElement(); // entries
	writer->writeEndElement(); // directory
}

void SnapshotDirectory::copyTo( const QString& path ) const
{
}

void SnapshotDirectory::fromDirectory( const QString& path )
{
	QFileInfo info( path );
	m_path = info.absoluteFilePath();
	m_permissions.setPermissions( info.permissions() );
	
	QDir dir( m_path );
	dir.setFilter( QDir::AllEntries | QDir::System | QDir::Hidden | QDir::NoDotAndDotDot );
	dir.setSorting( QDir::Name );
	
	foreach ( info, dir.entryInfoList() )
	{
		if ( info.isSymLink() )
			addEntry( SnapshotLink::createFromLink( info.absoluteFilePath() ) );
		else if ( info.isFile() )
			addEntry( SnapshotFile::createFromFile( info.absoluteFilePath() ) );
		else if ( info.isDir() )
			addEntry( SnapshotDirectory::createFromDirectory( info.absoluteFilePath() ) );
	}
}

void SnapshotDirectory::fromDirectory( const DefinitionPath& path )
{
	fromDirectory( path.path() );
}

SnapshotDirectory* SnapshotDirectory::createFromDirectory( const QString& path )
{
	SnapshotDirectory* ret( new SnapshotDirectory );
	ret->fromDirectory( path );
	return ret;
}

SnapshotDirectory* SnapshotDirectory::createFromDirectory( const DefinitionPath& path )
{
	return createFromDirectory( path.path() );
}
