/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "ActionDefinition.h"
#include "ActionDefinitionCreate.h"
#include "ActionDefinitionAdd.h"
#include "ActionDefinitionDel.h"
#include "ActionDefinitionStorage.h"
#include "ActionDefinitionShow.h"
#include "ActionDefinitionRemove.h"
#include "ActionDefinitionList.h"

#include <QStringList>


ActionDefinition::ActionDefinition()
	: m_outputString( 0 )
{
}

ActionDefinition::~ActionDefinition()
{
}

void ActionDefinition::setOutputString( QString* str )
{
	Action::setOutputString( str );
	m_outputString = str;
}

void ActionDefinition::execute( const QStringList& args )
{
	if ( args.size() < 1 )
		return;
	
	QStringList args2( args );
	QString cmd( args2.takeFirst() );
	
	Action* action = 0;
	
	if ( cmd == "create" )
		action = new ActionDefinitionCreate;
	else if ( cmd == "add" )
		action = new ActionDefinitionAdd;
	else if ( cmd == "del" )
		action = new ActionDefinitionDel;
	else if ( cmd == "storage" )
		action = new ActionDefinitionStorage;
	else if ( cmd == "show" )
		action = new ActionDefinitionShow;
	else if ( cmd == "remove" )
		action = new ActionDefinitionRemove;
	else if ( cmd == "list" )
		action = new ActionDefinitionList;
	else
		return;
	
	action->setBackupEnvironment( backupEnvironment() );
	action->setOutputString( m_outputString );
	action->execute( args2 );
	
	delete action;
}
