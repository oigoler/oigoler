/**
 *   Copyright (C) 2012, 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef DEFINITION_H
#define DEFINITION_H

#include "DefinitionEntityList.h"
#include "DefinitionStorage.h"

#include <QString>

class Snapshot;


class Definition
{

public:
	Definition();
	Definition( const Definition& other );
	virtual ~Definition();
	
	bool operator==( const Definition& other ) const;
	bool operator!=( const Definition& other ) const;
	
	Definition& operator=( const Definition& other );
	
	void setName( const QString& name );
	QString name() const;
	
	const DefinitionEntityList& backupObjectList() const;
	void addPath( DefinitionPath path );
	void removePath( DefinitionPath path );
	
	DefinitionStorage backupStorage() const;
	void setBackupStorage( DefinitionStorage storage );
	
	void toXml( QXmlStreamWriter* writer ) const;
	void fromXml( QXmlStreamReader* reader);
	
	QStringList toStringList() const;
	
	Snapshot snapshot() const;
	
private:
	QString m_name;
	DefinitionEntityList m_objectList;
	DefinitionStorage m_storage;
	
};

#endif // DEFINITION_H
