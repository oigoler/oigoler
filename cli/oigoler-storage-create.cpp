/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "BackupSession.h"
#include "BackupStorage.h"

#include <QCoreApplication>
#include <QStringList>
#include <QTextStream>


int main( int argc, char **argv )
{
	QCoreApplication app( argc, argv );
	QStringList args( QCoreApplication::arguments() );
	
	QTextStream out( stdout );
	QTextStream err( stderr );
	
	if ( args.size() != 2 )
	{
		err << "Usage: oigoler-storage-create <storage_path>" << '\n';
		return 1;
	}
	
	BackupSession session;
	BackupStorage storage( session.createStorage( args.at(1) ) );
	
	if ( !storage.isValid() )
	{
		err << storage << '\n';
		return 2;
	}
	
	out << storage << '\n';
	
	return 0;
}
