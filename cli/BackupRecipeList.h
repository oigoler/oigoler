/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef BACKUPRECIPELIST_H
#define BACKUPRECIPELIST_H

#include "BackupRecipe.h"

#include <QList>


class BackupRecipeList : public QList< BackupRecipe >
{
	
public:
	enum BackupRecipeListError { NoError, SessionError };

public:
	BackupRecipeList();
	BackupRecipeList( BackupRecipeListError error );
	BackupRecipeList( const BackupRecipeList& other );
	virtual ~BackupRecipeList();
	
	BackupRecipeList& operator=( const BackupRecipeList& other );
	
	bool isValid() const;
	BackupRecipeListError error() const;
	
private:
	BackupRecipeListError m_error;
	
};


QTextStream& operator<<( QTextStream& stream, const BackupRecipeList& list );

#endif // BACKUPRECIPELIST_H
