/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef SNAPSHOTFILE_H
#define SNAPSHOTFILE_H

#include "SnapshotEntity.h"
#include "Permissions.h"

#include <QString>
#include <QByteArray>

class DefinitionPath;


class SnapshotFile : public SnapshotEntity
{
	Q_OBJECT

public:
	SnapshotFile();
	virtual ~SnapshotFile();
	
	void setPath( const QString& path );
	QString path() const;
	
	void setPermissions( const Permissions& permissions );
	Permissions permissions() const;
	
	void setFileHash( const QByteArray& hash );
	QByteArray fileHash() const;
	
	QByteArray id() const;
	
	void toXml( QXmlStreamWriter* writer ) const;
	
	void copyTo( const QString& path ) const;
	
	void fromFile( const QString& path );
	void fromFile( const DefinitionPath& path );
	
	static SnapshotFile* createFromFile( const QString& path );
	static SnapshotFile* createFromFile( const DefinitionPath& path );

private:
	QString m_path;
	Permissions m_permissions;
	QByteArray m_hash;
	
};

#endif // SNAPSHOTFILE_H
