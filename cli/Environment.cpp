/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "Environment.h"
#include "Definition.h"
#include "DefinitionList.h"
#include "Snapshot.h"
#include "SnapshotEntity.h"

#include <QDir>
#include <QXmlStreamWriter>
#include <QDateTime>


Environment::Environment()
	: m_configPath( QDir::homePath() + "/.oigoler" )
	, m_timestamp( QDateTime::currentMSecsSinceEpoch() )
{
}

Environment::Environment( const Environment& other )
	: m_configPath( other.m_configPath )
	, m_timestamp( other.m_timestamp )
{
}

Environment::~Environment()
{
}

bool Environment::operator==( const Environment& other ) const
{
	return ( m_configPath == other.m_configPath );
}

bool Environment::operator!=( const Environment& other ) const
{
	return ( m_configPath != other.m_configPath );
}

Environment& Environment::operator=( const Environment& other )
{
	if ( this == &other )
		return *this;
	
	m_configPath = other.m_configPath;
	m_timestamp  = other.m_timestamp;
	
	return *this;
}

void Environment::setConfigDirectory( const QString& path )
{
	m_configPath = path;
}

QString Environment::configDirectory() const
{
	return m_configPath;
}

void Environment::setTimestamp( qint64 timestamp )
{
	m_timestamp = timestamp;
}

qint64 Environment::timestamp() const
{
	return m_timestamp;
}

QString Environment::definitionDirectory() const
{
	return m_configPath + "/definition";
}

QString Environment::snapshotDirectory() const
{
	return m_configPath + "/snapshot";
}

void Environment::storeDefinition( const Definition& definition )
{
	QDir dir;
	
	if ( !dir.exists( definitionDirectory() ) )
	{
		if ( !dir.mkpath( definitionDirectory() ) )
		{
			return;
		}
	}
	
	QFile xmlFile( p_definitionPath( definition.name() ) );
	xmlFile.open( QFile::WriteOnly );
	
	QXmlStreamWriter writer( &xmlFile );
	writer.setAutoFormatting( true );
	
	writer.writeStartDocument();
	definition.toXml( &writer );
	writer.writeEndDocument();
}

bool Environment::definitionExists( const QString& name ) const
{
	return QFile::exists( p_definitionPath( name ) );
}

Definition Environment::definition( const QString& name ) const
{
	Definition ret;
	ret.setName( name );
	
	QFile file( p_definitionPath( name ) );
	
	if ( file.exists() )
	{
		file.open( QFile::ReadOnly );
		QXmlStreamReader reader( &file );
		reader.readNextStartElement();
		ret.fromXml( &reader );
	}
	
	return ret;
}

void Environment::removeDefinition( const QString& name )
{
	QFile file( p_definitionPath( name ) );
	file.remove();
}

DefinitionList Environment::definitions() const
{
	DefinitionList ret;
	QDir dir( definitionDirectory() );
	
	QStringList filters;
	filters.append( "*.xml" );
	dir.setNameFilters( filters );
	dir.setFilter( QDir::Files );
	
	foreach ( QFileInfo info, dir.entryInfoList() )
		ret.append( definition( info.completeBaseName() ) );
	
	return ret;
}

void Environment::backup( const QString& name )
{
	QDir dir;
	
	if ( !dir.exists( snapshotDirectory() ) )
	{
		dir.mkpath( snapshotDirectory() );
	}
	
	QString fileName( snapshotDirectory()
	                + '/'
	                + p_snapName( name ) );
	
	QFile file( fileName );
	file.open( QFile::WriteOnly );
	
	Definition definition( this->definition( name ) );
	Snapshot snapshot( definition.snapshot() );
	
	QXmlStreamWriter writer1( &file );
	writer1.setAutoFormatting( true );
	writer1.writeStartDocument();
	snapshot.toXml( &writer1 );
	writer1.writeEndDocument();
	file.close();
	
	fileName = definition.backupStorage().path()
	         + '/'
	         + p_snapName( name );
			 
	file.setFileName( fileName );
	file.open( QFile::WriteOnly );
	
	QXmlStreamWriter writer2( &file );
	writer2.setAutoFormatting( true );
	writer2.writeStartDocument();
	snapshot.toXml( &writer2 );
	writer2.writeEndDocument();
	
	foreach ( SnapshotEntity* entity, snapshot.entityList() )
		entity->copyTo( definition.backupStorage().path() );
}

QString Environment::p_definitionPath( const QString& name ) const
{
	return definitionDirectory() + '/' + name + ".xml";
}

QString Environment::p_snapName( const QString& name ) const
{
	return QString::number( m_timestamp ) + '-' + name + ".xml";
}
