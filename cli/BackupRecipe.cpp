/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "BackupRecipe.h"

#include <QMutableLinkedListIterator>
#include <QFileInfo>
#include <QTextStream>
#include <QDir>


BackupRecipe::BackupRecipe()
	: m_error( InvalidRecipe )
{
}

BackupRecipe::BackupRecipe( const QString& name )
	: m_name( name )
	, m_error( NoError )
{
	if ( name.isEmpty() )
		m_error = InvalidName;
}

BackupRecipe::BackupRecipe( const RecipeError& error )
	: m_error( error )
{
}

BackupRecipe::BackupRecipe( const BackupRecipe& other )
	: m_name( other.m_name )
	, m_paths( other.m_paths )
	, m_error( other.m_error )
	, m_invalidPath( other.m_invalidPath )
	, m_storageList( other.m_storageList )
{
}

BackupRecipe::~BackupRecipe()
{
}

BackupRecipe& BackupRecipe::operator=( const BackupRecipe& other )
{
	if ( this == &other )
		return *this;
	
	m_name        = other.m_name;
	m_paths       = other.m_paths;
	m_error       = other.m_error;
	m_invalidPath = other.m_invalidPath;
	m_storageList = other.m_storageList;
	
	return *this;
}

bool BackupRecipe::operator<( const BackupRecipe& other ) const
{
	return m_name < other.m_name;
}

QString BackupRecipe::name() const
{
	return m_name;
}

void BackupRecipe::addPath( const QString& path )
{
	QFileInfo info( path );
	QString absolutePath( info.absoluteFilePath() );
	absolutePath = QDir::cleanPath( absolutePath );
	
	QMutableLinkedListIterator< QString > iterator( m_paths );
	
	while ( iterator.hasNext() )
	{
		if ( iterator.next() > absolutePath )
		{
			iterator.previous();
			iterator.insert( absolutePath );
			return;
		}
	}
	
	iterator.insert( absolutePath );
}

QList< QString > BackupRecipe::pathList() const
{
	QList< QString > ret;
	
	foreach ( QString path, m_paths )
		ret.append( path );
	
	return ret;
}

QString BackupRecipe::invalidPath() const
{
	return m_invalidPath;
}

bool BackupRecipe::hasInvalidPath() const
{
	foreach ( const QString& path, m_paths )
	{
		if ( !QFile::exists( path ) )
			return true;
	}
	
	return false;
}

QList< QString > BackupRecipe::invalidPathList() const
{
	QList< QString > ret;
	
	foreach ( const QString& path, m_paths )
	{
		if ( !QFile::exists( path ) )
			ret.append( path );
	}
	
	return ret;
}

void BackupRecipe::addStorage( const QByteArray& key )
{
	m_storageList.append( key );
}

const QList< QByteArray > BackupRecipe::storageList() const
{
	return m_storageList;
}

bool BackupRecipe::isValid() const
{
	return m_error == NoError;
}

BackupRecipe::RecipeError BackupRecipe::error() const
{
	return m_error;
}

void BackupRecipe::setError( RecipeError error )
{
	m_error = error;
}

QTextStream& operator<<( QTextStream& stream, const BackupRecipe& recipe )
{
	switch ( recipe.error() )
	{
		case BackupRecipe::NoError:
			stream << "recipe " << recipe.name();
			
			foreach ( const QString& path, recipe.pathList() )
			{
				stream << '\n';
				
				if ( QFile::exists( path ) )
					stream << "path ";
				else
					stream << "invalidpath ";
				
				stream << path;
			}
			
			foreach ( const QByteArray& storage, recipe.storageList() )
				stream << '\n' << "storage " << storage;
			
			break;
			
			
		case BackupRecipe::SessionError:
			stream << "Error: Backup session not setup right!";
			break;
			
		case BackupRecipe::InvalidName:
			stream << "Error: Backup recipe has invalid name '"
			       << recipe.name() << "'!";
			break;
			
		case BackupRecipe::InvalidPath:
			stream << "Error: The following paths are invalid:\n"
			       << QStringList( recipe.invalidPathList() ).join( "\n" );
			break;
		
		case BackupRecipe::InvalidRecipe:
			stream << "Error: Backup recipe is invalid!";
			break;
			
		case BackupRecipe::InexistentRecipe:
			stream << "Error: The backup recipe '"
			       << recipe.name()
				   << "' does not exist!";
			break;
			
		case BackupRecipe::DuplicateRecipe:
			stream << "Error: Backup recipe '"
			       << recipe.name()
				   << "' already exists!";
			break;
	}

	return stream;
}
