/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H

#include <QString>

class Definition;
class DefinitionList;


class Environment
{

public:
	Environment();
	Environment( const Environment& other );
	virtual ~Environment();
	
	bool operator==( const Environment& other ) const;
	bool operator!=( const Environment& other ) const;
	
	Environment& operator=( const Environment& other );
	
	void setConfigDirectory( const QString& path );
	QString configDirectory() const;
	
	void setTimestamp( qint64 timestamp );
	qint64 timestamp() const;
	
	QString definitionDirectory() const;
	QString snapshotDirectory() const;
	
	void storeDefinition( const Definition& definition );
	bool definitionExists( const QString& name ) const;
	Definition definition( const QString& name ) const;
	void removeDefinition( const QString& name );
	DefinitionList definitions() const;
	
	void backup( const QString& name );
	
private:
	QString p_definitionPath( const QString& name ) const;
	QString p_snapName( const QString& name ) const;
	
private:
	QString m_configPath;
	qint64 m_timestamp;
	
};

#endif // ENVIRONMENT_H
