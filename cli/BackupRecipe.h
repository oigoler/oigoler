/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef BACKUPRECIPE_H
#define BACKUPRECIPE_H

#include <QString>
#include <QLinkedList>
#include <QList>
#include <QByteArray>


class QTextStream;


class BackupRecipe
{
	
public:
	enum RecipeError { NoError
		             , SessionError
		             , InvalidName
		             , InvalidPath
		             , InvalidRecipe
		             , InexistentRecipe
		             , DuplicateRecipe };

public:
	BackupRecipe();
	BackupRecipe( const QString& name );
	BackupRecipe( const RecipeError& error );
	BackupRecipe( const BackupRecipe& other );
	virtual ~BackupRecipe();
	
	BackupRecipe& operator=( const BackupRecipe& other );
	bool operator<( const BackupRecipe& other ) const;
	
	QString name() const;
	
	void addPath( const QString& path );
	QList< QString > pathList() const;
	QString invalidPath() const;
	bool hasInvalidPath() const;
	QList< QString > invalidPathList() const;
	
	void addStorage( const QByteArray& key );
	const QList< QByteArray > storageList() const;
	
	bool isValid() const;
	RecipeError error() const;
	void setError( RecipeError error );
	
private:
	QString m_name;
	QLinkedList< QString > m_paths;
	RecipeError m_error;
	QString m_invalidPath;
	QList< QByteArray > m_storageList;
	
};


QTextStream& operator<<( QTextStream& stream, const BackupRecipe& recipe );

#endif // BACKUPRECIPE_H
