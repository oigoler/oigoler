/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "ActionDefinitionRemove.h"

#include <QStringList>
#include <QTextStream>


ActionDefinitionRemove::ActionDefinitionRemove()
{
}

ActionDefinitionRemove::~ActionDefinitionRemove()
{
}

void ActionDefinitionRemove::execute( const QStringList& args )
{
	if ( args.size() != 1 )
		return;
	
	backupEnvironment().removeDefinition( args.at( 0 ) );
	
	outputStream() << "Definition '" << args.at( 0 ) << "' removed\n";
}
