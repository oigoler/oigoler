/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "BackupSession.h"
#include "BackupStorageList.h"

#include <QTextStream>


int main( int argc, char **argv )
{
	BackupSession session;
	BackupStorageList storageList( session.listStorage() );
	
	QTextStream err( stderr );
	QTextStream out( stdout );
	
	storageList.setAvailabilityPrinting( true );
	
	if ( !storageList.isValid() )
	{
		err << storageList;
		return 2;
	}
	
	out << storageList;
	
	return 0;
}
