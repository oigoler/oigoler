/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef BACKUPSESSION_H
#define BACKUPSESSION_H

#include <QString>


class BackupStorage;
class BackupStorageList;
class BackupRecipe;
class BackupRecipeList;
class BackupSnapshot;
class QDir;
class QFileInfo;


class BackupSession
{

public:
	BackupSession();
	virtual ~BackupSession();
	
	void setConfigDirectory( const QString& path );
	QString configDirectory() const;
	
	BackupStorage createStorage( const QString& path );
	BackupStorageList listStorage() const;
	
	void createRecipe( BackupRecipe* recipe );
	void registerRecipe( BackupRecipe* recipe );
	BackupRecipeList listRecipe() const;
	BackupRecipe recipe( const QString& name ) const;
	void removeRecipe( const QString& name );
	
	BackupSnapshot backup( const QString& recipeName );
	
private:
	QString p_storagePath() const;
	BackupStorage p_storage( const QByteArray& key ) const;
	QString p_recipePath() const;
	QList< QString > p_directoryEntries( QDir dir ) const;
	QByteArray p_processSymLink( const QFileInfo& info, const QString& destination ) const;
	QByteArray p_processDir( const QFileInfo& info, const QString& destination ) const;
	QByteArray p_processFile( const QFileInfo& info, const QString& destination ) const;
	QString p_target( const QFileInfo& info ) const;
	QByteArray p_permissions( const QFileInfo& info ) const;
	
private:
	QString m_config;
	
};

#endif // BACKUPSESSION_H
