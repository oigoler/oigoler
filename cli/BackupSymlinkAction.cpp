/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "BackupSymlinkAction.h"

#include <QFileInfo>
#include <QCryptographicHash>
#include <QXmlStreamWriter>


BackupSymlinkAction::BackupSymlinkAction()
{
}

BackupSymlinkAction::~BackupSymlinkAction()
{
}

void BackupSymlinkAction::setConfigDirectory( const QString& path )
{
	m_configPath = path + "/storage";
}

QString BackupSymlinkAction::execute( const QString& link, const QString& storageId )
{
	size_t bufsize( 1024 );
	ssize_t retsize( 0 );
	char* buf( 0 );
	
	while ( true )
	{
		buf = (char*) realloc( buf, bufsize );
		retsize = readlink( link.toUtf8().data(), buf, bufsize );
		
		if ( retsize < 0 )
		{
			free( buf );
			return QString();
		}
		
		if ( retsize < bufsize )
		{
			buf[retsize] = '\0';
			break;
		}
		
		bufsize *= 2;
	}
	
	QString target( buf );
	free( buf );
	
	QFileInfo info( link );
	
	QCryptographicHash hasher( QCryptographicHash::Sha1 );
	hasher.addData( "symlink" );
	hasher.addData( info.absoluteFilePath().toUtf8() );
	hasher.addData( target.toUtf8() );
	
	QByteArray linkId( hasher.result().toHex() );
	
	QFile storageFile( m_configPath + '/' + storageId );
	storageFile.open( QFile::ReadOnly );
	
	QString storagePath( storageFile.readLine() );
	storagePath.chop( 1 );
	storageFile.close();
	
	QFile linkFile( storagePath + '/' + linkId + ".xml" );
	linkFile.open( QFile::WriteOnly );
	
	QXmlStreamWriter writer( &linkFile );
	writer.setAutoFormatting( true );
	writer.writeStartDocument();
	writer.writeStartElement( "symlink" );
	writer.writeTextElement( "path", info.absoluteFilePath() );
	writer.writeTextElement( "target", target );
	writer.writeEndDocument();
	
	return linkId + ' ' + info.absoluteFilePath() + " -> " + target + '\n';
}
