/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "Snapshot.h"
#include "SnapshotEntity.h"

#include <QXmlStreamWriter>
#include <QCryptographicHash>


Snapshot::Snapshot()
	: m_date( QDateTime::currentDateTime() )
{
}

Snapshot::Snapshot( const Snapshot& other )
	: m_entities( other.m_entities )
	, m_date( other.m_date )
	, m_backupName( other.m_backupName )
{
	p_takeOwnership();
}

Snapshot::~Snapshot()
{
}

Snapshot& Snapshot::operator=( const Snapshot& other )
{
	if ( this == &other )
		return *this;
	
	m_entities = other.m_entities;
	m_date = other.m_date;
	m_backupName = other.m_backupName;
	p_takeOwnership();
	
	return *this;
}

void Snapshot::setDate( const QDateTime& date )
{
	m_date = date;
}

QDateTime Snapshot::date() const
{
	return m_date;
}

void Snapshot::setBackupName( const QString& name )
{
	m_backupName = name;
}

QString Snapshot::backupName() const
{
	return m_backupName;
}

const SnapshotEntityList& Snapshot::entityList() const
{
	return m_entities;
}

void Snapshot::addEntity( SnapshotEntity* entity )
{
	entity->setParent( this );
	m_entities.append( entity );
}

QByteArray Snapshot::id() const
{
	QCryptographicHash hasher( QCryptographicHash::Sha1 );
	
	foreach ( SnapshotEntity* entity, m_entities )
		hasher.addData( entity->id() );
	
	return hasher.result().toHex();
}

QString Snapshot::fileName() const
{
	return QString::number( m_date.toMSecsSinceEpoch() )
	     + '-'
	     + id()
		 + '-'
		 + m_backupName
		 + ".xml";
}

void Snapshot::toXml( QXmlStreamWriter* writer ) const
{
	writer->writeStartElement( "BackupSnapshot" );
	m_entities.toXml( writer );
	writer->writeEndElement();
}

void Snapshot::p_takeOwnership()
{
	foreach ( SnapshotEntity* entity, m_entities )
		entity->setParent( this );
}
