/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "SnapshotFile.h"
#include "DefinitionPath.h"

#include <QXmlStreamWriter>
#include <QFile>
#include <QCryptographicHash>
#include <QFileInfo>


SnapshotFile::SnapshotFile()
{
}

SnapshotFile::~SnapshotFile()
{
}

void SnapshotFile::setPath( const QString& path )
{
	m_path = path;
}

QString SnapshotFile::path() const
{
	return m_path;
}

void SnapshotFile::setPermissions( const Permissions& permissions )
{
	m_permissions = permissions;
}

Permissions SnapshotFile::permissions() const
{
	return m_permissions;
}

void SnapshotFile::setFileHash( const QByteArray& hash )
{
	m_hash = hash;
}

QByteArray SnapshotFile::fileHash() const
{
	return m_hash;
}

QByteArray SnapshotFile::id() const
{
	QCryptographicHash hasher( QCryptographicHash::Sha1 );
	hasher.addData( "file" );
	hasher.addData( m_path.toUtf8() );
	hasher.addData( m_permissions.toString().toUtf8() );
	hasher.addData( m_hash );
	
	return hasher.result().toHex();
}

void SnapshotFile::toXml( QXmlStreamWriter* writer ) const
{
	writer->writeStartElement( "file" );
	writer->writeTextElement( "path", m_path );
	writer->writeTextElement( "permissions", m_permissions.toString() );
	writer->writeTextElement( "hash", m_hash );
	writer->writeEndElement();
}

void SnapshotFile::copyTo( const QString& path ) const
{
	QFile::copy( m_path, path + '/' + m_hash );
}

void SnapshotFile::fromFile( const QString& path )
{
	QFileInfo info( path );
	m_path = info.absoluteFilePath();
	m_permissions.setPermissions( info.permissions() );
	
	QFile file( path );
	file.open( QFile::ReadOnly );
	
	QCryptographicHash hasher( QCryptographicHash::Sha1 );
	hasher.addData( file.readAll() );
	
	m_hash = hasher.result().toHex();
}

void SnapshotFile::fromFile( const DefinitionPath& path )
{
	QFileInfo info( path.path() );
	m_path = info.absoluteFilePath();
	m_permissions.setPermissions( info.permissions() );
	
	QFile file( path.path() );
	file.open( QFile::ReadOnly );
	
	QCryptographicHash hasher( QCryptographicHash::Sha1 );
	hasher.addData( file.readAll() );
	
	m_hash = hasher.result().toHex();
}

SnapshotFile* SnapshotFile::createFromFile( const QString& path )
{
	SnapshotFile* ret( new SnapshotFile );
	
	QFileInfo info( path );
	ret->setPath( info.absoluteFilePath() );
	ret->setPermissions( Permissions( info.permissions() ) );
	
	QFile file( path );
	file.open( QFile::ReadOnly );
	
	QCryptographicHash hasher( QCryptographicHash::Sha1 );
	hasher.addData( file.readAll() );
	
	ret->setFileHash( hasher.result().toHex() );
	
	return ret;
}

SnapshotFile* SnapshotFile::createFromFile( const DefinitionPath& path )
{
	SnapshotFile* ret( new SnapshotFile );
	
	QFileInfo info( path.path() );
	ret->setPath( info.absoluteFilePath() );
	ret->setPermissions( Permissions( info.permissions() ) );
	
	QFile file( path.path() );
	file.open( QFile::ReadOnly );
	
	QCryptographicHash hasher( QCryptographicHash::Sha1 );
	hasher.addData( file.readAll() );
	
	ret->setFileHash( hasher.result().toHex() );
	
	return ret;
}
