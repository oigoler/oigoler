/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef DEFINITIONSTORAGE_H
#define DEFINITIONSTORAGE_H

#include <QString>

class QXmlStreamWriter;
class QXmlStreamReader;


class DefinitionStorage
{

public:
	DefinitionStorage();
	DefinitionStorage( const QString& path );
	DefinitionStorage( const DefinitionStorage& other );
	virtual ~DefinitionStorage();
	
	bool operator==( const DefinitionStorage& other ) const;
	bool operator!=( const DefinitionStorage& other ) const;
	
	DefinitionStorage& operator=( const DefinitionStorage& other );
	
	void setPath( const QString& path );
	QString path() const;
	
	void toXml( QXmlStreamWriter* writer ) const;
	void fromXml( QXmlStreamReader* reader );
	
	QString toString() const;
	
private:
	QString m_path;
	
};

#endif // DEFINITIONSTORAGE_H
