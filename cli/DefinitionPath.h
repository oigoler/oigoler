/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef DEFINITIONPATH_H
#define DEFINITIONPATH_H

#include <QString>

class QXmlStreamWriter;
class QXmlStreamReader;


class DefinitionPath
{

public:
	DefinitionPath();
	DefinitionPath( const DefinitionPath& other );
	DefinitionPath( const QString& path );
	virtual ~DefinitionPath();
	
	bool operator==( const DefinitionPath& other ) const;
	bool operator!=( const DefinitionPath& other ) const;
	
	DefinitionPath& operator=( const DefinitionPath& other );
	
	void setPath( const QString& path );
	QString path() const;
	
	void toXml( QXmlStreamWriter* writer ) const;
	void fromXml( QXmlStreamReader* reader );
	
	QString toString() const;
	
private:
	QString m_path;
	
};

#endif // DEFINITIONPATH_H
