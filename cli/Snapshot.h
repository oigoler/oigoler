/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef SNAPSHOT_H
#define SNAPSHOT_H

#include <QObject>
#include <QDateTime>
#include <QString>

#include "SnapshotEntityList.h"

class SnapshotEntity;


class Snapshot : public QObject
{
	Q_OBJECT

public:
	Snapshot();
	Snapshot( const Snapshot& other );
	virtual ~Snapshot();
	
	Snapshot& operator=( const Snapshot& other );
	
	void setDate( const QDateTime& date );
	QDateTime date() const;
	
	void setBackupName( const QString& name );
	QString backupName() const;
	
	const SnapshotEntityList& entityList() const;
	void addEntity( SnapshotEntity* entity );
	
	QByteArray id() const;
	QString fileName() const;
	
	void toXml( QXmlStreamWriter* writer ) const;
	
private:
	void p_takeOwnership();
	
private:
	SnapshotEntityList m_entities;
	QDateTime m_date;
	QString m_backupName;
	
};

#endif // SNAPSHOT_H
