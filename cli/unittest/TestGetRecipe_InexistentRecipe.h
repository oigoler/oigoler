/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef TESTGETRECIPE_INEXISTENTRECIPE_H
#define TESTGETRECIPE_INEXISTENTRECIPE_H

#include <QtTest/QtTest>

#include "../BackupRecipe.h"


class TestGetRecipe_InexistentRecipe : public QObject
{
	Q_OBJECT

private slots:
	void initTestCase();
	void testValidity();
	void testError();
	void testName();
	void testPaths();
	void testToString();
	
private:
	BackupRecipe m_recipe;
	
};

#endif // TESTGETRECIPE_INEXISTENTRECIPE_H
