/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestDefinitionStorage.h"
#include "../DefinitionStorage.h"


void TestDefinitionStorage::hasPath()
{
	DefinitionStorage storage;
	QVERIFY( storage.path().isEmpty() );
	
	storage.setPath( ".." );
	QCOMPARE( storage.path(), QString( ".." ) );
}

void TestDefinitionStorage::testToXml()
{
	DefinitionStorage storage;
	storage.setPath( "." );
	
	QString xml;
	QXmlStreamWriter writer1( &xml );
	writer1.writeStartDocument();
	storage.toXml( &writer1 );
	writer1.writeEndDocument();
	
	QString expected;
	QXmlStreamWriter writer2( &expected );
	writer2.writeStartDocument();
	writer2.writeTextElement( "BackupStorage", QDir::currentPath() );
	writer2.writeEndDocument();
	
	QCOMPARE( xml, expected );
}

void TestDefinitionStorage::testFromXml()
{
	DefinitionStorage storage;
	storage.setPath( "." );
	
	QString xml;
	QXmlStreamWriter writer( &xml );
	writer.writeStartDocument();
	storage.toXml( &writer );
	writer.writeEndDocument();
	
	QXmlStreamReader reader( xml );
	reader.readNextStartElement();
	storage.fromXml( &reader );
	
	QCOMPARE( storage.path(), QDir::currentPath() );
}

void TestDefinitionStorage::testPathInConstructor()
{
	DefinitionStorage storage( ".." );
	QCOMPARE( storage.path(), QString( ".." ) );
}

void TestDefinitionStorage::testToString()
{
	DefinitionStorage storage( "." );
	QCOMPARE( storage.toString(), "storage " + QDir::currentPath() );
}

void TestDefinitionStorage::hasEqualsOperator()
{
	DefinitionStorage storage1( "." );
	DefinitionStorage storage2( ".." );
	DefinitionStorage storage3( "." );
	
	QVERIFY( storage1 == storage3 );
	QVERIFY( !( storage1 == storage2 ) );
}

void TestDefinitionStorage::hasDifferenceOperator()
{
	DefinitionStorage storage1( "." );
	DefinitionStorage storage2( ".." );
	DefinitionStorage storage3( "." );
	
	QVERIFY( storage1 != storage2 );
	QVERIFY( !( storage1 != storage3 ) );
}

void TestDefinitionStorage::hasAssignmentOperator()
{
	DefinitionStorage storage1( ".." );
	DefinitionStorage storage2;

	QVERIFY( storage1 != storage2 );
	
	storage2 = storage1;
	
	QVERIFY( storage1 == storage2 );
}

void TestDefinitionStorage::hasCopyConstructor()
{
	DefinitionStorage storage1( ".." );
	DefinitionStorage storage2;

	QVERIFY( storage1 != storage2 );
	
	DefinitionStorage storage3( storage1 );
	
	QVERIFY( storage1 == storage3 );
}

QTEST_MAIN(TestDefinitionStorage)
