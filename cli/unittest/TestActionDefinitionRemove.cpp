/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestActionDefinitionRemove.h"
#include "DirectoryCleaner.h"
#include "../ActionDefinitionRemove.h"
#include "../Definition.h"


void TestActionDefinitionRemove::hasDefaultBackupEnvironment()
{
	ActionDefinitionRemove action;
	Environment expected;
	
	QVERIFY( action.backupEnvironment() == expected );
}

void TestActionDefinitionRemove::hasBackupEnvironment()
{
	Environment env;
	env.setConfigDirectory( "test.dir" );
	
	ActionDefinitionRemove action;
	
	QVERIFY( action.backupEnvironment() != env );
	
	action.setBackupEnvironment( env );
	
	QVERIFY( action.backupEnvironment() == env );
}

void TestActionDefinitionRemove::testRemove()
{
	Definition definition;
	definition.setName( "test" );
	definition.setBackupStorage( DefinitionStorage( "/tmp" ) );
	definition.addPath( DefinitionPath( QDir::currentPath() ) );
	
	DirectoryCleaner cleaner( "test.dir" );

	Environment env;
	env.setConfigDirectory( "test.dir" );
	env.storeDefinition( definition );
	
	QStringList args;
	args << "test";
	
	QString output;

	ActionDefinitionRemove action;
	action.setBackupEnvironment( env );
	action.setOutputString( &output );
	action.execute( args );
	
	QVERIFY( !env.definitionExists( "test" ) );
	QCOMPARE( output, QString( "Definition 'test' removed\n" ) );
}

QTEST_MAIN(TestActionDefinitionRemove)
