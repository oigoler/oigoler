/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef TESTENVIRONMENT_H
#define TESTENVIRONMENT_H

#include <QtTest/QtTest>


class TestEnvironment : public QObject
{
	Q_OBJECT

private slots:
	void hasConfigDirectory();
	void hasTimestamp();
	void testDefinitionDirectory();
	void testSnapshotDirectory();
	void testDefaults();
	void testNonExistentDefinitionDirectory();
	void testDefinitionFileCreation();
	void testStoreDefinition();
	void testDefinitionXmlFormatting();
	void testDefinitionExists();
	void testRetrieveDefinition();
	void testRetrieveNonExistentDefinition();
	void testDefinitionOverwrite();
	void testRemoveDefinition();
	void testDefinitionList();
	void testEqualsOperator();
	void testDifferenceOperator();
	void testAssignmentOperator();
	void testCopyConstructor();
	void testNonExistentSnapshotDirectory();
	void testSnapshotInConfigDirectory();
	void testSnapshotInStorageDirectory();
	void testFileInStorage();
	
};

#endif // TESTENVIRONMENT_H
