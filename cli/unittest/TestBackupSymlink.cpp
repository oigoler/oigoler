/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestBackupSymlink.h"
#include "LinkCleaner.h"
#include "DirectoryCleaner.h"
#include "FileCleaner.h"
#include "../BackupSymlinkAction.h"


void TestBackupSymlink::testBackupSymlink()
{
	QFile::link( "..", "testlink" );
	LinkCleaner cleaner1( "testlink" );
	
	QDir workdir( QDir::current() );
	workdir.mkdir( "storage" );
	DirectoryCleaner cleaner2( "storage" );
	
	workdir.mkpath( "config/storage" );
	DirectoryCleaner cleaner3( "config" );
	
	QCryptographicHash hasher( QCryptographicHash::Sha1 );
	hasher.addData( workdir.absoluteFilePath( "storage" ).toUtf8() );
	QByteArray storage( hasher.result().toHex() );
	QFile storageFile( "config/storage/" + storage );
	storageFile.open( QFile::WriteOnly );
	storageFile.write( workdir.absoluteFilePath( "storage" ).toUtf8() + '\n' );
	storageFile.close();
	
	BackupSymlinkAction action;
	action.setConfigDirectory( "config" );
	QString response( action.execute( "testlink", storage ) );
	
	hasher.reset();
	hasher.addData( "symlink" );
	hasher.addData( workdir.absoluteFilePath( "testlink" ).toUtf8() );
	hasher.addData( ".." );
	QByteArray link( hasher.result().toHex() );
	
	QString expectedResponse( link + ' '
	                        + workdir.absoluteFilePath( "testlink" )
							+ " -> ..\n" );
	
	QCOMPARE( response, expectedResponse );
	
	QFile linkFile( "storage/" + link + ".xml" );
	QVERIFY( linkFile.exists() );
	
	QFile expectedFile( "expected.xml" );
	FileCleaner cleaner4( "expected.xml" );
	expectedFile.open( QFile::ReadWrite );
	QXmlStreamWriter writer( &expectedFile );
	writer.setAutoFormatting( true );
	writer.writeStartDocument();
	writer.writeStartElement( "symlink" );
	writer.writeTextElement( "path", workdir.absoluteFilePath( "testlink" ) );
	writer.writeTextElement( "target", ".." );
	writer.writeEndDocument();
	
	linkFile.open( QFile::ReadOnly );
	expectedFile.flush();
	expectedFile.seek( 0 );
	
	QCOMPARE( QString( linkFile.readAll() ), QString( expectedFile.readAll() ) );
}

QTEST_MAIN(TestBackupSymlink)
