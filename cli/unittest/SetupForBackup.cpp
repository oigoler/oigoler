/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "SetupForBackup.h"
#include "../ActionDefinitionCreate.h"
#include "../Snapshot.h"
#include "../SnapshotLink.h"
#include "../SnapshotFile.h"
#include "../SnapshotDirectory.h"

#include <QXmlStreamWriter>
#include <QCryptographicHash>


SetupForBackup::SetupForBackup()
	: m_cleaner1( "testdir" )
	, m_cleaner2( "storage.dir" )
	, m_cleaner3( "config.dir" )
	, m_cleaner4( "testlink" )
	, m_generator( "testfile" )
{
	QDir::current().mkdir( "testdir" );
	QDir::current().mkdir( "storage.dir" );
	m_generator.make( "abc" );
	QFile::link( "testfile", "testlink" );
	
	m_env.setConfigDirectory( "config.dir" );
	m_env.setTimestamp( 123 );
	
	QStringList args;
	args << "test" << "storage.dir" << "testlink" << "testfile" << "testdir";
	
	QString dummy;
	ActionDefinitionCreate action;
	action.setBackupEnvironment( m_env );
	action.setOutputString( &dummy );
	action.execute( args );
}

SetupForBackup::~SetupForBackup()
{
}

void SetupForBackup::backup()
{
	m_env.backup( "test" );
}

QString SetupForBackup::snapshotDirectory() const
{
	return m_env.snapshotDirectory();
}

const Environment& SetupForBackup::env() const
{
	return m_env;
}

void SetupForBackup::setEnv( const Environment& env )
{
	m_env = env;
}

QString SetupForBackup::snapshotInConfigPath() const
{
	return QString( "config.dir/snapshot/123-test.xml" );
}

QString SetupForBackup::snapshotInStoragePath() const
{
	return QString( "storage.dir/123-test.xml" );
}

QString SetupForBackup::fileInStoragePath() const
{
	return "storage.dir/"
	     + QCryptographicHash::hash( "abc", QCryptographicHash::Sha1 ).toHex();
}

QString SetupForBackup::expectedSnapshotXml() const
{
	FileGenerator cleaner( "expected.xml" );
	QFile expected( "expected.xml" );
	expected.open( QFile::WriteOnly );
	QXmlStreamWriter writer( &expected );
	writer.setAutoFormatting( true );
	writer.writeStartDocument();
	
	Snapshot snapshot;
	snapshot.addEntity( SnapshotLink::createFromLink( "testlink" ) );
	snapshot.addEntity( SnapshotFile::createFromFile( "testfile" ) );
	snapshot.addEntity( SnapshotDirectory::createFromDirectory( "testdir" ) );
	snapshot.toXml( &writer );
	writer.writeEndDocument();
	expected.close();
	
	expected.open( QFile::ReadOnly );

	return QString( expected.readAll() );
}

QByteArray SetupForBackup::expectedFileContents() const
{
	return QByteArray( "abc" );
}

QString SetupForBackup::name() const
{
	return QString( "test" );
}
