/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestCreateRecipe_InvalidName.h"
#include "DirectoryCleaner.h"
#include "../BackupSession.h"
#include "../BackupRecipe.h"

#include <QDir>


void TestCreateRecipe_InvalidName::initTestCase()
{
	m_recipe = 0;
	
	BackupSession session;
	session.setConfigDirectory( "config" );
	new DirectoryCleaner( "config", this );
	
	m_recipe = new BackupRecipe( "" );
	session.createRecipe( m_recipe );
}

void TestCreateRecipe_InvalidName::testValidity()
{
	QVERIFY( !m_recipe->isValid() );
}

void TestCreateRecipe_InvalidName::testError()
{
	QCOMPARE( m_recipe->error(), BackupRecipe::InvalidName );
}

void TestCreateRecipe_InvalidName::testToString()
{
	QString expected( "Error: Backup recipe has invalid name ''!" );
	
	QString actual;
	QTextStream stream( &actual );
	
	stream << *m_recipe;
	
	QCOMPARE( actual, expected );
}

void TestCreateRecipe_InvalidName::testNoFileCreated()
{
	QDir config( "config/recipe" );
	QVERIFY( config.exists() );
	
	QStringList filters;
	filters.append( "*.xml" );
	config.setNameFilters( filters );
	
	QStringList entries( config.entryList() );
	QCOMPARE( entries.size(), 0 );
}

void TestCreateRecipe_InvalidName::cleanupTestCase()
{
	delete m_recipe;
}

QTEST_MAIN(TestCreateRecipe_InvalidName)
