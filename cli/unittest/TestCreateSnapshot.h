/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef TESTCREATESNAPSHOT_H
#define TESTCREATESNAPSHOT_H

#include <QtTest/QtTest>

#include "../BackupSnapshot.h"


class TestCreateSnapshot : public QObject
{
	Q_OBJECT

private slots:
	void initTestCase();
	void testValidity();
	void testError();
	void testName();
	void testPathList();
	void testTimestamp();
	void testToString();
	void testNumberOfCreatedFiles();
	void testSnapshotFile();
	void testFile0();
	void testLink0();
	void testDir0();
	void testFile1();
	void testLink1();
	void testDir1();
	void testFile2();
	void testLink2();
	void testDir2();
	
private:
	QXmlStreamReader::TokenType readNextToken( QXmlStreamReader* reader ) const;
	QByteArray permissions( char type, QFile::Permissions filePermissions ) const;
	
private:
	BackupSnapshot m_snapshot;
	qint64 m_before;
	qint64 m_after;
	
};

#endif // TESTCREATESNAPSHOT_H
