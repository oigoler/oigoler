/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestSnapshotDirectory.h"
#include "DirectoryCleaner.h"
#include "FileGenerator.h"
#include "../SnapshotDirectory.h"
#include "../Permissions.h"
#include "../SnapshotFile.h"
#include "../SnapshotLink.h"
#include "../DefinitionPath.h"


void TestSnapshotDirectory::hasPath()
{
	SnapshotDirectory dir;
	QVERIFY( dir.path().isEmpty() );
	
	dir.setPath( ".." );
	QCOMPARE( dir.path(), QString( ".." ) );
}

void TestSnapshotDirectory::hasPermissions()
{
	SnapshotDirectory dir;
	QCOMPARE( dir.permissions().toString(), QString( "000" ) );
	
	Permissions permissions( 755 );
	dir.setPermissions( permissions );
	QCOMPARE( dir.permissions().toString(), QString( "755" ) );
}

void TestSnapshotDirectory::hasEntries()
{
	SnapshotDirectory dir;
	QVERIFY( dir.entries().isEmpty() );
	
	SnapshotEntity* entity( new SnapshotFile );
	entity->setParent( this );
	dir.addEntry( entity );
	QCOMPARE( dir.entries().count(), 1 );
	QCOMPARE( dir.entries().at( 0 ), entity );
	
	entity = new SnapshotLink;
	entity->setParent( this );
	dir.addEntry( entity );
	QCOMPARE( dir.entries().count(), 2 );
	QCOMPARE( dir.entries().at( 1 ), entity );
}

void TestSnapshotDirectory::testEmptyDirectoryToXml()
{
	QDir::current().mkdir( "testdir" );
	DirectoryCleaner cleaner( "testdir" );
	
	QString xml;
	QXmlStreamWriter writer1( &xml );
	writer1.writeStartDocument();
	
	SnapshotDirectory dir;
	dir.fromDirectory( "testdir" );
	dir.toXml( &writer1 );
	writer1.writeEndDocument();
	
	QString expected;
	QXmlStreamWriter writer2( &expected );
	writer2.writeStartDocument();
	writer2.writeStartElement( "directory" );
	
	QFileInfo info( "testdir" );
	writer2.writeTextElement( "path", info.absoluteFilePath() );
	
	Permissions permissions( info.permissions() );
	writer2.writeTextElement( "permissions", permissions.toString() );
	writer2.writeEmptyElement( "entries" );
	writer2.writeEndDocument();
	
	QCOMPARE( xml, expected );
}

void TestSnapshotDirectory::testFullDirectoryToXml()
{
	QDir::current().mkpath( "testdir/dir" );
	DirectoryCleaner cleaner1( "testdir" );
	FileGenerator generator( "testdir/file" );
	generator.make( "abc" );
	QFile::link( QDir::currentPath() + "testdir/file", "testdir/link" );
	
	QString xml;
	QXmlStreamWriter writer1( &xml );
	writer1.writeStartDocument();
	
	SnapshotDirectory dir;
	dir.fromDirectory( "testdir" );
	dir.toXml( &writer1 );
	writer1.writeEndDocument();
	
	QString expected;
	QXmlStreamWriter writer2( &expected );
	writer2.writeStartDocument();
	writer2.writeStartElement( "directory" );
	
	QFileInfo info1( "testdir" );
	writer2.writeTextElement( "path", info1.absoluteFilePath() );
	
	Permissions permissions( info1.permissions() );
	writer2.writeTextElement( "permissions", permissions.toString() );
	
	writer2.writeStartElement( "entries" );
	
	QFileInfo info2( "testdir/dir" );
	writer2.writeStartElement( "directory" );
	writer2.writeTextElement( "path", info2.absoluteFilePath() );
	permissions.setPermissions( info2.permissions() );
	writer2.writeTextElement( "permissions", permissions.toString() );
	writer2.writeEmptyElement( "entries" );
	writer2.writeEndElement();
	
	SnapshotFile file;
	file.fromFile( "testdir/file" );
	file.toXml( &writer2 );
	
	SnapshotLink link;
	link.fromLink( "testdir/link" );
	link.toXml( &writer2 );
	
	writer2.writeEndDocument();
	
	QCOMPARE( xml, expected );
}

void TestSnapshotDirectory::testFromEmptyDirectoryString()
{
	QDir::current().mkdir( "testdir" );
	DirectoryCleaner cleaner( "testdir" );
	QFileInfo info( "testdir" );
	
	SnapshotDirectory dir;
	dir.fromDirectory( "testdir" );

	QCOMPARE( dir.path(), info.absoluteFilePath() );
	QCOMPARE( dir.permissions().toString(), Permissions( info.permissions() ).toString() );
	QVERIFY( dir.entries().isEmpty() );
}

void TestSnapshotDirectory::testFromFullDirectoryString()
{
	QDir::current().mkpath( "testdir/dir" );
	DirectoryCleaner cleaner1( "testdir" );
	FileGenerator generator( "testdir/file" );
	generator.make( "abc" );
	QFile::link( "file", "testdir/link" );
	
	SnapshotDirectory dir;
	dir.fromDirectory( "testdir" );

	QFileInfo info( "testdir" );
	QCOMPARE( dir.path(), info.absoluteFilePath() );
	QCOMPARE( dir.permissions().toString(), Permissions( info.permissions() ).toString() );
	QCOMPARE( dir.entries().count(), 3 );
	
	SnapshotFile* subfile( 0 );
	SnapshotLink* sublink( 0 );
	SnapshotDirectory* subdir( 0 );
	
	foreach ( SnapshotEntity* entity, dir.entries() )
	{
		if ( entity->metaObject()->className() == QString( "SnapshotFile" ) )
			subfile = qobject_cast< SnapshotFile* >( entity );
		else if ( entity->metaObject()->className() == QString( "SnapshotLink" ) )
			sublink = qobject_cast< SnapshotLink* >( entity );
		else if ( entity->metaObject()->className() == QString( "SnapshotDirectory" ) )
			subdir = qobject_cast< SnapshotDirectory* >( entity );
	}
	
	QVERIFY( subfile );
	QVERIFY( sublink );
	QVERIFY( subdir );
	
	info.setFile( "testdir/file" );
	QByteArray hash( QCryptographicHash::hash( "abc", QCryptographicHash::Sha1 ) );
	QCOMPARE( subfile->path(), info.absoluteFilePath() );
	QCOMPARE( subfile->permissions().toString()
	        , Permissions( info.permissions() ).toString() );
	QCOMPARE( subfile->fileHash(), hash.toHex() );
	
	info.setFile( "testdir/link" );
	QCOMPARE( sublink->path(), info.absoluteFilePath() );
	QCOMPARE( sublink->target(), QString( "file" ) );
	
	info.setFile( "testdir/dir" );
	QCOMPARE( subdir->path(), info.absoluteFilePath() );
	QCOMPARE( subdir->permissions().toString()
	        , Permissions( info.permissions() ).toString() );
	QVERIFY( subdir->entries().isEmpty() );
}

void TestSnapshotDirectory::testCreateFromDirectoryString()
{
	QDir::current().mkpath( "testdir/dir" );
	DirectoryCleaner cleaner1( "testdir" );
	FileGenerator generator( "testdir/file" );
	generator.make( "abc" );
	QFile::link( "file", "testdir/link" );
	
	SnapshotDirectory* dir( SnapshotDirectory::createFromDirectory( "testdir" ) );
	QVERIFY( dir );
	dir->setParent( this );

	QFileInfo info( "testdir" );
	QCOMPARE( dir->path(), info.absoluteFilePath() );
	QCOMPARE( dir->permissions().toString(), Permissions( info.permissions() ).toString() );
	QCOMPARE( dir->entries().count(), 3 );
	
	SnapshotFile* subfile( 0 );
	SnapshotLink* sublink( 0 );
	SnapshotDirectory* subdir( 0 );
	
	foreach ( SnapshotEntity* entity, dir->entries() )
	{
		if ( entity->metaObject()->className() == QString( "SnapshotFile" ) )
			subfile = qobject_cast< SnapshotFile* >( entity );
		else if ( entity->metaObject()->className() == QString( "SnapshotLink" ) )
			sublink = qobject_cast< SnapshotLink* >( entity );
		else if ( entity->metaObject()->className() == QString( "SnapshotDirectory" ) )
			subdir = qobject_cast< SnapshotDirectory* >( entity );
	}
	
	QVERIFY( subfile );
	QVERIFY( sublink );
	QVERIFY( subdir );
	
	info.setFile( "testdir/file" );
	QByteArray hash( QCryptographicHash::hash( "abc", QCryptographicHash::Sha1 ) );
	QCOMPARE( subfile->path(), info.absoluteFilePath() );
	QCOMPARE( subfile->permissions().toString()
	        , Permissions( info.permissions() ).toString() );
	QCOMPARE( subfile->fileHash(), hash.toHex() );
	
	info.setFile( "testdir/link" );
	QCOMPARE( sublink->path(), info.absoluteFilePath() );
	QCOMPARE( sublink->target(), QString( "file" ) );
	
	info.setFile( "testdir/dir" );
	QCOMPARE( subdir->path(), info.absoluteFilePath() );
	QCOMPARE( subdir->permissions().toString()
	        , Permissions( info.permissions() ).toString() );
	QVERIFY( subdir->entries().isEmpty() );
}

void TestSnapshotDirectory::testFromDirectoryPath()
{
	QDir::current().mkpath( "testdir/dir" );
	DirectoryCleaner cleaner1( "testdir" );
	FileGenerator generator( "testdir/file" );
	generator.make( "abc" );
	QFile::link( "file", "testdir/link" );
	DefinitionPath path( "testdir" );
	
	SnapshotDirectory dir;
	dir.fromDirectory( path );

	QFileInfo info( "testdir" );
	QCOMPARE( dir.path(), info.absoluteFilePath() );
	QCOMPARE( dir.permissions().toString(), Permissions( info.permissions() ).toString() );
	QCOMPARE( dir.entries().count(), 3 );
	
	SnapshotFile* subfile( 0 );
	SnapshotLink* sublink( 0 );
	SnapshotDirectory* subdir( 0 );
	
	foreach ( SnapshotEntity* entity, dir.entries() )
	{
		if ( entity->metaObject()->className() == QString( "SnapshotFile" ) )
			subfile = qobject_cast< SnapshotFile* >( entity );
		else if ( entity->metaObject()->className() == QString( "SnapshotLink" ) )
			sublink = qobject_cast< SnapshotLink* >( entity );
		else if ( entity->metaObject()->className() == QString( "SnapshotDirectory" ) )
			subdir = qobject_cast< SnapshotDirectory* >( entity );
	}
	
	QVERIFY( subfile );
	QVERIFY( sublink );
	QVERIFY( subdir );
	
	info.setFile( "testdir/file" );
	QByteArray hash( QCryptographicHash::hash( "abc", QCryptographicHash::Sha1 ) );
	QCOMPARE( subfile->path(), info.absoluteFilePath() );
	QCOMPARE( subfile->permissions().toString()
	        , Permissions( info.permissions() ).toString() );
	QCOMPARE( subfile->fileHash(), hash.toHex() );
	
	info.setFile( "testdir/link" );
	QCOMPARE( sublink->path(), info.absoluteFilePath() );
	QCOMPARE( sublink->target(), QString( "file" ) );
	
	info.setFile( "testdir/dir" );
	QCOMPARE( subdir->path(), info.absoluteFilePath() );
	QCOMPARE( subdir->permissions().toString()
	        , Permissions( info.permissions() ).toString() );
	QVERIFY( subdir->entries().isEmpty() );
}

void TestSnapshotDirectory::testCreateFromDirectoryPath()
{
	QDir::current().mkpath( "testdir/dir" );
	DirectoryCleaner cleaner1( "testdir" );
	FileGenerator generator( "testdir/file" );
	generator.make( "abc" );
	QFile::link( "file", "testdir/link" );
	DefinitionPath path( "testdir" );
	
	SnapshotDirectory* dir( SnapshotDirectory::createFromDirectory( path ) );
	QVERIFY( dir );
	dir->setParent( this );

	QFileInfo info( "testdir" );
	QCOMPARE( dir->path(), info.absoluteFilePath() );
	QCOMPARE( dir->permissions().toString(), Permissions( info.permissions() ).toString() );
	QCOMPARE( dir->entries().count(), 3 );
	
	SnapshotFile* subfile( 0 );
	SnapshotLink* sublink( 0 );
	SnapshotDirectory* subdir( 0 );
	
	foreach ( SnapshotEntity* entity, dir->entries() )
	{
		if ( entity->metaObject()->className() == QString( "SnapshotFile" ) )
			subfile = qobject_cast< SnapshotFile* >( entity );
		else if ( entity->metaObject()->className() == QString( "SnapshotLink" ) )
			sublink = qobject_cast< SnapshotLink* >( entity );
		else if ( entity->metaObject()->className() == QString( "SnapshotDirectory" ) )
			subdir = qobject_cast< SnapshotDirectory* >( entity );
	}
	
	QVERIFY( subfile );
	QVERIFY( sublink );
	QVERIFY( subdir );
	
	info.setFile( "testdir/file" );
	QByteArray hash( QCryptographicHash::hash( "abc", QCryptographicHash::Sha1 ) );
	QCOMPARE( subfile->path(), info.absoluteFilePath() );
	QCOMPARE( subfile->permissions().toString()
	        , Permissions( info.permissions() ).toString() );
	QCOMPARE( subfile->fileHash(), hash.toHex() );
	
	info.setFile( "testdir/link" );
	QCOMPARE( sublink->path(), info.absoluteFilePath() );
	QCOMPARE( sublink->target(), QString( "file" ) );
	
	info.setFile( "testdir/dir" );
	QCOMPARE( subdir->path(), info.absoluteFilePath() );
	QCOMPARE( subdir->permissions().toString()
	        , Permissions( info.permissions() ).toString() );
	QVERIFY( subdir->entries().isEmpty() );
}

void TestSnapshotDirectory::testEmptyDirectoryId()
{
	SnapshotDirectory dir;
	dir.setPath( "testdir" );
	dir.setPermissions( 755 );
	
	QCryptographicHash hasher( QCryptographicHash::Sha1 );
	hasher.addData( "directory" );
	hasher.addData( "testdir" );
	hasher.addData( "755" );
	
	QCOMPARE( dir.id(), hasher.result().toHex() );
}

void TestSnapshotDirectory::testFullDirectoryId()
{
	SnapshotFile* subfile( new SnapshotFile );
	subfile->setPath( "subfile" );
	subfile->setPermissions( 644 );
	subfile->setFileHash( "dummy" );
	
	SnapshotLink* sublink( new SnapshotLink );
	sublink->setPath( "sublink" );
	sublink->setTarget( ".." );
	
	SnapshotDirectory* subdir( new SnapshotDirectory );
	subdir->setPath( "subdir" );
	subdir->setPermissions( 755 );
	
	SnapshotDirectory dir;
	dir.setPath( "testdir" );
	dir.setPermissions( 755 );
	dir.addEntry( subfile );
	dir.addEntry( sublink );
	dir.addEntry( subdir );
	
	QCryptographicHash hasher( QCryptographicHash::Sha1 );
	hasher.addData( "directory" );
	hasher.addData( "testdir" );
	hasher.addData( "755" );
	hasher.addData( subfile->id() );
	hasher.addData( sublink->id() );
	hasher.addData( subdir->id() );
	
	QCOMPARE( dir.id(), hasher.result().toHex() );
}

QTEST_MAIN(TestSnapshotDirectory)
