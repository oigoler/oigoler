/**
 *   Copyright (C) 2012, 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestDefinition.h"
#include "DirectoryCleaner.h"
#include "FileGenerator.h"
#include "LinkCleaner.h"
#include "../Definition.h"
#include "../DefinitionStorage.h"
#include "../Snapshot.h"
#include "../SnapshotEntity.h"
#include "../SnapshotLink.h"
#include "../SnapshotFile.h"
#include "../SnapshotDirectory.h"


void TestDefinition::hasName()
{
	Definition definition;
	definition.setName( "Name" );
	QCOMPARE( definition.name(), QString( "Name" ) );
	
	definition.setName( "Other Name" );
	QCOMPARE( definition.name(), QString( "Other Name" ) );
}

void TestDefinition::hasBackupObjectList()
{
	Definition definition;
	QCOMPARE( definition.backupObjectList().count(), 0 );
	
	DefinitionPath path1( "." );
	DefinitionPath path2( ".." );
	
	definition.addPath( path1 );
	definition.addPath( path2 );
	
	QCOMPARE( definition.backupObjectList().count(), 2 );
	QVERIFY( definition.backupObjectList().at(0) == path1 );
	QVERIFY( definition.backupObjectList().at(1) == path2 );
}

void TestDefinition::hasBackupStorage()
{
	Definition definition;
	DefinitionStorage storage( ".." );
	
	QVERIFY( definition.backupStorage() != storage );
	
	definition.setBackupStorage( storage );
	
	QVERIFY( definition.backupStorage() == storage );
}

void TestDefinition::testToXml()
{
	Definition definition;
	definition.setName( "xmltest" );
	definition.addPath( DefinitionPath( "." ) );
	definition.addPath( DefinitionPath( ".." ) );
	definition.setBackupStorage( DefinitionStorage( "/tmp" ) );
	
	QString xml;
	QXmlStreamWriter writer1( &xml );
	writer1.writeStartDocument();
	definition.toXml( &writer1 );
	writer1.writeEndDocument();
	
	QString expected;
	QXmlStreamWriter writer2( &expected );
	writer2.writeStartDocument();
	writer2.writeStartElement( "BackupDefinition" );
	writer2.writeAttribute( "name", "xmltest" );
	definition.backupObjectList().toXml( &writer2 );
	definition.backupStorage().toXml( &writer2 );
	writer2.writeEndDocument();
	
	QCOMPARE( xml, expected );
}

void TestDefinition::testFromXml()
{
	Definition def1;
	def1.setName( "xmltest" );
	def1.addPath( DefinitionPath( "." ) );
	def1.addPath( DefinitionPath( ".." ) );
	def1.setBackupStorage( DefinitionStorage( "/tmp" ) );
	
	QString xml;
	QXmlStreamWriter writer( &xml );
	writer.writeStartDocument();
	def1.toXml( &writer );
	writer.writeEndDocument();
	
	QXmlStreamReader reader( xml );
	reader.readNextStartElement();
	Definition def2;
	def2.fromXml( &reader );
	QCOMPARE( reader.readNext(), QXmlStreamReader::EndDocument );
	
	QCOMPARE( def2.name(), def1.name() );
	QCOMPARE( def2.backupObjectList().toStringList(), def1.backupObjectList().toStringList() );
	QCOMPARE( def2.backupStorage().toString(), def1.backupStorage().toString() );
}

void TestDefinition::testRemovePath()
{
	Definition definition;
	definition.addPath( DefinitionPath( "." ) );
	definition.addPath( DefinitionPath( ".." ) );
	
	QCOMPARE( definition.backupObjectList().size(), 2 );
	
	definition.removePath( DefinitionPath( ".." ) );
	
	QCOMPARE( definition.backupObjectList().size(), 1 );
	QCOMPARE( definition.backupObjectList().at(0).path(), QString( "." ) );
}

void TestDefinition::testToStringList()
{
	Definition definition;
	definition.setName( "StringListTest" );
	definition.addPath( DefinitionPath( "." ) );
	definition.addPath( DefinitionPath( ".." ) );
	definition.setBackupStorage( DefinitionStorage( "/tmp" ) );
	
	QStringList expected;
	expected.append( "Backup: StringListTest" );
	expected.append( definition.backupStorage().toString() );
	expected.append( definition.backupObjectList().toStringList() );
	
	QCOMPARE( definition.toStringList(), expected );
}

void TestDefinition::testEqualsOperator()
{
	Definition def1;
	Definition def2;
	QVERIFY( def1 == def2 );
	
	def1.setName( "test" );
	QVERIFY( !( def1 == def2 ) );
	def2.setName( "test" );
	QVERIFY( def1 == def2 );
	
	def1.addPath( DefinitionPath( "~" ) );
	QVERIFY( !( def1 == def2 ) );
	def2.addPath( DefinitionPath( "~" ) );
	QVERIFY( def1 == def2 );
	
	def1.setBackupStorage( DefinitionStorage( "/tmp" ) );
	QVERIFY( !( def1 == def2 ) );
	def2.setBackupStorage( DefinitionStorage( "/tmp" ) );
	QVERIFY( def1 == def2 );
}

void TestDefinition::testDifferenceOperator()
{
	Definition def1;
	Definition def2;
	QVERIFY( !( def1 != def2 ) );
	
	def1.setName( "test" );
	QVERIFY( def1 != def2 );
	def2.setName( "test" );
	QVERIFY( !( def1 != def2 ) );
	
	def1.addPath( DefinitionPath( "~" ) );
	QVERIFY( def1 != def2 );
	def2.addPath( DefinitionPath( "~" ) );
	QVERIFY( !( def1 != def2 ) );
	
	def1.setBackupStorage( DefinitionStorage( "/tmp" ) );
	QVERIFY( def1 != def2 );
	def2.setBackupStorage( DefinitionStorage( "/tmp" ) );
	QVERIFY( !( def1 != def2 ) );
}

void TestDefinition::testAssignmentOperator()
{
	Definition def1;
	def1.setName( "test" );
	def1.addPath( DefinitionPath( "~" ) );
	def1.setBackupStorage( DefinitionStorage( "/tmp" ) );

	Definition def2;

	QVERIFY( def1 != def2 );
	
	def2 = def1;
	
	QVERIFY( def1 == def2 );
}

void TestDefinition::testCopyConstructor()
{
	Definition def1;
	def1.setName( "test" );
	def1.addPath( DefinitionPath( "~" ) );
	def1.setBackupStorage( DefinitionStorage( "/tmp" ) );

	Definition def2;

	QVERIFY( def1 != def2 );
	
	Definition def3( def1 );
	
	QVERIFY( def1 == def3 );
}

void TestDefinition::testSnapshot()
{
	QDir::current().mkdir( "testdir" );
	DirectoryCleaner cleaner1( "testdir" );
	
	FileGenerator generator( "testfile" );
	generator.make( "abc" );
	
	QFile::link( "testfile", "testlink" );
	LinkCleaner cleaner2( "testlink" );
	
	Definition definition;
	definition.addPath( DefinitionPath( "testlink" ) );
	definition.addPath( DefinitionPath( "testfile" ) );
	definition.addPath( DefinitionPath( "testdir" ) );
	
	Snapshot snapshot( definition.snapshot() );
	SnapshotEntityList entities( snapshot.entityList() );
	
	foreach ( SnapshotEntity* entry, entities )
		entry->setParent( this );
	
	QCOMPARE( entities.size(), 3 );
	
	SnapshotLink* link( qobject_cast< SnapshotLink* >( entities.at(0) ) );
	SnapshotFile* file( qobject_cast< SnapshotFile* >( entities.at(1) ) );
	SnapshotDirectory* dir( qobject_cast< SnapshotDirectory* >( entities.at(2) ) );
	
	QVERIFY( link );
	QVERIFY( file );
	QVERIFY( dir );
	
	QCOMPARE( link->path(), QFileInfo( "testlink" ).absoluteFilePath() );
	QCOMPARE( file->path(), QFileInfo( "testfile" ).absoluteFilePath() );
	QCOMPARE( dir->path() , QFileInfo( "testdir" ).absoluteFilePath() );
}

QTEST_MAIN(TestDefinition)
