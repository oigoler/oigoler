/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestCreateRecipe_SessionError.h"
#include "../BackupSession.h"
#include "../BackupRecipe.h"


void TestCreateRecipe_SessionError::initTestCase()
{
	m_recipe = 0;
	
	BackupSession session;
	session.setConfigDirectory( QString() );
	
	m_recipe = new BackupRecipe( "testrecipe" );
	session.createRecipe( m_recipe );
}

void TestCreateRecipe_SessionError::testValidity()
{
	QVERIFY( !m_recipe->isValid() );
}

void TestCreateRecipe_SessionError::testError()
{
	QCOMPARE( m_recipe->error(), BackupRecipe::SessionError );
}

void TestCreateRecipe_SessionError::testToString()
{
	QString expected( "Error: Backup session not setup right!" );
	
	QString actual;
	QTextStream stream( &actual );
	
	stream << *m_recipe;
	
	QCOMPARE( actual, expected );
}

void TestCreateRecipe_SessionError::cleanupTestCase()
{
	delete m_recipe;
}

QTEST_MAIN(TestCreateRecipe_SessionError)
