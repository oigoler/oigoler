/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestListRecipe.h"
#include "DirectoryCleaner.h"
#include "../BackupSession.h"
#include "../BackupStorage.h"


void TestListRecipe::initTestCase()
{
	BackupSession session;
	session.setConfigDirectory( "config" );
	new DirectoryCleaner( "config", this );
	
	QDir::current().mkdir( "storage1" );
	QDir::current().mkdir( "storage2" );
	new DirectoryCleaner( "storage1", this );
	new DirectoryCleaner( "storage2", this );
	m_storage1 = session.createStorage( "storage1" ).key();
	m_storage2 = session.createStorage( "storage2" ).key();
	
	m_recipe1 = new BackupRecipe( "empty" );
	session.createRecipe( m_recipe1 );
	
	m_recipe2 = new BackupRecipe( "half-full" );
	m_recipe2->addPath( "." );
	m_recipe2->addStorage( m_storage1 );
	session.createRecipe( m_recipe2 );
	
	m_recipe3 = new BackupRecipe( "full" );
	m_recipe3->addPath( "." );
	m_recipe3->addPath( ".." );
	m_recipe3->addStorage( m_storage1 );
	m_recipe3->addStorage( m_storage2 );
	session.createRecipe( m_recipe3 );
	
	QVERIFY( m_recipe1->isValid() );
	QVERIFY( m_recipe2->isValid() );
	QVERIFY( m_recipe3->isValid() );
	
	m_list = session.listRecipe();
}

void TestListRecipe::testValidity()
{
	QVERIFY( m_list.isValid() );
}

void TestListRecipe::testError()
{
	QCOMPARE( m_list.error(), BackupRecipeList::NoError );
}

void TestListRecipe::testSize()
{
	QCOMPARE( m_list.size(), 3 );
}

void TestListRecipe::testElements()
{
	QDir dir( QDir::current() );
	QString path2( dir.absolutePath() );
	dir.cdUp();
	QString path1( dir.absolutePath() );
	
	QCOMPARE( m_list.size(), 3 );
	
	BackupRecipe recipe1( m_list.at(0) );
	BackupRecipe recipe2( m_list.at(1) );
	BackupRecipe recipe3( m_list.at(2) );
	
	QCOMPARE( recipe1.name(), QString( "empty" ) );
	QVERIFY( recipe1.pathList().isEmpty() );
	QVERIFY( recipe1.storageList().isEmpty() );
	
	QCOMPARE( recipe2.name(), QString( "full" ) );
	QCOMPARE( recipe2.pathList().size() , 2 );
	QCOMPARE( recipe2.pathList().first(), path1 );
	QCOMPARE( recipe2.pathList().last() , path2 );
	QCOMPARE( recipe2.storageList().size() , 2 );
	QCOMPARE( recipe2.storageList().first(), m_storage1 );
	QCOMPARE( recipe2.storageList().last() , m_storage2 );
	
	QCOMPARE( recipe3.name(), QString( "half-full" ) );
	QCOMPARE( recipe3.pathList().size() , 1 );
	QCOMPARE( recipe3.pathList().first(), path2 );
	QCOMPARE( recipe3.storageList().size() , 1 );
	QCOMPARE( recipe3.storageList().first(), m_storage1 );
}

void TestListRecipe::testToString()
{
	QString expected;
	QTextStream stream1( &expected );
	
	stream1 << "empty\n"
	        << "full\n"
	        << "half-full\n";
	
	QString actual;
	QTextStream stream( &actual );
	
	stream << m_list;
	
	QCOMPARE( actual, expected );
}

void TestListRecipe::cleanupTestCase()
{
	delete m_recipe1;
	delete m_recipe2;
	delete m_recipe3;
}

QTEST_MAIN(TestListRecipe)
