/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestCreateRecipe_DuplicateRecipe.h"
#include "DirectoryCleaner.h"
#include "../BackupSession.h"
#include "../BackupRecipe.h"

#include <QDir>


void TestCreateRecipe_DuplicateRecipe::initTestCase()
{
	BackupSession session;
	session.setConfigDirectory( "config" );
	new DirectoryCleaner( "config", this );
	
	m_recipe = new BackupRecipe( "testrecipe" );
	m_recipe->addPath( "." );
	session.createRecipe( m_recipe );
	
	QFile recipeFile( "config/recipe/testrecipe.xml" );
	QVERIFY( recipeFile.exists() );
	
	QCryptographicHash hasher( QCryptographicHash::Sha1 );
	recipeFile.open( QFile::ReadOnly );
	hasher.addData( recipeFile.readAll() );
	m_hash = hasher.result();

	m_recipe->addPath( ".." );
	session.createRecipe( m_recipe );
}

void TestCreateRecipe_DuplicateRecipe::testValidity()
{
	QVERIFY( !m_recipe->isValid() );
}

void TestCreateRecipe_DuplicateRecipe::testError()
{
	QCOMPARE( m_recipe->error(), BackupRecipe::DuplicateRecipe );
}

void TestCreateRecipe_DuplicateRecipe::testToString()
{
	QString expected( "Error: Backup recipe 'testrecipe' already exists!" );
	
	QString actual;
	QTextStream stream( &actual );
	
	stream << *m_recipe;
	
	QCOMPARE( actual, expected );
}

void TestCreateRecipe_DuplicateRecipe::testOriginalFile()
{
	QFile recipeFile( "config/recipe/testrecipe.xml" );
	QVERIFY( recipeFile.exists() );
	
	QCryptographicHash hasher( QCryptographicHash::Sha1 );
	recipeFile.open( QFile::ReadOnly );
	hasher.addData( recipeFile.readAll() );
	QCOMPARE( hasher.result(), m_hash );
}

void TestCreateRecipe_DuplicateRecipe::cleanupTestCase()
{
	delete m_recipe;
}

QTEST_MAIN(TestCreateRecipe_DuplicateRecipe)
