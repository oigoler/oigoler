/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestDefinitionPath.h"
#include "../DefinitionPath.h"


void TestDefinitionPath::hasPath()
{
	DefinitionPath def;
	QVERIFY( def.path().isEmpty() );
	
	def.setPath( ".." );
	QCOMPARE( def.path(), QString( ".." ) );
}

void TestDefinitionPath::testToXml()
{
	DefinitionPath def;
	def.setPath( "." );
	
	QString xml;
	QXmlStreamWriter writer1( &xml );
	writer1.writeStartDocument();
	def.toXml( &writer1 );
	writer1.writeEndDocument();
	
	QString expected;
	QXmlStreamWriter writer2( &expected );
	writer2.writeStartDocument();
	writer2.writeTextElement( "path", QDir::currentPath() );
	writer2.writeEndDocument();
	
	QCOMPARE( xml, expected );
}

void TestDefinitionPath::testFromXml()
{
	DefinitionPath def;
	def.setPath( "." );
	
	QString xml;
	QXmlStreamWriter writer( &xml );
	writer.writeStartDocument();
	def.toXml( &writer );
	writer.writeEndDocument();
	
	QXmlStreamReader reader( xml );
	reader.readNextStartElement();
	def.fromXml( &reader );
	
	QCOMPARE( def.path(), QDir::currentPath() );
}

void TestDefinitionPath::testPathInConstructor()
{
	DefinitionPath def( ".." );
	QCOMPARE( def.path(), QString( ".." ) );
}

void TestDefinitionPath::testToString()
{
	DefinitionPath def( "." );
	QCOMPARE( def.toString(), "path " + QDir::currentPath() );
}

void TestDefinitionPath::hasEqualsOperator()
{
	DefinitionPath path1( "." );
	DefinitionPath path2( ".." );
	DefinitionPath path3( "." );
	
	QVERIFY( path1 == path3 );
	QVERIFY( !( path1 == path2 ) );
}

void TestDefinitionPath::hasDifferenceOperator()
{
	DefinitionPath path1( "." );
	DefinitionPath path2( ".." );
	DefinitionPath path3( "." );
	
	QVERIFY( path1 != path2 );
	QVERIFY( !( path1 != path3 ) );
}

void TestDefinitionPath::hasAssignmentOperator()
{
	DefinitionPath path1( ".." );
	DefinitionPath path2;

	QVERIFY( path1 != path2 );
	
	path2 = path1;
	
	QVERIFY( path1 == path2 );
}

void TestDefinitionPath::hasCopyConstructor()
{
	DefinitionPath path1( ".." );
	DefinitionPath path2;

	QVERIFY( path1 != path2 );
	
	DefinitionPath path3( path1 );
	
	QVERIFY( path1 == path3 );
}

QTEST_MAIN(TestDefinitionPath)
