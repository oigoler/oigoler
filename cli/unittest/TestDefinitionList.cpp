/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestDefinitionList.h"
#include "../DefinitionList.h"


void TestDefinitionList::isList()
{
	DefinitionList list;
	QVERIFY( list.isEmpty() );
	
	list.append( Definition() );
	QCOMPARE( list.count(), 1 );
	
	list << Definition() << Definition();
	QCOMPARE( list.count(), 3 );
}

void TestDefinitionList::testNameList()
{
	Definition definition;
	definition.setName( "name1" );
	
	DefinitionList list;
	list.append( definition );
	
	definition.setName( "name2" );
	list.append( definition );
	
	definition.setName( "name3" );
	list.append( definition );
	
	QStringList expected;
	expected << "name1" << "name2" << "name3";
	
	QCOMPARE( list.names(), expected );
}

QTEST_MAIN(TestDefinitionList)
