/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestOverwriteRecipe.h"
#include "DirectoryCleaner.h"
#include "../BackupSession.h"
#include "../BackupRecipe.h"
#include "../BackupStorage.h"

#include <QDir>


void TestOverwriteRecipe::initTestCase()
{
	BackupSession session;
	session.setConfigDirectory( "config" );
	new DirectoryCleaner( "config", this );
	
	QDir::current().mkdir( "storage1" );
	QDir::current().mkdir( "storage2" );
	new DirectoryCleaner( "storage1", this );
	new DirectoryCleaner( "storage2", this );
	m_storage1 = session.createStorage( "storage1" ).key();
	m_storage2 = session.createStorage( "storage2" ).key();
	
	m_recipe = new BackupRecipe( "testrecipe" );
	session.createRecipe( m_recipe );
	
	m_recipe->addStorage( m_storage1 );
	m_recipe->addStorage( m_storage2 );
	m_recipe->addPath( "." );
	m_recipe->addPath( ".." );
	session.registerRecipe( m_recipe );
}

void TestOverwriteRecipe::testValidity()
{
	QVERIFY( m_recipe->isValid() );
}

void TestOverwriteRecipe::testError()
{
	QCOMPARE( m_recipe->error(), BackupRecipe::NoError );
}

void TestOverwriteRecipe::testFileName()
{
	QDir config( "config/recipe" );
	QVERIFY( config.exists() );
	QVERIFY( config.exists( "testrecipe.xml" ) );
}

void TestOverwriteRecipe::testToString()
{
	QDir dir( QDir::current() );
	dir.cdUp();
	
	QString path1( dir.absolutePath() );
	QString path2( QDir::currentPath() );
	
	QString expected( "recipe testrecipe\n" );
	expected += "path " + path1 + "\n";
	expected += "path " + path2 + "\n";
	expected += "storage " + m_storage1 + "\n";
	expected += "storage " + m_storage2;
	
	QString actual;
	QTextStream stream( &actual );
	
	stream << *m_recipe;
	
	QCOMPARE( actual, expected );
}

void TestOverwriteRecipe::testFileContents()
{
	QDir config( "config/recipe" );
	QVERIFY( config.exists() );
	QVERIFY( config.exists( "testrecipe.xml" ) );
	
	QFile file( config.filePath( "testrecipe.xml" ) );
	file.open( QFile::ReadOnly );
	
	QXmlStreamReader reader( &file );
	
	QXmlStreamReader::TokenType token( reader.readNext() );
	QCOMPARE( token, QXmlStreamReader::StartDocument );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "recipe" ) );
	QXmlStreamAttributes attributes( reader.attributes() );
	QCOMPARE( attributes.size(), 1 );
	QVERIFY( attributes.hasAttribute( "name" ) );
	QCOMPARE( attributes.value( "name" ).toString(), QString( "testrecipe" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "path" ) );
	
	QDir dir( QDir::current() );
	dir.cdUp();
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::Characters );
	QCOMPARE( reader.text().toString(), dir.absolutePath() );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "path" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "path" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::Characters );
	QCOMPARE( reader.text().toString(), QDir::currentPath() );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "path" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "storage" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::Characters );
	QCOMPARE( reader.text().toUtf8(), m_storage1 );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "storage" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "storage" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::Characters );
	QCOMPARE( reader.text().toUtf8(), m_storage2 );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "storage" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "recipe" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndDocument );
}

void TestOverwriteRecipe::cleanupTestCase()
{
	delete m_recipe;
}

QXmlStreamReader::TokenType
TestOverwriteRecipe::readNextToken( QXmlStreamReader* reader ) const
{
	QXmlStreamReader::TokenType token;
	
	while ( !reader->atEnd() )
	{
		token = reader->readNext();
		
		if ( !reader->isWhitespace() )
			return token;
	}
	
	return QXmlStreamReader::Invalid;
}

QTEST_MAIN(TestOverwriteRecipe)
