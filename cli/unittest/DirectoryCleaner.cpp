/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "DirectoryCleaner.h"


DirectoryCleaner::DirectoryCleaner( const QString& directory, QObject* parent )
	: QObject( parent )
	, m_dir( directory )
{
}

DirectoryCleaner::~DirectoryCleaner()
{
	if ( m_dir.exists() )
	{
		p_removeDirectory( m_dir );
	}
}

void DirectoryCleaner::p_removeDirectory( QDir dir )
{
	foreach ( QFileInfo info, dir.entryInfoList( QDir::AllEntries
		                                       | QDir::System
		                                       | QDir::Hidden
		                                       | QDir::NoDotAndDotDot ) )
	{
		if ( info.isDir() && !info.isSymLink() )
		{
			p_removeDirectory( QDir( info.filePath() ) );
		}
		else
		{
			dir.remove( info.fileName() );
		}
	}
	
	QString name( dir.dirName() );
	dir.cdUp();
	dir.rmdir( name );
}
