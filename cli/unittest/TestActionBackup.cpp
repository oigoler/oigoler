/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestActionBackup.h"
#include "SetupForBackup.h"
#include "../ActionBackup.h"


void TestActionBackup::hasDefaultBackupEnvironment()
{
	ActionBackup action;
	Environment expected;
	
	QVERIFY( action.backupEnvironment() == expected );
}

void TestActionBackup::hasBackupEnvironment()
{
	Environment env;
	env.setConfigDirectory( "test.dir" );
	
	ActionBackup action;
	
	QVERIFY( action.backupEnvironment() != env );
	
	action.setBackupEnvironment( env );
	
	QVERIFY( action.backupEnvironment() == env );
}

void TestActionBackup::testBackup()
{
	SetupForBackup setup;
	ActionBackup action;
	QStringList args;
	QString output;
	
	args.append( setup.name() );
	
	action.setBackupEnvironment( setup.env() );
	action.setOutputString( &output );
	action.execute( args );
	setup.setEnv( action.backupEnvironment() );
	
	QCOMPARE( output, "Successfully backup up '" + setup.name() + "'\n" );
	
	QFile configSnapshot( setup.snapshotInConfigPath() );
	QVERIFY( configSnapshot.exists() );
	
	QFile storageSnapshot( setup.snapshotInStoragePath() );
	QVERIFY( storageSnapshot.exists() );
	
	QFile file( setup.fileInStoragePath() );
	QVERIFY( file.exists() );
	
	configSnapshot.open( QFile::ReadOnly );
	storageSnapshot.open( QFile::ReadOnly );
	file.open( QFile::ReadOnly );
	
	QCOMPARE( QString( configSnapshot.readAll() ), setup.expectedSnapshotXml() );
	QCOMPARE( QString( storageSnapshot.readAll() ), setup.expectedSnapshotXml() );
	QCOMPARE( file.readAll(), setup.expectedFileContents() );
}

QTEST_MAIN(TestActionBackup)
