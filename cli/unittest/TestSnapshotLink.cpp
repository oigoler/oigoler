/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestSnapshotLink.h"
#include "LinkCleaner.h"
#include "../SnapshotLink.h"
#include "../DefinitionPath.h"


void TestSnapshotLink::hasPath()
{
	SnapshotLink link;
	QVERIFY( link.path().isEmpty() );
	
	link.setPath( ".." );
	QCOMPARE( link.path(), QString( ".." ) );
}

void TestSnapshotLink::hasTarget()
{
	SnapshotLink link;
	QVERIFY( link.target().isEmpty() );
	
	link.setTarget( ".." );
	QCOMPARE( link.target(), QString( ".." ) );
}

void TestSnapshotLink::testToXml()
{
	QVERIFY( QFile::link( "..", "testlink" ) );
	LinkCleaner cleaner( "testlink" );
	
	QString xml;
	QXmlStreamWriter writer1( &xml );
	
	SnapshotLink link;
	link.fromLink( "testlink" );
	
	writer1.writeStartDocument();
	link.toXml( &writer1 );
	writer1.writeEndDocument();
	
	QString expected;
	QXmlStreamWriter writer2( &expected );
	QFileInfo info( "testlink" );
	
	writer2.writeStartDocument();
	writer2.writeStartElement( "link" );
	writer2.writeStartElement( "path" );
	writer2.writeCharacters( info.absoluteFilePath() );
	writer2.writeEndElement();
	writer2.writeStartElement( "target" );
	writer2.writeCharacters( ".." );
	writer2.writeEndElement();
	writer2.writeEndElement();
	writer2.writeEndDocument();
	
	QCOMPARE( xml, expected );
}

void TestSnapshotLink::testFromLinkString()
{
	QVERIFY( QFile::link( "..", "testlink" ) );
	LinkCleaner cleaner( "testlink" );
	QFileInfo info( "testlink" );
	
	SnapshotLink link;
	link.fromLink( "testlink" );
	
	QCOMPARE( link.path(), info.absoluteFilePath() );
	QCOMPARE( link.target(), QString( ".." ) );
}

void TestSnapshotLink::testFromLinkPath()
{
	QVERIFY( QFile::link( "..", "testlink" ) );
	LinkCleaner cleaner( "testlink" );
	QFileInfo info( "testlink" );
	DefinitionPath path( "testlink" );
	
	SnapshotLink link;
	link.fromLink( path );
	
	QCOMPARE( link.path(), info.absoluteFilePath() );
	QCOMPARE( link.target(), QString( ".." ) );
}

void TestSnapshotLink::testCreateFromLinkString()
{
	QVERIFY( QFile::link( "..", "testlink" ) );
	LinkCleaner cleaner( "testlink" );
	QFileInfo info( "testlink" );
	
	SnapshotLink* link( SnapshotLink::createFromLink( "testlink" ) );
	QVERIFY( link );
	link->setParent( this );
	
	QCOMPARE( link->path(), info.absoluteFilePath() );
	QCOMPARE( link->target(), QString( ".." ) );
}

void TestSnapshotLink::testCreateFromLinkPath()
{
	QVERIFY( QFile::link( "..", "testlink" ) );
	LinkCleaner cleaner( "testlink" );
	QFileInfo info( "testlink" );
	DefinitionPath path( "testlink" );
	
	SnapshotLink* link( SnapshotLink::createFromLink( path ) );
	QVERIFY( link );
	link->setParent( this );
	
	QCOMPARE( link->path(), info.absoluteFilePath() );
	QCOMPARE( link->target(), QString( ".." ) );
}

void TestSnapshotLink::testId()
{
	QVERIFY( QFile::link( "..", "testlink" ) );
	LinkCleaner cleaner( "testlink" );
	QFileInfo info( "testlink" );
	
	QCryptographicHash hasher( QCryptographicHash::Sha1 );
	hasher.addData( "link" );
	hasher.addData( info.absoluteFilePath().toUtf8() );
	hasher.addData( ".." );
	
	SnapshotLink link;
	link.fromLink( "testlink" );
	
	QCOMPARE( link.id(), hasher.result().toHex() );
}

QTEST_MAIN(TestSnapshotLink)
