/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestEnvironment.h"
#include "SetupForBackup.h"
#include "../Definition.h"
#include "../DefinitionList.h"


void TestEnvironment::hasConfigDirectory()
{
	Environment env;
	env.setConfigDirectory( "/tmp" );
	
	QCOMPARE( env.configDirectory(), QString( "/tmp" ) );
}

void TestEnvironment::hasTimestamp()
{
	Environment env;
	env.setTimestamp( 123456789 );
	
	QCOMPARE( env.timestamp(), qint64( 123456789 ) );
}

void TestEnvironment::testDefinitionDirectory()
{
	Environment env;
	env.setConfigDirectory( "/tmp" );
	
	QCOMPARE( env.definitionDirectory(), QString( "/tmp/definition" ) );
}

void TestEnvironment::testSnapshotDirectory()
{
	Environment env;
	env.setConfigDirectory( "/tmp" );
	
	QCOMPARE( env.snapshotDirectory(), QString( "/tmp/snapshot" ) );
}

void TestEnvironment::testDefaults()
{
	qint64 before( QDateTime::currentMSecsSinceEpoch() );
	
	Environment env;
	QString expected( QDir::homePath() + "/.oigoler" );
	
	QCOMPARE( env.configDirectory(), expected );
	QCOMPARE( env.definitionDirectory(), expected + "/definition" );
	QCOMPARE( env.snapshotDirectory(), expected + "/snapshot" );
	
	qint64 after( QDateTime::currentMSecsSinceEpoch() );
	
	QVERIFY( env.timestamp() >= before );
	QVERIFY( env.timestamp() <= after  );
}

void TestEnvironment::testNonExistentDefinitionDirectory()
{
	Environment env;
	env.setConfigDirectory( "test/configuration/directory" );
	
	QFileInfo info( "test" );
	QVERIFY( !info.exists() );
	DirectoryCleaner cleaner( "test" );
	
	Definition definition;
	definition.setName( "test" );
	definition.setBackupStorage( DefinitionStorage( "/tmp" ) );
	definition.addPath( DefinitionPath( "." ) );
	
	env.storeDefinition( definition );
	info.setFile( env.definitionDirectory() );
	QVERIFY( info.exists() );
}

void TestEnvironment::testDefinitionFileCreation()
{
	Environment env;
	env.setConfigDirectory( "test.dir" );
	DirectoryCleaner cleaner( "test.dir" );
	
	Definition definition;
	definition.setName( "test" );
	definition.setBackupStorage( DefinitionStorage( "/tmp" ) );
	definition.addPath( DefinitionPath( "." ) );
	
	env.storeDefinition( definition );
	QVERIFY( QFile::exists( "test.dir/definition/test.xml" ) );
}

void TestEnvironment::testStoreDefinition()
{
	Environment env;
	env.setConfigDirectory( "test.dir" );
	DirectoryCleaner cleaner( "test.dir" );
	
	Definition definition;
	definition.setName( "test" );
	definition.setBackupStorage( DefinitionStorage( "/tmp" ) );
	definition.addPath( DefinitionPath( QDir::currentPath() ) );
	
	env.storeDefinition( definition );
	
	QFile xmlFile( "test.dir/definition/test.xml" );
	xmlFile.open( QFile::ReadOnly );
	QXmlStreamReader reader( &xmlFile );
	
	Definition expected;
	reader.readNextStartElement();
	expected.fromXml( &reader );
	
	QVERIFY( definition == expected );
}

void TestEnvironment::testDefinitionXmlFormatting()
{
	Environment env;
	env.setConfigDirectory( "test.dir" );
	DirectoryCleaner cleaner( "test.dir" );
	
	Definition definition;
	definition.setName( "test" );
	definition.setBackupStorage( DefinitionStorage( "/tmp" ) );
	definition.addPath( DefinitionPath( QDir::currentPath() ) );
	
	env.storeDefinition( definition );
	
	QFile xmlFile( "test.dir/definition/test.xml" );
	xmlFile.open( QFile::ReadOnly );
	QTextStream reader( &xmlFile );
	QString xmlString( reader.readAll() );
	
	QFile expectedFile ( "test.dir/expected.xml" );
	expectedFile.open( QFile::ReadWrite );
	QXmlStreamWriter writer( &expectedFile );
	writer.setAutoFormatting( true );
	writer.writeStartDocument();
	definition.toXml( &writer );
	writer.writeEndDocument();
	expectedFile.flush();
	
	expectedFile.reset();
	QTextStream expectedReader( &expectedFile );
	QString expected( expectedReader.readAll() );
	
	QCOMPARE( xmlString, expected );
}

void TestEnvironment::testDefinitionExists()
{
	Environment env;
	env.setConfigDirectory( "test.dir" );
	DirectoryCleaner cleaner( "test.dir" );
	
	Definition definition;
	definition.setName( "test" );
	definition.setBackupStorage( DefinitionStorage( "/tmp" ) );
	definition.addPath( DefinitionPath( QDir::currentPath() ) );
	
	env.storeDefinition( definition );
	
	QVERIFY( !env.definitionExists( "nonexistent" ) );
	QVERIFY( env.definitionExists( "test" ) );
}

void TestEnvironment::testRetrieveDefinition()
{
	Environment env1;
	env1.setConfigDirectory( "test.dir" );
	DirectoryCleaner cleaner( "test.dir" );
	
	Definition def1;
	def1.setName( "test" );
	def1.setBackupStorage( DefinitionStorage( "/tmp" ) );
	def1.addPath( DefinitionPath( QDir::currentPath() ) );
	
	env1.storeDefinition( def1 );

	Environment env2;
	env2.setConfigDirectory( "test.dir" );
	Definition def2( env2.definition( "test" ) );
	
	QVERIFY( def1 == def2 );
}

void TestEnvironment::testRetrieveNonExistentDefinition()
{
	Environment env;
	env.setConfigDirectory( "test.dir" );
	
	Definition definition( env.definition( "test" ) );
	
	QCOMPARE( definition.name(), QString( "test" ) );
	QVERIFY( definition.backupStorage().path().isEmpty() );
	QVERIFY( definition.backupObjectList().isEmpty() );
}

void TestEnvironment::testDefinitionOverwrite()
{
	Environment env;
	env.setConfigDirectory( "test.dir" );
	DirectoryCleaner cleaner( "test.dir" );
	
	Definition definition;
	definition.setName( "test" );
	definition.setBackupStorage( DefinitionStorage( "/tmp" ) );
	definition.addPath( DefinitionPath( QDir::currentPath() ) );
	
	env.storeDefinition( definition );
	
	definition.addPath( DefinitionPath( QDir::homePath() ) );
	env.storeDefinition( definition );
	
	QVERIFY( env.definition( "test" ) == definition );
}

void TestEnvironment::testRemoveDefinition()
{
	Environment env;
	env.setConfigDirectory( "test.dir" );
	DirectoryCleaner cleaner( "test.dir" );
	
	Definition definition;
	definition.setName( "test" );
	definition.setBackupStorage( DefinitionStorage( "/tmp" ) );
	definition.addPath( DefinitionPath( QDir::currentPath() ) );
	
	env.storeDefinition( definition );
	
	QVERIFY( env.definitionExists( "test" ) );
	env.removeDefinition( "test" );
	QVERIFY( !env.definitionExists( "test" ) );
}

void TestEnvironment::testDefinitionList()
{
	Environment env;
	env.setConfigDirectory( "test.dir" );
	DirectoryCleaner cleaner( "test.dir" );
	DefinitionList expected;

	Definition definition;
	definition.setName( "definition1" );
	definition.setBackupStorage( DefinitionStorage( "/tmp" ) );
	definition.addPath( DefinitionPath( QDir::currentPath() ) );
	env.storeDefinition( definition );
	expected.append( definition );
	
	definition.setName( "definition2" );
	definition.setBackupStorage( DefinitionStorage( "/var/tmp" ) );
	definition.addPath( DefinitionPath( QDir::homePath() ) );
	env.storeDefinition( definition );
	expected.append( definition );
	
	QCOMPARE( env.definitions(), expected );
}

void TestEnvironment::testEqualsOperator()
{
	Environment env1;
	Environment env2;
	QVERIFY( env1 == env2 );
	
	env1.setConfigDirectory( "test.dir" );
	QVERIFY( !( env1 == env2 ) );
	env2.setConfigDirectory( "test.dir" );
	QVERIFY( env1 == env2 );
}

void TestEnvironment::testDifferenceOperator()
{
	Environment env1;
	Environment env2;
	QVERIFY( !( env1 != env2 ) );
	
	env1.setConfigDirectory( "test.dir" );
	QVERIFY( env1 != env2 );
	env2.setConfigDirectory( "test.dir" );
	QVERIFY( !( env1 != env2 ) );
}

void TestEnvironment::testAssignmentOperator()
{
	Environment env1;
	env1.setConfigDirectory( "test.dir" );
	env1.setTimestamp( 654 );

	Environment env2;
	env2 = env1;
	
	QCOMPARE( env2.configDirectory(), env1.configDirectory() );
	QCOMPARE( env2.timestamp(), env1.timestamp() );
}

void TestEnvironment::testCopyConstructor()
{
	Environment env1;
	env1.setConfigDirectory( "test.dir" );
	env1.setTimestamp( 654 );

	Environment env2( env1 );
	
	QCOMPARE( env2.configDirectory(), env1.configDirectory() );
	QCOMPARE( env2.timestamp(), env1.timestamp() );
}

void TestEnvironment::testNonExistentSnapshotDirectory()
{
	SetupForBackup setup;
	
	QFileInfo info( setup.snapshotDirectory() );
	QVERIFY( !info.exists() );
	
	setup.backup();

	info.setFile( setup.snapshotDirectory() );
	QVERIFY( info.exists() );
}

void TestEnvironment::testSnapshotInConfigDirectory()
{
	SetupForBackup setup;
	setup.backup();
	
	QFile xmlFile( setup.snapshotInConfigPath() );
	QVERIFY( xmlFile.exists() );
	
	xmlFile.open( QFile::ReadOnly );
	
	QCOMPARE( QString( xmlFile.readAll() ), setup.expectedSnapshotXml() );
}

void TestEnvironment::testSnapshotInStorageDirectory()
{
	SetupForBackup setup;
	setup.backup();

	QFile xmlFile( setup.snapshotInStoragePath() );
	QVERIFY( xmlFile.exists() );
	
	xmlFile.open( QFile::ReadOnly );
	
	QCOMPARE( QString( xmlFile.readAll() ), setup.expectedSnapshotXml() );
}

void TestEnvironment::testFileInStorage()
{
	SetupForBackup setup;
	setup.backup();
	
	QFile file( setup.fileInStoragePath() );
	QVERIFY( file.exists() );
	
	file.open( QFile::ReadOnly );
	
	QCOMPARE( file.readAll(), setup.expectedFileContents() );
}

QTEST_MAIN(TestEnvironment)
