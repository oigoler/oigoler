/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef FILEGENERATOR_H
#define FILEGENERATOR_H

#include <QObject>
#include <QString>


class FileGenerator : public QObject
{
	Q_OBJECT

public:
	FileGenerator( const QString& file, QObject* parent = 0 );
	FileGenerator( const QString& file, const QByteArray& data, QObject* parent = 0 );
	virtual ~FileGenerator();
	
	void make( const QByteArray& data );
	
private:
	QString m_file;
	
};

#endif // FILEGENERATOR_H
