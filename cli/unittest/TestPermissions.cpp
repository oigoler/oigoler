/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestPermissions.h"
#include "../Permissions.h"


void TestPermissions::testFilePermissionsToString()
{
	QFile::Permissions filePermissions;
	Permissions permissions;
	QCOMPARE( permissions.toString(), QString( "000" ) );
	
	filePermissions = QFile::ReadOwner;
	permissions.setPermissions( filePermissions );
	QCOMPARE( permissions.toString(), QString( "400" ) );
	
	filePermissions |= QFile::WriteOwner;
	permissions.setPermissions( filePermissions );
	QCOMPARE( permissions.toString(), QString( "600" ) );
	
	filePermissions |= QFile::ExeOwner;
	permissions.setPermissions( filePermissions );
	QCOMPARE( permissions.toString(), QString( "700" ) );
	
	filePermissions |= QFile::ReadGroup;
	permissions.setPermissions( filePermissions );
	QCOMPARE( permissions.toString(), QString( "740" ) );
	
	filePermissions |= QFile::WriteGroup;
	permissions.setPermissions( filePermissions );
	QCOMPARE( permissions.toString(), QString( "760" ) );
	
	filePermissions |= QFile::ExeGroup;
	permissions.setPermissions( filePermissions );
	QCOMPARE( permissions.toString(), QString( "770" ) );
	
	filePermissions |= QFile::ReadOther;
	permissions.setPermissions( filePermissions );
	QCOMPARE( permissions.toString(), QString( "774" ) );
	
	filePermissions |= QFile::WriteOther;
	permissions.setPermissions( filePermissions );
	QCOMPARE( permissions.toString(), QString( "776" ) );
	
	filePermissions |= QFile::ExeOther;
	permissions.setPermissions( filePermissions );
	QCOMPARE( permissions.toString(), QString( "777" ) );
}

void TestPermissions::testFilePermissionsConstructor()
{
	Permissions perm1( QFile::ReadOwner
	                 | QFile::WriteOwner
	                 | QFile::ReadGroup
	                 | QFile::ReadOther );
	
	QCOMPARE( perm1.toString(), QString( "644" ) );
	
	Permissions perm2( QFile::ReadOwner
	                 | QFile::WriteOwner
	                 | QFile::ReadGroup
	                 | QFile::ReadOther
	                 | QFile::ExeOwner
	                 | QFile::ExeGroup
	                 | QFile::ExeOther );
	
	QCOMPARE( perm2.toString(), QString( "755" ) );
}

void TestPermissions::testIntSetter()
{
	Permissions perm;
	perm.setPermissions( 644 );

	QCOMPARE( perm.toString(), QString( "644" ) );
	
	perm.setPermissions( 755 );

	QCOMPARE( perm.toString(), QString( "755" ) );
}

void TestPermissions::testIntConstructor()
{
	Permissions perm1( 644 );
	QCOMPARE( perm1.toString(), QString( "644" ) );
	
	Permissions perm2( 755 );
	QCOMPARE( perm2.toString(), QString( "755" ) );
}

void TestPermissions::testCopyConstructor()
{
	Permissions perm1( 644 );
	Permissions perm2( perm1 );
	QCOMPARE( perm2.toString(), QString( "644" ) );
}

void TestPermissions::testAssignmentOperator()
{
	Permissions perm1( 644 );
	Permissions perm2;
	
	perm2 = perm1;
	
	QCOMPARE( perm2.toString(), QString( "644" ) );
}

QTEST_MAIN(TestPermissions)
