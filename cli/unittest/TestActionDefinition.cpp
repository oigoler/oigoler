/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestActionDefinition.h"
#include "DirectoryCleaner.h"
#include "../ActionDefinition.h"
#include "../Definition.h"
#include "../DefinitionList.h"


void TestActionDefinition::hasDefaultBackupEnvironment()
{
	ActionDefinition action;
	Environment expected;
	
	QVERIFY( action.backupEnvironment() == expected );
}

void TestActionDefinition::hasBackupEnvironment()
{
	Environment env;
	env.setConfigDirectory( "test.dir" );
	
	ActionDefinition action;
	
	QVERIFY( action.backupEnvironment() != env );
	
	action.setBackupEnvironment( env );
	
	QVERIFY( action.backupEnvironment() == env );
}

void TestActionDefinition::testCreate()
{
	ActionDefinition action;
	Environment env;
	QStringList args;
	QString output;
	DirectoryCleaner cleaner( "test.dir" );
	
	args << "create" << "test" << "/tmp" << ".";
	
	env.setConfigDirectory( "test.dir" );
	action.setBackupEnvironment( env );
	action.setOutputString( &output );
	action.execute( args );
	
	QCOMPARE( output, QString( "Created definition 'test'\n" ) );
	QVERIFY( env.definitionExists( "test" ) );
	
	Definition expected;
	expected.setName( "test" );
	expected.setBackupStorage( DefinitionStorage( "/tmp" ) );
	expected.addPath( DefinitionPath( QDir::currentPath() ) );
	
	QVERIFY( env.definition( "test" ) == expected );
}

void TestActionDefinition::testAdd()
{
	Definition definition;
	definition.setName( "test" );
	definition.setBackupStorage( DefinitionStorage( "/tmp" ) );
	definition.addPath( DefinitionPath( QDir::currentPath() ) );
	
	DirectoryCleaner cleaner( "test.dir" );

	Environment env;
	env.setConfigDirectory( "test.dir" );
	env.storeDefinition( definition );
	
	QStringList args;
	args << "add" << "test" << QDir::homePath();
	
	QString output;

	ActionDefinition action;
	action.setBackupEnvironment( env );
	action.setOutputString( &output );
	action.execute( args );
	
	QCOMPARE( output, QString( "Definition 'test' updated\n" ) );
	
	definition.addPath( QDir::homePath() );
	
	QVERIFY( env.definition( "test" ) == definition );
}

void TestActionDefinition::testDel()
{
	Definition definition;
	definition.setName( "test" );
	definition.setBackupStorage( DefinitionStorage( "/tmp" ) );
	definition.addPath( DefinitionPath( QDir::currentPath() ) );
	definition.addPath( QDir::homePath() );
	
	DirectoryCleaner cleaner( "test.dir" );

	Environment env;
	env.setConfigDirectory( "test.dir" );
	env.storeDefinition( definition );
	
	QStringList args;
	args << "del" << "test" << QDir::homePath();
	
	QString output;

	ActionDefinition action;
	action.setBackupEnvironment( env );
	action.setOutputString( &output );
	action.execute( args );
	
	QCOMPARE( output, QString( "Definition 'test' updated\n" ) );
	
	definition.removePath( QDir::homePath() );
	
	QVERIFY( env.definition( "test" ) == definition );
}

void TestActionDefinition::testStorage()
{
	Definition definition;
	definition.setName( "test" );
	definition.setBackupStorage( DefinitionStorage( "/tmp" ) );
	definition.addPath( DefinitionPath( QDir::currentPath() ) );
	
	DirectoryCleaner cleaner( "test.dir" );

	Environment env;
	env.setConfigDirectory( "test.dir" );
	env.storeDefinition( definition );
	
	QStringList args;
	args << "storage" << "test" << "/mnt/backup";
	
	QString output;

	ActionDefinition action;
	action.setBackupEnvironment( env );
	action.setOutputString( &output );
	action.execute( args );
	
	QCOMPARE( output, QString( "Definition 'test' updated\n" ) );
	
	definition.setBackupStorage( DefinitionStorage( "/mnt/backup" ) );
	
	QVERIFY( env.definition( "test" ) == definition );
}

void TestActionDefinition::testShow()
{
	Definition definition;
	definition.setName( "test" );
	definition.setBackupStorage( DefinitionStorage( "/tmp" ) );
	definition.addPath( DefinitionPath( QDir::currentPath() ) );
	
	DirectoryCleaner cleaner( "test.dir" );

	Environment env;
	env.setConfigDirectory( "test.dir" );
	env.storeDefinition( definition );
	
	QStringList args;
	args << "show" << "test";
	
	QString output;

	ActionDefinition action;
	action.setBackupEnvironment( env );
	action.setOutputString( &output );
	action.execute( args );
	
	QCOMPARE( output, definition.toStringList().join( "\n" ) + '\n' );
}

void TestActionDefinition::testRemove()
{
	Definition definition;
	definition.setName( "test" );
	definition.setBackupStorage( DefinitionStorage( "/tmp" ) );
	definition.addPath( DefinitionPath( QDir::currentPath() ) );
	
	DirectoryCleaner cleaner( "test.dir" );

	Environment env;
	env.setConfigDirectory( "test.dir" );
	env.storeDefinition( definition );
	
	QStringList args;
	args << "remove" << "test";
	
	QString output;

	ActionDefinition action;
	action.setBackupEnvironment( env );
	action.setOutputString( &output );
	action.execute( args );
	
	QVERIFY( !env.definitionExists( "test" ) );
	QCOMPARE( output, QString( "Definition 'test' removed\n" ) );
}

void TestActionDefinition::testList()
{
	Definition definition;
	definition.setName( "test" );
	definition.setBackupStorage( DefinitionStorage( "/tmp" ) );
	definition.addPath( DefinitionPath( QDir::currentPath() ) );
	
	DirectoryCleaner cleaner( "test.dir" );

	Environment env;
	env.setConfigDirectory( "test.dir" );
	env.storeDefinition( definition );
	
	QStringList args;
	args.append( "list" );
	
	QString output;

	ActionDefinition action;
	action.setBackupEnvironment( env );
	action.setOutputString( &output );
	action.execute( args );
	
	QCOMPARE( output, env.definitions().names().join( "\n" ) + '\n' );
}

QTEST_MAIN(TestActionDefinition)
