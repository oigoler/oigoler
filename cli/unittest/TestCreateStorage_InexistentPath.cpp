/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestCreateStorage_InexistentPath.h"
#include "DirectoryCleaner.h"
#include "../BackupSession.h"


void TestCreateStorage_InexistentPath::initTestCase()
{
	BackupSession session;
	session.setConfigDirectory( "config" );
	new DirectoryCleaner( "config", this );
	
	m_storage = session.createStorage( "storage" );
}

void TestCreateStorage_InexistentPath::testStorageKey()
{
	QVERIFY( m_storage.key().isEmpty() );
}

void TestCreateStorage_InexistentPath::testStorageStringRepresentation()
{
	QString expected( "Error: The storage path doesn't exist!" );
	
	QString actual;
	QTextStream stream( &actual );
	
	stream << m_storage;
	
	QCOMPARE( actual, expected );
}

void TestCreateStorage_InexistentPath::testCreatedFile()
{
	QFile file( "config/storage/" + m_storage.key() );
	QVERIFY( !file.exists() );
}

void TestCreateStorage_InexistentPath::testStorageValidity()
{
	QVERIFY( !m_storage.isValid() );
}

void TestCreateStorage_InexistentPath::testStorageError()
{
	QCOMPARE( m_storage.error(), BackupStorage::InexistentPath );
}

QTEST_MAIN(TestCreateStorage_InexistentPath)
