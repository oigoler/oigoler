/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "FileGenerator.h"

#include <QFile>


FileGenerator::FileGenerator( const QString& file, QObject* parent )
	: QObject( parent )
	, m_file( file )
{
}

FileGenerator::FileGenerator( const QString& file, const QByteArray& data, QObject* parent )
	: QObject( parent )
	, m_file( file )
{
	this->make( data );
}

FileGenerator::~FileGenerator()
{
	QFile::remove( m_file );
}

void FileGenerator::make( const QByteArray& data )
{
	QFile file( m_file );
	file.open( QFile::WriteOnly );
	file.write( data );
}
