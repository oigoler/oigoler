/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestActionDefinitionAdd.h"
#include "DirectoryCleaner.h"
#include "../ActionDefinitionAdd.h"
#include "../Definition.h"


void TestActionDefinitionAdd::hasDefaultBackupEnvironment()
{
	ActionDefinitionAdd action;
	Environment expected;
	
	QVERIFY( action.backupEnvironment() == expected );
}

void TestActionDefinitionAdd::hasBackupEnvironment()
{
	Environment env;
	env.setConfigDirectory( "test.dir" );
	
	ActionDefinitionAdd action;
	
	QVERIFY( action.backupEnvironment() != env );
	
	action.setBackupEnvironment( env );
	
	QVERIFY( action.backupEnvironment() == env );
}

void TestActionDefinitionAdd::testAddPath()
{
	Definition definition;
	definition.setName( "test" );
	definition.setBackupStorage( DefinitionStorage( "/tmp" ) );
	definition.addPath( DefinitionPath( QDir::currentPath() ) );
	
	DirectoryCleaner cleaner( "test.dir" );

	Environment env;
	env.setConfigDirectory( "test.dir" );
	env.storeDefinition( definition );
	
	QStringList args;
	args << "test" << QDir::homePath();
	
	QString output;

	ActionDefinitionAdd action;
	action.setBackupEnvironment( env );
	action.setOutputString( &output );
	action.execute( args );
	
	QCOMPARE( output, QString( "Definition 'test' updated\n" ) );
	
	definition.addPath( QDir::homePath() );
	
	QVERIFY( env.definition( "test" ) == definition );
}

QTEST_MAIN(TestActionDefinitionAdd)
