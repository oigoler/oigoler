/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestDefinitionEntityList.h"
#include "../DefinitionEntityList.h"
#include "../DefinitionPath.h"


void TestDefinitionEntityList::isList()
{
	DefinitionEntityList list;
	QVERIFY( list.isEmpty() );
	
	DefinitionPath path;
	list.append( path );
	QCOMPARE( list.count(), 1 );
	
	list << path << path;
	QCOMPARE( list.count(), 3 );
}

void TestDefinitionEntityList::testToXml()
{
	QString xml;
	QString expected;
	
	QXmlStreamWriter writer1( &xml );
	QXmlStreamWriter writer2( &expected );
	
	DefinitionPath path1( "." );
	DefinitionPath path2( ".." );
	
	DefinitionEntityList list;
	list << path1 << path2;
	
	writer1.writeStartDocument();
	list.toXml( &writer1 );
	writer1.writeEndDocument();
	
	writer2.writeStartDocument();
	writer2.writeStartElement( "BackupObjectList" );
	path1.toXml( &writer2 );
	path2.toXml( &writer2 );
	writer2.writeEndElement(); // BackupObjectList
	writer2.writeEndDocument();
	
	QCOMPARE( xml, expected );
}

void TestDefinitionEntityList::testFromXml()
{
	DefinitionEntityList in;
	in.append( DefinitionPath( "." ) );
	in.append( DefinitionPath( ".." ) );
	
	QString xml;
	QXmlStreamWriter writer( &xml );
	writer.writeStartDocument();
	in.toXml( &writer );
	writer.writeEndDocument();
	
	DefinitionEntityList out;
	QXmlStreamReader reader( xml );
	reader.readNextStartElement();
	out.fromXml( &reader );
	QCOMPARE( reader.readNext(), QXmlStreamReader::EndDocument );
	
	QCOMPARE( out.count(), 2 );
	QCOMPARE( out.at(0).toString(), in.at(0).toString() );
	QCOMPARE( out.at(1).toString(), in.at(1).toString() );
}

void TestDefinitionEntityList::testToStringList()
{
	DefinitionEntityList list;
	list.append( DefinitionPath( "." ) );
	list.append( DefinitionPath( ".." ) );
	
	QStringList expected;
	expected << list.at(0).toString()<< list.at(1).toString();
	
	QCOMPARE( list.toStringList(), expected );
}

QTEST_MAIN(TestDefinitionEntityList)
