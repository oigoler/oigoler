/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestBackupFile.h"
#include "FileGenerator.h"
#include "DirectoryCleaner.h"
#include "FileCleaner.h"
#include "../BackupFileAction.h"


void TestBackupFile::testBackupFile()
{
	FileGenerator generator( "testfile" );
	generator.make( "This is a test file" );
	
	QDir workdir( QDir::current() );
	workdir.mkdir( "storage" );
	DirectoryCleaner cleaner1( "storage" );
	
	workdir.mkpath( "config/storage" );
	DirectoryCleaner cleaner2( "config" );
	
	QCryptographicHash hasher( QCryptographicHash::Sha1 );
	hasher.addData( workdir.absoluteFilePath( "storage" ).toUtf8() );
	QByteArray storage( hasher.result().toHex() );
	QFile storageFile( "config/storage/" + storage );
	storageFile.open( QFile::WriteOnly );
	storageFile.write( workdir.absoluteFilePath( "storage" ).toUtf8() + '\n' );
	storageFile.close();
	
	BackupFileAction action;
	action.setConfigDirectory( "config" );
	QString response( action.execute( "testfile", storage ) );
	
	QFileInfo info( "testfile" );
	QByteArray permissions( 10, '-' );
	if ( info.permission( QFile::ReadUser   ) ) permissions[1] = 'r';
	if ( info.permission( QFile::WriteUser  ) ) permissions[2] = 'w';
	if ( info.permission( QFile::ExeUser    ) ) permissions[3] = 'x';
	if ( info.permission( QFile::ReadGroup  ) ) permissions[4] = 'r';
	if ( info.permission( QFile::WriteGroup ) ) permissions[5] = 'w';
	if ( info.permission( QFile::ExeGroup   ) ) permissions[6] = 'x';
	if ( info.permission( QFile::ReadOther  ) ) permissions[7] = 'r';
	if ( info.permission( QFile::WriteOther ) ) permissions[8] = 'w';
	if ( info.permission( QFile::ExeOther   ) ) permissions[9] = 'x';
	
	hasher.reset();
	hasher.addData( "file" );
	hasher.addData( workdir.absoluteFilePath( "testfile" ).toUtf8() );
	hasher.addData( permissions );
	hasher.addData( info.lastModified().toString( Qt::ISODate ).toUtf8() );
	QByteArray fileHash( hasher.result().toHex() );
	
	hasher.reset();
	hasher.addData( "This is a test file" );
	QByteArray dataHash( hasher.result().toHex() );
	
	QString expectedResponse( fileHash + ' '
	                        + permissions + ' '
	                        + workdir.absoluteFilePath( "testfile" ) + '\n' );
	
	QCOMPARE( response, expectedResponse );
	
	QFile fileInfo( "storage/" + fileHash + ".xml" );
	QVERIFY( fileInfo.exists() );
	
	QFile expectedFile( "expected.xml" );
	FileCleaner cleaner3( "expected.xml" );
	expectedFile.open( QFile::ReadWrite );
	QXmlStreamWriter writer( &expectedFile );
	writer.setAutoFormatting( true );
	writer.writeStartDocument();
	writer.writeStartElement( "file" );
	writer.writeTextElement( "path", workdir.absoluteFilePath( "testfile" ) );
	writer.writeTextElement( "permissions", permissions );
	writer.writeTextElement( "contents", dataHash );
	writer.writeEndDocument();
	
	fileInfo.open( QFile::ReadOnly );
	expectedFile.flush();
	expectedFile.seek( 0 );
	
	QCOMPARE( QString( fileInfo.readAll() ), QString( expectedFile.readAll() ) );
	
	QFile dataFile( "storage/" + dataHash );
	QVERIFY( dataFile.exists() );
	
	dataFile.open( QFile::ReadOnly );
	
	QCOMPARE( QString( dataFile.readAll() ), QString( "This is a test file" ) );
}

QTEST_MAIN(TestBackupFile)
