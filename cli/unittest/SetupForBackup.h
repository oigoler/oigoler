/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef SETUPFORBACKUP_H
#define SETUPFORBACKUP_H

#include "DirectoryCleaner.h"
#include "LinkCleaner.h"
#include "FileGenerator.h"
#include "../Environment.h"


class SetupForBackup
{

public:
	SetupForBackup();
	virtual ~SetupForBackup();
	
	void backup();
	
	QString snapshotDirectory() const;
	const Environment& env() const;
	void setEnv( const Environment& env );
	QString snapshotInConfigPath() const;
	QString snapshotInStoragePath() const;
	QString fileInStoragePath() const;
	QString expectedSnapshotXml() const;
	QByteArray expectedFileContents() const;
	QString name() const;
	
private:
	DirectoryCleaner m_cleaner1;
	DirectoryCleaner m_cleaner2;
	DirectoryCleaner m_cleaner3;
	LinkCleaner m_cleaner4;
	FileGenerator m_generator;
	Environment m_env;
	
};

#endif // SETUPFORBACKUP_H
