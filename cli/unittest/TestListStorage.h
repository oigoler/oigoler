/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef TESTLISTSTORAGE_H
#define TESTLISTSTORAGE_H

#include <QtTest/QtTest>

#include "../BackupStorage.h"
#include "../BackupStorageList.h"


class TestListStorage : public QObject
{
	Q_OBJECT

private slots:
	void initTestCase();
	void testListSize();
	void testListElements();
	void testListStringRepresentation();
	void testListValidity();
	void testListError();
	void testStorageAvailability();
	void testListStringRepresentationWithAvailability();
	
private:
	BackupStorage m_storage1;
	BackupStorage m_storage2;
	BackupStorage m_storage3;
	BackupStorageList m_list;
	
};

#endif // TESTLISTSTORAGE_H
