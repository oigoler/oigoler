/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestActionMain.h"
#include "DirectoryCleaner.h"
#include "SetupForBackup.h"
#include "../ActionMain.h"
#include "../Definition.h"
#include "../DefinitionList.h"


void TestActionMain::hasDefaultBackupEnvironment()
{
	ActionMain action;
	Environment expected;
	
	QVERIFY( action.backupEnvironment() == expected );
}

void TestActionMain::hasBackupEnvironment()
{
	Environment env;
	env.setConfigDirectory( "test.dir" );
	
	ActionMain action;
	
	QVERIFY( action.backupEnvironment() != env );
	
	action.setBackupEnvironment( env );
	
	QVERIFY( action.backupEnvironment() == env );
}

void TestActionMain::testDefinitionCreate()
{
	ActionMain action;
	Environment env;
	QStringList args;
	QString output;
	DirectoryCleaner cleaner( "test.dir" );
	
	args << "definition" << "create" << "test" << "/tmp" << ".";
	
	env.setConfigDirectory( "test.dir" );
	action.setBackupEnvironment( env );
	action.setOutputString( &output );
	action.execute( args );
	
	QCOMPARE( output, QString( "Created definition 'test'\n" ) );
	QVERIFY( env.definitionExists( "test" ) );
	
	Definition expected;
	expected.setName( "test" );
	expected.setBackupStorage( DefinitionStorage( "/tmp" ) );
	expected.addPath( DefinitionPath( QDir::currentPath() ) );
	
	QVERIFY( env.definition( "test" ) == expected );
}

void TestActionMain::testDefinitionAdd()
{
	Definition definition;
	definition.setName( "test" );
	definition.setBackupStorage( DefinitionStorage( "/tmp" ) );
	definition.addPath( DefinitionPath( QDir::currentPath() ) );
	
	DirectoryCleaner cleaner( "test.dir" );

	Environment env;
	env.setConfigDirectory( "test.dir" );
	env.storeDefinition( definition );
	
	QStringList args;
	args << "definition" << "add" << "test" << QDir::homePath();
	
	QString output;

	ActionMain action;
	action.setBackupEnvironment( env );
	action.setOutputString( &output );
	action.execute( args );
	
	QCOMPARE( output, QString( "Definition 'test' updated\n" ) );
	
	definition.addPath( QDir::homePath() );
	
	QVERIFY( env.definition( "test" ) == definition );
}

void TestActionMain::testDefinitionDel()
{
	Definition definition;
	definition.setName( "test" );
	definition.setBackupStorage( DefinitionStorage( "/tmp" ) );
	definition.addPath( DefinitionPath( QDir::currentPath() ) );
	definition.addPath( QDir::homePath() );
	
	DirectoryCleaner cleaner( "test.dir" );

	Environment env;
	env.setConfigDirectory( "test.dir" );
	env.storeDefinition( definition );
	
	QStringList args;
	args << "definition" << "del" << "test" << QDir::homePath();
	
	QString output;

	ActionMain action;
	action.setBackupEnvironment( env );
	action.setOutputString( &output );
	action.execute( args );
	
	QCOMPARE( output, QString( "Definition 'test' updated\n" ) );
	
	definition.removePath( QDir::homePath() );
	
	QVERIFY( env.definition( "test" ) == definition );
}

void TestActionMain::testDefinitionStorage()
{
	Definition definition;
	definition.setName( "test" );
	definition.setBackupStorage( DefinitionStorage( "/tmp" ) );
	definition.addPath( DefinitionPath( QDir::currentPath() ) );
	
	DirectoryCleaner cleaner( "test.dir" );

	Environment env;
	env.setConfigDirectory( "test.dir" );
	env.storeDefinition( definition );
	
	QStringList args;
	args << "definition" << "storage" << "test" << "/mnt/backup";
	
	QString output;

	ActionMain action;
	action.setBackupEnvironment( env );
	action.setOutputString( &output );
	action.execute( args );
	
	QCOMPARE( output, QString( "Definition 'test' updated\n" ) );
	
	definition.setBackupStorage( DefinitionStorage( "/mnt/backup" ) );
	
	QVERIFY( env.definition( "test" ) == definition );
}

void TestActionMain::testDefinitionShow()
{
	Definition definition;
	definition.setName( "test" );
	definition.setBackupStorage( DefinitionStorage( "/tmp" ) );
	definition.addPath( DefinitionPath( QDir::currentPath() ) );
	
	DirectoryCleaner cleaner( "test.dir" );

	Environment env;
	env.setConfigDirectory( "test.dir" );
	env.storeDefinition( definition );
	
	QStringList args;
	args << "definition" << "show" << "test";
	
	QString output;

	ActionMain action;
	action.setBackupEnvironment( env );
	action.setOutputString( &output );
	action.execute( args );
	
	QCOMPARE( output, definition.toStringList().join( "\n" ) + '\n' );
}

void TestActionMain::testDefinitionRemove()
{
	Definition definition;
	definition.setName( "test" );
	definition.setBackupStorage( DefinitionStorage( "/tmp" ) );
	definition.addPath( DefinitionPath( QDir::currentPath() ) );
	
	DirectoryCleaner cleaner( "test.dir" );

	Environment env;
	env.setConfigDirectory( "test.dir" );
	env.storeDefinition( definition );
	
	QStringList args;
	args << "definition" << "remove" << "test";
	
	QString output;

	ActionMain action;
	action.setBackupEnvironment( env );
	action.setOutputString( &output );
	action.execute( args );
	
	QVERIFY( !env.definitionExists( "test" ) );
	QCOMPARE( output, QString( "Definition 'test' removed\n" ) );
}

void TestActionMain::testDefinitionList()
{
	Definition definition;
	definition.setName( "test" );
	definition.setBackupStorage( DefinitionStorage( "/tmp" ) );
	definition.addPath( DefinitionPath( QDir::currentPath() ) );
	
	DirectoryCleaner cleaner( "test.dir" );

	Environment env;
	env.setConfigDirectory( "test.dir" );
	env.storeDefinition( definition );
	
	QStringList args;
	args << "definition" << "list";
	
	QString output;

	ActionMain action;
	action.setBackupEnvironment( env );
	action.setOutputString( &output );
	action.execute( args );
	
	QCOMPARE( output, env.definitions().names().join( "\n" ) + '\n' );
}

void TestActionMain::testBackup()
{
	SetupForBackup setup;
	ActionMain action;
	QStringList args;
	QString output;
	
	args << "backup" << setup.name();
	
	action.setBackupEnvironment( setup.env() );
	action.setOutputString( &output );
	action.execute( args );
	setup.setEnv( action.backupEnvironment() );
	
	QCOMPARE( output, "Successfully backup up '" + setup.name() + "'\n" );
	
	QFile configSnapshot( setup.snapshotInConfigPath() );
	QVERIFY( configSnapshot.exists() );
	
	QFile storageSnapshot( setup.snapshotInStoragePath() );
	QVERIFY( storageSnapshot.exists() );
	
	QFile file( setup.fileInStoragePath() );
	QVERIFY( file.exists() );
	
	configSnapshot.open( QFile::ReadOnly );
	storageSnapshot.open( QFile::ReadOnly );
	file.open( QFile::ReadOnly );
	
	QCOMPARE( QString( configSnapshot.readAll() ), setup.expectedSnapshotXml() );
	QCOMPARE( QString( storageSnapshot.readAll() ), setup.expectedSnapshotXml() );
	QCOMPARE( file.readAll(), setup.expectedFileContents() );
}

QTEST_MAIN(TestActionMain)
