/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef TESTCREATERECIPE_H
#define TESTCREATERECIPE_H

#include <QtTest/QtTest>

class BackupRecipe;


class TestCreateRecipe : public QObject
{
	Q_OBJECT

private slots:
	void initTestCase();
	
	void testValidity();
	void testError();
	void testFileName();
	
	void testToString_empty();
	void testFileContents_empty();
	
	void testToString_path();
	void testFileContents_path();
	
	void testToString_storage();
	void testFileContents_storage();
	
	void testToString_path_storage();
	void testFileContents_path_storage();
	
	void cleanupTestCase();
	
private:
	QXmlStreamReader::TokenType readNextToken( QXmlStreamReader* reader ) const;
	
private:
	QByteArray m_storage1;
	QByteArray m_storage2;
	
	BackupRecipe* m_recipe_empty;
	BackupRecipe* m_recipe_path;
	BackupRecipe* m_recipe_storage;
	BackupRecipe* m_recipe_path_storage;
	
};

#endif // TESTCREATERECIPE_H
