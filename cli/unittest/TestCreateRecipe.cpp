/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestCreateRecipe.h"
#include "DirectoryCleaner.h"
#include "../BackupSession.h"
#include "../BackupRecipe.h"
#include "../BackupStorage.h"

#include <QDir>


void TestCreateRecipe::initTestCase()
{
	BackupSession session;
	session.setConfigDirectory( "config" );
	new DirectoryCleaner( "config", this );
	
	QDir::current().mkdir( "storage1" );
	QDir::current().mkdir( "storage2" );
	new DirectoryCleaner( "storage1", this );
	new DirectoryCleaner( "storage2", this );
	m_storage1 = session.createStorage( "storage1" ).key();
	m_storage2 = session.createStorage( "storage2" ).key();
	
	m_recipe_empty = new BackupRecipe( "recipe_empty" );
	session.createRecipe( m_recipe_empty );
	
	m_recipe_path = new BackupRecipe( "recipe_path" );
	m_recipe_path->addPath( "." );
	m_recipe_path->addPath( ".." );
	session.createRecipe( m_recipe_path );
	
	m_recipe_storage = new BackupRecipe( "recipe_storage" );
	m_recipe_storage->addStorage( m_storage1 );
	m_recipe_storage->addStorage( m_storage2 );
	session.createRecipe( m_recipe_storage );
	
	m_recipe_path_storage = new BackupRecipe( "recipe_path_storage" );
	m_recipe_path_storage->addStorage( m_storage1 );
	m_recipe_path_storage->addStorage( m_storage2 );
	m_recipe_path_storage->addPath( "." );
	m_recipe_path_storage->addPath( ".." );
	session.createRecipe( m_recipe_path_storage );
}

void TestCreateRecipe::testValidity()
{
	QVERIFY( m_recipe_empty->isValid() );
	QVERIFY( m_recipe_path->isValid() );
	QVERIFY( m_recipe_storage->isValid() );
	QVERIFY( m_recipe_path_storage->isValid() );
}

void TestCreateRecipe::testError()
{
	QCOMPARE( m_recipe_empty->error(), BackupRecipe::NoError );
	QCOMPARE( m_recipe_path->error(), BackupRecipe::NoError );
	QCOMPARE( m_recipe_storage->error(), BackupRecipe::NoError );
	QCOMPARE( m_recipe_path_storage->error(), BackupRecipe::NoError );
}

void TestCreateRecipe::testFileName()
{
	QDir config( "config/recipe" );
	QVERIFY( config.exists() );
	QVERIFY( config.exists( "recipe_empty.xml" ) );
	QVERIFY( config.exists( "recipe_path.xml" ) );
	QVERIFY( config.exists( "recipe_storage.xml" ) );
	QVERIFY( config.exists( "recipe_path_storage.xml" ) );
}

void TestCreateRecipe::testToString_empty()
{
	QString expected( "recipe recipe_empty" );
	
	QString actual;
	QTextStream stream( &actual );
	
	stream << *m_recipe_empty;
	
	QCOMPARE( actual, expected );
}

void TestCreateRecipe::testFileContents_empty()
{
	QDir config( "config/recipe" );
	QVERIFY( config.exists() );
	QVERIFY( config.exists( "recipe_empty.xml" ) );
	
	QFile file( config.filePath( "recipe_empty.xml" ) );
	file.open( QFile::ReadOnly );
	
	QXmlStreamReader reader( &file );
	
	QXmlStreamReader::TokenType token( reader.readNext() );
	QCOMPARE( token, QXmlStreamReader::StartDocument );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "recipe" ) );
	QXmlStreamAttributes attributes( reader.attributes() );
	QCOMPARE( attributes.size(), 1 );
	QVERIFY( attributes.hasAttribute( "name" ) );
	QCOMPARE( attributes.value( "name" ).toString(), QString( "recipe_empty" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "recipe" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndDocument );
}

void TestCreateRecipe::testToString_path()
{
	QDir dir( QDir::current() );
	dir.cdUp();
	
	QString path1( dir.absolutePath() );
	QString path2( QDir::currentPath() );
	
	QString expected( "recipe recipe_path\n" );
	expected += "path " + path1 + "\n";
	expected += "path " + path2;
	
	QString actual;
	QTextStream stream( &actual );
	
	stream << *m_recipe_path;
	
	QCOMPARE( actual, expected );
}

void TestCreateRecipe::testFileContents_path()
{
	QDir config( "config/recipe" );
	QVERIFY( config.exists() );
	QVERIFY( config.exists( "recipe_path.xml" ) );
	
	QFile file( config.filePath( "recipe_path.xml" ) );
	file.open( QFile::ReadOnly );
	
	QXmlStreamReader reader( &file );
	
	QXmlStreamReader::TokenType token( reader.readNext() );
	QCOMPARE( token, QXmlStreamReader::StartDocument );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "recipe" ) );
	QXmlStreamAttributes attributes( reader.attributes() );
	QCOMPARE( attributes.size(), 1 );
	QVERIFY( attributes.hasAttribute( "name" ) );
	QCOMPARE( attributes.value( "name" ).toString(), QString( "recipe_path" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "path" ) );
	
	QDir dir( QDir::current() );
	dir.cdUp();
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::Characters );
	QCOMPARE( reader.text().toString(), dir.absolutePath() );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "path" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "path" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::Characters );
	QCOMPARE( reader.text().toString(), QDir::currentPath() );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "path" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "recipe" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndDocument );
}

void TestCreateRecipe::testToString_storage()
{
	QString expected( "recipe recipe_storage\n" );
	expected += "storage " + m_storage1 + "\n";
	expected += "storage " + m_storage2;
	
	QString actual;
	QTextStream stream( &actual );
	
	stream << *m_recipe_storage;
	
	QCOMPARE( actual, expected );
}

void TestCreateRecipe::testFileContents_storage()
{
	QDir config( "config/recipe" );
	QVERIFY( config.exists() );
	QVERIFY( config.exists( "recipe_storage.xml" ) );
	
	QFile file( config.filePath( "recipe_storage.xml" ) );
	file.open( QFile::ReadOnly );
	
	QXmlStreamReader reader( &file );
	
	QXmlStreamReader::TokenType token( reader.readNext() );
	QCOMPARE( token, QXmlStreamReader::StartDocument );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "recipe" ) );
	QXmlStreamAttributes attributes( reader.attributes() );
	QCOMPARE( attributes.size(), 1 );
	QVERIFY( attributes.hasAttribute( "name" ) );
	QCOMPARE( attributes.value( "name" ).toString(), QString( "recipe_storage" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "storage" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::Characters );
	QCOMPARE( reader.text().toUtf8(), m_storage1 );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "storage" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "storage" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::Characters );
	QCOMPARE( reader.text().toUtf8(), m_storage2 );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "storage" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "recipe" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndDocument );
}

void TestCreateRecipe::testToString_path_storage()
{
	QDir dir( QDir::current() );
	dir.cdUp();
	
	QString path1( dir.absolutePath() );
	QString path2( QDir::currentPath() );
	
	QString expected( "recipe recipe_path_storage\n" );
	expected += "path " + path1 + "\n";
	expected += "path " + path2 + "\n";
	expected += "storage " + m_storage1 + "\n";
	expected += "storage " + m_storage2;
	
	QString actual;
	QTextStream stream( &actual );
	
	stream << *m_recipe_path_storage;
	
	QCOMPARE( actual, expected );
}

void TestCreateRecipe::testFileContents_path_storage()
{
	QDir config( "config/recipe" );
	QVERIFY( config.exists() );
	QVERIFY( config.exists( "recipe_path_storage.xml" ) );
	
	QFile file( config.filePath( "recipe_path_storage.xml" ) );
	file.open( QFile::ReadOnly );
	
	QXmlStreamReader reader( &file );
	
	QXmlStreamReader::TokenType token( reader.readNext() );
	QCOMPARE( token, QXmlStreamReader::StartDocument );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "recipe" ) );
	QXmlStreamAttributes attributes( reader.attributes() );
	QCOMPARE( attributes.size(), 1 );
	QVERIFY( attributes.hasAttribute( "name" ) );
	QCOMPARE( attributes.value( "name" ).toString(), QString( "recipe_path_storage" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "path" ) );
	
	QDir dir( QDir::current() );
	dir.cdUp();
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::Characters );
	QCOMPARE( reader.text().toString(), dir.absolutePath() );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "path" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "path" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::Characters );
	QCOMPARE( reader.text().toString(), QDir::currentPath() );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "path" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "storage" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::Characters );
	QCOMPARE( reader.text().toUtf8(), m_storage1 );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "storage" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::StartElement );
	QCOMPARE( reader.name().toString(), QString( "storage" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::Characters );
	QCOMPARE( reader.text().toUtf8(), m_storage2 );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "storage" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndElement );
	QCOMPARE( reader.name().toString(), QString( "recipe" ) );
	
	token = readNextToken( &reader );
	QCOMPARE( token, QXmlStreamReader::EndDocument );
}

void TestCreateRecipe::cleanupTestCase()
{
	delete m_recipe_empty;
	delete m_recipe_path;
	delete m_recipe_storage;
	delete m_recipe_path_storage;
}

QXmlStreamReader::TokenType
TestCreateRecipe::readNextToken( QXmlStreamReader* reader ) const
{
	QXmlStreamReader::TokenType token;
	
	while ( !reader->atEnd() )
	{
		token = reader->readNext();
		
		if ( !reader->isWhitespace() )
			return token;
	}
	
	return QXmlStreamReader::Invalid;
}

QTEST_MAIN(TestCreateRecipe)
