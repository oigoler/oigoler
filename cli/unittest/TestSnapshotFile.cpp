/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestSnapshotFile.h"
#include "FileGenerator.h"
#include "DirectoryCleaner.h"
#include "../SnapshotFile.h"
#include "../Permissions.h"
#include "../DefinitionPath.h"


void TestSnapshotFile::hasPath()
{
	SnapshotFile file;
	QVERIFY( file.path().isEmpty() );
	
	file.setPath( ".." );
	QCOMPARE( file.path(), QString( ".." ) );
}

void TestSnapshotFile::hasPermissions()
{
	SnapshotFile file;
	QCOMPARE( file.permissions().toString(), QString( "000" ) );
	
	file.setPermissions( Permissions( 640 ) );
	QCOMPARE( file.permissions().toString(), QString( "640" ) );
}

void TestSnapshotFile::hasHash()
{
	SnapshotFile file;
	QVERIFY( file.fileHash().isEmpty() );
	
	file.setFileHash( QByteArray( "abc" ) );
	QCOMPARE( file.fileHash(), QByteArray( "abc" ) );
}

void TestSnapshotFile::testToXml()
{
	FileGenerator gen( "testfile" );
	gen.make( "aaa" );
	
	QString xml;
	QXmlStreamWriter writer1( &xml );
	writer1.writeStartDocument();
	
	SnapshotFile file;
	file.fromFile( "testfile" );
	file.toXml( &writer1 );
	writer1.writeEndDocument();
	
	QString expected;
	QXmlStreamWriter writer2( &expected );
	writer2.writeStartDocument();
	writer2.writeStartElement( "file" );
	
	QFileInfo info( "testfile" );
	writer2.writeTextElement( "path", info.absoluteFilePath() );
	
	Permissions permissions;
	permissions.setPermissions( info.permissions() );
	writer2.writeTextElement( "permissions", permissions.toString() );
	
	writer2.writeTextElement( "hash", QCryptographicHash::hash( "aaa", QCryptographicHash::Sha1 ).toHex() );
	
	writer2.writeEndElement(); // file
	writer2.writeEndDocument();
	
	QCOMPARE( xml, expected );
}

void TestSnapshotFile::testCopyTo()
{
	FileGenerator gen( "testfile" );
	gen.make( "aaa" );
	
	QDir::current().mkdir( "destdir" );
	DirectoryCleaner cleaner( "destdir" );
	
	SnapshotFile file;
	file.fromFile( "testfile" );
	file.copyTo( "destdir" );
	
	QString fileName( QCryptographicHash::hash( "aaa", QCryptographicHash::Sha1 ).toHex() );
	QFile dest( "destdir/" + fileName );
	
	QVERIFY( dest.exists() );
	
	dest.open( QFile::ReadOnly );
	
	QCOMPARE( dest.readAll(), QByteArray( "aaa" ) );
}

void TestSnapshotFile::testFromFileString()
{
	FileGenerator gen( "testfile" );
	gen.make( "aaa" );
	
	SnapshotFile file;
	file.fromFile( "testfile" );
	
	QFileInfo info( "testfile" );
	Permissions perm( info.permissions() );
	QByteArray hash( QCryptographicHash::hash( "aaa", QCryptographicHash::Sha1 ).toHex() );
	
	QCOMPARE( file.path(), info.absoluteFilePath() );
	QCOMPARE( file.permissions().toString(), perm.toString() );
	QCOMPARE( file.fileHash(), hash );
}

void TestSnapshotFile::testFromFilePath()
{
	FileGenerator gen( "testfile" );
	gen.make( "aaa" );
	DefinitionPath path( "testfile" );
	
	SnapshotFile file;
	file.fromFile( path );
	
	QFileInfo info( "testfile" );
	Permissions perm( info.permissions() );
	QByteArray hash( QCryptographicHash::hash( "aaa", QCryptographicHash::Sha1 ).toHex() );
	
	QCOMPARE( file.path(), info.absoluteFilePath() );
	QCOMPARE( file.permissions().toString(), perm.toString() );
	QCOMPARE( file.fileHash(), hash );
}

void TestSnapshotFile::testCreateFromFileString()
{
	FileGenerator gen( "testfile" );
	gen.make( "aaa" );
	
	SnapshotFile* file( SnapshotFile::createFromFile( "testfile" ) );
	QVERIFY( file );
	file->setParent( this );
	
	QFileInfo info( "testfile" );
	Permissions perm( info.permissions() );
	QByteArray hash( QCryptographicHash::hash( "aaa", QCryptographicHash::Sha1 ).toHex() );
	
	QCOMPARE( file->path(), info.absoluteFilePath() );
	QCOMPARE( file->permissions().toString(), perm.toString() );
	QCOMPARE( file->fileHash(), hash );
}

void TestSnapshotFile::testCreateFromFilePath()
{
	FileGenerator gen( "testfile" );
	gen.make( "aaa" );
	DefinitionPath path( "testfile" );
	
	SnapshotFile* file( SnapshotFile::createFromFile( path ) );
	QVERIFY( file );
	file->setParent( this );
	
	QFileInfo info( "testfile" );
	Permissions perm( info.permissions() );
	QByteArray hash( QCryptographicHash::hash( "aaa", QCryptographicHash::Sha1 ).toHex() );
	
	QCOMPARE( file->path(), info.absoluteFilePath() );
	QCOMPARE( file->permissions().toString(), perm.toString() );
	QCOMPARE( file->fileHash(), hash );
}

void TestSnapshotFile::testId()
{
	FileGenerator gen( "testfile" );
	gen.make( "aaa" );
	
	SnapshotFile file;
	file.fromFile( "testfile" );
	
	QCryptographicHash hasher( QCryptographicHash::Sha1 );
	hasher.addData( "file" );
	hasher.addData( file.path().toUtf8() );
	hasher.addData( file.permissions().toString().toUtf8() );
	hasher.addData( file.fileHash() );
	
	QCOMPARE( file.id(), hasher.result().toHex() );
}

QTEST_MAIN(TestSnapshotFile)
