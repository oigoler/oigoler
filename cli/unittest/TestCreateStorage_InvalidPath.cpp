/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestCreateStorage_InvalidPath.h"
#include "DirectoryCleaner.h"
#include "FileGenerator.h"
#include "../BackupSession.h"


void TestCreateStorage_InvalidPath::initTestCase()
{
	BackupSession session;
	session.setConfigDirectory( "config" );
	new DirectoryCleaner( "config", this );
	
	FileGenerator* generator( new FileGenerator( "storage", this ) );
	generator->make( "This is not a storage!!!" );
	
	m_storage = session.createStorage( "storage" );
}

void TestCreateStorage_InvalidPath::testStorageKey()
{
	QVERIFY( m_storage.key().isEmpty() );
}

void TestCreateStorage_InvalidPath::testStorageStringRepresentation()
{
	QString expected( "Error: Invalid storage path!" );
	
	QString actual;
	QTextStream stream( &actual );
	
	stream << m_storage;
	
	QCOMPARE( actual, expected );
}

void TestCreateStorage_InvalidPath::testCreatedFile()
{
	QFile file( "config/storage/" + m_storage.key() );
	QVERIFY( !file.exists() );
}

void TestCreateStorage_InvalidPath::testStorageValidity()
{
	QVERIFY( !m_storage.isValid() );
}

void TestCreateStorage_InvalidPath::testStorageError()
{
	QCOMPARE( m_storage.error(), BackupStorage::InvalidPath );
}

QTEST_MAIN(TestCreateStorage_InvalidPath)
