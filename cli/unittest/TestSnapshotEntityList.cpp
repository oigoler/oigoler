/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestSnapshotEntityList.h"
#include "DirectoryCleaner.h"
#include "FileGenerator.h"
#include "LinkCleaner.h"
#include "../SnapshotEntityList.h"
#include "../SnapshotDirectory.h"
#include "../SnapshotFile.h"
#include "../SnapshotLink.h"


void TestSnapshotEntityList::isList()
{
	SnapshotEntityList list;
	QVERIFY( list.isEmpty() );
	
	list.append( new SnapshotDirectory );
	QCOMPARE( list.count(), 1 );
	
	list << new SnapshotFile << new SnapshotLink;
	QCOMPARE( list.count(), 3 );
	
	foreach ( SnapshotEntity* entity, list )
		delete entity;
}

void TestSnapshotEntityList::testToXml()
{
	QDir::current().mkdir( "testdir" );
	DirectoryCleaner cleaner1( "testdir" );
	FileGenerator generator( "testfile" );
	generator.make( "abc" );
	QFile::link( "testfile", "testlink" );
	LinkCleaner cleaner2( "testlink" );
	
	SnapshotFile file;
	file.fromFile( "testfile" );
	
	SnapshotDirectory dir;
	dir.fromDirectory( "testdir" );
	
	SnapshotLink link;
	link.fromLink( "testlink" );
	
	SnapshotEntityList list;
	list << &dir << &file << &link;
	
	QString xml;
	QXmlStreamWriter writer1( &xml );
	writer1.writeStartDocument();
	list.toXml( &writer1 );
	writer1.writeEndDocument();
	
	QString expected;
	QXmlStreamWriter writer2( &expected );
	writer2.writeStartDocument();
	dir.toXml( &writer2 );
	file.toXml( &writer2 );
	link.toXml( &writer2 );
	writer2.writeEndDocument();
	
	QCOMPARE( xml, expected );
}

void TestSnapshotEntityList::testCopyConstructor()
{
	SnapshotEntityList list1;
	SnapshotFile* file( new SnapshotFile );
	file->setPath( "test" );
	list1.append( file );
	
	SnapshotEntityList list2( list1 );
	
	QCOMPARE( list2.size(), list1.size() );
	QVERIFY( list1.at(0) == list2.at(0) );
}

void TestSnapshotEntityList::testAssignmentOperator()
{
	SnapshotEntityList list1;
	SnapshotFile* file( new SnapshotFile );
	file->setPath( "test" );
	list1.append( file );
	
	SnapshotEntityList list2;
	list2 = list1;
	
	QCOMPARE( list2.size(), list1.size() );
	QVERIFY( list1.at(0) == list2.at(0) );
}

QTEST_MAIN(TestSnapshotEntityList)
