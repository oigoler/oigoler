/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef TESTDEFINITIONPATH_H
#define TESTDEFINITIONPATH_H

#include <QtTest/QtTest>


class TestDefinitionPath : public QObject
{
	Q_OBJECT

private slots:
	void hasPath();
	void testToXml();
	void testFromXml();
	void testPathInConstructor();
	void testToString();
	void hasEqualsOperator();
	void hasDifferenceOperator();
	void hasAssignmentOperator();
	void hasCopyConstructor();
	
};

#endif // TESTDEFINITIONPATH_H
