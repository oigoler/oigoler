/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestActionDefinitionCreate.h"
#include "DirectoryCleaner.h"
#include "../ActionDefinitionCreate.h"
#include "../Definition.h"


void TestActionDefinitionCreate::hasDefaultBackupEnvironment()
{
	ActionDefinitionCreate action;
	Environment expected;
	
	QVERIFY( action.backupEnvironment() == expected );
}

void TestActionDefinitionCreate::hasBackupEnvironment()
{
	Environment env;
	env.setConfigDirectory( "test.dir" );
	
	ActionDefinitionCreate action;
	
	QVERIFY( action.backupEnvironment() != env );
	
	action.setBackupEnvironment( env );
	
	QVERIFY( action.backupEnvironment() == env );
}

void TestActionDefinitionCreate::testDefinitionCreation()
{
	ActionDefinitionCreate action;
	Environment env;
	QStringList args;
	QString output;
	DirectoryCleaner cleaner( "test.dir" );
	
	args << "test" << "/tmp" << ".";
	
	env.setConfigDirectory( "test.dir" );
	action.setBackupEnvironment( env );
	action.setOutputString( &output );
	action.execute( args );
	
	QCOMPARE( output, QString( "Created definition 'test'\n" ) );
	QVERIFY( env.definitionExists( "test" ) );
	
	Definition expected;
	expected.setName( "test" );
	expected.setBackupStorage( DefinitionStorage( "/tmp" ) );
	expected.addPath( DefinitionPath( QDir::currentPath() ) );
	
	QVERIFY( env.definition( "test" ) == expected );
}

void TestActionDefinitionCreate::testExistingDefinition()
{
	Definition definition;
	definition.setName( "test" );
	definition.setBackupStorage( DefinitionStorage( "/tmp" ) );
	definition.addPath( DefinitionPath( "." ) );
	
	DirectoryCleaner cleaner( "test.dir" );

	Environment env;
	env.setConfigDirectory( "test.dir" );
	env.storeDefinition( definition );
	
	QStringList args;
	args << "test" << "/tmp" << ".";
	
	QString output;

	ActionDefinitionCreate action;
	action.setBackupEnvironment( env );
	action.setOutputString( &output );
	action.execute( args );
	
	QCOMPARE( output, QString( "Definition 'test' already exists!\n" ) );
}

QTEST_MAIN(TestActionDefinitionCreate)
