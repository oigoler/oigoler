/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestSnapshot.h"
#include "DirectoryCleaner.h"
#include "FileGenerator.h"
#include "LinkCleaner.h"
#include "../Snapshot.h"
#include "../SnapshotDirectory.h"
#include "../SnapshotFile.h"
#include "../SnapshotLink.h"


void TestSnapshot::hasSnapshotEntityList()
{
	Snapshot snapshot;
	QVERIFY( snapshot.entityList().isEmpty() );
	
	SnapshotEntity* dir( new SnapshotDirectory );
	snapshot.addEntity( dir );
	QCOMPARE( snapshot.entityList().size(), 1 );
	
	delete dir;
}

void TestSnapshot::hasDate()
{
	QDateTime date;
	date.setDate( QDate( 1996, 10, 14 ) );
	date.setTime( QTime( 23, 29 ) );
	
	Snapshot snapshot;
	QVERIFY( snapshot.date() != date );
	
	snapshot.setDate( date );
	QCOMPARE( snapshot.date(), date );
}

void TestSnapshot::hasBackupName()
{
	Snapshot snapshot;
	QVERIFY( snapshot.backupName().isEmpty() );
	
	snapshot.setBackupName( "test" );
	QCOMPARE( snapshot.backupName(), QString( "test" ) );
}

void TestSnapshot::testEntityOwnership()
{
	QPointer< SnapshotEntity > ptr( new SnapshotDirectory );
	ptr->setParent( this );
	
	Snapshot* snapshot( new Snapshot );
	snapshot->addEntity( ptr );
	
	delete snapshot;
	QVERIFY( ptr.isNull() );
}

void TestSnapshot::testToXml()
{
	QDir::current().mkdir( "testdir" );
	DirectoryCleaner cleaner1( "testdir" );
	FileGenerator generator( "testfile" );
	generator.make( "abc" );
	QFile::link( "testfile", "testlink" );
	LinkCleaner cleaner2( "testlink" );
	
	SnapshotDirectory* dir( SnapshotDirectory::createFromDirectory( "testdir" ) );
	SnapshotFile* file( SnapshotFile::createFromFile( "testfile" ) );
	SnapshotLink* link( SnapshotLink::createFromLink( "testlink" ) );
	
	Snapshot snapshot;
	snapshot.addEntity( dir );
	snapshot.addEntity( file );
	snapshot.addEntity( link );
	
	QString xml;
	QXmlStreamWriter writer1( &xml );
	writer1.writeStartDocument();
	snapshot.toXml( &writer1 );
	writer1.writeEndDocument();
	
	QString expected;
	QXmlStreamWriter writer2( &expected );
	writer2.writeStartDocument();
	writer2.writeStartElement( "BackupSnapshot" );
	dir->toXml( &writer2 );
	file->toXml( &writer2 );
	link->toXml( &writer2 );
	writer2.writeEndDocument();
	
	QCOMPARE( xml, expected );
}

void TestSnapshot::testCopyConstructor()
{
	Snapshot snapshot1;
	snapshot1.addEntity( new SnapshotDirectory );
	snapshot1.setBackupName( "testname" );
	
	QDateTime expectedDate( QDate( 1996, 10, 14 ), QTime( 23, 41 ) );
	snapshot1.setDate( expectedDate );
	
	Snapshot snapshot2( snapshot1 );
	
	QCOMPARE( snapshot2.entityList().size(), 1 );
	QVERIFY( snapshot2.entityList().at(0) );
	QVERIFY( snapshot1.entityList().at(0) == snapshot2.entityList().at(0) );
	QCOMPARE( snapshot2.date(), expectedDate );
	QCOMPARE( snapshot2.backupName(), QString( "testname" ) );
}

void TestSnapshot::testAssignmentOperator()
{
	Snapshot snapshot1;
	snapshot1.addEntity( new SnapshotDirectory );
	snapshot1.setBackupName( "testname" );
	
	QDateTime expectedDate( QDate( 1996, 10, 14 ), QTime( 23, 41 ) );
	snapshot1.setDate( expectedDate );
	
	Snapshot snapshot2;
	snapshot2 = snapshot1;
	
	QCOMPARE( snapshot2.entityList().size(), 1 );
	QVERIFY( snapshot2.entityList().at(0) );
	QVERIFY( snapshot1.entityList().at(0) == snapshot2.entityList().at(0) );
	QCOMPARE( snapshot2.date(), expectedDate );
	QCOMPARE( snapshot2.backupName(), QString( "testname" ) );
}

void TestSnapshot::testId()
{
	QDir::current().mkdir( "testdir" );
	DirectoryCleaner cleaner1( "testdir" );
	FileGenerator generator( "testfile" );
	generator.make( "abc" );
	QFile::link( "testfile", "testlink" );
	LinkCleaner cleaner2( "testlink" );
	
	SnapshotDirectory* dir( SnapshotDirectory::createFromDirectory( "testdir" ) );
	SnapshotFile* file( SnapshotFile::createFromFile( "testfile" ) );
	SnapshotLink* link( SnapshotLink::createFromLink( "testlink" ) );
	
	Snapshot snapshot;
	snapshot.addEntity( dir );
	snapshot.addEntity( file );
	snapshot.addEntity( link );
	
	QCryptographicHash hasher( QCryptographicHash::Sha1 );
	hasher.addData( dir->id() );
	hasher.addData( file->id() );
	hasher.addData( link->id() );
	
	QCOMPARE( snapshot.id(), hasher.result().toHex() );
}

void TestSnapshot::testDateDefault()
{
	QDateTime before( QDateTime::currentDateTime() );
	Snapshot snapshot;
	QDateTime after( QDateTime::currentDateTime() );
	
	QVERIFY( snapshot.date() >= before );
	QVERIFY( snapshot.date() <= after );
}

void TestSnapshot::testFileName()
{
	QDir::current().mkdir( "testdir" );
	DirectoryCleaner cleaner1( "testdir" );
	
	Snapshot snapshot;
	snapshot.addEntity( SnapshotDirectory::createFromDirectory( "testdir" ) );
	snapshot.setBackupName( "testname" );
	
	QString expected( QString::number( snapshot.date().toMSecsSinceEpoch() )
	                + '-' + snapshot.id() + "-testname.xml" );
	
	QCOMPARE( snapshot.fileName(), expected );
}

QTEST_MAIN(TestSnapshot)
