/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef ACTION_H
#define ACTION_H

#include "Environment.h"

class QTextStream;


class Action
{

public:
	Action();
	virtual ~Action();
	
	void setBackupEnvironment( const Environment& environment );
	Environment backupEnvironment() const;
	
	virtual void setOutputString( QString* str );
	
	virtual void execute( const QStringList& args ) =0;
	
protected:
	QTextStream& outputStream() const;
	
private:
	static QTextStream* p_defaultOutputStream();
	
private:
	Environment m_environment;
	QTextStream* m_writer;
	
};

#endif // ACTION_H
