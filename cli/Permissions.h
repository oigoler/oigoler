/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef PERMISSIONS_H
#define PERMISSIONS_H

#include <QFile>


class Permissions
{

public:
	Permissions();
	Permissions( QFile::Permissions permissions );
	Permissions( int permissions );
	Permissions( const Permissions& other );
	virtual ~Permissions();
	
	Permissions& operator=( const Permissions& other );
	
	void setPermissions( QFile::Permissions permissions );
	void setPermissions( int permissions );
	
	QString toString() const;
	
private:
	int m_user;
	int m_group;
	int m_other;
	
};

#endif // PERMISSIONS_H
