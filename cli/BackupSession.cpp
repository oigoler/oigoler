/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "BackupSession.h"
#include "BackupStorage.h"
#include "BackupStorageList.h"
#include "BackupRecipe.h"
#include "BackupRecipeList.h"
#include "BackupSnapshot.h"

#include <QFile>
#include <QDir>
#include <QXmlStreamWriter>
#include <QDateTime>
#include <QCryptographicHash>


BackupSession::BackupSession()
	: m_config( QDir::home().filePath( ".oigoler" ) )
{
}

BackupSession::~BackupSession()
{
}

void BackupSession::setConfigDirectory( const QString& path )
{
	m_config = path;
}

QString BackupSession::configDirectory() const
{
	return m_config;
}

BackupStorage BackupSession::createStorage( const QString& path )
{
	QFileInfo info( path );
	
	if ( !info.exists() )
		return BackupStorage( BackupStorage::InexistentPath );
	
	if ( !info.isDir() )
		return BackupStorage( BackupStorage::InvalidPath );
	
	QString configPath( p_storagePath() );
	
	if ( configPath.isEmpty() )
		return BackupStorage( BackupStorage::SessionError );
	
	BackupStorage storage( path );
	
	QFile file( configPath + '/' + storage.key() );
	file.open( QFile::WriteOnly );
	file.write( storage.path().toUtf8() );
	file.write( "\n" );
	
	return storage;
}

BackupStorageList BackupSession::listStorage() const
{
	QString configPath( p_storagePath() );
	
	if ( configPath.isEmpty() )
		return BackupStorageList( BackupStorageList::SessionError );
	
	QDir config( configPath );
	BackupStorageList list;
	
	foreach ( const QFileInfo& info
	        , config.entryInfoList( QDir::Files | QDir::NoDotAndDotDot ) )
	{
		list.append( p_storage( info.fileName().toUtf8() ) );
	}
	
	qSort( list );
	
	return list;
}

void BackupSession::createRecipe( BackupRecipe* recipe )
{
	if ( this->recipe( recipe->name() ).isValid() )
	{
		recipe->setError( BackupRecipe::DuplicateRecipe );
		return;
	}
	
	this->registerRecipe( recipe );
}

void BackupSession::registerRecipe( BackupRecipe* recipe )
{
	QString config( p_recipePath() );
	
	if ( config.isEmpty() )
	{
		recipe->setError( BackupRecipe::SessionError );
		return;
	}
	
	if ( !recipe->isValid() )
		return;
	
	if ( recipe->hasInvalidPath() )
	{
		recipe->setError( BackupRecipe::InvalidPath );
		return;
	}
	
	QFile file( config + '/' + recipe->name() + ".xml" );
	file.open( QFile::WriteOnly );
	
	QXmlStreamWriter writer( &file );
	writer.setAutoFormatting( true );
	writer.writeStartDocument();
	writer.writeStartElement( "recipe" );
	writer.writeAttribute( "name", recipe->name() );
	
	foreach ( const QString& path, recipe->pathList() )
		writer.writeTextElement( "path", path );
	
	foreach ( const QByteArray& storage, recipe->storageList() )
		writer.writeTextElement( "storage", storage );
	
	writer.writeEndElement();
	writer.writeEndDocument();
}

BackupRecipeList BackupSession::listRecipe() const
{
	QString config( p_recipePath() );
	
	if ( config.isEmpty() )
		return BackupRecipeList( BackupRecipeList::SessionError );
	
	QStringList filters;
	filters.append( "*.xml" );
	
	QDir configDir( config );
	configDir.setNameFilters( filters );
	
	QStringList nameList;
	
	foreach ( QString recipeFile, configDir.entryList() )
	{
		recipeFile.chop( 4 );
		nameList.append( recipeFile );
	}
	
	nameList.sort();
	BackupRecipeList recipeList;
	
	foreach ( const QString& name, nameList )
		recipeList.append( this->recipe( name ) );
	
	return recipeList;
}

BackupRecipe BackupSession::recipe( const QString& name ) const
{
	QString config( p_recipePath() );
	
	if ( config.isEmpty() )
		return BackupRecipe( BackupRecipe::SessionError );
	
	QDir configDir( config );
	QFile file( configDir.filePath( name + ".xml" ) );
	BackupRecipe recipe( name );
	
	if ( !file.exists() )
	{
		recipe.setError( BackupRecipe::InexistentRecipe );
		return recipe;
	}
	
	file.open( QFile::ReadOnly );
	QXmlStreamReader reader( &file );
	
	while ( !reader.atEnd() )
	{
		if ( reader.readNextStartElement() )
		{
			if ( reader.name() == "path" )
			{
				reader.readNext();
				recipe.addPath( reader.text().toString() );
			}
			else if ( reader.name() == "storage" )
			{
				reader.readNext();
				recipe.addStorage( reader.text().toUtf8() );
			}
		}
	}
	
	return recipe;
}

void BackupSession::removeRecipe( const QString& name )
{
	QString config( p_recipePath() );
	
	if ( config.isEmpty() )
		return;
	
	QDir configDir( config );
	configDir.remove( name + ".xml" );
}

BackupSnapshot BackupSession::backup( const QString& recipeName )
{
	BackupRecipe recipe( this->recipe( recipeName ) );
	
	if ( recipe.error() == BackupRecipe::SessionError )
		return BackupSnapshot( BackupSnapshot::SessionError );
	
	if ( !recipe.isValid() )
		return BackupSnapshot( BackupSnapshot::RecipeError );
	
	QList< QByteArray > storageList( recipe.storageList() );
	
	if ( storageList.isEmpty() )
		return BackupSnapshot( BackupSnapshot::StorageError );
	
	BackupStorage storage;
	
	foreach ( const QByteArray& key, storageList )
	{
		storage = p_storage( key );
		
		if ( !storage.isValid() )
			return BackupSnapshot( BackupSnapshot::StorageError );
		
		if ( storage.isAvailable() )
			break;
	}
	
	if ( !storage.isAvailable() )
		return BackupSnapshot( BackupSnapshot::UnavailableStorage );
	
	BackupSnapshot snapshot;
	snapshot.setName( recipeName );
	snapshot.setTimestamp( QDateTime::currentDateTime() );
	
	QList< QString > pathList;
	
	foreach ( const QString& path, recipe.pathList() )
	{
		pathList.append( path );
		
		QFileInfo info( path );
		
		if ( info.isDir() && !info.isSymLink() )
			pathList.append( p_directoryEntries( path ) );
	}
	
	snapshot.setPathList( pathList );
	
	QString fileName( storage.path()
	                + '/'
	                + QString::number( snapshot.timestamp().toMSecsSinceEpoch() )
	                + '-'
	                + recipeName );
	
	QFile file( fileName );
	file.open( QFile::WriteOnly );
	
	foreach ( const QString& path, snapshot.pathList() )
	{
		QFileInfo info( path );
		
		if ( info.isSymLink() )
			file.write( p_processSymLink( info, storage.path() ) + '\n' );
		else if ( info.isDir() )
			file.write( p_processDir( info, storage.path() ) + '\n' );
		else if ( info.isFile() )
			file.write( p_processFile( info, storage.path() ) + '\n' );
	}
	
	return snapshot;
}

QString BackupSession::p_storagePath() const
{
	QDir dir( QDir::current() );
	
	if ( dir.mkpath( m_config + "/storage" ) )
		return dir.absoluteFilePath( m_config + "/storage" );
	else
		return QString();
}

BackupStorage BackupSession::p_storage( const QByteArray& key ) const
{
	QString config( p_storagePath() );
	
	QFile file( config + '/' + key );
	
	if ( !file.exists() )
		return BackupStorage( BackupStorage::InvalidKey );
	
	file.open( QFile::ReadOnly );
	
	QString path( file.readLine() );
	path.chop( 1 );
	
	return BackupStorage( key, path );
}

QString BackupSession::p_recipePath() const
{
	QDir dir( QDir::current() );
	
	if ( dir.mkpath( m_config + "/recipe" ) )
		return dir.absoluteFilePath( m_config + "/recipe" );
	else
		return QString();
}

QList< QString > BackupSession::p_directoryEntries( QDir dir ) const
{
	QList< QString > pathList;
	
	dir.setFilter( QDir::AllEntries | QDir::NoDotAndDotDot | QDir::System );
	
	foreach ( const QFileInfo& info, dir.entryInfoList() )
	{
		pathList.append( info.absoluteFilePath() );
		
		if ( info.isDir() && !info.isSymLink() )
			pathList.append( p_directoryEntries( info.absoluteFilePath() ) );
	}
	
	return pathList;
}

QByteArray BackupSession::p_processSymLink( const QFileInfo& info, const QString& destination ) const
{
	QString target( p_target( info ) );
	
	QCryptographicHash hasher( QCryptographicHash::Sha1 );
	hasher.addData( "symlink" );
	hasher.addData( info.absoluteFilePath().toUtf8() );
	hasher.addData( target.toUtf8() );
	QByteArray hash( hasher.result().toHex() );
	
	QFile file( destination + '/' + hash );
	file.open( QFile::WriteOnly );
	
	QXmlStreamWriter writer( &file );
	writer.setAutoFormatting( true );
	writer.writeStartDocument();
	writer.writeStartElement( "symlink" );
	writer.writeTextElement( "path", info.absoluteFilePath() );
	writer.writeTextElement( "target", target );
	writer.writeEndElement();
	writer.writeEndDocument();
	
	return hash;
}

QByteArray BackupSession::p_processDir( const QFileInfo& info, const QString& destination ) const
{
	QByteArray permissions( p_permissions( info ) );
	permissions[0] = 'd';
	
	QCryptographicHash hasher( QCryptographicHash::Sha1 );
	hasher.addData( "directory" );
	hasher.addData( info.absoluteFilePath().toUtf8() );
	hasher.addData( permissions );
	QByteArray hash( hasher.result().toHex() );
	
	QFile file( destination + '/' + hash );
	file.open( QFile::WriteOnly );
	
	QXmlStreamWriter writer( &file );
	writer.setAutoFormatting( true );
	writer.writeStartDocument();
	writer.writeStartElement( "directory" );
	writer.writeTextElement( "path", info.absoluteFilePath() );
	writer.writeTextElement( "permissions", permissions );
	writer.writeEndElement();
	writer.writeEndDocument();
	
	return hash;
}

QByteArray BackupSession::p_processFile( const QFileInfo& info, const QString& destination ) const
{
	QByteArray permissions( p_permissions( info ) );
	
	QCryptographicHash hasher( QCryptographicHash::Sha1 );
	hasher.addData( "file" );
	hasher.addData( info.absoluteFilePath().toUtf8() );
	hasher.addData( permissions );
	hasher.addData( info.lastModified().toString().toUtf8() );
	QByteArray hash( hasher.result().toHex() );
	
	QFile file( info.filePath() );
	file.open( QFile::ReadOnly );
	hasher.reset();
	
	while ( !file.atEnd() )
		hasher.addData( file.read( 2048 ) );
	
	QByteArray contentsHash( hasher.result().toHex() );
	QFile destFile( destination + '/' + contentsHash );
	destFile.open( QFile::WriteOnly );
	file.reset();
	
	while ( !file.atEnd() )
		destFile.write( file.read( 2048 ) );
	
	destFile.close();
	file.close();
	file.setFileName( destination + '/' + hash );
	file.open( QFile::WriteOnly );
	
	QXmlStreamWriter writer( &file );
	writer.setAutoFormatting( true );
	writer.writeStartDocument();
	writer.writeStartElement( "file" );
	writer.writeTextElement( "path", info.absoluteFilePath() );
	writer.writeTextElement( "permissions", permissions );
	writer.writeTextElement( "contents", contentsHash );
	writer.writeEndElement();
	writer.writeEndDocument();
	
	return hash;
}

QString BackupSession::p_target( const QFileInfo& info ) const
{
	QByteArray link( info.absoluteFilePath().toUtf8() );
	size_t bufsize( 1024 );
	ssize_t retsize( 0 );
	char* buf( 0 );
	
	while ( true )
	{
		buf = (char*) realloc( buf, bufsize );
		retsize = readlink( link.data(), buf, bufsize );
		
		if ( retsize < 0 )
		{
			free( buf );
			return QString();
		}
		
		if ( retsize < bufsize )
		{
			buf[retsize] = '\0';
			break;
		}
		
		bufsize *= 2;
	}
	
	QString target( buf );
	free( buf );
	
	return target;
}

QByteArray BackupSession::p_permissions( const QFileInfo& info ) const
{
	QByteArray permissions( 10, '-' );
	
	if ( info.permission( QFile::ReadUser ) )
		permissions[1] = 'r';
	
	if ( info.permission( QFile::WriteUser ) )
		permissions[2] = 'w';
	
	if ( info.permission( QFile::ExeUser ) )
		permissions[3] = 'x';
	
	if ( info.permission( QFile::ReadGroup ) )
		permissions[4] = 'r';
	
	if ( info.permission( QFile::WriteGroup ) )
		permissions[5] = 'w';
	
	if ( info.permission( QFile::ExeGroup ) )
		permissions[6] = 'x';
	
	if ( info.permission( QFile::ReadOther ) )
		permissions[7] = 'r';
	
	if ( info.permission( QFile::WriteOther ) )
		permissions[8] = 'w';
	
	if ( info.permission( QFile::ExeOther ) )
		permissions[9] = 'x';
	
	return permissions;
}
