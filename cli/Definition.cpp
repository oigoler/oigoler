/**
 *   Copyright (C) 2012, 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "Definition.h"
#include "Snapshot.h"
#include "SnapshotLink.h"
#include "SnapshotDirectory.h"
#include "SnapshotFile.h"

#include <QXmlStreamWriter>
#include <QStringList>
#include <QFileInfo>


Definition::Definition()
{
}

Definition::Definition( const Definition& other )
	: m_name( other.m_name )
	, m_objectList( other.m_objectList )
	, m_storage( other.m_storage )
{
}

Definition::~Definition()
{
}

bool Definition::operator==( const Definition& other ) const
{
	return ( m_name       == other.m_name )
	    && ( m_storage    == other.m_storage )
		&& ( m_objectList == other.m_objectList );
}

bool Definition::operator!=( const Definition& other ) const
{
	return ( m_name       != other.m_name )
	    || ( m_storage    != other.m_storage )
		|| ( m_objectList != other.m_objectList );
}

Definition& Definition::operator=( const Definition& other )
{
	if ( this == &other )
		return *this;
	
	m_name       = other.m_name;
	m_objectList = other.m_objectList;
	m_storage    = other.m_storage;
	
	return *this;
}

void Definition::setName( const QString& name )
{
	m_name = name;
}

QString Definition::name() const
{
	return m_name;
}

const DefinitionEntityList& Definition::backupObjectList() const
{
	return m_objectList;
}

void Definition::addPath( DefinitionPath path )
{
	m_objectList.append( path );
}

void Definition::removePath( DefinitionPath path )
{
	m_objectList.removeAll( path );
}

DefinitionStorage Definition::backupStorage() const
{
	return m_storage;
}

void Definition::setBackupStorage( DefinitionStorage storage )
{
	m_storage = storage;
}

void Definition::toXml( QXmlStreamWriter* writer ) const
{
	writer->writeStartElement( "BackupDefinition" );
	writer->writeAttribute( "name", m_name );
	m_objectList.toXml( writer );
	m_storage.toXml( writer );
	writer->writeEndElement();
}

void Definition::fromXml( QXmlStreamReader* reader)
{
	if ( reader->name() != "BackupDefinition" )
		return;
	
	if ( !reader->attributes().hasAttribute( "name" ) )
		return;
	
	m_name = reader->attributes().value( "name" ).toString();
	
	while ( reader->readNextStartElement() )
	{
		if ( reader->name() == "BackupObjectList" )
		{
			m_objectList.fromXml( reader );
		}
		else if ( reader->name() == "BackupStorage" )
		{
			DefinitionStorage storage;
			storage.fromXml( reader );
			setBackupStorage( storage );
		}
		else
		{
			reader->skipCurrentElement();
		}
	}
}

QStringList Definition::toStringList() const
{
	QStringList ret;
	
	ret << ( "Backup: " + m_name ) << m_storage.toString() << m_objectList.toStringList();
	
	return ret;
}

Snapshot Definition::snapshot() const
{
	Snapshot ret;
	
	foreach ( const DefinitionPath& path, m_objectList )
	{
		QFileInfo info( path.path() );
		
		if ( info.isSymLink() )
			ret.addEntity( SnapshotLink::createFromLink( path ) );
		else if ( info.isDir() )
			ret.addEntity( SnapshotDirectory::createFromDirectory( path.path() ) );
		else if ( info.isFile() )
			ret.addEntity( SnapshotFile::createFromFile( path ) );
	}
	
	return ret;
}
