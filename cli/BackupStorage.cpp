/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "BackupStorage.h"

#include <QCryptographicHash>
#include <QFileInfo>
#include <QTextStream>
#include <QDir>


BackupStorage::BackupStorage()
	: m_error( NoError )
{
}

BackupStorage::BackupStorage( const QString& path )
	: m_path( QFileInfo( path ).absoluteFilePath() )
	, m_error( NoError )
{
	QCryptographicHash hasher( QCryptographicHash::Sha1 );
	hasher.addData( m_path.toUtf8() );
	m_key = hasher.result().toHex();
}

BackupStorage::BackupStorage( BackupStorageError error )
	: m_error( error )
{
}

BackupStorage::BackupStorage( const QByteArray& key, const QString& path )
	: m_key( key )
	, m_path( path )
	, m_error( NoError )
{
}

BackupStorage::BackupStorage( const BackupStorage& other )
	: m_key( other.m_key )
	, m_path( other.m_path )
	, m_error( other.m_error )
{
}

BackupStorage::~BackupStorage()
{
}

BackupStorage& BackupStorage::operator=( const BackupStorage& other )
{
	if ( this == &other )
		return *this;
	
	m_key   = other.m_key;
	m_path  = other.m_path;
	m_error = other.m_error;
	
	return *this;
}

bool BackupStorage::operator<( const BackupStorage& other ) const
{
	return m_path < other.m_path;
}

QByteArray BackupStorage::key() const
{
	return m_key;
}

QString BackupStorage::path() const
{
	return m_path;
}

bool BackupStorage::isValid() const
{
	return m_error == NoError;
}

BackupStorage::BackupStorageError BackupStorage::error() const
{
	return m_error;
}

bool BackupStorage::isAvailable() const
{
	return QDir( m_path ).exists();
}

QTextStream& operator<<( QTextStream& stream, const BackupStorage& storage )
{
	switch ( storage.error() )
	{
		case BackupStorage::NoError:
			stream << storage.key() << ' ' << storage.path();
			break;
			
		case BackupStorage::SessionError:
			stream << "Error: Backup session not setup right!";
			break;
			
		case BackupStorage::InexistentPath:
			stream << "Error: The storage path doesn't exist!";
			break;
			
		case BackupStorage::InvalidPath:
			stream << "Error: Invalid storage path!";
			break;
	}
	
	return stream;
}
