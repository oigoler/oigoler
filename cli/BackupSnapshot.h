/**
 *   Copyright (C) 2013 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef BACKUPSNAPSHOT_H
#define BACKUPSNAPSHOT_H

#include <QString>
#include <QList>
#include <QDateTime>


class QTextStream;


class BackupSnapshot
{
	
public:
	enum SnapshotError { NoError
	                   , SessionError
	                   , RecipeError
	                   , StorageError
	                   , UnavailableStorage };

public:
	BackupSnapshot();
	BackupSnapshot( SnapshotError error );
	BackupSnapshot( const BackupSnapshot& other );
	virtual ~BackupSnapshot();
	
	BackupSnapshot& operator=( const BackupSnapshot& other );
	
	QString name() const;
	void setName( const QString& name );
	
	QList< QString > pathList() const;
	void setPathList( const QList< QString >& pathList );
	
	QDateTime timestamp() const;
	void setTimestamp( const QDateTime& timestamp );
	
	bool isValid() const;
	SnapshotError error() const;
	
private:
	QString m_name;
	QList< QString > m_paths;
	QDateTime m_timestamp;
	SnapshotError m_error;
	
};


QTextStream& operator<<( QTextStream& stream, const BackupSnapshot& snapshot );

#endif // BACKUPSNAPSHOT_H
