/**
 *   Copyright (C) 2012 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "FileInfoExtractor.h"
#include "FileElement.h"
#include <QFileInfo>
#include <QDateTime>


FileInfoExtractor::FileInfoExtractor( QObject* parent )
	: QObject( parent )
	, d(0)
{
}

FileInfoExtractor::~FileInfoExtractor()
{
}

void FileInfoExtractor::process( const QString& file )
{
	FileElement elem;
	QFileInfo info( file );
	
	if ( !info.exists() || !info.isFile() || info.isSymLink() )
	{
		emit failed();
		return;
	}
	
	elem.setName( file );
	elem.setPath( info.canonicalPath() );
	elem.setSize( info.size() );
	elem.setLastModifiedDate( info.lastModified() );

	emit finished( elem );
}
