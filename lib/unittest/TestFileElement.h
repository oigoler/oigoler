/**
 *   Copyright (C) 2012 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef TESTFILEELEMENT_H
#define TESTFILEELEMENT_H

#include <QtTest/QtTest>

class TestFileElement: public QObject
{
	Q_OBJECT

private slots:
	void testName();
	void testName_copy();
	void testName_assign();
	
	void testPath();
	void testPath_copy();
	void testPath_assign();
	
	void testSize();
	void testSize_copy();
	void testSize_assign();
	
	void testLastModifiedDate();
	void testLastModifiedDate_copy();
	void testLastModifiedDate_assign();
	
	void testPrint();

};

#endif // TESTFILEELEMENT_H
