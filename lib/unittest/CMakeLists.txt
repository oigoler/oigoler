#################################################################
# TestFileElement
#################################################################

set( TestFileElement_SRCS TestFileElement.cpp ../FileElement.cpp )
set( TestFileElement_HDRS TestFileElement.h )
qt4_wrap_cpp( TestFileElement_MOCS ${TestFileElement_HDRS} )

add_executable( TestFileElement ${TestFileElement_SRCS} ${TestFileElement_MOCS} )
target_link_libraries( TestFileElement ${QT_QTTEST_LIBRARY} ${QT_LIBRARIES} )
add_test( NAME TestFileElement COMMAND TestFileElement )


#################################################################
# TestFileInfoExtractor
#################################################################

set( TestFileInfoExtractor_SRCS TestFileInfoExtractor.cpp ../FileInfoExtractor.cpp ../FileElement.cpp
                                FileCreator.cpp LinkCreator.cpp DirectoryCreator.cpp )
set( TestFileInfoExtractor_HDRS TestFileInfoExtractor.h ../FileInfoExtractor.h )
qt4_wrap_cpp( TestFileInfoExtractor_MOCS ${TestFileInfoExtractor_HDRS} )

add_executable( TestFileInfoExtractor ${TestFileInfoExtractor_SRCS} ${TestFileInfoExtractor_MOCS} )
target_link_libraries( TestFileInfoExtractor ${QT_QTTEST_LIBRARY} ${QT_LIBRARIES} )
add_test( NAME TestFileInfoExtractor COMMAND TestFileInfoExtractor )


#################################################################
# TestBackupDefinition
#################################################################

set( TestBackupDefinition_SRCS TestBackupDefinition.cpp
                               FakeElement.cpp
                               FakeExclusion.cpp
                               ../BackupDefinition.cpp
                               ../BackupElement.cpp
                               ../BackupExclusion.cpp )
set( TestBackupDefinition_HDRS TestBackupDefinition.h
                               ../BackupElement.h
                               ../BackupExclusion.h )
qt4_wrap_cpp( TestBackupDefinition_MOCS ${TestBackupDefinition_HDRS} )

add_executable( TestBackupDefinition ${TestBackupDefinition_SRCS} ${TestBackupDefinition_MOCS} )
target_link_libraries( TestBackupDefinition ${QT_QTTEST_LIBRARY} ${QT_LIBRARIES} )
add_test( NAME TestBackupDefinition COMMAND TestBackupDefinition )
