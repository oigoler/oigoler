/**
 *   Copyright (C) 2012 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "LinkCreator.h"
#include <QFile>

LinkCreator::LinkCreator( const QString& filename, const QString& linkname )
	: m_linkname(linkname)
{
	QFile::link( filename, this->m_linkname );
}

LinkCreator::~LinkCreator()
{
	QFile::remove( this->m_linkname );
}
