/**
 *   Copyright (C) 2012 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef TESTFILEPARSER_H
#define TESTFILEPARSER_H

#include <QtTest/QtTest>
#include "../FileElement.h"

class FileCreator;

class TestFileInfoExtractor: public QObject
{
	Q_OBJECT

private slots:
	void initTestCase();
	void testName();
	void testPath();
	void testSize();
	void testLastModifiedDate();
	void testFailInexistent();
	void testFailLink();
	void testFailDirectory();
	void cleanupTestCase();
	
private:
	FileElement m_element;
	QString m_filename;
	FileCreator* m_creator;

};

#endif // TESTFILEPARSER_H
