/**
 *   Copyright (C) 2012 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestFileElement.h"
#include "../FileElement.h"

void TestFileElement::testName()
{
	QString name( "filename.txt" );
	FileElement elem;
	
	elem.setName( name );
	
	QCOMPARE( elem.name(), name );
}

void TestFileElement::testName_copy()
{
	FileElement elem1;
	elem1.setName( "filename.txt" );
	FileElement elem2( elem1 );
	
	QVERIFY( elem1.name() == elem2.name() );
}

void TestFileElement::testName_assign()
{
	FileElement elem1;
	FileElement elem2;
	elem1.setName( "filename.txt" );
	elem2 = elem1;
	
	QVERIFY( elem1.name() == elem2.name() );
}

void TestFileElement::testPath()
{
	QString path( "/media/usbstick" );
	FileElement elem;
	
	elem.setPath( path );
	
	QCOMPARE( elem.path(), path );
}

void TestFileElement::testPath_copy()
{
	FileElement elem1;
	elem1.setPath( "/media/usbstick" );
	FileElement elem2( elem1 );
	
	QVERIFY( elem1.path() == elem2.path() );
}

void TestFileElement::testPath_assign()
{
	FileElement elem1;
	FileElement elem2;
	elem1.setPath( "/media/usbstick" );
	elem2 = elem1;
	
	QVERIFY( elem1.path() == elem2.path() );
}

void TestFileElement::testSize()
{
	qint64 size = 1024;
	FileElement elem;
	
	elem.setSize( size );
	
	QCOMPARE( elem.size(), size );
}

void TestFileElement::testSize_copy()
{
	FileElement elem1;
	elem1.setSize( 1024 );
	FileElement elem2( elem1 );
	
	QVERIFY( elem1.size() == elem2.size() );
}

void TestFileElement::testSize_assign()
{
	FileElement elem1;
	FileElement elem2;
	elem1.setSize( 1024 );
	elem2 = elem1;
	
	QVERIFY( elem1.size() == elem2.size() );
}

void TestFileElement::testLastModifiedDate()
{
	QDateTime date = QDateTime::currentDateTime();
	FileElement elem;
	
	elem.setLastModifiedDate( date );
	
	QCOMPARE( elem.lastModifiedDate(), date );
}

void TestFileElement::testLastModifiedDate_copy()
{
	FileElement elem1;
	elem1.setLastModifiedDate( QDateTime::currentDateTime() );
	FileElement elem2( elem1 );
	
	QVERIFY( elem1.lastModifiedDate() == elem2.lastModifiedDate() );
}

void TestFileElement::testLastModifiedDate_assign()
{
	FileElement elem1;
	FileElement elem2;
	elem1.setLastModifiedDate( QDateTime::currentDateTime() );
	elem2 = elem1;
	
	QVERIFY( elem1.lastModifiedDate() == elem2.lastModifiedDate() );
}

void TestFileElement::testPrint()
{
	QDateTime date = QDateTime::currentDateTime();
	FileElement elem;
	elem.setName( "test.txt" );
	elem.setPath( "/path/to/test" );
	elem.setSize( 2048 );
	elem.setLastModifiedDate( date );
	
	QString expected;
	expected = "FileElement[/path/to/test/test.txt; size=2048; last modified=" + date.toString() + "]";
	
	QString result;
	QTextStream stream( &result );
	stream << elem;
	
	QCOMPARE( result, expected );
}

QTEST_MAIN(TestFileElement)
