/**
 *   Copyright (C) 2012 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestFileInfoExtractor.h"
#include "../FileInfoExtractor.h"
#include "FileCreator.h"
#include "LinkCreator.h"
#include "DirectoryCreator.h"

void TestFileInfoExtractor::initTestCase()
{
	this->m_filename = "TestFileInfoExtractor.txt";
	this->m_creator = new FileCreator( this->m_filename );
	
	FileInfoExtractor extractor;
	
	qRegisterMetaType<FileElement>("FileElement");
	QSignalSpy spy( &extractor, SIGNAL(finished(FileElement)) );
	
	extractor.process( this->m_filename );
	
	QCOMPARE( spy.count(), 1 );
	
	this->m_element = qvariant_cast<FileElement>( spy.at(0).at(0) );
}

void TestFileInfoExtractor::testName()
{
	QCOMPARE( this->m_element.name(), this->m_filename );
}

void TestFileInfoExtractor::testPath()
{
	QCOMPARE( this->m_element.path(), QDir::currentPath() );
}

void TestFileInfoExtractor::testSize()
{
	QFileInfo info( this->m_filename );
	
	QCOMPARE( this->m_element.size(), info.size() );
}

void TestFileInfoExtractor::testLastModifiedDate()
{
	QFileInfo info( this->m_filename );
	
	QCOMPARE( this->m_element.lastModifiedDate(), info.lastModified() );
}

void TestFileInfoExtractor::testFailInexistent()
{
	FileInfoExtractor extractor;
	QSignalSpy spyFail( &extractor, SIGNAL(failed()) );
	QSignalSpy spySucess( &extractor, SIGNAL(finished(FileElement)) );
	
	extractor.process( "nonexistent.txt" );
	
	QCOMPARE( spyFail.count(), 1 );
	QCOMPARE( spySucess.count(), 0 );
}

void TestFileInfoExtractor::testFailLink()
{
	FileInfoExtractor extractor;
	QSignalSpy spyFail( &extractor, SIGNAL(failed()) );
	QSignalSpy spySucess( &extractor, SIGNAL(finished(FileElement)) );
	QString link( "TestFileInfoExtractor.lnk" );
	LinkCreator creator( this->m_filename, link );
	
	extractor.process( link );
	
	QCOMPARE( spyFail.count(), 1 );
	QCOMPARE( spySucess.count(), 0 );
}

void TestFileInfoExtractor::testFailDirectory()
{
	FileInfoExtractor extractor;
	QSignalSpy spyFail( &extractor, SIGNAL(failed()) );
	QSignalSpy spySucess( &extractor, SIGNAL(finished(FileElement)) );
	QString dir( "TestFileInfoExtractor.dir" );
	DirectoryCreator creator( dir );
	
	extractor.process( dir );
	
	QCOMPARE( spyFail.count(), 1 );
	QCOMPARE( spySucess.count(), 0 );
}

void TestFileInfoExtractor::cleanupTestCase()
{
	delete this->m_creator;
}

QTEST_MAIN(TestFileInfoExtractor)
