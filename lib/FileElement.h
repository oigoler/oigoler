/**
 *   Copyright (C) 2012 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef FILEELEMENT_H
#define FILEELEMENT_H

#include <QtGlobal>
#include <QMetaType>

#include "oigoler_export.h"

class PrivateFileElement;
class QString;
class QDateTime;
class QTextStream;

class OIGOLER_EXPORT FileElement
{

public:
	FileElement();
	FileElement( const FileElement& other );
	virtual ~FileElement();
	
	FileElement& operator=( const FileElement& other );
	
	void setName( const QString& name );
	QString name() const;
	
	void setPath( const QString& path );
	QString path() const;
	
	void setSize( qint64 size );
	qint64 size() const;
	
	void setLastModifiedDate( const QDateTime& date );
	QDateTime lastModifiedDate() const;
	
private:
	PrivateFileElement* const d;

};

Q_DECLARE_METATYPE(FileElement)

QTextStream& operator<<( QTextStream& stream, const FileElement& element );

#endif // FILEELEMENT_H
