/**
 *   Copyright (C) 2012 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "FileElement.h"
#include <QString>
#include <QDateTime>
#include <QTextStream>


struct PrivateFileElement
{
	QString   name;
	QString   path;
	qint64    size;
	QDateTime lastModified;
	
	PrivateFileElement() : size(0) {}
	
	PrivateFileElement( PrivateFileElement* other )
	{
		this->name = other->name;
		this->path = other->path;
		this->size = other->size;
		this->lastModified = other->lastModified;
	}
};



FileElement::FileElement()
	: d( new PrivateFileElement )
{
}

FileElement::FileElement( const FileElement& other )
	: d( new PrivateFileElement( other.d ) )
{
}

FileElement::~FileElement()
{
	delete d;
}

FileElement& FileElement::operator=( const FileElement& other )
{
	if ( this == &other )
		return *this;
	
	d->name = other.d->name;
	d->path = other.d->path;
	d->size = other.d->size;
	d->lastModified = other.d->lastModified;
	
	return *this;
}

void FileElement::setName( const QString& name )
{
	d->name = name;
}

QString FileElement::name() const
{
	return d->name;
}
	
void FileElement::setPath( const QString& path )
{
	d->path = path;
}

QString FileElement::path() const
{
	return d->path;
}
	
void FileElement::setSize( qint64 size )
{
	d->size = size;
}

qint64 FileElement::size() const
{
	return d->size;
}
	
void FileElement::setLastModifiedDate( const QDateTime& date )
{
	d->lastModified = date;
}

QDateTime FileElement::lastModifiedDate() const
{
	return d->lastModified;
}

QTextStream& operator<<( QTextStream& stream, const FileElement& element )
{
	stream << "FileElement[" << element.path() << '/' << element.name() 
	       << "; size=" << element.size() << "; last modified=" << element.lastModifiedDate().toString() << "]";
	       
	return stream;
}
