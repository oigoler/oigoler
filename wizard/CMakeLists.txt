project( oigolerwizard )

set( oigolerwizard_SRCS main.cpp
                        BackupWizard.cpp
                        BackupNamePage.cpp
                        LastPage.cpp
                        DefinitionStorage.cpp )
set( oigolerwizard_HDRS BackupWizard.h BackupNamePage.h LastPage.h )
qt4_wrap_cpp( oigolerwizard_MOCS ${oigolerwizard_HDRS} )

add_executable( oigolerwizard ${oigolerwizard_SRCS} ${oigolerwizard_MOCS} )
target_link_libraries( oigolerwizard ${QT_LIBRARIES} )

add_subdirectory( unittest )
