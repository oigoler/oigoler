/**
 *   Copyright (C) 2012 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "BackupWizard.h"
#include "BackupNamePage.h"
#include "LastPage.h"

#include <QAbstractButton>


BackupWizard::BackupWizard( QWidget* parent )
	: QWizard( parent )
	, m_backupNamePage( new BackupNamePage )
{
	this->addPage( m_backupNamePage );
	this->addPage( new LastPage );
	this->setWindowTitle( "Backup Assistant" );
	
	connect( this, SIGNAL(currentIdChanged(int)), this, SLOT(p_disableLastPageCancelButton(int)) );
}

BackupWizard::~BackupWizard()
{
}

void BackupWizard::setDefinitionStorage( AbstractDefinitionStorage* storage )
{
	m_backupNamePage->setDefinitionStorage( storage );
}

void BackupWizard::p_disableLastPageCancelButton( int pageId )
{
	if ( (pageId != -1) && (this->page( pageId )->isFinalPage()) )
	{
		this->button( QWizard::CancelButton )->setEnabled(false);
	}
}
