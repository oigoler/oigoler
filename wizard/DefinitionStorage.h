/**
 *   Copyright (C) 2012 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef DEFINITIONSTORAGE_H
#define DEFINITIONSTORAGE_H

#include "AbstractDefinitionStorage.h"
#include <QString>

class QFile;


class DefinitionStorage : public AbstractDefinitionStorage
{

public:
	DefinitionStorage();
	virtual ~DefinitionStorage();
	
	void store( const QString& backupName );
	bool exists( const QString& backupName ) const;
	
	QString filePath() const;
	void setFilePath( const QString& filePath );
	
private:
	QString m_filePath;
	QFile* m_file;
	
};

#endif // DEFINITIONSTORAGE_H
