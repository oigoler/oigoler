/**
 *   Copyright (C) 2012 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "BackupNamePage.h"
#include "AbstractDefinitionStorage.h"

#include <QLineEdit>
#include <QVBoxLayout>
#include <QLabel>


BackupNamePage::BackupNamePage( QWidget* parent )
	: QWizardPage( parent )
	, m_backupNameLineEdit( new QLineEdit )
	, m_definitionStorage( 0 )
	, m_warningLabel( new QLabel )
{
	QVBoxLayout* layout = new QVBoxLayout;
	
	m_warningLabel->setText( "<span style=\" color:#ff0000;\">This name is already in use. Please choose another one.</span>" );
	m_warningLabel->setWordWrap( true );
	m_warningLabel->setVisible( false );
	
	layout->addStretch();
	layout->addWidget( m_backupNameLineEdit );
	layout->addWidget( m_warningLabel );
	layout->addStretch();

	this->setLayout( layout );
	this->setTitle( "Backup Name" );
	this->setSubTitle( "Please choose a name to help you identify this Backup later." );
	this->registerField( "backupName*", m_backupNameLineEdit );
	this->setCommitPage(true);
}

BackupNamePage::~BackupNamePage()
{
	delete m_definitionStorage;
}

QString BackupNamePage::backupName() const
{
	return m_backupNameLineEdit->text();
}

void BackupNamePage::setDefinitionStorage( AbstractDefinitionStorage* storage )
{
	delete m_definitionStorage;
	m_definitionStorage = storage;
}

bool BackupNamePage::validatePage()
{
	if ( m_definitionStorage )
	{
		if ( m_definitionStorage->exists( this->backupName() ) )
		{
			this->showDuplicateWarning( true );
			return false;
		}
		
		m_definitionStorage->store( this->backupName() );
	}
	
	return true;
}

void BackupNamePage::showDuplicateWarning( bool enable )
{
	m_warningLabel->setVisible( enable );
	
	if ( enable )
		connect( m_backupNameLineEdit, SIGNAL(textEdited(QString)), this, SLOT(p_turnOffDuplicateWarning()) );
	else
		disconnect( m_backupNameLineEdit, SIGNAL(textEdited(QString)), this, SLOT(p_turnOffDuplicateWarning()) );
}

bool BackupNamePage::isDuplicateWarningShown() const
{
	return m_warningLabel->isVisible();
}

void BackupNamePage::p_turnOffDuplicateWarning()
{
	this->showDuplicateWarning( false );
}
