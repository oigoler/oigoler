/**
 *   Copyright (C) 2012 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestDefinitionStorage.h"
#include "../DefinitionStorage.h"


void TestDefinitionStorage::initTestCase()
{
	m_testFileName = "testdefinitionrc";
	m_testDirName  = "testdir";
}

void TestDefinitionStorage::cleanup()
{
	QFile::remove( m_testFileName );
	QFile::remove( m_testDirName + '/' + m_testFileName );
	QDir dir;
	dir.rmdir( m_testDirName );
}

void TestDefinitionStorage::hasDefaultDefinitionFilePath()
{
	QString defaultPath( QDir::homePath() + "/.oigoler/backupdefinitionrc" );
	DefinitionStorage storage;
	QCOMPARE( storage.filePath(), defaultPath );
}

void TestDefinitionStorage::canChangeDefinitionFilePath()
{
	DefinitionStorage storage;
	storage.setFilePath( m_testFileName );
	QString expected( QDir::currentPath() + '/' + m_testFileName );
	QCOMPARE( storage.filePath(), expected );
}

void TestDefinitionStorage::createsDefinitionFile()
{
	DefinitionStorage storage;
	storage.setFilePath( m_testFileName );
	storage.store( "dummy" );
	QVERIFY( QFile::exists( m_testFileName ) );
}

void TestDefinitionStorage::definitionFileHasStoredName()
{
	DefinitionStorage* storage = new DefinitionStorage;
	storage->setFilePath( m_testFileName );
	storage->store( "dummy" );
	delete storage;
	
	QFile file( m_testFileName );
	file.open( QFile::ReadOnly );
	
	QString word;
	QString expected( "dummy" );
	bool ok = false;
	QTextStream reader( &file );
	
	while ( !reader.atEnd() )
	{
		reader >> word;
		
		if ( word == expected )
		{
			ok = true;
			break;
		}
	}
	
	QVERIFY( ok );
}

void TestDefinitionStorage::createsDirectories()
{
	DefinitionStorage* storage = new DefinitionStorage;
	storage->setFilePath( m_testDirName + '/' + m_testFileName );
	storage->store( "dummy" );
	delete storage;

	QVERIFY( QFile::exists( m_testDirName + '/' + m_testFileName ) );
}

void TestDefinitionStorage::checksStoredNames()
{
	DefinitionStorage* storage1 = new DefinitionStorage;
	storage1->setFilePath( m_testFileName );
	storage1->store( "stored" );
	delete storage1;
	
	DefinitionStorage storage2;
	storage2.setFilePath( m_testFileName );
	QVERIFY( storage2.exists( "stored" ) );
	QVERIFY( !storage2.exists( "notstored" ) );
}

QTEST_MAIN(TestDefinitionStorage)
