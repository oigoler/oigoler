/**
 *   Copyright (C) 2012 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef TESTBACKUPWIZARD_H
#define TESTBACKUPWIZARD_H

#include <QtTest/QtTest>

class BackupWizard;


class TestBackupWizard: public QObject
{
	Q_OBJECT

private slots:
	void hasWindowTitle();
	void hasTwoPages();
	void page1IsBackupNamePage();
	void page1HasRequiredField();
	void page1HasCommit();
	void page1CommitStores();
	void page1DoesNotStoreOnDuplicate();
	void page1ShowsWarningOnDuplicate();
	void page2IsLastPage();
	void page2HasDisabledBackButton();
	void page2HasDisabledCancelButton();
	void page2HasFinishButton();
	
private:
	void showPage2( BackupWizard* wizard );
};

#endif // TESTBACKUPWIZARD_H
