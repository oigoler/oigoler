/**
 *   Copyright (C) 2012 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestBackupWizard.h"
#include "MockDefinitionStorage.h"
#include "../BackupWizard.h"
#include "../BackupNamePage.h"

#include <QAbstractButton>


void TestBackupWizard::hasWindowTitle()
{
	BackupWizard wizard;
	QVERIFY( !wizard.windowTitle().isEmpty() );
}

void TestBackupWizard::hasTwoPages()
{
	BackupWizard wizard;
	QCOMPARE( wizard.pageIds().count(), 2 );
}

void TestBackupWizard::page1IsBackupNamePage()
{
	BackupWizard wizard;
	wizard.show();
	QVERIFY( wizard.currentPage() );
	QCOMPARE( wizard.currentPage()->metaObject()->className(), "BackupNamePage" );
}

void TestBackupWizard::page1HasRequiredField()
{
	BackupWizard wizard;
	wizard.show();
	QTest::qWait(100);
	
	QVERIFY( wizard.button( QWizard::NextButton ) );
	QVERIFY( !wizard.button( QWizard::NextButton )->isEnabled() );
	
	QVERIFY( wizard.focusWidget() );
	QTest::keyClick( wizard.focusWidget(), 'a' );
	QVERIFY( wizard.button( QWizard::NextButton )->isEnabled() );
}

void TestBackupWizard::page1HasCommit()
{
	BackupWizard wizard;
	wizard.show();
	QVERIFY( wizard.currentPage() );
	QVERIFY( wizard.currentPage()->isCommitPage() );
}

void TestBackupWizard::page1CommitStores()
{
	MockDefinitionStorage* storage = new MockDefinitionStorage;
	BackupWizard wizard;
	wizard.setDefinitionStorage( storage );
	
	wizard.show();
	QTest::qWait(100);
	QVERIFY( wizard.focusWidget() );
	QTest::keyClicks( wizard.focusWidget(), "store" );
	QVERIFY( wizard.button( QWizard::CommitButton ) );
	QTest::mouseClick( wizard.button( QWizard::CommitButton ), Qt::LeftButton );
	
	QCOMPARE( storage->lastStored(), QString( "store" ) );
}

void TestBackupWizard::page1DoesNotStoreOnDuplicate()
{
	MockDefinitionStorage* storage = new MockDefinitionStorage;
	storage->setDuplicate( "duplicate" );
	BackupWizard wizard;
	wizard.setDefinitionStorage( storage );
	
	wizard.show();
	QTest::qWait(100);
	int page1( wizard.currentId() );
	
	QVERIFY( wizard.focusWidget() );
	QTest::keyClicks( wizard.focusWidget(), "duplicate" );
	QVERIFY( wizard.button( QWizard::CommitButton ) );
	QTest::mouseClick( wizard.button( QWizard::CommitButton ), Qt::LeftButton );
	
	QCOMPARE( wizard.currentId(), page1 );
}

void TestBackupWizard::page1ShowsWarningOnDuplicate()
{
	MockDefinitionStorage* storage = new MockDefinitionStorage;
	storage->setDuplicate( "duplicate" );
	BackupWizard wizard;
	wizard.setDefinitionStorage( storage );
	
	wizard.show();
	QTest::qWait(100);
	QVERIFY( wizard.focusWidget() );
	QTest::keyClicks( wizard.focusWidget(), "duplicate" );
	QVERIFY( wizard.button( QWizard::CommitButton ) );
	QTest::mouseClick( wizard.button( QWizard::CommitButton ), Qt::LeftButton );
	
	QVERIFY( wizard.currentPage() );
	BackupNamePage* page = qobject_cast<BackupNamePage*>( wizard.currentPage() );
	QVERIFY( page );
	QVERIFY( page->isDuplicateWarningShown() );
}

void TestBackupWizard::page2IsLastPage()
{
	BackupWizard wizard;
	this->showPage2( &wizard );
	QVERIFY( wizard.currentPage() );
	QCOMPARE( wizard.currentPage()->metaObject()->className(), "LastPage" );
}

void TestBackupWizard::page2HasDisabledBackButton()
{
	BackupWizard wizard;
	this->showPage2( &wizard );
	QVERIFY( wizard.button( QWizard::BackButton ) );
	QVERIFY( !wizard.button( QWizard::BackButton )->isEnabled() );
}

void TestBackupWizard::page2HasDisabledCancelButton()
{
	BackupWizard wizard;
	this->showPage2( &wizard );
	QVERIFY( wizard.button( QWizard::CancelButton ) );
	QVERIFY( !wizard.button( QWizard::CancelButton )->isEnabled() );
}

void TestBackupWizard::page2HasFinishButton()
{
	BackupWizard wizard;
	this->showPage2( &wizard );
	QVERIFY( wizard.currentPage() );
	QVERIFY( wizard.currentPage()->isFinalPage() );
}

void TestBackupWizard::showPage2( BackupWizard* wizard )
{
	wizard->show();
	
	QTest::qWait(100);
	QTest::keyClick( wizard->focusWidget(), 'a' );
	QTest::mouseClick( wizard->button( QWizard::NextButton ), Qt::LeftButton );
}

QTEST_MAIN(TestBackupWizard)
