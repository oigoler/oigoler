include_directories( ${CMAKE_CURRENT_BINARY_DIR} )

#################################################################
# TestBackupWizard
#################################################################

set( TestBackupWizard_SRCS TestBackupWizard.cpp
                           ../BackupWizard.cpp
                           ../BackupNamePage.cpp
                           ../LastPage.cpp
                           MockDefinitionStorage.cpp )
set( TestBackupWizard_HDRS TestBackupWizard.h
                           ../BackupWizard.h
                           ../BackupNamePage.h
                           ../LastPage.h )
qt4_wrap_cpp( TestBackupWizard_MOCS ${TestBackupWizard_HDRS} )

add_executable( TestBackupWizard ${TestBackupWizard_SRCS} ${TestBackupWizard_MOCS} )
target_link_libraries( TestBackupWizard ${QT_QTTEST_LIBRARY} ${QT_LIBRARIES} )
add_test( NAME TestBackupWizard COMMAND TestBackupWizard )


#################################################################
# TestBackupNamePage
#################################################################

set( TestBackupNamePage_SRCS TestBackupNamePage.cpp
                             ../BackupNamePage.cpp )
set( TestBackupNamePage_HDRS TestBackupNamePage.h ../BackupNamePage.h )
qt4_wrap_cpp( TestBackupNamePage_MOCS ${TestBackupNamePage_HDRS} )

add_executable( TestBackupNamePage ${TestBackupNamePage_SRCS} ${TestBackupNamePage_MOCS} )
target_link_libraries( TestBackupNamePage ${QT_QTTEST_LIBRARY} ${QT_LIBRARIES} )
add_test( NAME TestBackupNamePage COMMAND TestBackupNamePage )


#################################################################
# TestLastPage
#################################################################

set( TestLastPage_SRCS TestLastPage.cpp ../LastPage.cpp )
set( TestLastPage_HDRS TestLastPage.h ../LastPage.h )
qt4_wrap_cpp( TestLastPage_MOCS ${TestLastPage_HDRS} )

add_executable( TestLastPage ${TestLastPage_SRCS} ${TestLastPage_MOCS} )
target_link_libraries( TestLastPage ${QT_QTTEST_LIBRARY} ${QT_LIBRARIES} )
add_test( NAME TestLastPage COMMAND TestLastPage )


#################################################################
# TestDefinitionStorage
#################################################################

set( TestDefinitionStorage_SRCS TestDefinitionStorage.cpp ../DefinitionStorage.cpp )
set( TestDefinitionStorage_HDRS TestDefinitionStorage.h )
qt4_wrap_cpp( TestDefinitionStorage_MOCS ${TestDefinitionStorage_HDRS} )

add_executable( TestDefinitionStorage ${TestDefinitionStorage_SRCS} ${TestDefinitionStorage_MOCS} )
target_link_libraries( TestDefinitionStorage ${QT_QTTEST_LIBRARY} ${QT_LIBRARIES} )
add_test( NAME TestDefinitionStorage COMMAND TestDefinitionStorage )
