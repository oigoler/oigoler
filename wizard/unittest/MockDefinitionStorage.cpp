/**
 *   Copyright (C) 2012 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "MockDefinitionStorage.h"


MockDefinitionStorage::MockDefinitionStorage()
{
}

MockDefinitionStorage::~MockDefinitionStorage()
{
}

void MockDefinitionStorage::store( const QString& backupName )
{
	m_lastStored = backupName;
}

QString MockDefinitionStorage::lastStored() const
{
	return m_lastStored;
}

void MockDefinitionStorage::setDuplicate( const QString& duplicate )
{
	m_duplicate = duplicate;
}

bool MockDefinitionStorage::exists( const QString& backupName ) const
{
	return (backupName == m_duplicate);
}
