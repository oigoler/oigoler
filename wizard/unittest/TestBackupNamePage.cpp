/**
 *   Copyright (C) 2012 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "TestBackupNamePage.h"
#include "../BackupNamePage.h"

void TestBackupNamePage::hasTitle()
{
	BackupNamePage page;
	QVERIFY( !page.title().isEmpty() );
}

void TestBackupNamePage::hasSubTitle()
{
	BackupNamePage page;
	QVERIFY( !page.subTitle().isEmpty() );
}

void TestBackupNamePage::inputsBackupName()
{
	BackupNamePage page;
	page.show();
	QTest::qWait(100);
	
	QVERIFY( page.focusWidget() );
	QTest::keyClicks( page.focusWidget(), "testname" );
	QCOMPARE( page.backupName(), QString("testname") );
}

void TestBackupNamePage::duplicateWarningIsOffByDefault()
{
	BackupNamePage page;
	page.show();
	QVERIFY( !page.isDuplicateWarningShown() );
}

void TestBackupNamePage::togglesDuplicateWarning()
{
	BackupNamePage page;
	page.show();
	
	page.showDuplicateWarning( true );
	QVERIFY( page.isDuplicateWarningShown() );
	
	page.showDuplicateWarning( false );
	QVERIFY( !page.isDuplicateWarningShown() );
}

void TestBackupNamePage::turnOffWarningOnInput()
{
	BackupNamePage page;
	page.show();
	page.showDuplicateWarning( true );
	QTest::qWait(100);
	
	QVERIFY( page.focusWidget() );
	QTest::keyClick( page.focusWidget(), 'a' );
	QVERIFY( !page.isDuplicateWarningShown() );
}

QTEST_MAIN(TestBackupNamePage)
