/**
 *   Copyright (C) 2012 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#include "DefinitionStorage.h"
#include <QDir>
#include <QTextStream>

DefinitionStorage::DefinitionStorage()
	: m_file( 0 )
{
	m_filePath = QDir::homePath() + "/.oigoler/backupdefinitionrc";
}

DefinitionStorage::~DefinitionStorage()
{
	delete m_file;
}

void DefinitionStorage::store( const QString& backupName )
{
	if ( !m_file )
	{
		QFileInfo info( m_filePath );
		
		if ( !info.dir().exists() )
		{
			QDir dir;
			dir.mkpath( info.path() );
		}
		
		m_file = new QFile( m_filePath );
		m_file->open( QFile::WriteOnly | QFile::Append );
	}
	
	QTextStream writeStream( m_file );
	writeStream << backupName << '\n';
}

bool DefinitionStorage::exists( const QString& backupName ) const
{
	QFile file( m_filePath );
	file.open( QFile::ReadOnly );
	QTextStream reader( &file );
	QString name;
	
	while ( !reader.atEnd() )
	{
		reader >> name;
		
		if ( name == backupName )
			return true;
	}
	
	return false;
}

QString DefinitionStorage::filePath() const
{
	return m_filePath;
}

void DefinitionStorage::setFilePath( const QString& filePath )
{
	QDir dir( filePath );
	m_filePath = dir.absolutePath();
	
	delete m_file;
	m_file = 0;
}
