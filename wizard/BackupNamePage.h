/**
 *   Copyright (C) 2012 by Rui Dias
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef BACKUPNAMEPAGE_H
#define BACKUPNAMEPAGE_H

#include <QWizardPage>

class QLineEdit;
class AbstractDefinitionStorage;
class QLabel;


class BackupNamePage : public QWizardPage
{
	Q_OBJECT

public:
	BackupNamePage( QWidget* parent = 0 );
	virtual ~BackupNamePage();
	
	QString backupName() const;
	void setDefinitionStorage( AbstractDefinitionStorage* storage );
	bool validatePage();
	
	void showDuplicateWarning( bool enable );
	bool isDuplicateWarningShown() const;
	
private slots:
	void p_turnOffDuplicateWarning();
	
private:
	Q_DISABLE_COPY( BackupNamePage )
	QLineEdit* m_backupNameLineEdit;
	AbstractDefinitionStorage* m_definitionStorage;
	QLabel* m_warningLabel;

};

#endif // BACKUPNAMEPAGE_H
